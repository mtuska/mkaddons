AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	self:SetBotType("MKNextBot_Civ_Criminal")
	self:SetDisplayName("Civilian")

	self.m_JWalk = 0.75 // 75% chance to use avoid navareas

	self:SetRunSpeed(240)
end

function ENT:RunBehaviour()
    while (true) do
		if (self:GetThreat() > .6) then
			self.loco:SetDesiredSpeed(self:GetRunSpeed())

			self.AtSpawnPos = false
			local pos = self:FindSpot("random", {type = "hiding"})
			self:MoveToPosAdv(pos)
			self:LoopSequence("idle_all_cower")
			coroutine.wait(10)
			self:LoopSequence("") // Nil out the loop sequence
		elseif (self.ReturnToSpawn && !self.AtSpawnPos) then
			self.loco:SetDesiredSpeed(self:GetWalkSpeed())
			if (self:MoveToPosAdv(self.SpawnPos, {tolerance = 5}) == "ok") then // tolerance is set because we need guards being more exact
				self.AtSpawnPos = true
				self:SetAngles(self.SpawnRot)
			end
			self.AtSpawnRot = false
		elseif (self.CanWander) then
			self.loco:SetDesiredSpeed(self:GetWalkSpeed())
			self:MoveToPosAdv(self:FindNewDestination())
			self.AtSpawnPos = false
		end

		coroutine.yield()
    end
end
