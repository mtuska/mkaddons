AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	local model, name = table.Random( player_manager.AllValidModels())
	self:SetModel(model)
	--self:SetModel("models/Humans/Group01/Female_01.mdl")

	self:SetBotType("MKNextBot_Civ_BankSecurity")
	self:SetDisplayName("Bank Security")

	self.StayNearSpawn = true
	self.AtSpawnPos = true
	self.AtSpawnRot = true

	self:SetArmor(100)

	self:Give("m9k_glock")
	self:Give("m9k_g36")
	self:GetActiveWeapon():SetHoldType("passive")

	self:SetWalkSpeed(60)
	self:SetRunSpeed(GAMEMODE.Config.runspeedcp)
end

function ENT:AttackBehaviour()
	self:ChooseWeapon()
	self.BaseClass.AttackBehaviour(self)
end

function ENT:RunBehaviour()
    while(true) do
		self.SpawnLookAt = self.SpawnLookAt or self:GetEyeTrace()
        if (self:FindEnemy()) then
			if (self:CheckLOS(self:GetEnemy())) then
				if (IsValid(self:GetEnemy()) && self:IsEnemy(self:GetEnemy())) then
					self:AttackBehaviour()
				end
				self.AtSpawnPos = false
				self.AtSpawnRot = false
			end
			coroutine.yield()
			continue
		end

		self:CheckWeapons() // Reload weapons while not in combat
        if (!self.AtSpawnPos) then
			if (self:MoveToPosAdv(self.SpawnPos, {tolerance = 5}) == "ok") then // tolerance is set because we need guards being more exact
				self.AtSpawnPos = true
				self:SetAngles(self.SpawnRot)
			end
			self.AtSpawnRot = false
		else

        end

        coroutine.yield()
    end
end

function ENT:ChooseWeapon()
	if (self:GetWeapon("m9k_g36"):Clip1() > 0) then
		self:SelectWeapon("m9k_g36")
	elseif (self:GetWeapon("m9k_glock"):Clip1() > 0) then
		self:SelectWeapon("m9k_glock")
		self:GetActiveWeapon():SetHoldType("pistol")
	else
		self:SelectWeapon("m9k_g36")
		self:Reload()
	end
end

function ENT:CheckWeapons()
	//BroadcastLua("print('CheckWeapons')")
	if (self.Reloading) then
		return
	end
	for k,v in pairs(self.Weapons) do
		if (v:Clip1() < v.Primary.ClipSize) then
			self:SelectWeapon(k)
			self:Reload()
			return
		end
	end

	self:SelectWeapon("m9k_g36")
	self:GetActiveWeapon():SetHoldType("passive")
end

function ENT:OnSetEnemy()
	if (self:HasEnemy()) then
		self:GetActiveWeapon():SetHoldType("ar2")
	else
		self:GetActiveWeapon():SetHoldType("passive")
	end
end

function ENT:IsEnemy(ent)
	//return false
	local b = self.BaseClass.IsEnemy(self, ent)
	if (ent.isCP and ent:isCP()) then
		return false
	end
	if (self:GetClass() == ent:GetClass()) then
		return false
	end
	return b
	//return ent.SteamID and ent:SteamID() == "STEAM_0:1:7102488"
end

function ENT:OnInjured(dmginfo)
	self.BaseClass.OnInjured(self, dmginfo)
	//self:Witness(dmginfo:GetAttacker(), "Shot Police Officer", 1)
	if (self:Alive() && IsValid(dmginfo:GetAttacker())) then
		local criminal = dmginfo:GetAttacker()
		//self:WantPlayer(criminal, "Shot Police Officer")
		self:SetEnemy(criminal)
		self:SetThreat(1)
		self:VoiceSound("npc/metropolice/pain"..math.random(1,4)..".wav", true)
	end
end
