AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	self:SetBotType("MKNextBot_Civ_OilMan")
end

function ENT:Use(activator, caller, useType, value)
	if (!self.nextUse or CurTime() >= self.nextUse) then
		if (IsValid(caller) && caller:IsPlayer()) then
			local oil = tonumber(caller:GetNWInt("Oil", 0))
			if (oil == 0) then
				--self:EmitSound("vo/npc/male01/gethellout.wav");
				DarkRP.notify(caller, 0, 4, "You don't have any Oil.")
			elseif (oil > 0) then
				local damt = tonumber((oil * cost))
				ply:addMoney(damt)
				DarkRP.notify(caller, 0, 4, "You sold "..oil.." Oil for $"..damt..".")
				caller:SetNWInt("Oil",0)

				if (caller.addXP) then
					caller:addXP(50, true)
				end
			end;
			self.nextUse = CurTime() + 1;
		end
	end;
end;
