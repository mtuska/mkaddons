AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	local model, name = table.Random( player_manager.AllValidModels())
	self:SetModel(model)
	--self:SetModel("models/Humans/Group01/Female_01.mdl")

	self:SetBotType("MKNextBot_Civ_Security")
	self:SetDisplayName("Security Guard")

	self.StayNearSpawn = true
	self.AtSpawnPos = true
	self.AtSpawnRot = true

	self:Give("m9k_g36")
	self:GetActiveWeapon():SetHoldType("passive")

	self:SetWalkSpeed(60)
	self:SetRunSpeed(GAMEMODE.Config.runspeedcp)
end

function ENT:AttackBehaviour()
	self:ChooseWeapon()
	self.BaseClass.AttackBehaviour(self)
end

function ENT:RunBehaviour()
    while(true) do
		self.SpawnLookAt = self.SpawnLookAt or self:GetEyeTrace()
        if (self:FindEnemy()) then
			self.loco:SetDesiredSpeed(self:GetRunSpeed())

			self:ChaseAdv()
			self.AtSpawnPos = false
			self.AtSpawnRot = false
        elseif (!self.AtSpawnPos) then
			if (self:MoveToPosAdv(self.SpawnPos) == "ok") then
				self.AtSpawnPos = true
			end
			self.AtSpawnRot = false
		elseif (!self.AtSpawnRot) then
			self:SetAngles(self.SpawnRot)
		else

        end

        coroutine.yield()
    end
end

function ENT:ChooseWeapon()
	self:SelectWeapon("m9k_usp")
end

function ENT:OnSetEnemy()
	if (self:HasEnemy()) then
		self:GetActiveWeapon():SetHoldType("ar2")
	else
		self:GetActiveWeapon():SetHoldType("passive")
	end
end

function ENT:IsEnemy(ent)
	//return false
	local b = self.BaseClass.IsEnemy(self, ent)
	if (ent.isCP and ent:isCP()) then
		return false
	end
	if (self:GetClass() == ent:GetClass()) then
		return false
	end
	return b
	//return ent.SteamID and ent:SteamID() == "STEAM_0:1:7102488"
end
