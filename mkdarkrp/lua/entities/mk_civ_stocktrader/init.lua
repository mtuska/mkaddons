AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	self:SetBotType("MKNextBot_Civ_StockTrader")
	self:SetDisplayName("Stock Trader")

	self.ReturnToSpawn = true
	self.CanWander = false

	self:SetRunSpeed(240)
end

util.AddNetworkString("MK_StockTrader")
function ENT:Use(activator, caller, useType, value)
	if ((!self.nextUse || CurTime() >= self.nextUse) && IsValid(caller) && caller:IsPlayer()) then
		net.Start("MK_StockTrader")
			net.WriteEntity(self)
		net.Send(caller)
		self.nextUse = CurTime() + 1
	end
end
