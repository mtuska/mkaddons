// RAWR!

include('shared.lua')

function ENT:TradeMenu()
	local ticker = nil

	local frame = vgui.Create("DFrame")
	frame:SetSize(800,600)
	frame:Center()
	frame:SetTitle("Stock Trader")
	//frame:SetVisible(false) --This is important to set false or your frame will pop-up every time you join to server or a single player game.
	frame:SetDraggable(true)
	frame:ShowCloseButton(true)
	frame:SetBackgroundBlur(false)
	frame:MakePopup()

	local DComboBox = vgui.Create("DComboBox", frame)
	local DTextEntryShare = vgui.Create( "DTextEntry", frame )
	local DNumSlider = vgui.Create( "DNumSlider", frame )
	local DTextEntryCost = vgui.Create("DTextEntry", frame)

	DComboBox:SetPos( 5, 30 )
	DComboBox:SetSize( 200, 20 )
	DComboBox:SetValue( "Select Ticker..." )
	for k,v in pairs(MK.stock.corporations) do
		DComboBox:AddChoice(v.ticker)
	end
	DComboBox.OnSelect = function( panel, index, value )
		ticker = value:upper()
		if (!MK.stock.stocks[ticker]) then
			ticker = nil
		end
		DTextEntryShare:SetValue(0)
		DTextEntryCost:SetValue(0)
		if (ticker) then
			DTextEntryShare:SetValue(MK.stock.stocks[ticker].value)
			DTextEntryCost:SetValue(MK.stock.stocks[ticker].value*math.Round(DNumSlider:GetValue()))
		end
	end

	DTextEntryShare:SetPos(5, 50)
	DTextEntryShare:SetSize(200, 20)
	DTextEntryShare:SetEditable(false)

	DNumSlider:SetPos(5, 80)
	DNumSlider:SetSize(200, 20)
	DNumSlider:SetText("Shares")
	DNumSlider:SetMin(1)
	DNumSlider:SetMax(100)
	DNumSlider:SetDecimals(0)
	DNumSlider:SetValue(1)
	DNumSlider.OnValueChanged = function(panel, value)
		if (ticker) then
			DTextEntryCost:SetValue(MK.stock.stocks[ticker].value*math.Round(DNumSlider:GetValue()))
		end
	end

	DTextEntryCost:SetPos(5, 110)
	DTextEntryCost:SetSize(200, 20)
	DTextEntryCost:SetEditable(false)

	local DButtonBuy = vgui.Create( "DButton", frame )
	DButtonBuy:SetText( "Buy Shares" )
	DButtonBuy:SetPos( 25, 140 )
	DButtonBuy:SetSize( 75, 30 )
	DButtonBuy.DoClick = function()
		RunConsoleCommand("mkgm", "buy", ticker, math.Round(DNumSlider:GetValue()))
	end

	local DButtonSell = vgui.Create( "DButton", frame )
	DButtonSell:SetText( "Sell Shares" )
	DButtonSell:SetPos( 100, 140 )
	DButtonSell:SetSize( 75, 30 )
	DButtonSell.DoClick = function()
		RunConsoleCommand("mkgm", "sell", ticker, math.Round(DNumSlider:GetValue()))
	end
end

net.Receive("MK_StockTrader", function(len)
	//net.ReadEntity():TradeMenu()
	if (!MK.stock.confirmedTax) then
		Derma_Query("Every share you sell has a tax of "..MK.stock.tax.."%", "Taxation",
		"Okay", function() end)
		MK.stock.confirmedTax = true
	end
	if (IsValid(MK.menus.stocks)) then
		if (MK.menus.stocks.OnClose) then
			MK.menus.stocks:OnClose()
		end

		MK.menus.stocks:Close()
	else
		vgui.Create("mk_Stocks")
	end
end)

local PANEL = {}
	function PANEL:Init()
	    self:SetMouseInputEnabled(true)
	    self:SetKeyboardInputEnabled(true)

	    self:SetCursor("hand")

	    self:SetTextColor(Color(255, 255, 255, 255))
	    self:SetTall(60)
	    self:DockPadding(0, 0, 10, 5)

	    self.txtRight = self.txtRight or vgui.Create("DLabel", self)
	    self.txtRight:Dock(RIGHT)
	    self.txtRight:SetTextColor(Color(255, 255, 255, 255))
	end

	local col = Color(49,130,173)
	function PANEL:Paint(w, h)
	    local disabled = self:GetDisabled()
	    local x, y = self.Depressed and 2 or 0, self.Depressed and 2 or 0
	    w, h = self.Depressed and w - 4 or w, self.Depressed and h - 4 or h

	    draw.RoundedBox(4, x, y, w, h, col) -- background

	    draw.RoundedBoxEx(4, h, h - 10 + y, w - h + x, 10, col, false, false, false, true) -- the colored bar
	end

	function PANEL:Refresh()
		if (self.ticker) then
			self.txtRight:SetText(math.Round(MK.stock.stocks[ticker].value, 3))
		end
	end

	function PANEL:SetStock(ticker, panel)
		if (MK.stock.stocks[ticker]) then
			self.parentPanel = panel
			self.ticker = ticker
			self:SetText(ticker)
			self.txtRight:SetText(math.Round(MK.stock.stocks[ticker].value, 3))
			hook.Add("MK_Stocks_Update", "MK_Stocks_Update.StocksTrader_"..ticker, function()
				if (self) then
					if (self.ticker) then
						self.txtRight:SetText(math.Round(MK.stock.stocks[ticker].value, 3))
					end
				else
					hook.Remove("MK_Stocks_Update", "MK_Stocks_Update.StocksTrader_"..ticker)
				end
			end)
		end
	end

	function PANEL:DoClick()
		if (self.parentPanel) then
			self.parentPanel:OnSelectStock(self.ticker)
		end
	end
derma.DefineControl("MK_StockButton", "", PANEL, "DButton")

local PANEL = {}
	local gradient = Material("vgui/gradient-r")
	local gradient2 = Material("gui/gradient")

	surface.CreateFont("mk_TitleFont", {
		size = 32,
		font = "Tahoma",
		weight = 200
	})

	surface.CreateFont("mk_TextFont", {
		size = 16,
		font = "Tahoma",
		weight = 200
	})

	surface.CreateFont("mk_SubTitleFont", {
		size = 24,
		font = "Tahoma",
		weight = 200
	})

	local sideWidthFraction = 0.3

	local function PaintButton(this, w, h)
		surface.SetDrawColor(255, 255, 255, 80)
		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(255, 255, 255, 120)
		surface.DrawOutlinedRect(1, 1, w - 2, h - 2)

		surface.SetDrawColor(0, 0, 0, 60)
		surface.DrawOutlinedRect(0, 0, w, h)

		if (this.entered) then
			surface.SetDrawColor(0, 0, 0, 25)
			surface.DrawRect(0, 0, w, h)
		end
	end

	function PANEL:Init()
		local w, h = 800,600//math.max(ScrW() * 0.45, 640), math.max(ScrH() * 0.5, 600)

		MK.menus.stocks = self

		self:SetTitle("")
		self:SetPos(-w, (ScrH() * 0.5) - (h * 0.5))
		self:SetSize(w, h)
		self:MakePopup()
		self:DockPadding(0, 0, 0, 0)
		self:ShowCloseButton(false)
		self:SetDraggable(true)
		self:SetDeleteOnClose(false)
		self:SetKeyBoardInputEnabled(false)
		self.Paint = function() end

		self.header = self:Add("DPanel")
		self.header:SetTall(48)
		self.header:Dock(TOP)
		self.header.Paint = function(this, w, h)
			surface.SetDrawColor(255, 255, 255, 4)
			surface.DrawLine(0, h - 2, w, h - 2)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(5, 5, 5, 120)
			surface.DrawLine(0, h - 1, w, h - 1)
		end

		/*self.titleBar.text = self.titleBar:Add("DLabel")
		self.titleBar.text:SetText("Stock Market")
		self.titleBar.text:Dock(FILL)
		self.titleBar.text:SetFont("mk_TextFont")
		self.titleBar.text:SetContentAlignment(5)
		self.titleBar.text:SetTextColor(Color(255, 255, 255))
		self.titleBar.text:SetExpensiveShadow(2, Color(5, 5, 5, 150))*/

		self.grab = self.header:Add("EditablePanel")
		self.grab:Dock(FILL)
		self.grab.OnMousePressed = function(this, code)
			if (code == MOUSE_LEFT) then
				self.dragging = {gui.MouseX() - self.x, gui.MouseY() - self.y}
			end
		end
		self.grab.OnMouseReleased = function(this)
			self.dragging = nil
		end
		self.grab.Think = function(this)
			if (!input.IsMouseDown(MOUSE_LEFT)) then self.dragging = nil return end

			if (self.dragging) then
				local mouseX, mouseY = math.Clamp(gui.MouseX(), 1, ScrW() - 1), math.Clamp(gui.MouseY(), 1, ScrH() - 1)
				local x = math.Clamp(mouseX - self.dragging[1], 0, ScrW() - w)
				local y = math.Clamp(mouseY - self.dragging[2], 0, ScrH() - h)

				self:SetPos(x, y)
			end
		end

		self.close = self.header:Add("DImageButton")
		self.close:SetWide(16)
		self.close:SetImage("moderator/leave.png")
		self.close.DoClick = function(this)
			self:Close()
		end
		self.close:DockMargin(16, 16, 16, 16)
		self.close:SetAlpha(230)
		self.close:Dock(RIGHT)

		self.side = self:Add("DPanel")
		self.side:SetWide(w * sideWidthFraction)
		self.side:Dock(RIGHT)
		self.side.Paint = function(this, w, h)
			//surface.SetDrawColor(Color(49,130,173))
			surface.SetDrawColor(Color(255, 255, 255))
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(25, 25, 25, 55)
			surface.DrawOutlinedRect(0, 0, w + 1, h)

			surface.SetDrawColor(25, 25, 25, 25)
			surface.SetMaterial(gradient)
			surface.DrawTexturedRect(w - 24, 0, 24, h)
		end

		self.body = self:Add("DPanel")
		self.body:Dock(FILL)
		self.body.Paint = function(this, w, h)
			surface.SetDrawColor(236, 240, 241)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(25, 25, 25, 150)
			surface.DrawOutlinedRect(-1, 0, w + 1, h)
		end

		/*self.titleBar = self.body:Add("DPanel")
		self.titleBar:SetTall(48)
		self.titleBar:Dock(TOP)
		self.titleBar.Paint = function(this, w, h)
			surface.SetDrawColor(Color(49,130,173))
			surface.DrawLine(0, h - 2, w, h - 2)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(5, 5, 5, 120)
			surface.DrawLine(0, h - 1, w, h - 1)
		end

		self.titleBar.text = self.titleBar:Add("DLabel")
		self.titleBar.text:SetText("The Stock Market")
		self.titleBar.text:Dock(FILL)
		self.titleBar.text:SetFont("mk_TitleFont")
		self.titleBar.text:SetContentAlignment(5)
		self.titleBar.text:SetTextColor(Color(255, 255, 255))
		self.titleBar.text:SetExpensiveShadow(2, Color(5, 5, 5, 150))*/

		self.scroll = self.body:Add("DScrollPanel")
		//self.scroll:SetPos(0, 48)
		self.scroll.Piant = PaintButton
		self.scroll.realWidth = w - (w * sideWidthFraction) - 1
		self.scroll.realHeight = h/* - 43*/
		self.scroll:SetSize(self.scroll.realWidth, h/* - 50*/)
		self.scroll.AddHeader = function(this, text, parent, update)
			local label = (IsValid(parent) and parent or self.scroll):Add("DLabel")
			label:SetText(text or "Label")
			label:SetFont("mk_SubTitleFont")
			label:Dock(TOP)
			label:SetTextColor(Color(0, 0, 0, 230))
			label:SetExpensiveShadow(1, Color(255, 255, 255, 255))
			label:DockMargin(5, 5, 5, 5)

			if (update) then
				label.Think = function(this)
					if ((this.nextUpdate or 0) < CurTime()) then
						this.nextUpdate = CurTime() + 1
						label:SetText(update())
					end
				end
			end

			return label
		end
		self.scroll.ShowMessage = function(this, text)
			surface.SetFont("mk_SubTitleFont")
			local w, h = surface.GetTextSize(text)

			local message = this:Add("DLabel")
			message:InvalidateParent(true)
			message:SetTall(this:GetTall())
			message:Dock(FILL)
			message:SetTextColor(Color(0, 0, 0, 150))
			message:SetText(text)
			message:SetFont("mk_SubTitleFont")
			message:SetContentAlignment(5)
		end

		self.stocks = self.side:Add("DCollapsibleCategory")
		self.stocks:SetPos(0, 48)
		self.stocks:Dock(FILL)
		self.stocks:SetExpanded(true)
		self.stocks:SetHeight(self:GetTall() - 2)
		self.stocks.list = self.side:Add("DPanelList")
		self.stocks.list:SetSpacing(5)
		self.stocks:SetContents(self.stocks.list)
		//PrintTable(LocalPlayer():MK_Data_Get("stockexchange", {}))
		for k, v in pairs(MK.stock.corporations) do
		//for k, v in SortedPairsByMemberValue(MK.stock.stocks, "value") do
			//local line = self.stocks:AddLine(k, v.value)
			local stock = vgui.Create("MK_StockButton")
			stock:SetStock(v.ticker, self)
			self.stocks.list:AddItem(stock)
		end

		self.scroll.ticker = self.scroll:Add("DLabel")
		self.scroll.ticker:Dock(TOP)
		self.scroll.ticker:DockMargin(2, 2, 2, 0)
		self.scroll.ticker:SetTextColor(Color(0, 0, 0, 200))
		self.scroll.ticker:SetText("Currect stock: ")
		self.scroll.ticker:SetFont("mk_TextFont")
		self.scroll.ticker:SizeToContents()

		self.scroll.ownedShares = self.scroll:Add("DLabel")
		self.scroll.ownedShares:Dock(TOP)
		self.scroll.ownedShares:DockMargin(2, 2, 2, 0)
		self.scroll.ownedShares:SetTextColor(Color(0, 0, 0, 200))
		self.scroll.ownedShares:SetText("Owned shares: ")
		self.scroll.ownedShares:SetFont("mk_TextFont")
		self.scroll.ownedShares:SizeToContents()

		self.scroll.boughtAt = self.scroll:Add("DLabel")
		self.scroll.boughtAt:Dock(TOP)
		self.scroll.boughtAt:DockMargin(2, 2, 2, 0)
		self.scroll.boughtAt:SetTextColor(Color(0, 0, 0, 200))
		self.scroll.boughtAt:SetText("Bought at: NaN")
		self.scroll.boughtAt:SetFont("mk_TextFont")
		self.scroll.boughtAt:SizeToContents()

		self.scroll.shares = self.scroll:Add("DLabel")
		self.scroll.shares:Dock(TOP)
		self.scroll.shares:DockMargin(2, 2, 2, 0)
		self.scroll.shares:SetTextColor(Color(0, 0, 0, 200))
		self.scroll.shares:SetText("Shares to modify:")
		self.scroll.shares:SetFont("mk_TextFont")
		self.scroll.shares:SizeToContents()

		self.scroll.sharesWang = self.scroll:Add("DNumSlider")
		self.scroll.sharesWang:Dock(TOP)
		self.scroll.sharesWang:DockMargin(2, 2, 2, 0)
		self.scroll.sharesWang:SetMin(1)
		self.scroll.sharesWang:SetMax(100)
		self.scroll.sharesWang:SetDecimals(0)

		self.scroll.buy = self.scroll:Add("DButton")
		self.scroll.buy:Dock(TOP)
		self.scroll.buy:SetTall(28)
		self.scroll.buy.Paint = PaintButton
		self.scroll.buy:DockMargin(2, 2, 2, 0)
		self.scroll.buy.OnCursorEntered = function(this)
			this.entered = true
		end
		self.scroll.buy.OnCursorExited = function(this)
			this.entered = nil
		end
		self.scroll.buy:SetText("Buy")
		self.scroll.buy:SetTextColor(Color(0, 0, 0, 230))
		self.scroll.buy.DoClick = function()
			if (self.ticker) then
				RunConsoleCommand("mkgm", "buy", self.ticker, math.Round(self.scroll.sharesWang:GetValue()))
			end
		end

		self.scroll.sell = self.scroll:Add("DButton")
		self.scroll.sell:Dock(TOP)
		self.scroll.sell:SetTall(28)
		self.scroll.sell.Paint = PaintButton
		self.scroll.sell:DockMargin(2, 2, 2, 0)
		self.scroll.sell.OnCursorEntered = function(this)
			this.entered = true
		end
		self.scroll.sell.OnCursorExited = function(this)
			this.entered = nil
		end
		self.scroll.sell:SetText("Sell")
		self.scroll.sell:SetTextColor(Color(0, 0, 0, 230))
		self.scroll.sell.DoClick = function()
			if (self.ticker) then
				RunConsoleCommand("mkgm", "sell", self.ticker, math.Round(self.scroll.sharesWang:GetValue()))
			end
		end

		self:MoveTo((ScrW() * 0.5) - (self:GetWide() * 0.5), (ScrH() * 0.5) - (self:GetTall() * 0.5), 0.1, 0, 0.75)
	end

	function PANEL:OnSelectStock(ticker)
		self.ticker = ticker
		self.scroll.ticker:SetText("Currect stock: "..ticker)
		local stocks = LocalPlayer():MK_Data_Get("stockexchange", {})
		local stock = stocks[ticker] or table.Copy(MK.stock.defaultStockData)
		if (istable(stock)) then
			self.scroll.ownedShares:SetText("Owned shares: "..stock.value)
			self.scroll.boughtAt:SetText("Bought at: "..stock.bought)
		else
			self.scroll.ownedShares:SetText("Owned shares: "..(stock or 0))
			self.scroll.boughtAt:SetText("Bought at: NaN")
		end
	end

	function PANEL:OnClose()
		local x, y = self:LocalToScreen(0, 0)

		self:SetVisible(true)
		self:MoveTo(-self:GetWide(), y, 0.1, 0, 0.75, function()
			self:Remove()
			MK.menus.stocks = nil
		end)
	end
vgui.Register("mk_Stocks", PANEL, "DFrame")
