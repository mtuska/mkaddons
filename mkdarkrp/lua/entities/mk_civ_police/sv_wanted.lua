// RAWR!

local WantedSystem = {}
WantedSystem["WeaponWhitelist"] = {
	"itemstore_pickup",
	"keys",
	"weapon_physcannon",
	"gmod_camera",
	"gmod_tool",
	"pocket",
	"weapon_physgun",
	"weapon_keypadchecker",
	"door_ram",
	"arrest_stick",
	"unarrest_stick",
	"stunstick",
	"weaponchecker",
	"weapon_checker",
	"lockpick",
	"vip_lockpick",
	"advanced_lockpick",
	"weapon_rad15"
}
WantedSystem["IllegalItems"] = {
	"money_printer",
	"drug_lab",
	"drug",
	"rprint_*", // RPhone printers
	"eml_*", // Enhanced Meth Laboratory entities
}

local function IsIllegalEntity(ent)
	if (IsValid(ent)) then
		if (ent:IsWeapon()) then
			for _,str in pairs(WantedSystem["WeaponWhitelist"]) do
				if (string.match(ent:GetClass(), str)) then
					return false
				end
			end
			return true
		else
			if (string.match(ent:GetClass(), "eml_buyer*")) then
				return false
			end
			for _,str in pairs(WantedSystem["IllegalItems"]) do
				if (string.match(ent:GetClass(), str)) then
					return true
				end
			end
		end
	end
	return false
end

hook.Add("GravGunOnPickedUp", "GravGunOnPickedUp.WantedSystem", function(ply, ent)
	ent.carryingPly = ply
	ply.carryingEnt = ent
end)

hook.Add("GravGunOnDropped", "GravGunOnDropped.WantedSystem", function(ply, ent)
	ent.carryingPly = nil
	ply.carryingEnt = nil
end)
BroadcastLua("print('update')")

function MKPoliceSearch()
	//BroadcastLua("print('tick')")
	local criminals = {}
	for k,v in pairs(player.GetAll()) do
		if (IsValid(v) && v:Alive()) then
			local weapon = v:GetActiveWeapon()
			if (weapon && IsValid(weapon)) then
				if ((weapon.GetIsLockpicking && weapon:GetIsLockpicking()) || weapon.IsLockPicking || weapon.IsCracking) then
					table.insert(criminals, {ply = v, reason = "Illegal Activities", threat = .25})
					continue
				end
				if (IsIllegalEntity(weapon) && !v:getDarkRPVar("HasGunlicense")) then
					table.insert(criminals, {ply = v, reason = "Possesion of Illegal Weapons", threat = .6})
					continue
				end
			end
			if (!v:isCP() && v.carryingEnt && IsValid(v.carryingEnt) && IsIllegalEntity(v.carryingEnt)) then
				table.insert(criminals, {ply = v, reason = "Possesion of Illegal Items", threat = .2})
				continue
			end
		end
	end

	local police = {}
	local illegalEnts = {}
	for k,v in pairs(ents.GetAll()) do
		if (v:GetClass():lower():find("mk_civ_police")) then
			table.insert(police, v)
		end
		// We can lookup any other object while still in here
		if (IsValid(v) && !v:IsWeapon()) then // Don't check for weapons, for now I guess
			if (IsIllegalEntity(v)) then
				table.insert(illegalEnts, v)
			end
		end
	end
	//for k,cp in pairs(ents.FindByClass("mk_civ_police")) do // Retired as it uses ents.GetAll under the hood
	// We do the lookup for the classes ourselves so we can also look for criminal objects
	for k,cp in pairs(police) do
		//BroadcastLua("print('police tick')")
		if (!cp:HasEnemy() || cp:GetThreat() < .5) then
			for i,criminal in pairs(criminals) do
				if (!cp:HasEnemy() || cp:GetThreat() < criminal.threat) then
					local ply = criminal.ply
					if (cp:CanTarget(ply)) then
						cp:WantPlayer(ply, criminal.reason)
						cp:SetEnemy(ply)
						cp:SetThreat(criminal.threat)
						criminals[i] = nil
						continue // On to the next criminal
					end
				end
			end
			// Only check for illegal ents for this officer if he couldn't see a criminal
			if (!cp:HasEnemy()) then
				for i,ent in pairs(illegalEnts) do
					BroadcastLua("print('"..i..": "..ent:GetClass().."')")
					if (cp:CanTarget(ent)) then
						//cp:WantPlayer(ply, criminal.reason)
						ent.MKIllegalEnt = true
						cp:SetEnemy(ent)
						cp:SetThreat(.1)
						illegalEnts[i] = nil
						continue // On to the next criminal
					end
				end
			end
		end
	end
end
if (timer.Exists("PoliceLookForIllegalItems")) then
	timer.Destroy("PoliceLookForIllegalItems")
end
timer.Create('PoliceLookForIllegalItems', 1, 0, MKPoliceSearch)

hook.Add("PlayerDeath", "PlayerDeath.WantedSystem", function(ply, wep, killer)
	if (IsValid(killer) && killer:IsPlayer() && killer != ply && !killer:isCP() && killer:Alive()) then
		for k,cp in pairs(ents.FindByClass("mk_civ_police")) do
			if (cp:CanTarget(killer)) then
				cp:WantPlayer(killer, "Murder")
				break
			end
		end
	end
end)
