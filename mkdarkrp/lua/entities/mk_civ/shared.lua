// RAWR!

AddCSLuaFile()
AddCSLuaFile("sh_functions.lua")
include("sh_functions.lua")

ENT.Base            = "mk_darkrp"
ENT.PrintName		= "Madkiller Max's Nextbot Civilian"
ENT.Spawnable       = true
ENT.AdminSpawnable  = true

list.Set( "NPC", "mk_civ", {
	Name = "Civilian",
	Class = "mk_civ",
	Category = "Madkiller Civs"
} )

function ENT:SetupDataTables()
	self:MKSetupDataTables()

	self:NetworkVar("Bool", 7, "Wanted")
	self:NetworkVar("Float", 2, "Threat")
end
