AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	self:SetBotType("MKNextBot_Civ_Druggie")
end

function ENT:Use(activator, caller, useType, value)
	if (!self.nextUse or CurTime() >= self.nextUse) then
		if (IsValid(caller) && caller:IsPlayer()) then
			if (caller:GetNWInt("player_meth") == 0) then
				--self:EmitSound("vo/npc/male01/gethellout.wav");
				caller:SendLua("local tab = {Color(1,241,249,255), [[Meth Addicted: ]], Color(255,255,255), [["..table.Random(EML_Meth_Salesman_NoMeth).."]] } chat.AddText(unpack(tab))");
				timer.Simple(0.25, function() self:EmitSound(table.Random(EML_Meth_Salesman_NoMeth_Sound), 100, 100) end);
			elseif (caller:GetNWInt("player_meth") > 0) then
				caller:addMoney(caller:GetNWInt("player_meth"));
				caller:SendLua("local tab = {Color(1,241,249,255), [[Meth Addicted: ]], Color(255,255,255), [["..table.Random(EML_Meth_Salesman_GotMeth)..", here is your ]], Color(128, 255, 128), [["..caller:GetNWInt("player_meth").."$.]] } chat.AddText(unpack(tab))");
				caller:SetNWInt("player_meth", 0);
				if (EML_Meth_MakeWanted) then
					caller:wanted(self, "Selling Meth");
				end;
				timer.Simple(0.25, function() self:EmitSound(table.Random(EML_Meth_Salesman_GotMeth_Sound), 100, 100) end);
				timer.Simple(2.5, function() self:EmitSound("vo/npc/male01/moan0"..math.random(1, 5)..".wav") end);
			end;
			self.nextUse = CurTime() + 1;
		end
	end;
end;
