AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	local model, name = table.Random( player_manager.AllValidModels())
	self:SetModel(model)
	--self:SetModel("models/Humans/Group01/Female_01.mdl")

	self:SetBotType("MKNextBot_Civ_GunDealer")
	self:SetDisplayName("Gun Dealer")

	self.m_Afraid = 0
	self.m_Zen = false

	self:SetRunSpeed(180)
end
