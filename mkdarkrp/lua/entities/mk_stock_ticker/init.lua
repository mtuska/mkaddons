AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")


function ENT:Initialize()
	self:SetRenderMode(RENDERMODE_TRANSALPHA)
	self:DrawShadow(false)
	self:SetModel("models/hunter/plates/plate1x1.mdl")
	self:SetMaterial("models/effects/vol_light001")
	self:SetSolid(SOLID_VPHYSICS)
	self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	self.heldby = 0
	self:SetMoveType(MOVETYPE_NONE)
	self.lines = {}
	if (MK && MK.stock && MK.stock.corporations) then
		local i = 0
		for k,v in pairs(MK.stock.corporations) do
			i = i + 1
			local text = v.ticker..": "..math.Round(MK.stock.stocks[v.ticker].value, 3)
			self:SetLine(i, text, Color(255,255,255), 40)
		end
	end
end

function ENT:PhysicsUpdate(phys)
	if self.heldby <= 0 then
		phys:Sleep()
	end
end

local function textScreenPickup(ply, ent)
	if IsValid(ent) and ent:GetClass() == "mk_stock_ticker" then
		ent.heldby = ent.heldby + 1
	end
end
hook.Add("PhysgunPickup", "textScreensPreventTravelPickupTicker", textScreenPickup)

local function textScreenDrop(ply, ent)
	if IsValid(ent) and ent:GetClass() == "mk_stock_ticker" then
		ent.heldby = ent.heldby - 1
	end
end
hook.Add("PhysgunDrop", "textScreensPreventTravelDropTicker", textScreenDrop)

local function textScreenCanTool(ply, trace, tool)
	/*if IsValid(trace.Entity) and trace.Entity:GetClass() == "mk_stock_ticker" then
		if !(tool == "textscreen" or tool == "remover") then
			return false
		end
	end*/
end
hook.Add("CanTool", "textScreensPreventToolsTicker", textScreenCanTool)

util.AddNetworkString("sm_textscreens_update_Ticker")
util.AddNetworkString("sm_textscreens_download_Ticker")

function ENT:SetLine(line, text, color, size)
	if string.sub(text, 1, 1) == "#" then
		text = string.sub(text, 2)
	end
	if string.len(text) > 180 then
		text = string.sub(text, 1, 180) .. "..."
	end

	self.lines = self.lines or {}
	self.lines[tonumber(line)] = {
		["text"] = text,
		["color"] = color,
		["size"] = size
	}
end

net.Receive("sm_textscreens_download_Ticker", function(len, ply)
	if not IsValid(ply) then return end

	local ent = net.ReadEntity()
	if IsValid(ent) and ent:GetClass() == "mk_stock_ticker" then
		ent.lines = ent.lines or {}
		net.Start("sm_textscreens_update_Ticker")
			net.WriteEntity(ent)
			net.WriteTable(ent.lines)
		net.Send(ply)
	end
end)

function ENT:Broadcast()
	net.Start("sm_textscreens_update_Ticker")
		net.WriteEntity(self)
		net.WriteTable(self.lines)
	net.Broadcast()
end
