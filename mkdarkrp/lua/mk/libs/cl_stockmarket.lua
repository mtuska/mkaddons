// RAWR!

MK.stock = MK.stock or {}
MK.stock.stocks = MK.stock.stocks or {}
MK.stock.confirmedTax = MK.stock.confirmedTax or false
MK.stock.lastUpdateTime = MK.stock.lastUpdateTime or CurTime()

net.Receive("MK_Stocks_Update", function(len)
	MK.stock.stocks = net.ReadTable()
	hook.Run("MK_Stocks_Update")
	MK.stock.lastUpdateTime = CurTime()
	for k,ent in pairs(ents.FindByClass("mk_stock_ticker")) do
		local i = 0
		for k,v in pairs(MK.stock.corporations) do
			i = i + 1
			local perc = 0
			if (MK.stock.stocks[v.ticker].oldValue != 0) then
				perc = ((MK.stock.stocks[v.ticker].value - MK.stock.stocks[v.ticker].oldValue)/MK.stock.stocks[v.ticker].oldValue)
			end
			local sign
			if (perc > 0) then
				sign = "+"
			elseif (perc < 0) then
				sign = "-"
			end
			perc = math.abs(perc)
			local text = v.ticker..": "..math.Round(MK.stock.stocks[v.ticker].value, 3).." ("..sign.."%"..math.Round(perc*100,1)..")"
			ent.lines[i] = {
				["text"] = text,
				["color"] = Color(255,255,255),
				["size"] = 40
			}
			//ent:SetLine(i, text, Color(255,255,255), 40)
			//BroadcastLua("print('"..text.."')")
		end
	end
end)

hook.Add("Think", "Think.Ticker", function()
	for k,v in pairs(ents.FindByClass("mk_stock_ticker")) do
		v.lines[0] = {
			["text"] = "Next update in "..(10 - math.Round((CurTime() - MK.stock.lastUpdateTime), 1)).."s",
			["color"] = Color(255,255,255),
			["size"] = 30
		}
	end
end)
