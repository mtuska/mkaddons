// RAWR!

MK.raid = MK.raid or {}
MK.raid.raids = MK.raid.raids or {}

function MK.raid.StartRaid(ply, raiders, initiates, targets)
	raiders = raiders or {}
	targets = targets or {}
	local raid = {}
	raid.time = CurTime()
	
	raid.raiders = raiders
	raid.targets = targets
	raid.initiates = initiates
	
	table.insert(MK.raid.raids, raid)
	
	if (MK.administration) then
		local plys = player.GetByPermission("see_raid")
		for k,v in pairs(plys) do
			if (table.HasValue(targets, v)) then
				plys[k] = nil
			end
		end
		MK.administration.SelectiveBroadcast(plys, ply, " is beginning raid of ", targets)
	end
end