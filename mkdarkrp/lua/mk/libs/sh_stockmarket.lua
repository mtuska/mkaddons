// RAWR!

MK.stock = MK.stock or {}
MK.stock.stocks = MK.stock.stocks or {}
MK.stock.closed = false
MK.stock.interval = 10
MK.stock.tax = 0.01
MK.stock.maxSharesPerStock = 100
MK.stock.maxShares = 500
MK.stock.defaultStockData = {
	value = 0,
	bought = 0,
}
MK.stock.corporations = {
	{name = "Asylum Central", ticker = "AC", value = 50},
	{name = "MadMax Inc.", ticker = "MMI", value = 50},
	{name = "Unknown Miracles", ticker = "UM", value = 50},
	{name = "Xeviax Sucks", ticker = "XS", value = 50},
	{name = "Nixon JailBreak", ticker = "NJB", value = 50},
}
