// RAWR!

MK.stock = MK.stock or {}
MK.stock.stocks = MK.stock.stocks or {}
MK.stock.corporations = MK.stock.corporations or {}

util.AddNetworkString("MK_Stocks_Update")
function MK.stock.Iterate()
	if (MK.stock.closed) then
		return
	end
	//BroadcastLua("print('=======')")
	for k,v in pairs(MK.stock.corporations) do
		MK.stock.stocks[v.ticker] = MK.stock.stocks[v.ticker] or {}
		MK.stock.stocks[v.ticker].value = MK.stock.stocks[v.ticker].value or v.value or 100
		MK.stock.stocks[v.ticker].oldValue = MK.stock.stocks[v.ticker].oldValue or 0

		/*local intensity = math.Rand(0, 20)
		local percent = math.Rand(-1, 1)
		if (MK.stock.stocks[v.ticker] < 20) then
			percent = math.Rand(-0.5,1)
		elseif (MK.stock.stocks[v.ticker] < 50) then
			percent = math.Rand(-0.75,1)
		end
		local price = MK.stock.stocks[v.ticker]
		local ratio = (percent/intensity)
		local add = ratio*price
		BroadcastLua("print('"..v.ticker..": percent:"..percent.." intensity:"..intensity.." ratio:"..ratio.." addition:"..add.."')")
		MK.stock.stocks[v.ticker] = math.abs(price + add);*/

		local price = MK.stock.stocks[v.ticker].value
		local intensity = math.Rand(0, price*.2)
		local percent = math.Rand(-1, 1)
		if (price < 20) then
			percent = math.Rand(-0.5,1)
		elseif (price < 50) then
			percent = math.Rand(-0.75,1)
		end
		if (math.Rand(0, 1) <= .1) then
			intensity = math.Rand(0, price*.75)
		end
		local diff = math.abs((price - v.value)/v.value)
		MK.stock.stocks[v.ticker].oldValue = price
		MK.stock.stocks[v.ticker].value = math.abs(price + (percent*intensity));

		//BroadcastLua("print('"..v.ticker..": "..MK.stock.stocks[v.ticker].value.."')")
	end
	net.Start("MK_Stocks_Update")
		net.WriteTable(MK.stock.stocks)
	net.Broadcast()
	hook.Run("MK_Stocks_Update")
	//BroadcastLua("print('=======')")
	/*for k,ent in pairs(ents.FindByClass("mk_stock_ticker")) do
		local i = 0
		ent:SetLine(i, "Next update in 0.0s", Color(255,255,255), 30)
		for k,v in pairs(MK.stock.corporations) do
			i = i + 1
			local perc = 0
			if (MK.stock.stocks[v.ticker].oldValue != 0) then
				perc = ((MK.stock.stocks[v.ticker].value - MK.stock.stocks[v.ticker].oldValue)/MK.stock.stocks[v.ticker].oldValue)
			end
			local sign
			if (perc > 0) then
				sign = "+"
			elseif (perc < 0) then
				sign = "-"
			end
			perc = math.abs(perc)
			local text = v.ticker..": "..math.Round(MK.stock.stocks[v.ticker].value, 3).." ("..sign.."%"..math.Round(perc*100,1)..")"
			ent:SetLine(i, text, Color(255,255,255), 40)
			//BroadcastLua("print('"..text.."')")
		end
		ent:Broadcast()
	end*/
	//BroadcastLua("print('=======')")
end
if (timer.Exists("MKStockIteratation")) then
	timer.Destroy("MKStockIteratation")
end
timer.Create("MKStockIteratation", MK.stock.interval, 0, MK.stock.Iterate)

function MK.stock.Reset()
	local stocks = table.Count(MK.stock.stocks)
	MK.stock.stocks = {}
	for k,v in pairs(MK.stock.corporations) do
		MK.stock.stocks[v.ticker] = {}
		MK.stock.stocks[v.ticker].value = v.value or 100
		MK.stock.stocks[v.ticker].oldValue = 0
	end

	for k,ent in pairs(ents.FindByClass("mk_stock_ticker")) do
		local i = 0
		ent.lines = {}
		for k,v in pairs(MK.stock.corporations) do
			i = i + 1
			local perc = 0
			if (MK.stock.stocks[v.ticker].oldValue != 0) then
				perc = ((MK.stock.stocks[v.ticker].value - MK.stock.stocks[v.ticker].oldValue)/MK.stock.stocks[v.ticker].oldValue)
			end
			local sign = ""
			if (perc > 0) then
				sign = "+"
			elseif (perc < 0) then
				sign = "-"
			end
			perc = math.abs(perc)
			local text = v.ticker..": "..math.Round(MK.stock.stocks[v.ticker].value, 3).." ("..sign.."%"..math.Round(perc*100,1)..")"
			ent:SetLine(i, text, Color(255,255,255), 40)
			//BroadcastLua("print('"..text.."')")
		end
		ent:Broadcast()
	end
end
