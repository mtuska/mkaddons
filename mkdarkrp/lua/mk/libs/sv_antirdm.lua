// RAWR!

MK.rdm = MK.rdm or {}

/*function MK.rdm.ShowSceneForCorpse(scene)
	local tr = scene.trace

	if (tr) then
		local e = EffectData()
		e:SetStart(tr.StartPos)
		e:SetOrigin(tr.HitPos)
		e:SetMagnitude(tr.HitBox)
		e:SetScale(10)

		util.Effect("crimescene_shot", e) // Using TTT effect
	end

	if not scene then return end

	for _, dummy_key in pairs({"victim", "killer"}) do
		local dummy = scene[dummy_key]

		if (dummy) then
			local e = EffectData()
			e:SetOrigin(dummy.pos)
			e:SetAngles(dummy.ang)
			e:SetColor(dummy.sequence)
			e:SetScale(dummy.cycle)
			e:SetStart(Vector(dummy.aim_yaw, dummy.aim_pitch, dummy.move_yaw))
			e:SetRadius(10)

			util.Effect("crimescene_dummy", e) // Using TTT effect
		end
	end
end*/

local expire = 60 * 60 * .5 // half hour

function MK.rdm.CleanUp()
	for _,ply in pairs(player.GetAll()) do
		local kills = ply:MK_GameData_Get("kills", {})
		
		local update = false
		for k,v in pairs(kills) do
			if (!v.time||(v.time + expire) <= os.time()) then
				kills[k] = nil
				update = true
			end
		end
		
		if (update) then
			ply:MK_GameData_Set("kills", kills)
		end
	end
end

timer.Create("MK_RDM_CLEANUP", 300, 0, MK.rdm.CleanUp)

function MK.rdm.Process(victim, attacker, dmginfo, kill)
	local ply = attacker
	ply:MK_GameData_Set("latestkill", kill.time) // Use kill.time just in case
	
	local kills = ply:MK_GameData_Get("kills", {})
	table.insert(kills, kill)
	
	for k,v in pairs(kills) do
		if (!v.time||(v.time + expire) <= os.time()) then
			kills[k] = nil
		end
	end
	
	table.SortByMember(kills, "time", true)
	ply:MK_GameData_Set("kills", kills)
	
	
	local formula = MK.rdm.Calc(ply, kills)
	
	local stage = MK.rdm.Stage(formula)
	if (stage.action) then
		stage.action(ply)
	end
	//player.BroadcastChat(ply:Name(), " "..stage.title.." "..formula)
end

function MK.rdm.Display()
	for k,v in pairs(player.GetAll()) do
		local formula = MK.rdm.Calc(v)
		
		local stage = MK.rdm.Stage(formula)
		player.BroadcastChat(v:Name(), " "..stage.title.." "..formula)
	end
end