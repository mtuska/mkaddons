// RAWR!

MK.civs = MK.civs or {}

MK.civs.config = MK.civs.config or {}

MK.civs.config.autoPopulate = true
MK.civs.config.spawnInterval = 30
MK.civs.config.despawnMode = 0
MK.civs.config.spawnDist = 0
MK.civs.config.civsPerPlayer = 10
MK.civs.config.maxCivs = 100
MK.civs.config.cleanupCivsBodies = 60

MK.civs.config.jwalk = 0.1

MK.civs.config.types = {
	["mk_police"] = 0.2,
	["mk_civ"] = 0.5,
	["mk_civ_druggie"] = 0.1,
	["mk_civ_criminal"] = 0.2,
}

MK.civs.config.weaponsWhitelist = {
	"itemstore_pickup",
	"keys",
	"weapon_physcannon",
	"gmod_camera",
	"gmod_tool",
	"pocket",
	"weapon_physgun",
	"weapon_keypadchecker",
	"door_ram",
	"arrest_stick",
	"unarrest_stick",
	"stunstick",
	"weaponchecker",
	"weapon_checker",
	"lockpick",
	"vip_lockpick",
	"advanced_lockpick",
	"weapon_rad15"
}
MK.civs.config.illegalItems = {
	"money_printer",
	"drug_lab",
	"drug",
	"rprint_*", // RPhone printers
	"eml_*", // Enhanced Meth Laboratory entities
}

function MK.civs.GmInitialize()
	if (FPP && FPP.plyCanTouchEnt) then
		local oldPlyCanTouchEnt = FPP.plyCanTouchEnt
		function FPP.plyCanTouchEnt(ply, ent, touchType)
			if (ply.IsMKNextBot && ply:IsMKNextBot()) then
				return true
			end
			return oldPlyCanTouchEnt(ply, ent, touchType)
		end
	end
end

function MK.civs.IsIllegalEntity(ent)
	if (IsValid(ent)) then
		if (ent:IsWeapon()) then
			for _,str in pairs(MK.civs.config.weaponsWhitelist) do
				if (string.match(ent:GetClass(), str)) then
					return false
				end
			end
			return true
		else
			for _,str in pairs(MK.civs.config.illegalItems) do
				if (string.match(ent:GetClass(), str)) then
					return true
				end
			end
		end
	end
	return false
end

function MK.civs.Crime(reason, threat, data)
	if (!MK.nextbots) then
		//what the fuccckkkkkk
	end
	//BroadcastLua("print('"..reason.."')")
	local bots = MK.nextbots.GetNexbots()
	for k,bot in pairs(bots) do
		if (IsValid(bot) && bot.Witness && bot.GetThreat) then // Only DarkRP bots witness things
			if (!bot:HasEnemy() || bot:GetThreat() < threat) then
				if (data.criminal) then
					//bot:Witness(data.criminal, reason, threat)
				elseif (data.pos && (!data.dist || bot:GetPos():DistToSqr(data.pos) <= data.dist)) then
					//bot:Perceive(data.pos, reason, threat)
				end
			end
		end
	end
end
