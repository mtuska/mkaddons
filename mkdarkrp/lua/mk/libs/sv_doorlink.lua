concommand.Add("doorlinker",function(ply,cmd,args)

	local AllowGroup = {}
	AllowGroup["superadmin"] = true
	AllowGroup["admin"] = true

	for k,v in pairs(AllowGroup) do
		if ply:GetNWString("usergroup") == k then
			ply:Give("door_linker")
			ply:SelectWeapon("door_linker")
			return
		end
	end
end)

hook.Add("getDoorCost", "getDoorCost.DoorLink", function(ply, ent)
	local DoorLink = Door_HasLink(ent)
	if DoorLink then
		local NeedMoney = table.Count(DoorLink.Items)
		NeedMoney = NeedMoney * GAMEMODE.Config.doorcost
		return NeedMoney
	end
end)

hook.Add("playerBoughtDoor", "playerBoughtDoor.DoorLink", function(ply, ent, cost)
	local DoorLink = Door_HasLink(ent)
	if DoorLink then
		local NeedMoney = table.Count(DoorLink.Items)
		NeedMoney = NeedMoney * GAMEMODE.Config.doorcost
		if (NeedMoney != cost) then
			BroadcastLua("print('NeedMoney: "..NeedMoney.." != Cost: "..cost.."')")
		end
		
		local Count = 0
		for k,v in pairs(DoorLink.Items) do
			local ent1 = ents.GetByIndex(v)
			if ent1 and ent1:IsValid() and ent1 != ent then
				Count = Count + 1
				if (ent1:isKeysOwned()) then
					ent1:addKeysAllowedToOwn(ply)
					ent1:addKeysDoorOwner(ply)
				else
					ent1:keysOwn(ply)
				end
				//ply:GetTable().OwnedNumz = ply:GetTable().OwnedNumz + 1
				//ply:GetTable().Ownedz[Ent:EntIndex()] = Ent
			end
		end
		//GAMEMODE:Notify(ply,3,4,Count .. " Linked Door bought. ( - $" .. Count*cost .. " )")
	end
end)

hook.Add("playerSellDoor", "playerSellDoor.DoorLink", function(ply, ent)
	local DoorLink = Door_HasLink(ent)
	if DoorLink then
		local Count = 0
		for k,v in pairs(DoorLink.Items) do
			local ent1 = ents.GetByIndex(v)
			if ent1 and ent1:IsValid() and ent1 != ent then
				Count = Count + 1
				ent1:keysUnOwn(ply)
			
				local cost = GAMEMODE.Config.doorcost * 2 / 3 + 0.5
				ply:addMoney(math.floor(cost))
			end
		end
		//GAMEMODE:Notify(ply,2,4,Count .. " Linked Door sold. ( + $" .. Count*refund .. " )")
	end
end)
/*
hook.Add("PlayerBoughtDoor","Linked Door Buy",function(ply,entity,cost)
	local DoorLink = Door_HasLink(entity)
	if DoorLink then
		local NeedMoney = table.Count(DoorLink.Items)
		NeedMoney = NeedMoney - 1
		NeedMoney = NeedMoney*cost
		
		if ply:getDarkRPVar("money") < NeedMoney then
			GAMEMODE:Notify(ply,4,4,"You need $ " .. NeedMoney .. " to buy rest of linked door!")
				entity:UnOwn(ply)
				ply:GetTable().OwnedNumz = math.abs(ply:GetTable().OwnedNumz - 1)
				ply:GetTable().Ownedz[entity:EntIndex()] = nil
			
				ply:AddMoney(cost)
			return
		end
		
		local Count = 0
		for k,v in pairs(DoorLink.Items) do
			local Ent = ents.GetByIndex(v)
			if Ent and Ent:IsValid() and Ent != entity then
				Count = Count + 1
				ply:AddMoney(-cost)
				Ent:Own(ply)
				ply:GetTable().OwnedNumz = ply:GetTable().OwnedNumz + 1
				ply:GetTable().Ownedz[Ent:EntIndex()] = Ent
			
			end
		end
		GAMEMODE:Notify(ply,3,4,Count .. " Linked Door bought. ( - $" .. Count*cost .. " )")
	end
end)

hook.Add("PlayerSoldDoor","Linked Door Sell",function(ply,entity,refund)
	local DoorLink = Door_HasLink(entity)
	if DoorLink then
		local Count = 0
		for k,v in pairs(DoorLink.Items) do
			local Ent = ents.GetByIndex(v)
			if Ent and Ent:IsValid() and Ent != entity then
				Count = Count + 1
				Ent:UnOwn(ply)
				ply:GetTable().OwnedNumz = math.abs(ply:GetTable().OwnedNumz - 1)
				ply:GetTable().Ownedz[Ent:EntIndex()] = nil
			
				ply:AddMoney(refund)
			end
		end
		GAMEMODE:Notify(ply,2,4,Count .. " Linked Door sold. ( + $" .. Count*refund .. " )")
	end
end)
*/