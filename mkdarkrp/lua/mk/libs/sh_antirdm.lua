// RAWR!

MK.rdm = MK.rdm or {}
MK.rdm.version = "0.0.1"
MK.rdm.scenes = MK.rdm.scenes or {}
MK.rdm.damages = MK.rdm.damages or {}
MK.rdm.kills = MK.rdm.kills or {}
MK.rdm.stages = {
	{title = "Green", value = 0, colour = Color(80, 200, 25)},
	{title = "Yellow", value = 150, colour = Color(255,255,0)},
	{title = "Red", value = 300, colour = Color(255,0,0)},
	{title = "Critical", value = 500, colour = Color(0,0,0), action = function(ply)
		local reason = "RDM Automation: Mass RDM"
		local properLength = MK.administration.Ban(ply, "30m", "RDM Automation: Mass RDM")
		MK.administration.Broadcast("Console", " banned ", ply, "(", reason, ") ", {(properLength == 0), "permanently", {"for ", MK.util.GetTimeleftByString(properLength)}}, "!")
	end},
}

function MK.rdm.Initialize()

end

function MK.rdm.Calc(ply, kills)
	local initial = 50
	if (ply.MK_IsOnWatchlist&&ply:MK_IsOnWatchlist()) then
		initial = initial + 50
	end

	local kills = kills or ply:MK_GameData_Get("kills", {})
	local playtime = ply:MK_Data_Get("playtime", 0) // seconds
	local playpoints = -1 * (playtime / 60 / 60 / .5) // 1 point per half hour
	if (table.Count(kills) == 0) then return (playpoints+initial) end

	local lkills = {}
	for k,v in SortedPairsByMemberValue(kills, "time") do
		table.insert(lkills, v)
	end

	// Initial Mass RDM setup
	local starttime = lkills[1].time
	local endtime = lkills[#lkills].time
	local timedifferences = {}
	local players = player.GetAll()
	local victims = {}
	local distances = {}
	local weapons = {}
	for k,v in ipairs(lkills) do
		victims[v.attacker.steamid] = (victims[v.attacker.steamid] or 0) + 1
		weapons[v.attacker.weapon] = (weapons[v.attacker.weapon] or 0) + 1

		distances[k] = {}
		timedifferences[k] = {}
		for k1,v1 in ipairs(lkills) do
			if (k == k1) then continue end
			distances[k][k1] = v.attacker.pos:Distance(v1.attacker.pos)
			if (k >= k1) then continue end
			timedifferences[k][k1] = v.time - v1.time
		end
	end

	local points = 0
	local mathE = 2.718
	local time = MK.time()
	local skipdistances = {}
	//local overalltest = 0
	for k,v in SortedPairsByMemberValue(lkills, "time", true) do // We order this way so that the newest one have the higher impact with distances later!
		local diff = time - v.time
		local rate = -0.0025
		local base = 30
		local decayed = (base*mathE^(rate*diff))

		points = points + decayed

		if (table.Count(distances[k]) > 3) then // We need data before we do distance checks
			//print("--- Testing "..k)
			//PrintTable(distances[k])
			local numlist = {}
			local averagedist = 0
			for k1,dist in pairs(distances[k]) do
				if (skipdistances[k1]) then continue end
				averagedist = averagedist + dist
				table.insert(numlist, dist)
			end
			//PrintTable(numlist)
			local num = table.Count(numlist)
			if (num > 2) then
				averagedist = averagedist / num
				local mediandist = MK.util.Median(numlist)
				local mixed = math.abs(averagedist - mediandist)
				local rate = -0.005
				local test = ((100*mathE^(rate*mixed)) * (decayed / base))
				//print("Player: "..ply:Name().." Average Distance: "..averagedist.." Median Distance: "..mediandist.. " Mixed Distance: "..mixed.." Test Decay: "..test)
				points = points + test
				//overalltest = overalltest + test
			end
		end

		skipdistances[k] = true
	end

	/*local overall = 0
	local biggest = nil
	for k,v in SortedPairsByValue(weapons) do
		overall = overall + v
		if (!biggest) then
			biggest = v
		end
	end
	local diff = biggest / overall
	local rate = 1.75
	local growth = (10*mathE^(rate*diff))
	//print("Player: "..ply:Name().." Biggest weapon count: "..biggest.." Overall wepaon count: "..overall.. " Percent: "..diff.." Test Growth: "..growth)
	points = points + growth*/

	//print(overalltest)
	local formula = points+playpoints+initial
	return formula
end

function MK.rdm.Stage(formula)
	local stage = MK.rdm.stages[1]
	if (!isnumber(formula)) then return stage end
	for k,v in pairs(MK.rdm.stages) do
		if ((tonumber(formula) or 0) >= (tonumber(v.value) or 0)) then
			stage = v
		end
	end
	return stage
end
