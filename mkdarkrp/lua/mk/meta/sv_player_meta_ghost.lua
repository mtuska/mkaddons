// RAWR!

local Player = FindMetaTable("Player")

function Player:MK_IsGhost() 
	return self:MK_GameData_Get("ghost", false)
end

function Player:MK_Ghost()
	self:MK_GameData_Set("ghost", true)
	
	// Ammo saving
	self.ammolist = {}
	self.ammoListCounted = {}
	for _,weapon in pairs(self:GetWeapons()) do
		if self:GetAmmoCount(weapon:GetPrimaryAmmoType()) > 1 && !table.HasValue(self.ammoListCounted,weapon:GetPrimaryAmmoType()) then
			local ammo = {weapon:GetPrimaryAmmoType(), self:GetAmmoCount(weapon:GetPrimaryAmmoType())}
			table.insert(self.ammolist,  ammo)
			table.insert(self.ammoListCounted, weapon:GetPrimaryAmmoType())
		end
	end

	// Weapon saving
	self.weaponlist = {}
	for _,weapon in pairs(self:GetWeapons()) do
		table.insert(self.weaponlist, weapon:GetClass())
	end
	self:SetRenderMode(RENDERMODE_TRANSALPHA)
	self:SetNWInt("LastRender", self:GetRenderMode())
	self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	self.Babygod = true

	// Material
	//self:SetNWString("LastMaterial", self:GetMaterial())
	//self:SetMaterial("models/shadertest/shader3")
	
	// Strip all weapons.
	self:StripWeapons() 
 
	self:GodEnable()

	// Custom colors
	local c = self:GetColor() 
	self:SetRenderMode(RENDERMODE_TRANSALPHA)
	self:SetColor(Color(0,0,0,180))
	timer.Simple(1, function() self:SetColor(Color(0,0,0,180)) end)
end

function Player:MK_UnGhost()
	self:MK_GameData_Set("ghost", false)
	
	self:SetColor(Color(255,255,255,255))
	timer.Simple(1, function() self:SetColor(Color(255,255,255,255)) end)

	//self:SetMaterial(self:GetNWString("LastMaterial"))
	
	// Make the player normal again and return weapons.
	self:GodDisable()
	self:SetCollisionGroup(COLLISION_GROUP_PLAYER)
	self.Babygod = false
	if self.weaponlist != nil then
		for _,otherweapon in pairs(self.weaponlist) do
			self:Give(tostring(otherweapon))
		end 
		self:SwitchToDefaultWeapon()
	end


	// Give back ammo
	self:RemoveAllAmmo()
	if self.ammolist != nil then
		for _,ammo in pairs(self.ammolist) do
			self:GiveAmmo(ammo[2], ammo[1])
		end
	end
end