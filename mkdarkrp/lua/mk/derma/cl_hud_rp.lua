// RAWR!

if (!file.Exists("materials/mk/hud/hud4.png", "GAME")) then
	return
end

local hideHUDElements = {
    ["DarkRP_HUD"] = true,
    ["DarkRP_EntityDisplay"] = false,
    ["DarkRP_LocalPlayerHUD"] = false,
    ["DarkRP_Hungermod"] = false,
    ["DarkRP_ZombieInfo"] = false,
    ["DarkRP_Agenda"] = false,
    ["DarkRP_LockdownHUD"] = false,
    ["DarkRP_ArrestedHUD"] = false,
    ["CHudHealth"] = true,
    ["CHudBattery"] = true,
    ["CHudAmmo"] = false,
    ["CHudSuitPower"] = true,
}

hook.Add("HUDShouldDraw", "HUDShouldDraw.MK_DarkRP", function(name)
	if hideHUDElements[name] then return false end
end)

surface.CreateFont("STHUD44", {font="coolvetica",size=44, weight=400, antialias=true})
surface.CreateFont("STHUD22", {font="coolvetica",size=22, weight=400, antialias=true})
surface.CreateFont("STHUD20", {font="coolvetica",size=20, weight=400, antialias=true})
surface.CreateFont("STHUD18", {font="coolvetica",size=18, weight=100, antialias=true})
surface.CreateFont("STHUD16", {font="coolvetica",size=16, weight=100, antialias=true})
surface.CreateFont("STHUD44NO", {font="Arial",size=44, weight=700, antialias=true})
surface.CreateFont("STHUD30NO", {font="Arial",size=30, weight=700, antialias=true})
surface.CreateFont("STHUD22NO", {font="Arial",size=22, weight=700, antialias=true})
surface.CreateFont("STHUD20NO", {font="Arial",size=20, weight=700, antialias=true})
surface.CreateFont("STHUD15NO", {font="Arial",size=15, weight=700, antialias=true})
surface.CreateFont("STHUD12", {font="Tahoma",size=12, weight=1000, antialias=true})

local PANEL = {}

PANEL.healthX = 42
PANEL.healthY = 79

PANEL.healthWidth = 158
PANEL.healthHeight = 13

PANEL.HPTextX = 60
PANEL.HPTextY = 60

PANEL.ArmorX = 42
PANEL.ArmorY = 94

PANEL.ArmorWidth = 154
PANEL.ArmorHeight = 5

PANEL.ArmorTextX = 120
PANEL.ArmorTextY = 73

PANEL.MoneyTextX = 74
PANEL.MoneyTextY = 105

PANEL.SalaryTextX = 10
PANEL.SalaryTextY = 108

PANEL.maxHealth = 100

PANEL.teamX = 170
PANEL.teamY = 108

PANEL.ShadowSpace = 1

local BGHud = Material("mk/hud/hud4.png")
local MissingAvatarHud = Material("mk/hud/missing_avatar.png")

function PANEL:Init()
	self:SetSize(256, 128)

	self.Target = LocalPlayer()

    self.image = vgui.Create("DImage", self)
    self.image:SetPos(0, 0)
	self.image:SetSize(256, 128)
	self.image:SetMaterial(BGHud)

    self.fakeavatar = vgui.Create("DImage", self)
    self.fakeavatar:SetPos(8,73)
	self.fakeavatar:SetSize(32,32)
	self.fakeavatar:SetMaterial(MissingAvatarHud)

	self.avatar = vgui.Create("AvatarImage", self)
	self.avatar:SetPos(8, 73)
	self.avatar:SetSize(32, 32)
	self.avatar:SetPlayer(self.Target)
end

function PANEL:PaintOver()
	if(!IsValid(self.Target)) then
		return
	end

    self.maxHealth = self.Target:GetMaxHealth()
	//self.maxHealth = (self.Target:IsVIP() and !GAMEMODE.NoHealthBonus) and 200 or 100
	/*if self.Target:IsFake() then
		if self.maxHealth > 100 and !(self.Target:GetRank() == "vip") then
			self.maxHealth = 100
		end
	end*/

	------------------------------------------------------------------------

	/*self.rank = self.Target:GetRank()
	if(RepMat and RepMat[self.rank]) then
		surface.SetDrawColor(255, 255, 255, 255)
		surface.SetMaterial(RepMat[self.rank])
		surface.DrawTexturedRect(9, 48, 32, 16)
	end*/

	------------------------------------------------------------------------

	self.healthText = tostring((self.Target:Health() >= 0 and self.Target:Health() or 0))
	/*if self.Target:IsFake() then
		if !(self.Target:GetRank() == "vip") and self.Target:Health() > 100 then
			self.healthText = 100
		end
	end*/

	if(self.Target:Alive()) then
		self.greenBarWidth = (math.Clamp(self.Target:Health(), 0, self.maxHealth) / self.maxHealth) * self.healthWidth
		self.redBarWidth = self.healthWidth - self.greenBarWidth

		surface.SetDrawColor(57, 146, 57, 230)
		surface.DrawRect(self.healthX, self.healthY, self.greenBarWidth, self.healthHeight)

		surface.SetDrawColor(195, 42, 16, 230)
		surface.DrawRect(self.healthX + self.greenBarWidth, self.healthY, self.redBarWidth, self.healthHeight)
	else
		self.healthText = "0"
		surface.SetDrawColor(172, 68, 67, 230)
		surface.DrawRect(self.healthX, self.healthY, self.healthWidth, self.healthHeight)
	end

	------------------------------------------------------------------------

	surface.SetFont("STHUD44")

	self.healthTextWidth, self.healthTextHeight = surface.GetTextSize(self.healthText)

	-- Shadow
	surface.SetTextPos(self.HPTextX+self.ShadowSpace, self.HPTextY+self.ShadowSpace)
	surface.SetTextColor(38, 38, 38, 255)
	surface.DrawText(self.healthText)

	surface.SetTextPos(self.HPTextX, self.HPTextY)
	surface.SetTextColor(255, 255, 255, 255)
	surface.DrawText(self.healthText)

	------------------------------------------------------------------------

	surface.SetFont("STHUD20")

	-- Shadow
	surface.SetTextPos(self.HPTextX + self.healthTextWidth + 5 + self.ShadowSpace, self.HPTextY + 17 + self.ShadowSpace)
	surface.SetTextColor(38, 38, 38, 255)
	surface.DrawText("HP")

	surface.SetTextPos(self.HPTextX + self.healthTextWidth + 5, self.HPTextY + 17)
	surface.SetTextColor(255, 255, 255, 255)
	surface.DrawText("HP")

	------------------------------------------------------------------------

    self.armorText = tostring((self.Target:Armor() >= 0 and self.Target:Armor() or 0))

    if(self.Target:Alive()) then
		self.blueBarWidth = (math.Clamp(self.Target:Armor(), 0, 100) / 100) * self.ArmorWidth
        self.yellowBarWidth = self.ArmorWidth - self.blueBarWidth

		surface.SetDrawColor(57, 57, 149, 230)
		surface.DrawRect(self.ArmorX, self.ArmorY, self.blueBarWidth, self.ArmorHeight)

        /*surface.SetDrawColor(195, 195, 16, 230)
		surface.DrawRect(self.ArmorX + self.blueBarWidth, self.ArmorY, self.yellowBarWidth, self.ArmorHeight)*/
	end

    self.armorTextWidth, self.armorTextHeight = surface.GetTextSize(self.armorText)

	------------------------------------------------------------------------

    /*surface.SetFont("STHUD18")

    -- Shadow
    surface.SetTextPos(self.ArmorTextX + self.armorTextWidth + 5 + self.ShadowSpace, self.ArmorTextY + 17 + self.ShadowSpace)
    surface.SetTextColor(38, 38, 38, 255)
    surface.DrawText(self.armorText.." Armor")

    surface.SetTextPos(self.ArmorTextX + self.armorTextWidth + 5, self.ArmorTextY + 17)
    surface.SetTextColor(255, 255, 255, 255)
    surface.DrawText(self.armorText.." Armor")*/

	------------------------------------------------------------------------

	self.money = self.Target:getDarkRPVar("money")

	surface.SetFont("STHUD22")

	-- Shadow
	surface.SetTextPos(self.MoneyTextX+self.ShadowSpace, self.MoneyTextY+self.ShadowSpace)
	surface.SetTextColor(38, 38, 38, 255)
	surface.DrawText(DarkRP.formatMoney(self.money))

	surface.SetTextPos(self.MoneyTextX, self.MoneyTextY)
	surface.SetTextColor(255, 255, 255, 255)
	surface.DrawText(DarkRP.formatMoney(self.money))

    ------------------------------------------------------------------------

	self.salary = self.Target:getDarkRPVar("salary")
    if (self.salary) then
    	surface.SetFont("STHUD16")

        local textWidth, textHeight = surface.GetTextSize(self.salary)

    	surface.SetTextPos(self.SalaryTextX + (textWidth / 2), self.SalaryTextY)
    	surface.SetTextColor(255, 255, 255, 255)
    	surface.DrawText(DarkRP.formatMoney(self.salary))
    end

	------------------------------------------------------------------------

	self.currentTeam = team.GetName(self.Target:Team())
	if(self.currentTeam) then
		surface.SetFont("STHUD12")

		local textWidth, textHeight = surface.GetTextSize(self.currentTeam)

		//draw.RoundedBox(4, self.teamX, self.teamY + 1, textWidth + 4, textHeight, team.GetColor(self.Target:Team()))

		surface.SetFont("STHUD12")

		surface.SetTextPos(self.teamX + (textWidth / 2) - ((textWidth + 4) / 2) + 4, self.teamY)
		surface.SetTextColor(255, 255, 255, 255)
		surface.DrawText(self.currentTeam)
	end
end

local NextThink = 0
function PANEL:Think()
	if (!IsValid(LocalPlayer())) then
		return
	end

	if NextThink <= CurTime() then
		if ScrH() >= 100 then
			self:SetPos(0, ScrH() - self:GetTall())
			NextThink = NextThink+15
		end
	end

	-- No more pesky avatars for fakers
	/*if self.Target:IsFake() then
		self.fakeavatar:SetVisible(true)
		self.avatar:SetVisible(false)
	else
		self.fakeavatar:SetVisible(false)
		self.avatar:SetVisible(true)
	end*/

	-- Change HUD target to observed player if valid
	local target = LocalPlayer():GetObserverTarget()
	if (IsValid(target)) then
		if (self.Target != target) then
			self.Target = target
		else
            return
        end
	else -- otherwise make sure the local player is the target
		if (self.Target != LocalPlayer()) then
			self.Target = LocalPlayer()
		else
			return
		end
	end

	self.avatar:SetPlayer(self.Target) -- change avatar on target change
end

vgui.Register("STGamemodes.VGUI.HUD", PANEL, "Panel")

hook.Add("MK_Player_ClientInitialized", "MK_Player_ClientInitialized.DarkRP_HUD", function()
    if (LocalPlayer().HUDPanel and IsValid(LocalPlayer().HUDPanel)) then
		LocalPlayer().HUDPanel:Remove()
	end
	LocalPlayer().HUDPanel = vgui.Create("STGamemodes.VGUI.HUD")
	LocalPlayer().HUDPanel:SetPos(0, ScrH() - LocalPlayer().HUDPanel:GetTall())
	LocalPlayer().HUDPanel:SetVisible(true)
end)
if (IsValid(LocalPlayer()) and LocalPlayer().HUDPanel and IsValid(LocalPlayer().HUDPanel)) then // For auto refresh
    LocalPlayer().HUDPanel:Remove()
    LocalPlayer().HUDPanel = vgui.Create("STGamemodes.VGUI.HUD")
    LocalPlayer().HUDPanel:SetPos(0, ScrH() - LocalPlayer().HUDPanel:GetTall())
    LocalPlayer().HUDPanel:SetVisible(true)
end
/*LocalPlayer().HUDPanel = vgui.Create("STGamemodes.VGUI.HUD")
LocalPlayer().HUDPanel:SetPos(0, ScrH() - LocalPlayer().HUDPanel:GetTall())
LocalPlayer().HUDPanel:SetVisible(true)*/
