// RAWR!

local CATEGORY = {}
	CATEGORY.name = "RDM Watch"
	CATEGORY.autoSelect = false

	//moderator.selected = {}
	//moderator.list = moderator.list or {}

	function CATEGORY:Layout(panel)
		panel.search = panel:Add("DTextEntry")
		panel.search:Dock(TOP)
		panel.search:DockMargin(4, 4, 4, 0)
		panel.search.OnTextChanged = function(this)
			local text = this:GetText()

			if (text == "") then
				self:ListPlayers(panel.scroll)
			else
				local players = {}

				for k, v in ipairs(player.GetAll()) do
					if (MK.util.StringMatches(v:Name(), text) or v:SteamID():match(text)) then
						players[k] = v
					end
				end

				self:ListPlayers(panel.scroll, players)
			end
		end
		panel.search:SetToolTip("Search for players by their name/SteamID.")

		panel.contents = panel:Add("DPanel")
		panel.contents:DockMargin(4, 4, 4, 4)
		panel.contents:Dock(TOP)
		panel.contents:SetTall(panel:GetTall() - 31)

		local function PaintButton(this, w, h)
			surface.SetDrawColor(255, 255, 255, 80)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(255, 255, 255, 120)
			surface.DrawOutlinedRect(1, 1, w - 2, h - 2)

			surface.SetDrawColor(0, 0, 0, 60)
			surface.DrawOutlinedRect(0, 0, w, h)

			if (this.entered) then
				surface.SetDrawColor(0, 0, 0, 25)
				surface.DrawRect(0, 0, w, h)
			end
		end

		panel.scroll = panel.contents:Add("DScrollPanel")
		panel.scroll:Dock(FILL)
		panel.scroll:SetWide(panel:GetWide() - 11)

		self.deltaCount = #player.GetAll()

		panel.Think = function()
			local count = #player.GetAll()

			if (self.deltaCount != count) then
				local players = player.GetAll()
				local text = (IsValid(panel.search) and panel.search:GetText() or "")

				if (text != "") then
					players = {}

					for k, v in ipairs(player.GetAll()) do
						if (MK.util.StringMatches(v:Name(), text)) then
							players[k] = v
						end
					end
				end

				self:ListPlayers(panel.scroll, players)
			end
		end

		self:ListPlayers(panel.scroll)

		return true
	end

	function CATEGORY:ListPlayers(scroll, players)
		if (!IsValid(scroll)) then
			return
		end

		MK.menus.players = {}

		scroll:Clear()
		players = players or player.GetAll()

		local plys = {}
		for k, v in SortedPairs(players) do
			table.insert(plys, {ply = v, formula = math.floor(MK.rdm.Calc(v))})
		end

		for k,v in SortedPairsByMemberValue(plys, "formula", true) do
			local item = scroll:Add("mk_Player")
			item:Dock(TOP)
			item:SetPlayer(v.ply)

			item.rdm = item:Add("DLabel")
			item.rdm:SetFont("mk_TextFont")
			item.rdm:Dock(LEFT)
			item.rdm:DockMargin(10, 0, 0, 0)
			item.rdm:SetDark(true)
			item.rdm:SetText("Green")
			item.rdm:SizeToContents()
			item.rdm.Paint = function(this, w, h)
				surface.SetDrawColor( 0,0,0 )
				surface.DrawLine( -w/2, 0, -w/2, 10 )
			end

			item.index = #MK.menus.players + 1

			MK.menus.players[item.index] = item
		end
	end

	function CATEGORY:ShouldDisplay()
		return LocalPlayer():IsAdmin()
	end
MK.menus.list.rdm = CATEGORY
