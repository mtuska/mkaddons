// RAWR!

local CATEGORY = {}
	CATEGORY.name = "Raid Inc."
	CATEGORY.autoSelect = false

	//moderator.selected = {}
	//moderator.list = moderator.list or {}

	CATEGORY.fplayers = {}
	CATEGORY.eplayers = {}

	local function PaintButton(this, w, h)
		surface.SetDrawColor(255, 255, 255, 80)
		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(255, 255, 255, 120)
		surface.DrawOutlinedRect(1, 1, w - 2, h - 2)

		surface.SetDrawColor(0, 0, 0, 60)
		surface.DrawOutlinedRect(0, 0, w, h)

		if (this.entered) then
			surface.SetDrawColor(0, 0, 0, 25)
			surface.DrawRect(0, 0, w, h)
		end
	end

	function CATEGORY:Layout(panel)
		panel.search = panel:Add("DTextEntry")
		panel.search:Dock(TOP)
		panel.search:DockMargin(4, 4, 4, 0)
		panel.search.OnTextChanged = function(this)
			self.UpdateLists()
		end
		panel.search:SetToolTip("Search for players by their name/SteamID.")

		panel.contents = panel:Add("DPanel")
		panel.contents:DockMargin(4, 4, 4, 4)
		panel.contents:Dock(TOP)
		panel.contents:SetTall(panel:GetTall() - 31)

		panel.button = panel.contents:Add("DButton")
		panel.button:Dock(BOTTOM)
		panel.button:SetTall(28)
		panel.button.Paint = PaintButton
		panel.button:DockMargin(2, 2, 2, 2)
		panel.button.OnCursorEntered = function(this)
			this.entered = true
		end
		panel.button.OnCursorExited = function(this)
			this.entered = nil
		end
		panel.button:SetText("Start Raid")
		panel.button:SetTextColor(Color(0, 0, 0, 230))
		panel.button.DoClick = function()
			local friends = {}
			local enemies = {}

			for k,v in pairs(self.fplayers) do
				table.insert(friends, k)
			end
			for k,v in pairs(self.eplayers) do
				table.insert(enemies, k)
			end

			MK.commands.Parse(LocalPlayer(), {"raid", enemies, friends}) // Friends aren't required, put them last
		end

		panel.enemies = panel.contents:Add("DScrollPanel")
		panel.enemies:Dock(RIGHT)
		panel.enemies:SetWide(panel:GetWide() * 0.5)

		panel.friends = panel.contents:Add("DScrollPanel")
		panel.friends:Dock(FILL)
		panel.friends:SetWide(panel:GetWide() * .5 - 11)

		self.deltaCount = #player.GetAll()

		panel.Think = function()
			local count = #player.GetAll()

			if (self.deltaCount != count) then
				self.UpdateLists()
			end
		end

		self.UpdateLists = function()
			local players = player.GetAll()
			local text = (IsValid(panel.search) and panel.search:GetText() or "")

			if (text != "") then
				players = {}

				for k, v in ipairs(player.GetAll()) do
					if (MK.util.StringMatches(v:Name(), text)) then
						players[k] = v
					end
				end
			end

			self:ListPlayers(panel.friends, players)
			self:ListEnemies(panel.enemies, players)
		end

		self.UpdateLists()

		return true
	end

	function CATEGORY:ListPlayers(scroll, players)
		if (!IsValid(scroll)) then
			return
		end

		MK.menus.players = {}

		scroll:Clear()
		players = players or player.GetAll()

		for k, v in SortedPairs(players) do
			if (self.eplayers[v]) then
				continue
			end
			local item = scroll:Add("mk_Player")
			item:Dock(TOP)
			item:SetPlayer(v, true, true)
			item.index = #MK.menus.players + 1
			function item.OnSelected(localItem, bool)
				self.fplayers[localItem.player] = bool
				if (!self.fplayers[localItem.player]) then
					self.fplayers[localItem.player] = nil
				end

				self.UpdateLists()
			end

			if (self.fplayers[v]) then
				item.selected = true
			end

			MK.menus.players[item.index] = item
		end
	end

	function CATEGORY:ListEnemies(scroll, players)
		if (!IsValid(scroll)) then
			return
		end

		MK.menus.players = {}

		scroll:Clear()
		players = players or player.GetAll()

		for k, v in SortedPairs(players) do
			if (v == LocalPlayer()) then
				continue
			end
			if (self.fplayers[v]) then
				continue
			end
			local item = scroll:Add("mk_Player")
			item:Dock(TOP)
			item:SetPlayer(v, true, true)
			item.index = #MK.menus.players + 1
			function item.OnSelected(localItem, bool)
				self.eplayers[localItem.player] = bool
				if (!self.eplayers[localItem.player]) then
					self.eplayers[localItem.player] = nil
				end

				self.UpdateLists()
			end

			if (self.eplayers[v]) then
				item.selected = true
			end

			MK.menus.players[item.index] = item
		end
	end

	function CATEGORY:ShouldDisplay()
		return LocalPlayer():IsAdmin()
	end
MK.menus.list.raid = CATEGORY
