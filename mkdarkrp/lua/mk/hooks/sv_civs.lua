// RAWR!

//MK.mapSpawn.Add("rp_downtown_evilmelon_v1", "vaultGuard_1", "mk_banksecurity", Vector(-1950, -2907, -300), Angle(5, 164, 0))
MK.mapSpawn.Add("rp_downtown_evilmelon_v1", "vaultGuard_1", "mk_banksecurity", Vector(-1840, -3035, -300), Angle(0, 65, 0), 450)
//MK.mapSpawn.Add("rp_downtown_evilmelon_v1", "vaultGuard_2", "mk_banksecurity", Vector(-1950, -2805, -300), Angle(5, -164, 0))
MK.mapSpawn.Add("rp_downtown_evilmelon_v1", "vaultGuard_2", "mk_banksecurity", Vector(-1840, -2665, -300), Angle(0, -65, 0), 450)

hook.Add("GravGunOnPickedUp", "GravGunOnPickedUp.MK_Civs", function(ply, ent)
	ent.carryingPly = ply
	ply.carryingEnt = ent
end)

hook.Add("GravGunOnDropped", "GravGunOnDropped.MK_Civs", function(ply, ent)
	ent.carryingPly = nil
	ply.carryingEnt = nil
end)

hook.Add("EntityFireBullets", "EntityFireBullets.MK_Civs", function(ent, bullet)
	local callback = nil
	if (bullet.Callback) then
		callback = bullet.Callback
	end
	bullet.Callback = function(attacker, tr, dmginfo)
		if (IsValid(attacker) && attacker:IsPlayer()) then
			MK.civs.Crime("Shots fired nearby", 0.6, {criminal = attacker})
		end
		MK.civs.Crime("Shots fired nearby", 0.6, {pos = tr.StartPos, dist = 150})
		MK.civs.Crime("Shots fired nearby", 0.6, {pos = tr.EndPos, dist = 150})
		if (callback) then
			callback(attacker, tr, dmginfo)
		end
	end
end)

hook.Add("PlayerDeath", "PlayerDeath.MK_Civs", function(ply, wep, killer)
	if (IsValid(killer) && killer:IsPlayer() && killer != ply && !killer:isCP() && killer:Alive()) then
		MK.civs.Crime("Murder", 1, {criminal = killer})
		MK.civs.Crime("Murder", 1, {pos = ply:GetPos(), dist = 150})
	end
end)

function MK.civs.IllegalEntityCheck()
	//BroadcastLua("print('tick')")
	local criminals = {}
	for k,v in pairs(player.GetAll()) do
		if (IsValid(v) && v:Alive()) then
			local weapon = v:GetActiveWeapon()
			if (weapon && IsValid(weapon)) then
				if ((weapon.GetIsLockpicking && weapon:GetIsLockpicking()) || weapon.IsLockPicking || weapon.IsCracking) then
					MK.civs.Crime("Illegal Activities", 0.25, {criminal = v})
					continue
				end
				if (MK.civs.IsIllegalEntity(weapon) && !v:getDarkRPVar("HasGunlicense")) then
					MK.civs.Crime("Possesion of Illegal Weapons", 0.6, {criminal = v})
					continue
				end
			end
			if (!v:isCP() && v.carryingEnt && IsValid(v.carryingEnt) && MK.civs.IsIllegalEntity(v.carryingEnt)) then
				MK.civs.Crime("Possesion of Illegal Items", 0.2, {criminal = v})
				continue
			end
		end
	end

	//local illegalEnts = {}
	for k,v in pairs(ents.GetAll()) do
		if (IsValid(v) && !v:IsWeapon()) then // Don't check for weapons, for now I guess
			if (MK.civs.IsIllegalEntity(v)) then
				//table.insert(illegalEnts, v)
				//MK.civs.Crime("Possesion of Illegal Items", 0.2, {entity = v})
			end
		end
	end
end
if (timer.Exists("MK.civs.IllegalEntityCheck")) then
	timer.Destroy("MK.civs.IllegalEntityCheck")
end
timer.Create('MK.civs.IllegalEntityCheck', 1, 0, MK.civs.IllegalEntityCheck)
