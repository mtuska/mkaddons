// RAWR!

local function GetSceneDataFromPlayer(ply)
	local data = {
		pos			= ply:GetPos(),
		ang			= ply:GetAngles(),
		sequence	= ply:GetSequence(),
		cycle		= ply:GetCycle(),
		steamid		= ply:SteamID()
	};

	for _, param in pairs(poseparams) do
		data[param] = ply:GetPoseParameter(param)
	end

	return data
end

hook.Add("EntityTakeDamage", "EntityTakeDamage.MK_RDM", function(ent, dmginfo)
	if (ent:IsPlayer()) then
		/*local ply = ent
		MK.rdm.damages[ply:SteamID()] = MK.rdm.damages[ply:SteamID()] or {}
		local damage = {}
		damage.time = os.time()
		damage.dmginfo = dmginfo
		table.insert(MK.rdm.damages[ply:SteamID()], dmginfo)*/
	end
end)

hook.Add("DoPlayerDeath", "DoPlayerDeath.MK_RDM", function(victim, attacker, dmginfo)
	if (IsValid(attacker) and attacker:IsPlayer() and attacker != victim) then // We can't need to track anything not between players
		local doNotTrack = false
		for k,v in pairs(MK.raid.raids) do
			if (table.HasValue(v.raiders, attacker)&&table.HasValue(v.targets, victim)) then
				doNotTrack = true // This is part of a raid!
			elseif (table.HasValue(v.raiders, victim)&&table.HasValue(v.targets, attacker)) then
				doNotTrack = true // This is part of a raid!
			end
		end
		if (!doNotTrack) then
			local kill = {}
			kill.time = os.time()
			
			kill.victim = {}
			kill.victim.steamid = victim:SteamID()
			kill.victim.pos = victim:GetPos()
			kill.victim.velocity = victim:GetVelocity()
			if (IsValid(victim:GetActiveWeapon())) then
				kill.victim.weapon = victim:GetActiveWeapon():GetClass()
			end
			kill.victim.team = victim:Team()
			kill.victim.teamName = team.GetName(victim:Team())
			kill.victim.weapons = {}
			for k,v in pairs(victim:GetWeapons()) do
				table.insert(kill.victim.weapons, v:GetClass())
			end
			
			kill.attacker = {}
			kill.attacker.steamid = attacker:SteamID()
			kill.attacker.pos = attacker:GetPos()
			kill.attacker.velocity = attacker:GetVelocity()
			if (IsValid(attacker:GetActiveWeapon())) then
				kill.attacker.weapon = attacker:GetActiveWeapon():GetClass()
			end
			kill.attacker.team = attacker:Team()
			kill.attacker.teamName = team.GetName(attacker:Team())
			kill.attacker.weapons = {}
			for k,v in pairs(attacker:GetWeapons()) do
				table.insert(kill.attacker.weapons, v:GetClass())
			end
			
			kill.distance = kill.victim.pos:Distance(kill.attacker.pos)
			kill.dmginfo = dmginfo
			
			MK.rdm.Process(victim, attacker, dmginfo, kill)
		end
	end
	
	// Visualizaton
	/*local scene = {}
	
	scene.dmginfo = dmg
	scene.endpos = dmg:GetDamagePosition()
	scene.victim = GetSceneDataFromPlayer(victim)

	if (IsValid(attacker) and attacker:IsPlayer()) then
		scene.killer = GetSceneDataFromPlayer(attacker)

		local att = attacker:LookupAttachment("anim_attachment_RH")
		local angpos = attacker:GetAttachment(att)
		if not angpos then
			scene.startpos = attacker:GetShootPos()
		else
			scene.startpos = angpos.Pos
		end
		
		scene.trace = util.TraceLine({
			start = scene.startpos
			endpos = scene.endpos
			filter = attacker
		})
	end
	
	table.insert(MK.rdm.scenes, scene)*/
end)