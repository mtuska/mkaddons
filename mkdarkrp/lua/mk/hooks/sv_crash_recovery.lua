// RAWR!

if (true) then return end

local config = {}
config.recovery_waittime = 300 * 10 -- time in seconds to wait for users to rejoin and get their items. Set to -1 to disable.
config.save_frequency = 20 -- interval in seconds between saves.
config.save_dir = 'crash_recovery.lua'
config.custom_entities = { -- if there are any entities that the system isn't detecting for some reason list them here.
  ['spawned_shipment'] = true -- true enables saving. false disables it.
}

// DONT EDIT THESE FUNCTIONS UNLESS YOU KNOW WHAT YOU ARE DOING
config.find_owner = function( ent ) -- function to find the entity's owner. Modify this if you have some other variable the owner is stored in.
	if( ent.dt and ent.dt.owning_ent )then
		return ent.dt.owning_ent
	end
end

config.restore_owner = function( e, ply ) -- takes the entity and the player.
	if( not e.dt and e.SetupDataTables )then
		e:SetupDataTables();
	end
	if( e.dt )then
		e.dt.owning_ent = ply;
	end
end

-- allows you to use another library to serialize or deserialise tables eg: Von or GLON or maybe SQLite if you want to code it.
config.table_encoder = util.TableToJSON -- if you want to use some other format or a custom function put it here.
config.table_decoder = util.JSONToTable -- if you want to use some other format or a custom function put it here.


/*==============================================================
DO NOT EDIT BELOW THIS LINE UNLESS YOU KNOW WHAT YOU ARE DOING
SUPPORT WILL NOT BE OFFERED FOR MODIFIED VERSIONS OF THIS SCRIPT
==============================================================*/



//print("Loaded PCrash Recovery by TheLastPenguin TM");

local function ent_save( entity )
	local tbl = {};
	local nwvars = entity:GetNetworkVars();
	tbl.nwvars = nwvars;
	tbl.mdl = entity:GetModel();
	tbl.ownerid = config.find_owner( entity ):SteamID();
	tbl.class = entity:GetClass();

	return tbl;
end

local function ent_load( tbl )
	-- make the entity.
	local e = ents.Create( tbl.class );

	-- init and spawn it
	if( e.Initalize )then
		e:Initalize();
	end
	e:Spawn();

	-- restore properties.
	e:RestoreNetworkVars( tbl.nwvars );
	e:SetModel( tbl.mdl );
	return e;
end

local function ent_saveall()
	if( not DarkRPEntities )then
		print("THIS MOD ONLY WORKS WITH DARKRP!");
		return ;
	end

	local output_tbl = {}
	local valid_classes = {}

	-- identify entity classes that we can save.
	local saveClasses = table.Copy( config.custom_entities );
	table.Merge( saveClasses, DarkRPEntities );

	for k,v in pairs( saveClasses )do
		if( type( v ) == 'table' )then
			saveClasses[ k ] = v.ent
			valid_classes[ v.ent ] = true;
		else
			valid_classes[ v ] = true;
		end
	end

	for k,v in pairs( ents.GetAll() )do
		if( valid_classes[ v:GetClass() ] )then
			local owner = config.find_owner( v );
			if( IsValid( owner ) and owner:IsPlayer() )then
				print("ATTEMPTING TO SAVE ENTITY CLASS: " .. v:GetClass() );
				table.insert( output_tbl, ent_save( v ) );
			end
		end
	end

	return output_tbl;
end

local function ent_loadplayer( tbl, ply )
	local steamid = ply:SteamID();

	if( not ply.Pocket )then ply.Pocket = {} end

	local c = 0;
	for k,v in pairs( tbl )do
		if( v.ownerid == steamid )then
			c = c + 1;
			local e = ent_load( v );

			// insert into darkrp pocket.
			table.insert( ply.Pocket, e );

			e:SetNoDraw(true);
			e:SetPos(Vector(0,0,0));
			local phys = e:GetPhysicsObject();
			phys:EnableMotion(false);
			e.OldCollisionGroup = e:GetCollisionGroup();
			e:SetCollisionGroup(COLLISION_GROUP_WORLD);
			e.PhysgunPickup = false;
			e.OldPlayerUse = e.PlayerUse;
			e.PlayerUse = false;
			e.IsPocketed = true;

			config.restore_owner( e, ply );
		end
	end
	print("Loaded "..c.." entities for "..ply:Name()..".");
	return c;
end

local function team_saveall()
	local teams = {}
	for k,v in pairs( player.GetAll())do
		local weps = {};
		for _, wep in pairs( v:GetWeapons() )do
			table.insert( weps, wep:GetClass() );
		end

		teams[v:SteamID()] = {
			team = v:Team(),
			weps = weps
		};
	end
	return teams;
end

local function team_loadplayer( teams, ply )
	local prevt = teams[ ply:SteamID() ];
	if( prevt )then
		if( ply:ChangeAllowed( prevt ) )then
			ply:ChangeTeam( prevt.team, true ); -- true to force it so no vote is needed.
			for k,v in pairs( prevt.weps )do
				ply:Give( v );
			end
		end
	end
end

if( not file.IsDir(config.save_dir,'DATA'))then
  file.CreateDir(config.save_dir,'DATA');
end

local function recovery_SaveData( )
	print("[CRASHRECOVERY] Running Save.");

	local teams = team_saveall();
	file.Write(config.save_dir..'/teams.txt', config.table_encoder( teams ));


	local enttbl = ent_saveall();
	file.Write(config.save_dir..'/ents.txt', config.table_encoder( enttbl ));
end

local recovered = {}
local function recovery_loadData( )
	local teamStr = file.Read(config.save_dir..'/teams.txt','DATA');
	if( teamStr )then
		recovered.teams = config.table_decoder( teamStr );
	end

	local entStr = file.Read(config.save_dir..'/ents.txt','DATA');
	if( entStr )then
		recovered.ents = config.table_decoder( entStr );
	end
end
recovery_loadData();
print("CRASH RECOVERY:")
PrintTable( recovered );

local function recovery_loadPlayer( ply )
	if( not recovered )then return end
		if( recovered.teams )then
		print("Recovering player's team info.");
		team_loadplayer( recovered.teams, ply );
		ply:ChatPrint('Restored your team information');
	end

	if( recovered.ents )then
		print("Recovering player's entity info.");
		local count = ent_loadplayer( recovered.ents, ply );
		ply:ChatPrint('Restored ' .. count .. ' entities. Check your darkrp pocket.' );
	end
end


timer.Create("PCrashRecovery",config.save_frequency,0, function()
	recovery_SaveData();
end);

concommand.Add("PCrashRecovery_ForceSave",function( ply )
	if( not ply:IsSuperAdmin() and not ply:IsListenServerHost() )then return end
	
	recovery_SaveData();
end );

timer.Simple(config.recovery_waittime, function()
	recovered = nil;
end);

hook.Add('PlayerInitialSpawn','PCrashRecover',function( ply )
	timer.Simple( 5, function()
		recovery_loadPlayer( ply );
	end)
end)