// RAWR!

net.Receive("MK_SyncNlr", function(len)
	local sync = net.ReadTable()
	LocalPlayer().NLR = {}
	for k,v in pairs(sync) do
		table.insert(LocalPlayer().NLR, {pos = v.pos, time = CurTime() + v.time})
	end
end)

hook.Add("Think", "NLR.Think", function()
	if LocalPlayer():Alive() then
		local found = false
		for k,v in pairs(LocalPlayer().NLR or {}) do
			if v.pos:Distance(LocalPlayer():GetShootPos()) < MK.nlr.sphereRadius then
				LocalPlayer().NLRTime = math.floor(v.time - CurTime())
				LocalPlayer().InNLR = k
				found = true
			end
		end
		if (!found) then
			LocalPlayer().InNLR = nil
		else
			if (DarkRP.getF4MenuPanel() != nil) then
				DarkRP.getF4MenuPanel():Hide()
			end
		end
	end
end)

hook.Add("PostDrawTranslucentRenderables", "NLR.PostDrawTranslucentRenderables", function()
	local spinspeed = MK.nlr.deathmarkspeed / 100   
	if angle == nil then
		angle = 0
	else
		angle = angle + spinspeed  
	end 
	for k,v in pairs(LocalPlayer().NLR or {}) do
		if (MK.nlr.drawSphere) then
			local circleColor = Color(255,255,255)

			cam.Start3D2D( v.pos, Angle(0,0,0), MK.nlr.ghostCircleSize )
				local dimension = MK.nlr.sphereRadius / MK.nlr.ghostCircleSize
				surface.DrawCircle( 0, 0, dimension, circleColor )
			cam.End3D2D()
		end
		/*if v.pos:Distance(LocalPlayer():GetShootPos()) < NLR.Radius * 2 then
			render.SetMaterial(Material("models/shadertest/shader3"))
			render.DrawSphere(v.pos, NLR.Radius, 100, 100, Color(30, 30, 230, 255))
			render.DrawSphere(v.pos, -1*NLR.Radius, 100, 100, Color(30, 30, 230, 255))
			--render.SetMaterial(Material("models/props_combine/stasisshield_sheet"))
			--render.DrawSphere(v.pos, NLR.Radius, 100, 100, Color(30, 30, 230, 255))
			--render.DrawSphere(v.pos, -1*NLR.Radius, 100, 100, Color(30, 30, 230, 255))
		end*/
	end
end)

hook.Add("RenderScreenspaceEffects", "NLR.RenderScreenspaceEffects", function()
	if LocalPlayer():Alive() then
		if !LocalPlayer().NLR then return end
		if LocalPlayer().InNLR then
			if (LocalPlayer().NLRTime >= 0) then
				draw.DrawText("You have "..LocalPlayer().NLRTime.." seconds remaining in the NLR", "DermaLarge", ScrW() / 2 - 70, ScrH() - 113, Color(255, 255, 255, 255),TEXT_ALIGN_CENTER)
			end
			local screen = 20 // > lighter  < darker
			color = math.Clamp(1.4 - ( ( 100 - screen ) * 0.02 ), 0, 1.1)
			brightness = math.Clamp(0.6 - ( ( 100 - screen ) * 0.01 ), -0.7, -0.05)
			bloom = math.Clamp(1.4 - ( ( 100 - screen ) * 0.02 ), 0, 1)
			ColorModify = {}
			ColorModify["$pp_colour_brightness"] = -0.05
			ColorModify["$pp_colour_contrast"] = 1.1
			ColorModify["$pp_colour_addr"] = 0
			ColorModify["$pp_colour_addg"] = 0
			ColorModify["$pp_colour_addb"] = 0
			ColorModify["$pp_colour_mulr"] = 0
			ColorModify["$pp_colour_mulg"] = 0
			ColorModify["$pp_colour_mulb"] = 0
			ColorModify["$pp_colour_colour"] = color
			ColorModify["$pp_colour_brightness"] = brightness
			DrawColorModify(ColorModify)
		end
	end
end)