// RAWR!

util.AddNetworkString("MK_SyncNlr")
hook.Add("PlayerDeath", "NLR.StartDeath", function(ply, weapon, killer)
	if (IsValid(ply) && IsValid(killer) && killer:IsPlayer() && ply != killer) then
		ply.RequestNLR = {pos = ply:GetPos(), time = CurTime() + MK.nlr.time}
	end
end)

hook.Add("PlayerSpawn", "NLR.StartSpawn", function(ply)
	if (ply.RequestNLR && !ply:isArrested() && (!TEAM_ADMIN || ply:Team() != TEAM_ADMIN)) then
		if (ply:GetPos():Distance(ply.RequestNLR.pos) > MK.nlr.sphereRadius) then
			local timeleft = ply.RequestNLR.time - CurTime()
			if (timeleft > 0) then
				ply.NLR = ply.NLR or {}
				table.insert(ply.NLR, {pos = ply.RequestNLR.pos, time = ply.RequestNLR.time})

				local sync = {}
				for k,v in pairs(ply.NLR) do
					table.insert(sync, {pos = v.pos, time = v.time - CurTime()})
				end
				net.Start("MK_SyncNlr")
					net.WriteTable(sync)
				net.Send(ply)

				local uniqueid = "MK_NLR_"..#ply.NLR.."_"..ply:SteamID()
				if (timer.Exists(uniqueid)) then
					timer.Destroy(uniqueid)
				end
				local k = #ply.NLR
				timer.Create(uniqueid, timeleft, 1, function()
					if IsValid(ply) then
						ply.NLR[k] = nil
						local sync = {}
						for k,v in pairs(ply.NLR) do
							table.insert(sync, {pos = v.pos, time = v.time - CurTime()})
						end
						net.Start("MK_SyncNlr")
							net.WriteTable(sync)
						net.Send(ply)
					end
				end)
			end
		end
	end
end)

hook.Add("Think", "NLR.Think", function()
	for k,ply in pairs(player.GetAll()) do
		local ghostify = false
		for k,nlr in pairs(ply.NLR or {}) do
			if (nlr.pos:Distance(ply:GetShootPos()) < MK.nlr.sphereRadius) then
				ghostify = true
			end
		end

		if (ghostify&&!ply:MK_GameData_Get("ghost", false)) then
			ply:MK_Ghost()
		end

		if (!ghostify&&ply:MK_GameData_Get("ghost", false)) then
			ply:MK_UnGhost()
		end

		/*if (ghostify && !ply:MK_GameData_Get("ghost", false)) then
			ply:MK_GameData_Set("ghost", true)
			ply:MK_Ghost(true)
		end*/
	end
end)

/////////// BLOCKINGS start//////////////
hook.Add("PlayerUse", "GhostsCantUse", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnEffect", "GhostNoEffect", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnNPC", "GhostNoNPC", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnObject", "GhostNoObject", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnProp", "GhostNoProp", function(ply)
	/*if (ply:SteamID() == "STEAM_0:0:65280394") then
		return false
	end*/
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnRagdoll", "GhostNoRagdoll", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnSENT", "GhostNoSENT", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnSWEP", "GhostNoSwep", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerSpawnVehicle", "GhostNoVehicle", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerCanJoinTeam", "GhostNoTeam", function(ply)
	if ply:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerCanHearPlayersVoice", "GhostNoVoice", function(listener, talker)
	if talker:MK_IsGhost() then
		return false
	end
end)

hook.Add("PlayerGiveSWEP", "GhostNoPickupSwep", function(ply)
	if ply:MK_IsGhost() && (!ply:IsAdmin() || !ply:IsSuperAdmin()) then
		return false
	end
end)
/////////// BLOCKINGS end //////////////
