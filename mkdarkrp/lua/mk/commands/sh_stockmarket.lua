// RAWR!

local function buy(ply, ticker, shares)
	if (MK.stock.closed) then
		ply:MK_SendChat(Color(255,255,255), "Stock market is currently closed!")
		return
	end
	local costPerShare = MK.stock.stocks[ticker:upper()].value or nil
	if (costPerShare) then
		if (ply:canAfford(costPerShare*shares)) then
			ply:addMoney(-math.ceil(costPerShare*shares))
			local stocks = ply:MK_Data_Get("stockexchange", {})
			local stock = stocks[ticker:upper()] or table.Copy(MK.stock.defaultStockData)
			if (!istable(stock)) then
				stocks[ticker:upper()] = table.Copy(MK.stock.defaultStockData)
				stocks[ticker:upper()].value = stock
				stock = stocks[ticker:upper()]
			end
			if ((stock.value + shares) <= 100) then
				stock.value = stock.value + shares
				stock.bought = costPerShare
				stocks[ticker:upper()] = stock
				ply:MK_Data_Set("stockexchange", stocks)
				DarkRP.notify(ply, NOTIFY_GENERIC, 5, "Purchased "..shares.." shares of stock ticker "..ticker:upper().." for $"..math.ceil(costPerShare*shares).." at "..math.Round(costPerShare, 3))
			else
				DarkRP.notify(ply, NOTIFY_ERROR, 5, "You can only purchase "..(100-stock.value).." more shares of stock ticker "..ticker:upper())
			end
		else
			DarkRP.notify(ply, NOTIFY_ERROR, 5, "You can't afford "..shares.." shares of stock ticker "..ticker:upper())
		end
	else
		DarkRP.notify(ply, NOTIFY_ERROR, 5, "Couldn't identify stock ticker "..ticker:upper())
	end
end
local buyCommand = MK.commands.Add("stocks", "buy", buy, {}, "/", "mkgm")
buyCommand:AddParam(MK.commands.args.String, "stock", true, {})
buyCommand:AddParam(MK.commands.args.Amount, "shares", true, {min = 1, max = 100})
buyCommand:DefaultRank("user")

local function sell(ply, ticker, shares)
	if (MK.stock.closed) then
		ply:MK_SendChat(Color(255,255,255), "Stock market is currently closed!")
		return
	end
	local costPerShare = MK.stock.stocks[ticker:upper()].value or nil
	if (costPerShare) then
		local stocks = ply:MK_Data_Get("stockexchange", {})
		local stock = stocks[ticker:upper()] or table.Copy(MK.stock.defaultStockData)
		if (!istable(stock)) then
			stocks[ticker:upper()] = table.Copy(MK.stock.defaultStockData)
			stocks[ticker:upper()].value = stock
			stock = stocks[ticker:upper()]
		end
		if (stock.value > 0) then
			if (shares <= stock.value) then
				stocks[ticker:upper()].value = stock.value - shares
				ply:MK_Data_Set("stockexchange", stocks)
				local cash = math.ceil(costPerShare*shares)
				local tax = math.ceil(cash*MK.stock.tax)
				cash = math.ceil(cash - (cash*MK.stock.tax))
				ply:addMoney(cash)
				DarkRP.notify(ply, NOTIFY_GENERIC, 5, "Selling "..shares.." shares of stock ticker "..ticker:upper().." for $"..cash.." at "..math.Round(costPerShare, 3))
			else
				DarkRP.notify(ply, NOTIFY_ERROR, 5, "You only have "..stock.value.." shares of stock ticker "..ticker:upper())
			end
		else
			DarkRP.notify(ply, NOTIFY_ERROR, 5, "You don't have any stocks for stock ticker "..ticker:upper())
		end
	else
		DarkRP.notify(ply, NOTIFY_ERROR, 5, "Couldn't identify stock ticker "..ticker:upper())
	end
end
local sellCommand = MK.commands.Add("stocks", "sell", sell, {}, "/", "mkgm")
sellCommand:AddParam(MK.commands.args.String, "stock", true, {})
sellCommand:AddParam(MK.commands.args.Amount, "shares", true, {})
sellCommand:DefaultRank("user")

local function shares(ply, ticker)
	local stocks = ply:MK_Data_Get("stockexchange", {})
	if (ticker && ticker != "") then
		if (stocks[ticker:upper()]) then
			ply:MK_SendChat(Color(255,255,255), ticker:upper()..": "..stocks[ticker:upper()].value.." shares")
		end
	else
		if (table.Count(stocks) == 0) then
			ply:MK_SendChat(Color(255,255,255), "You haven't purchased any shares!")
		else
			local updated = false
			for k,v in pairs(stocks) do
				if (!istable(v)) then
					updated = true
					local val = v
					stocks[k] = table.Copy(MK.stock.defaultStockData)
					stocks[k].value = val
				end
				ply:MK_SendChat(Color(255,255,255), k..": "..v.value.." shares")
			end
			if (updated) then
				ply:MK_Data_Set("stockexchange", stocks)
			end
		end
	end
end
local sharesCommand = MK.commands.Add("stocks", "shares", shares, {}, "/", "mkgm")
sharesCommand:AddParam(MK.commands.args.String, "stock", false, {})
sharesCommand:DefaultRank("user")
