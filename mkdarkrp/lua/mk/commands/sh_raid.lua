// RAWR!

MK.permissions.Register("see_raid", "operator")

local function raid(ply, targets, friends)
	local testplys = {ply}
	table.Add(testplys, friends)
	table.Add(testplys, targets)

	for k,v in pairs(testplys) do
		if (RPExtraTeams[v:Team()].nonrp) then
			MK.administration.SelectiveBroadcast(ply, v, " is in a nonRP class! Can't raid.")
			return
		end
	end

	for k,v in pairs(targets) do
		if (table.HasValue(friends, v)) then
			MK.administration.SelectiveBroadcast(ply, v, " is in the targets and friends list! Can't raid.")
			return
		end
	end

	local canRaid = false
	local initiates = {}
	for k,v in pairs({ply, unpack(friends)}) do
		if (!RPExtraTeams[v:Team()].canRaid) then
			MK.administration.SelectiveBroadcast(ply, v, " can not raid as their class!")
			return
		end
		if (RPExtraTeams[v:Team()].canRaid == 2) then
			canRaid = true
			table.insert(initiates, v)
		end
	end

	if (!canRaid) then
		MK.administration.SelectiveBroadcast(ply, "No one has the proper class to initate a raid!")
		return
	end

	MK.raid.StartRaid(ply, {ply, unpack(friends)}, initiates, targets)
end
local raidCommand = MK.commands.Add("player", "raid", raid, {}, "/", "mkgm")
raidCommand:AddParam(MK.commands.args.Players, "targets", true, {})
raidCommand:AddParam(MK.commands.args.Players, "friends", false, {})
raidCommand:DefaultRank("admin")
