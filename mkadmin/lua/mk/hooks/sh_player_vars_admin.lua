// RAWR!

hook.Add("MK_Player.SetupVars", "MK_Player.SetupVars.Administration", function(ply)
	//----------------------------------------------------------------------------------------------------\\

	// index, default, save, noNetwork, broadcast
	ply:MK_GameData_Create("devmode", nil, false, false, false, true)

	ply:MK_GameData_Create("permagagged", false, true, false, true)
	ply:MK_GameData_Create("gagged", false, false, false, true)

	ply:MK_GameData_Create("permamuted", false, true, false, true)
	ply:MK_GameData_Create("muted", false, false, false, true)

	ply:MK_GameData_Create("permascale", false, true, false, true)
	ply:MK_GameData_Create("scale", false, false, false, true)

	ply:MK_GameData_Create("name", false, false, false, true)

	ply:MK_GameData_Create("spamurl", false, false, false, true)
	
	// Diguise data
	ply:MK_GameData_Create("disguised", false, false, false, true)
	ply:MK_GameData_Create("fakename", false, false, false, true)
	ply:MK_GameData_Create("fakerank", false, false, false, true)
	ply:MK_GameData_Create("fakesteamid", false, false, false, true)
	ply:MK_GameData_Create("fakesteamid64", false, false, false, true)
	ply:MK_GameData_Create("fakeuniqueid", false, false, false, true)

	//----------------------------------------------------------------------------------------------------\\

	// index, default, noNetwork, broadcast
	ply:MK_Data_Create("rank", nil, false, true)

	ply:MK_Data_Create("sharedAccounts", {}, false, true)
	ply:MK_Data_Create("game_lenders", {}, false, true)

	ply:MK_Data_Create("watchlisted", false, false, true)
end)
