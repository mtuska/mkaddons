// RAWR!

if (CLIENT) then
	net.Receive("MK_SyncRanks", function()
		MK.ranks.list = util.JSONToTable(net.ReadString())
	end)
else
	util.AddNetworkString("MK_SyncRanks")
	
	hook.Add("MK_Player.ClientInitialized", "MK_Player.ClientInitialized.SyncRanks", function(ply)
		net.Start("MK_SyncRanks")
			net.WriteString(util.TableToJSON(MK.ranks.list))
		net.Send(ply)
	end)
end