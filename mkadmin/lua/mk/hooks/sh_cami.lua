// RAWR!

CAMI.MK_TOKEN = "MK"

local function onGroupRegistered(camiGroup, originToken)
	-- Ignore if ULX is the source, or if we receive bad data from another addon
	if originToken == CAMI.MK_TOKEN then return end
	if table.HasValue({"superadmin", "admin", "user"}, camiGroup.Name) then return end
	
	MK.ranks.Add(camiGroup.Name, {
		color = Color(255, 255, 255),
		inherit = camiGroup.Inherits,
	})
end
hook.Add("CAMI.OnUsergroupRegistered", "MK_Administration.CamiGroupRegistered", onGroupRegistered)

local function onGroupRemoved(camiGroup, originToken)
	if originToken == CAMI.MK_TOKEN then return end
	if table.HasValue({"superadmin", "admin", "user"}, camiGroup.Name) then return end

	MK.ranks.Remove(camiGroup.Name)
end
hook.Add("CAMI.OnUsergroupUnregistered", "MK_Administration.CamiGroupRemoved", onGroupRemoved)

local function onPlayerUserGroupChanged(ply, oldGroup, newGroup, originToken)
	if originToken == CAMI.MK_TOKEN then return end

	if newGroup == MK.ranks.defaultRank then
		ply:MK_Rank_RemoveRank(true)
	else
		if not MK.ranks.Get(newGroup) then
			local camiGroup = CAMI.GetUsergroup(usergroupName)
			local inherits = camiGroup and camiGroup.Inherits
			MK.ranks.Add(newGroup, {
				color = Color(255, 255, 255),
				inherit = inherits,
			})
		end
		ply:MK_Rank_SetRank(newGroup, true)
		local rank = MK.ranks.Get(newGroup)
		//MK.administration.Broadcast("Console", " set ", ply, "'s group to ", rank.color, rank.displayName)
	end
end
hook.Add("CAMI.PlayerUsergroupChanged", "MK_Administration.CamiPlayerUserGroupChanged", onPlayerUserGroupChanged)

local function onSteamIDUserGroupChanged(steamId, oldGroup, newGroup, originToken)
	if originToken == CAMI.MK_TOKEN then return end
	
	MK.players.external.GetPlayer(steamId, function(ply)
		onPlayerUserGroupChanged(ply, oldGroup, newGroup, originToken)
	end)
end
hook.Add("CAMI.SteamIDUsergroupChanged", "MK_Administration.CamiSteamIDUserGroupChanged", onSteamIDUserGroupChanged)

local function setPlayerRank(ply, oldRank, newRank)
	if (!IsValid(ply)) then return end
	CAMI.SignalUserGroupChanged(ply, oldRank, newRank, CAMI.MK_TOKEN)
end
hook.Add("MK_Ranks.OnPlayerRemoveRank", "CAMI.MK_Ranks.OnPlayerSetRank", setPlayerRank)

local function removePlayerRank(ply, oldRank)
	if (!IsValid(ply)) then return end
	CAMI.SignalUserGroupChanged(ply, oldRank, MK.ranks.defaultRank, CAMI.MK_TOKEN)
end
hook.Add("MK_Ranks.OnPlayerRemoveRank", "CAMI.MK_Ranks.OnPlayerRemoveRank", removePlayerRank)

local function addUserGroup(rank, data)
	if table.HasValue({"superadmin", "admin", "user"}, rank) then return end
	
	CAMI.RegisterUsergroup({Name=rank, Inherits=(data.inherit or "user")}, CAMI.MK_TOKEN)
end
hook.Add("MK_Ranks.OnRegisterRank", "CAMI.MK_Ranks.OnRegisterRank", addUserGroup)

local function removeUserGroup(rank, data)
	if table.HasValue({"superadmin", "admin", "user"}, rank) then return end
	
	CAMI.UnregisterUsergroup(rank, CAMI.MK_TOKEN)
end
hook.Add("MK_Ranks.OnUnregisterRank", "CAMI.MK_Ranks.OnUnregisterRank", removeUserGroup)

local function onPrivilegeRegistered(camiPriv)
	MK.permissions.Register(camiPriv.Name:lower(), camiPriv.MinAccess, true, true)
end
hook.Add("CAMI.OnPrivilegeRegistered", "MK_Administration.CamiPrivilegeRegistered", onPrivilegeRegistered)

local function onPrivilegeUnregistered(priv)
	if (priv&&isstring(priv)) then
		MK.permissions.Unregister(priv:lower(), true)
	end
end
hook.Add("CAMI.OnPrivilegeUnregistered", "MK_Administration.CamiPrivilegeUnregistered", onPrivilegeUnregistered)

local function playerHasAccess(ply, priv, callback, target, extra)
	local result = false
	if (IsValid(ply)&&ply.MK_Permission_Has) then
		result = ply:MK_Permission_Has(priv:lower(), false)
	end
	if (result&&IsValid(target)&&(!exta||!extra.IgnoreImmunity)) then
		result = MK.ranks.CanTarget(ply:MK_Rank_GetRank(), target:MK_Rank_GetRank())
		if (!result) then
			callback(false, "That target is immune to you.")
		end
	end
	callback(result, "You don't have permission to do that!")
end
hook.Add("CAMI.PlayerHasAccess", "MK_Administration.CamiPlayerHasAccess", playerHasAccess)

local function steamIDHasAccess(steamid, priv, callback, target, extra)
	MK.players.external.GetPlayer(steamid, function(ply)
		playerHasAccess(ply, priv, callback, target, extra)
	end)
end
hook.Add("CAMI.SteamIDHasAccess", "MK_Administration.CamiSteamidHasAccess", steamIDHasAccess)