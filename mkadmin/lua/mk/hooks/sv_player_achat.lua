// RAWR!

hook.Add("MK_Player.Ready", "MK_Player.Ready.AdminChat", function(ply)
	if (ply:MK_Rank_IsDonator(true)) then
		ply:MK_SendChat(Color(100,255,100), "[MK-D] ", Color(255,255,255), "Thanks for donating!")
	end
	if (ply:MK_IsOnWatchlist()) then
		player.BroadcastChat(player.GetByPermission("see_watchlist"), ply, Color(255, 255, 255), " is on the watchlist!")
		ply:MK_SendChat(Color(100,255,100), "[MK-W] ", Color(255,255,255), "You are on the watchlist! Admins have been notified to watch you.")
	end
	if (ply:MK_Permission_Has("see_watchlist")) then
		for k,v in pairs(player.GetAll()) do
			if (v:MK_IsOnWatchlist()) then
				ply:MK_SendChat(Color(100,255,100), "[MK-W] ", v, Color(255,255,255), " is on the watchlist!")
			end
		end
	end
end)