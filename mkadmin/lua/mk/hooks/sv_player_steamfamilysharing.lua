// RAWR!

hook.Add("CheckPassword", "CheckPassword.MK_SteamFamilySharing", function(steam, ipAddress, svPassword, clPassword, name)
	local steamid = util.SteamIDFrom64(steam)
	http.Fetch(
		string.format("http://api.steampowered.com/IPlayerService/IsPlayingSharedGame/v0001/?key=%s&format=json&steamid=%s&appid_playing=4000",
			MK.config.Get("steamKey", ""),
			steam
		),
		function(body)
			local body = util.JSONToTable(body)

			if (not body or not body.response or not body.response.lender_steamid) then
				error(string.format("FamilySharing: Invalid Steam API response for %s | %s\n", name, steamid))
			end

			if (body.response.lender_steamid != "0") then
				local lenderSteam = body.response.lender_steamid
				local lenderSteamID = util.SteamIDFrom64(lenderSteam)
				MK.dev.Log(5, string.format("SteamFamilySharing: %s | %s has been lent Garry's Mod by %s", name, steamid, lenderSteamID))

				MK.players.external.GetPlayer(lenderSteam, function(target)
					local lenders = target:MK_Data_Get("sharedAccounts", {})
					if (!table.HasValue(lenders, steam)) then
						table.insert(lenders, steam)
					end
					target:MK_Data_Set("sharedAccounts", lenders, true)
				end)

				MK.players.external.GetPlayer(steam, function(target)
					local lenders = target:MK_Data_Get("game_lenders", {})
					if (!table.HasValue(lenders, lenderSteam)) then
						table.insert(lenders, lenderSteam)
					end
					target:MK_Data_Set("game_lenders", lenders, true)
				end)

				if (MK.bans.IsBanned(lenderSteamID)) then
					MK.players.external.GetPlayer(steam, function(target)
						MK.administration.Ban(target, MK.bans.list[lenderSteamID].length, "The account that lent you Garry's Mod is banned on this server")
					end)
				else
					MK.ranks.Message("admin", string.format("SteamFamilySharing: %s | %s has been lent Garry's Mod", name, steamid))
				end
			end
		end,
		function(code)
			error(string.format("FamilySharing: Failed API call for %s | %s (Error: %s)\n", name, steamid, code))
		end
	)
end)
if (game.SinglePlayer()) then
	hook.Remove("CheckPassword", "MK_CheckPassword.SteamFamilySharing")
end
