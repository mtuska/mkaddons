// RAWR!

hook.Add("CheckPassword", "CheckPassword.BanSystem", function(steam, ipAddress, svPassword, clPassword, name)
	local steamid = util.SteamIDFrom64(steam)
	if (MK.bans.IsBanned(steamid)) then
		MK.administration.Kick(ply, MK.bans.GetReason(steamid))
	end
end)

hook.Add("PlayerInitialSpawn", "PlayerInitialSpawn.BanSystem", function(ply)
	if (MK.bans.IsBanned(ply:MK_RealSteamID())) then
		MK.administration.Kick(ply, MK.bans.GetReason(ply:MK_RealSteamID()))
	end
end)

hook.Add("MK_Player.Ready", "MK_Player.Ready.BanSync", function(ply)
	net.Start("MK_BanSync")
		net.WriteTable(MK.bans.list)
	net.Send(ply)
end)