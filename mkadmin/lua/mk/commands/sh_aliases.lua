// RAWR!

MK.permissions.Register("see_aliases", "operator")

local function aliases(ply, targets)
	for k,target in pairs(targets) do
		ply:MK_SendChat(Color(100,255,100), "[MK-A] ", target:Name(), "'s aliases are:")
		for k,name in pairs(target:MK_Data_Get("aliases", {})) do
			ply:MK_SendChat(Color(190,0,255), name)
		end
	end
end
local aliasesCommand = MK.commands.Add("utility", "aliases", aliases)
aliasesCommand:AddParam(MK.commands.args.Player, "targets", true, {})
aliasesCommand:DefaultRank("operator")