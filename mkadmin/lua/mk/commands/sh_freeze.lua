// RAWR!

local function freeze(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		if (bool&&target:InVehicle()) then
			target:ExitVehicle()
		end
		target:Freeze(bool)
		table.insert(affectedTargets, target)
	end
	
	MK.administration.Broadcast(ply, {bool, " froze ", " unfroze "}, affectedTargets)
end
local freezeCommand = MK.commands.Add("player", "freeze", freeze)
freezeCommand:AddParam(MK.commands.args.Players, "targets", true, {})
freezeCommand:AddParam(MK.commands.args.Boolean, "bool", false, {default = true})
freezeCommand:DefaultRank("admin")
freezeCommand:SetOpposite("unfreeze", {nil, false})