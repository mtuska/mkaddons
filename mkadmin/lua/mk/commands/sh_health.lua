// RAWR!

local function setHealth(ply, targets, amount)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		target:SetHealth(amount)
		table.insert(affectedTargets, target)
	end
	
	MK.administration.Broadcast(ply, " set health for ", affectedTargets, " to ", amount)
end
local setHealthCommand = MK.commands.Add("player", "health", setHealth, {"hp", "sethealth", "sethp"})
setHealthCommand:AddParam(MK.commands.args.Players, "targets", true, {})
setHealthCommand:AddParam(MK.commands.args.Amount, "amount", false, {default = 100, min = 0})
setHealthCommand:DefaultRank("admin")