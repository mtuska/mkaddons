// RAWR!

local function kick(ply, targets, reason)
	for k,target in pairs(targets) do
		MK.administration.Kick(target, reason, ply)
	end
	MK.administration.Broadcast(ply, " kicked ", targets, "(", reason, ")")
end
local kickCommand = MK.commands.Add("player", "kick", kick)
kickCommand:AddParam(MK.commands.args.Player, "targets", true, {})
kickCommand:AddParam(MK.commands.args.String, "reason", false, {default = "No reason, cause we can"})
kickCommand:DefaultRank("admin")