// RAWR!

function MK.administration.commands.ban(ply, targets, length, reason)
	local properLength = length
	for k,target in pairs(targets) do
		properLength = MK.administration.Ban(target, length, reason, ply)
		MK.administration.Kick(target, reason, ply)
	end
	MK.administration.Broadcast(ply, " banned ", targets, "(", reason, ") ", {(properLength == 0), "permanently", {"for ", MK.util.GetTimeleftByString(properLength)}}, "!")
end
local banCommand = MK.commands.Add("player", "ban", MK.administration.commands.ban)
banCommand:AddParam(MK.commands.args.Player, "target", true, {})
banCommand:AddParam(MK.commands.args.Time, "length", false, {default = 0})
banCommand:AddParam(MK.commands.args.String, "reason", false, {default = "No reason, cause we can"})
banCommand:DefaultRank("admin")

function MK.administration.commands.banId(ply, steam, length, reason)
	MK.players.external.GetPlayer(steam, function(target)
		if (IsValid(target)) then
			MK.administration.Kick(target, reason, ply)
		end
		local properLength = MK.administration.Ban(target, length, reason, ply)
		MK.administration.Broadcast(ply, " banned ", target, "(", reason, ") ", {(properLength == 0), "permanently", {"for ", MK.util.GetTimeleftByString(properLength)}}, "!")
	end)
end
local banIdCommand = MK.commands.Add("player", "banid", MK.administration.commands.banId)
banIdCommand:AddParam(MK.commands.args.String, "target", true, {})
banIdCommand:AddParam(MK.commands.args.Time, "length", false, {default = 0})
banIdCommand:AddParam(MK.commands.args.String, "reason", false, {default = "No reason, cause we can"})
banIdCommand:DefaultRank("admin")

function MK.administration.commands.unbanId(ply, steam, delete)
	MK.players.external.GetPlayer(steam, function(target)
		MK.bans.Unban(target:SteamID64(), delete)
		MK.administration.Broadcast(ply, " unbanned ", target, "("..steam..")")
	end)
end
local unbanCommand = MK.commands.Add("player", "unbanid", MK.administration.commands.unbanId, {"unban"})
unbanCommand:AddParam(MK.commands.args.String, "steamid64", true, {})
unbanCommand:AddParam(MK.commands.args.Boolean, "delete", false, {})
unbanCommand:DefaultRank("admin")

function MK.administration.commands.globalBan(ply, targets, length, reason)
	local properLength = length
	for k,target in pairs(targets) do
		properLength = MK.administration.Ban(target, length, reason, ply, true)
		MK.administration.Kick(target, reason, ply)
	end
	MK.administration.Broadcast(ply, " globally banned ", targets, "(", reason, ") ", {(properLength == 0), "permanently", {"for ", MK.util.GetTimeleftByString(properLength)}}, "!")
end
local globalBanCommand = MK.commands.Add("player", "globalban", MK.administration.commands.globalBan)
globalBanCommand:AddParam(MK.commands.args.Player, "target", true, {})
globalBanCommand:AddParam(MK.commands.args.Time, "length", false, {default = 0})
globalBanCommand:AddParam(MK.commands.args.String, "reason", false, {default = "No reason, cause we can"})
globalBanCommand:DefaultRank("superadmin")

function MK.administration.commands.globalBanId(ply, steam, length, reason)
	MK.players.external.GetPlayer(steam, function(target)
		if (IsValid(target)) then
			MK.administration.Kick(target, reason, ply)
		end
		local properLength = MK.administration.Ban(target, length, reason, ply, true)
		MK.administration.Broadcast(ply, " globally banned ", target, "(", reason, ") ", {(properLength == 0), "permanently", {"for ", MK.util.GetTimeleftByString(properLength)}}, "!")
	end)
end
local globalBanIdCommand = MK.commands.Add("player", "globalbanid", MK.administration.commands.globalBanId)
globalBanIdCommand:AddParam(MK.commands.args.String, "target", true, {})
globalBanIdCommand:AddParam(MK.commands.args.Time, "length", false, {default = 0})
globalBanIdCommand:AddParam(MK.commands.args.String, "reason", false, {default = "No reason, cause we can"})
globalBanIdCommand:DefaultRank("superadmin")