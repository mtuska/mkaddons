// RAWR!
/*
MK.commands.Add("setrank", {
	syntax = "[string target] [string rank]",
	canRun = function(ply)
		return ply:IsSuperAdmin()
	end,
	onRun = function(ply, args)
		local target = MK.util.findPlayer(args[1])
		if (IsValid(target)&&args[2]) then
			target:MK_Rank_SetRank(args[2])
		end
	end
})

MK.commands.Add("removerank", {
	syntax = "[string target]",
	canRun = function(ply)
		return ply:IsSuperAdmin()
	end,
	onRun = function(ply, args)
		local target = MK.util.findPlayer(args[1])
		if (IsValid(target)) then
			target:MK_Rank_RemoveRank()
		end
	end
})
*/
local function setRank(ply, targets, group)
	for k,target in pairs(targets) do
		target:MK_Rank_SetRank(group)
	end
	
	local rank = MK.ranks.Get(group)
	MK.administration.Broadcast(ply, " set ", targets, "'s group to ", rank.color, rank.displayName)
end
local setRankCommand = MK.commands.Add("administration", "setrank", setRank)
setRankCommand:AddParam(MK.commands.args.Player, "player", true, {})
setRankCommand:AddParam(MK.commands.args.Rank, "rank", true, {})
setRankCommand:DefaultRank("superadmin")
setRankCommand:SetHelp("user")

local function removeRank(ply, targets)
	for k,target in pairs(targets) do
		target:MK_Rank_RemoveRank()
	end
	MK.administration.Broadcast(ply, " removed rank from ", targets)
end
local removeRankCommand = MK.commands.Add("administration", "removerank", removeRank)
removeRankCommand:AddParam(MK.commands.args.Player, "player", true, {})
removeRankCommand:DefaultRank("superadmin")
removeRankCommand:SetHelp("user")

local function addUserId(ply, steam, group)
	MK.players.external.GetPlayer(steam, function(target)
		target:MK_Rank_SetRank(group)
		local rank = MK.ranks.Get(group)
		MK.administration.Broadcast(ply, " set ", target, "'s group to ", rank.color, rank.displayName)
	end)
end
local setRankCommand = MK.commands.Add("administration", "setrankid", addUserId)
setRankCommand:AddParam(MK.commands.args.String, "player", true, {})
setRankCommand:AddParam(MK.commands.args.Rank, "rank", true, {})
setRankCommand:DefaultRank("superadmin")
setRankCommand:SetHelp("user")

local function removeUserId(ply, steam)
	MK.players.external.GetPlayer(steam, function(target)
		target:MK_Rank_RemoveRank()
		MK.administration.Broadcast(ply, " removed rank from ", target)
	end)
end
local setRankCommand = MK.commands.Add("administration", "removerankid", removeUserId)
setRankCommand:AddParam(MK.commands.args.String, "player", true, {})
setRankCommand:DefaultRank("superadmin")
setRankCommand:SetHelp("user")