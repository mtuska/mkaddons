// RAWR!

local function cloak(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		target:SetNoDraw(bool)
		table.insert(affectedTargets, target)
	end
	
	MK.administration.Broadcast(ply, {bool, " cloaked ", " uncloaked "}, affectedTargets)
end
local cloakCommand = MK.commands.Add("player", "cloak", cloak)
cloakCommand:AddParam(MK.commands.args.Players, "targets", true, {})
cloakCommand:AddParam(MK.commands.args.Boolean, "bool", false, {default = true})
cloakCommand:DefaultRank("admin")
cloakCommand:SetOpposite("uncloak", {nil, false})