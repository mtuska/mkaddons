// RAWR!

MK.permissions.Register("see_watchlist", "operator")

local function watchlist(ply, targets)
	if (ply:MK_Permission_Has("see_watchlist")) then
		if (!targets) then
			local failsafe = true
			for k,v in pairs(player.GetAll()) do
				if (v:MK_IsOnWatchlist()) then
					failsafe = false
					ply:MK_SendChat(Color(100,255,100), "[MK-W] ", v, Color(255,255,255), " is on the watchlist!")
				end
			end
			if (failsafe) then
				ply:MK_SendChat(Color(100,255,100), "[MK-W] ", "No watchlist players are online!")
			end
		else
			for k,v in pairs(targets) do
				if (v:MK_IsOnWatchlist()) then
					ply:MK_SendChat(Color(100,255,100), "[MK-W] ", v, Color(255,255,255), " is on the watchlist!")
				else
					ply:MK_SendChat(Color(100,255,100), "[MK-W] ", v, Color(255,255,255), " is NOT on the watchlist!")
				end
			end
		end
	else
		ply:MK_SendChat(Color(100,255,100), "[MK-W] ", Color(255,255,255), "You can't view the watchlist!")
	end
end
local watchlistCommand = MK.commands.Add("watchlist", "watchlist", watchlist, {"viewwatchlist"})
watchlistCommand:AddParam(MK.commands.args.Player, "target", false, {})
watchlistCommand:DefaultRank("operator")

local function addwatchlist(ply, targets)
	for k,target in pairs(targets) do
		target:MK_Data_Set("watchlisted", true)
	end
	MK.administration.Broadcast(ply, " watchlisted ", targets, "!")
end
local addwatchlistCommand = MK.commands.Add("watchlist", "addwatchlist", addwatchlist, {"addwatch"})
addwatchlistCommand:AddParam(MK.commands.args.Player, "target", true, {})
addwatchlistCommand:DefaultRank("operator")

local function addwatchlistId(ply, steam)
	MK.players.external.GetPlayer(steam, function(target)
		target:MK_Data_Set("watchlisted", true)
		MK.administration.Broadcast(ply, " watchlisted ", target, "!")
	end)
end
local addwatchlistIdCommand = MK.commands.Add("watchlist", "addwatchlistid", addwatchlistId, {"addwatchid"})
addwatchlistIdCommand:AddParam(MK.commands.args.String, "target", true, {})
addwatchlistIdCommand:DefaultRank("operator")

local function unwatchlist(ply, targets)
	for k,target in pairs(targets) do
		target:MK_Data_Set("watchlisted", false)
	end
	MK.administration.Broadcast(ply, " unwatchlisted ", targets, "!")
end
local unwatchlistCommand = MK.commands.Add("watchlist", "unwatchlist", unwatchlist, {"unwatch"})
unwatchlistCommand:AddParam(MK.commands.args.Player, "target", true, {})
unwatchlistCommand:DefaultRank("admin")

local function unwatchlistId(ply, steam)
	MK.players.external.GetPlayer(steam, function(target)
		target:MK_Data_Set("watchlisted", false)
		MK.administration.Broadcast(ply, " unwatchlisted ", Color(150, 0, 255), target, "!")
	end)
end
local unwatchlistIdCommand = MK.commands.Add("watchlist", "unwatchlistid", unwatchlistId, {"unwatchid"})
unwatchlistIdCommand:AddParam(MK.commands.args.String, "target", true, {})
unwatchlistIdCommand:DefaultRank("admin")