// RAWR!

local function mute(ply, targets, bool, perm)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		table.insert(affectedTargets, target)
		if (!bool||perm) then
			target:MK_GameData_Set("permamuted", bool)
		end
		target:MK_GameData_Set("muted", bool)
	end
	
	MK.administration.Broadcast(ply, {perm, " permenantly", ""}, {bool, " muted ", " unmuted "}, affectedTargets)
end
local muteCommand = MK.commands.Add("chat", "mute", mute)
muteCommand:AddParam(MK.commands.args.Players, "targets", true, {})
muteCommand:AddParam(MK.commands.args.Boolean, "bool", false, {default = true})
muteCommand:AddParam(MK.commands.args.Boolean, "perm", false, {default = false})
muteCommand:DefaultRank("admin")
muteCommand:SetOpposite("unmute", {nil, false})

if (SERVER) then
	hook.Add("PlayerSay", "MK_Administration.PlayerSay", function(ply, text)
		if (ply:MK_GameData_Get("muted", false)||ply:MK_GameData_Get("permamuted", false)) then
			ply:MK_SendChat(Color(255,255,255), "You've been muted, sorry! No chat right now.")
			return ""
		end
	end)
end