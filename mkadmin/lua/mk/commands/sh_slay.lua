// RAWR!

local function slay(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		if (not target:Alive()) then
			ply:ChatPrint(target:Name().." is already dead!")
		elseif (target:IsFrozen()) then
			ply:ChatPrint(target:Name().." is frozen!")
		else
			target:Kill()
			table.insert(affectedTargets, target)
		end
	end
	
	MK.administration.Broadcast(ply, " has slain ", affectedTargets)
end
local slayCommand = MK.commands.Add("player", "slay", slay)
slayCommand:AddParam(MK.commands.args.Players, "targets", true, {})
slayCommand:DefaultRank("admin")

local function sslay(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		if (not target:Alive()) then
			ply:ChatPrint(target:Name().." is already dead!")
		elseif (target:IsFrozen()) then
			ply:ChatPrint(target:Name().." is frozen!")
		else
			if (target:InVehicle()) then
				target:ExitVehicle()
			end
			target:KillSilent()
			table.insert(affectedTargets, target)
		end
	end
	
	MK.administration.Broadcast(ply, " has silently slain ", affectedTargets)
end
local sslayCommand = MK.commands.Add("player", "sslay", sslay)
sslayCommand:AddParam(MK.commands.args.Players, "targets", true, {})
sslayCommand:DefaultRank("admin")