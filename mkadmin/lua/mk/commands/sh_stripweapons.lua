// RAWR!

local function strip(ply, targets)
	local affectedplys = {}
	for k,target in pairs(targets) do
		target:StripWeapons()
		table.insert(affectedplys, target)
	end
	
	MK.administration.Broadcast(ply, " stripped weapons from ", affectedplys)
end
local stripCommand = MK.commands.Add("player", "strip", strip, {"stripweapons"})
stripCommand:AddParam(MK.commands.args.Players, "targets", true, {})
stripCommand:DefaultRank("admin")