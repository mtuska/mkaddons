// RAWR!

// Thanks ULX team, make my life easier this way... (I'll only modify a little)
local function playerSend( from, to, force )
	if not to:IsInWorld() and not force then return false end -- No way we can do this one

	local yawForward = to:EyeAngles().yaw
	local directions = { -- Directions to try
		math.NormalizeAngle( yawForward - 180 ), -- Behind first
		math.NormalizeAngle( yawForward + 90 ), -- Right
		math.NormalizeAngle( yawForward - 90 ), -- Left
		yawForward,
	}

	local t = {}
	t.start = to:GetPos() + Vector( 0, 0, 32 ) -- Move them up a bit so they can travel across the ground
	t.filter = { to, from }

	local i = 1
	t.endpos = to:GetPos() + Angle( 0, directions[ i ], 0 ):Forward() * 47 -- (33 is player width, this is sqrt( 33^2 * 2 ))
	local tr = util.TraceEntity( t, from )
	while tr.Hit do -- While it's hitting something, check other angles
		i = i + 1
		if i > #directions then	 -- No place found
			if force then
				from._prevpos = from:GetPos()
				from._prevang = from:EyeAngles()
				return to:GetPos() + Angle( 0, directions[ 1 ], 0 ):Forward() * 47
			else
				return false
			end
		end

		t.endpos = to:GetPos() + Angle( 0, directions[ i ], 0 ):Forward() * 47

		tr = util.TraceEntity( t, from )
	end

	from._prevpos = from:GetPos()
	from._prevang = from:EyeAngles()
	return tr.HitPos
end

local function returntp(ply, targets, force)
	local target = targets[1]
	
	if (!target._prevpos) then
		ply:MK_SendChat(Color(100,255,100), "[MK-A] ", Color(255,255,255), "Target has no previous location!")
		return
	end
	target.ulx_prevpos = nil // Fuck yourself ulx
	if (target:InVehicle()) then
		target:ExitVehicle()
	end
	
	target:SetPos(target._prevpos)
	target:SetEyeAngles(target._prevang)
	target:SetLocalVelocity(Vector(0, 0, 0)) // Stop!
	target._prevpos = nil
	
	MK.administration.Broadcast(ply, " returned ", targets)
end
local returnCommand = MK.commands.Add("teleportation", "return", returntp)
returnCommand:AddParam(MK.commands.args.Player, "targets", true, {})
returnCommand:AddParam(MK.commands.args.Boolean, "force", false, {default = false})
returnCommand:DefaultRank("admin")

local function bring(ply, targets, force)
	local newpos, newang = false, Angle()
	local target = targets[1]
	newpos = playerSend(target, ply, force)
	if (!newpos) then
		ply:MK_SendChat(Color(100,255,100), "[MK-A] ", Color(255,255,255), "Can't find a place to put the target!")
		return
	end
	newang = (target:GetPos() - newpos):Angle()
	
	if (target:InVehicle()) then
		target:ExitVehicle()
	end

	target:SetPos(newpos)
	target:SetEyeAngles(newang)
	target:SetLocalVelocity(Vector(0, 0, 0)) -- Stop!
	
	MK.administration.Broadcast(ply, " brought ", targets)
end
local bringCommand = MK.commands.Add("teleportation", "bring", bring)
bringCommand:AddParam(MK.commands.args.Player, "targets", true, {})
bringCommand:AddParam(MK.commands.args.Boolean, "force", false, {default = false})
bringCommand:DefaultRank("admin")

local function goto(ply, targets, force)
	local newpos, newang = false, Angle()
	local target = targets[1]
	newpos = playerSend(ply, target, force)
	if (!newpos) then
		ply:MK_SendChat(Color(100,255,100), "[MK-A] ", Color(255,255,255), "Can't find a place to put you!")
		return
	end
	newang = (target:GetPos() - newpos):Angle()
	
	if (ply:InVehicle()) then
		ply:ExitVehicle()
	end

	ply:SetPos(newpos)
	ply:SetEyeAngles(newang)
	ply:SetLocalVelocity(Vector(0, 0, 0)) -- Stop!
	
	MK.administration.Broadcast(ply, " teleported to ", targets)
end
local gotoCommand = MK.commands.Add("teleportation", "goto", goto)
gotoCommand:AddParam(MK.commands.args.Player, "targets", true, {canTargetHigherRank = true})
gotoCommand:AddParam(MK.commands.args.Boolean, "force", false, {default = false})
gotoCommand:DefaultRank("admin")