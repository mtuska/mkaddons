// RAWR!

if (MK.ps) then
MK.ps.raffle = MK.ps.raffle or {}

local function raffle(ply, item_id)
	MK.administration.Broadcast(ply, " has started a raffle! Type !enterraffle to enter!")
	MK.ps.raffle.entries = {}
	MK.ps.raffle.item_id = item_id
	timer.Simple(60, function()
		local winner = table.Random(MK.ps.raffle.entries)
		MK.administration.Broadcast(winner, " has won the raffle!")
		PS.Drops.GiveItem(winner, PS.Items[MK.ps.raffle.item_id])
		MK.ps.raffle = {}
	end)
end
local raffleCommand = MK.commands.Add("pointshop", "raffle", raffle)
raffleCommand:AddParam(MK.commands.args.String, "item id", true, {})
raffleCommand:DefaultRank("superadmin")

local function enterraffle(ply)
	if (!MK.ps.raffle.entries) then
		ply:MK_SendChat(Color(255,255,255), "No raffle is going on!")
		return
	elseif (table.HasValue(MK.ps.raffle.entries, ply)) then
		ply:MK_SendChat(Color(255,255,255), "You already entered this raffle!")
		return
	end
	table.insert(MK.ps.raffle.entries, ply)
	MK.administration.Broadcast(ply, " has entered the raffle!")
end
local enterraffleCommand = MK.commands.Add("pointshop", "enterraffle", enterraffle)
enterraffleCommand:DefaultRank("user")
end