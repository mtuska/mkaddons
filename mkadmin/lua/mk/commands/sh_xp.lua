// RAWR!
/*
MK.commands.Add("setxp", {
	syntax = "[string target] [number maximum] [boolean gamemodeOnly] [string uniqueIdentifier]",
	canRun = function(ply)
		return ply:IsSuperAdmin()
	end,
	onRun = function(ply, args)
		local target = MK.util.findPlayer(args[1])
		if (IsValid(target)) then
			target:MK_XP_Set(tonumber(args[2]), tobool(args[3]), args[4])
		end
	end
})

MK.commands.Add("givexp", {
	syntax = "[string target] [number maximum] [boolean gamemodeOnly] [string uniqueIdentifier]",
	canRun = function(ply)
		return ply:IsSuperAdmin()
	end,
	onRun = function(ply, args)
		local target = MK.util.findPlayer(args[1])
		if (IsValid(target)) then
			target:MK_XP_Give(tonumber(args[2]), tobool(args[3]), args[4])
		end
	end
})

MK.commands.Add("takexp", {
	syntax = "[string target] [number maximum] [boolean gamemodeOnly] [string uniqueIdentifier]",
	canRun = function(ply)
		return ply:IsSuperAdmin()
	end,
	onRun = function(ply, args)
		local target = MK.util.findPlayer(args[1])
		if (IsValid(target)) then
			target:MK_XP_Take(tonumber(args[2]), tobool(args[3]), args[4])
		end
	end
})
*/

local function setXP(ply, targets, amount, bool, uid)
	if (!MK.config.Get("experience_enabled")) then
		ply:MK_SendChat(Color(100,255,100), "[MK-A] ", Color(255,255,255), "Experience Module is not enabled!")
		return
	end
	local affectedplys = {}
	for k,target in pairs(targets) do
		target:MK_XP_Set(amount, bool, uid)
		table.insert(affectedplys, target)
	end
	
	local text = " has set "
	if (bool) then
		text = text.."gamemode "
	end
	if (uid) then
		text = text.."("..uid..") "
	end
	text = text.."XP for "
	
	MK.administration.Broadcast(ply, text, affectedplys, " to ", amount, "!")
end
local setXPCommand = MK.commands.Add("player", "setxp", setXP)
setXPCommand:AddParam(MK.commands.args.Players, "targets", true, {})
setXPCommand:AddParam(MK.commands.args.Amount, "amount", false, {default = 0, min = 0})
setXPCommand:AddParam(MK.commands.args.Boolean, "bool", false, {default = true})
setXPCommand:AddParam(MK.commands.args.uid, "uniqueIdentifier", false, {})
setXPCommand:DefaultRank("admin")