// RAWR!

local function rtd(ply)
	if (!IsValid(ply)) then MK.util.Console(ply, "Can't use RTD as console!") end
	MK.rtd.Run(ply)
end
local rtdCommand = MK.commands.Add("fun", "rtd", rtd)
rtdCommand:DefaultRank("user")

local function rtd(ply, str)
	if (!IsValid(ply)) then MK.util.Console(ply, "Can't use RTD as console!") end
	MK.rtd.Run(ply, str)
end
local rtdCommand = MK.commands.Add("fun", "forcertd", rtd)
rtdCommand:AddParam(MK.commands.args.String, "string", true, {})
rtdCommand:DefaultRank("user")