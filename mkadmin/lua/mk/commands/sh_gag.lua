// RAWR!

local function gag(ply, targets, bool, perm)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		table.insert(affectedTargets, target)
		if (!bool||perm) then
			target:MK_GameData_Set("permagagged", bool)
		end
		target:MK_GameData_Set("gagged", bool)
	end
	
	MK.administration.Broadcast(ply, {perm, " permenantly", ""}, {bool, " gagged ", " ungagged "}, affectedTargets)
end
local gagCommand = MK.commands.Add("chat", "gag", gag)
gagCommand:AddParam(MK.commands.args.Players, "targets", true, {})
gagCommand:AddParam(MK.commands.args.Boolean, "bool", false, {default = true})
gagCommand:AddParam(MK.commands.args.Boolean, "perm", false, {default = false})
gagCommand:DefaultRank("admin")
gagCommand:SetOpposite("ungag", {nil, false})

if (SERVER) then
	hook.Add("PlayerCanHearPlayersVoice", "MK_Administration.PlayerCanHearPlayersVoice", function(listener, talker)
		if (talker:MK_GameData_Get("gagged", false)||talker:MK_GameData_Get("permagagged", false)) then return false end
	end)
else
	hook.Add("PlayerStartVoice", "MK_Administration.PlayerStartVoice", function(ply)
		if (ply == LocalPlayer()&&(ply:MK_GameData_Get("gagged", false)||ply:MK_GameData_Get("permagagged", false))) then
			ply:MK_SendChat(Color(255,255,255), "You've been gagged, sorry! No mic right now.")
		end
	end)
end