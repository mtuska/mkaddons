// RAWR!

local function devmode(ply, targets, bool, suppression)
	for k,v in pairs(targets) do
		v:MK_GameData_Set("devmode", bool)
	end

	if (!suppression) then
		MK.administration.Broadcast(ply, {bool, " enabled ", " disabled "}, "dev mode for ", targets)
	end
end
local devmodeCommand = MK.commands.Add("debug", "devmode", devmode)
devmodeCommand:AddParam(MK.commands.args.Players, "targets", true, {})
devmodeCommand:AddParam(MK.commands.args.Boolean, "boolean", false, {default = true})
devmodeCommand:AddParam(MK.commands.args.Boolean, "suppress", false, {default = false})
devmodeCommand:DefaultRank("dev")

local function debugc(ply, targets)
	if (!targets) then
		targets = {ply}
	end
	for k,v in pairs(targets) do
		ply:MK_SendChat(v, ": ", util.TableToJSON(v:MK_GameData_Get()))
	end
end
local debugCommand = MK.commands.Add("debug", "debug", debugc)
debugCommand:AddParam(MK.commands.args.Players, "targets", false, {})
debugCommand:DefaultRank("superadmin")

local function version(ply)
	if (IsValid(ply)) then
		ply:MK_DisplayVersions()
	else
		MK.DisplayVersions()
		/*MsgC(Color(100,255,100), "[MK] ", Color(255,255,255), "MKAddon version "..MK.version.." loaded!\n")
		MsgC(Color(100,255,100), "[MK-B] ", Color(255,255,255), "MK Bans version "..MK.bans.version.." loaded!")
		MsgC(Color(100,255,100), "[MK-W] ", Color(255,255,255), "MK Watchlist version "..MK.watchlist.version.." loaded!\n")
		if (MK.config.Get("administration_enabled", false)) then
			MsgC(Color(100,255,100), "[MK-A] ", Color(255,255,255), "MK Administration version "..MK.administration.version.." is enabled!\n")
		end*/
	end
end
local versionCommand = MK.commands.Add("debug", "version", version)
versionCommand:DefaultRank("superadmin")

local function ranks(ply)
	for k,v in pairs(MK.ranks.list) do
		ply:PrintMessage(HUD_PRINTCONSOLE, k)
	end
end
local ranksCommand = MK.commands.Add("debug", "ranks", ranks)
ranksCommand:DefaultRank("superadmin")

local function uniqueid(ply, targets)
	for k,v in pairs(targets) do
		MK.util.console(ply, v:UniqueID())
	end
end
local uniqueidCommand = MK.commands.Add("debug", "uniqueid", uniqueid)
uniqueidCommand:AddParam(MK.commands.args.Players, "targets", false, {})
uniqueidCommand:DefaultRank("superadmin")

concommand.Remove("mktest")
concommand.Add("mktest", function(ply, concmd, args, argStr)
	ply:ChatPrint("mktest command")
end, function(concmd, stringargs)
	local args = MK.util.ExtractArgs(stringargs)
	local possibilities = {}

	local commands = table.Copy(MK.commands.list)
	for cmd,cmdTbl in pairs(commands) do
		if (table.Count(args) >= 1) then
			if (!args[1]:lower():find(cmd:lower())) then // Find what commands we could be attempting to use
				commands[cmd] = nil
			end
		end
	end

	// We know the possible commands, if there is only one, we continue to the arguments
	if (table.Count(commands) == 1) then
		for cmd,cmdTbl in pairs(commands) do
			local text = " "..cmd
			for k,arg in pairs(cmdTbl.args) do
				text = text.." \""..arg[1].."\""
			end
			table.insert(possibilities, concmd..text)
		end
	// We have multiple possiblities of commands, just list them
	else
		for cmd,cmdTbl in pairs(commands) do
			local text = " "..cmd
			table.insert(possibilities, concmd..text)
		end
	end

	return possibilities
end)
