// RAWR!

local function who(ply, targets)
	local format = "%s%s %s%s "
	local text = string.format(format, "Name", string.rep(" ", 20 - 4), "SteamID", string.rep(" ", 20 - 7))
	text = text.."Rank"
	MK.util.console(ply, text)
	if (!targets) then
		targets = player.GetAll()
	end
	for k,v in pairs(targets) do
		local steamid = tostring(v:SteamID())
		local nick = v:Nick()
		local text = string.format(format, nick, string.rep(" ", 20 - nick:len()), steamid, string.rep(" ", 20 - steamid:len()))
		text = text..v:GetUserGroup()
		MK.util.console(ply, text)
	end
end
local whoCommand = MK.commands.Add("utility", "who", who)
whoCommand:AddParam(MK.commands.args.Players, "target", false, {})
whoCommand:DefaultRank("user")