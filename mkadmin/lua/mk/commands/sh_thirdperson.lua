// RAWR!

local function thirdperson(ply)
	if (ply:MK_GameData_Get("thirdperson_enabled", false)) then
		ply:MK_GameData_Set("thirdperson_enabled", false)
		net.Start("ots_off")
		net.Send(ply)
	else
		ply:MK_GameData_Set("thirdperson_enabled", true)
		net.Start("ots_on")
		net.Send(ply)
	end
end
local thirdpersonCommand = MK.commands.Add("player", "thirdperson", thirdperson, {"tp"})
thirdpersonCommand:DefaultRank("user")

local function firstperson(ply)
	if (ply:MK_GameData_Get("thirdperson_enabled", false)) then
		ply:MK_GameData_Set("thirdperson_enabled", false)
		net.Start("ots_off")
		net.Send(ply)
	end
end
local firstpersonCommand = MK.commands.Add("player", "firstperson", firstperson, {"fp"})
firstpersonCommand:DefaultRank("user")

local function thirdpersonflip(ply)
	if (ply:MK_GameData_Get("thirdperson_view", 0) == 1) then
		ply:MK_GameData_Set("thirdperson_view", 2)
	elseif (ply:MK_GameData_Get("thirdperson_view", 0) == 2) then
		ply:MK_GameData_Set("thirdperson_view", 1)
	end
end
local thirdpersonflipCommand = MK.commands.Add("player", "thirdpersonflip", thirdpersonflip, {"tpflip"})
thirdpersonflipCommand:DefaultRank("user")

local function thirdpersonview(ply, view)
	ply:MK_GameData_Set("thirdperson_view", view)
end
local thirdpersonflipCommand = MK.commands.Add("player", "thirdpersonview", thirdpersonview, {"tpview"})
thirdpersonflipCommand:AddParam(MK.commands.args.Amount, "view", false, {default = 0, min = 0, max = 2})
thirdpersonflipCommand:DefaultRank("user")