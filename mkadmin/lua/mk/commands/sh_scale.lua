// RAWR!

local function scale(ply, targets, amount, enum)
	for k,target in pairs(targets) do
		target:MK_Scale_Set(amount/100)
		if (enum > 0) then
			if (amount == 100) then
				target:MK_GameData_Set("scale", nil, false, nil, nil, (enum == 2))
			else
				target:MK_GameData_Set("scale", amount/100, false, nil, nil, (enum == 2))
			end
		end
	end
	MK.administration.Broadcast(ply, " scaled ", targets, " to ", (amount), "% (", enum, ")")
end
local scaleCommand = MK.commands.Add("fun", "scale", scale)
scaleCommand:AddParam(MK.commands.args.Players, "targets", true, {})
scaleCommand:AddParam(MK.commands.args.Amount, "amount", false, {default = 1})
scaleCommand:AddParam(MK.commands.args.Amount, "enum", false, {default = 0})
scaleCommand:DefaultRank("superadmin")

local function SetPermScale(ply)
	ply:MK_Scale_Set(ply:MK_GameData_Get("scale", 1))
end

hook.Add("PlayerSpawn", "PlayerSpawn.SetPermScale", SetPermScale)
hook.Add("MK_Player.LoadedData", "MK_Player.LoadedData.SetPermScale", SetPermScale)