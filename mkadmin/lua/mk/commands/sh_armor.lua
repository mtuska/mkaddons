// RAWR!

local function setArmor(ply, targets, amount)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		target:SetArmor(amount)
		table.insert(affectedTargets, target)
	end
	
	MK.administration.Broadcast(ply, " set armour for ", affectedTargets, " to ", amount)
end
local setArmorCommand = MK.commands.Add("player", "armor", setArmor, {"setarmor", "setarmour"})
setArmorCommand:AddParam(MK.commands.args.Players, "targets", true, {})
setArmorCommand:AddParam(MK.commands.args.Amount, "amount", false, {default = 100, min = 0, max = 100})
setArmorCommand:DefaultRank("admin")