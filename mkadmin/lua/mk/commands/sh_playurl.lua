// RAWR!

local function spamplayurl(ply, url, targets)
	targets = targets or player.GetAll()
	for k,v in pairs(targets) do
		v:MK_GameData_Set("spamurl", url)
	end
	MK.sound.PlayURL(url, targets)
end
local spamplayurlCommand = MK.commands.Add("fun", "spamplayurl", spamplayurl)
spamplayurlCommand:AddParam(MK.commands.args.String, "file", true, {})
spamplayurlCommand:AddParam(MK.commands.args.Player, "targets", false, {})
spamplayurlCommand:DefaultRank("superadmin")

if (CLIENT&&!timer.Exists("MK_SpamPlayURL")) then
	timer.Create("MK_SpamPlayURL", 5, 0, function()
		if (LocalPlayer().MK_GameData_Get) then
			local url = LocalPlayer():MK_GameData_Get("spamurl", false)
			if (url) then
				MK.sound.PlayURL(url)
			end
		end
	end)
end

local function playurl(ply, url, targets)
	targets = targets or player.GetAll()
	MK.sound.PlayURL(url, targets)
end
local playurlCommand = MK.commands.Add("fun", "playurl", playurl)
playurlCommand:AddParam(MK.commands.args.String, "file", true, {})
playurlCommand:AddParam(MK.commands.args.Players, "targets", false, {})
playurlCommand:DefaultRank("superadmin")

local function stopplayurl(ply)
	MK.sound.StopSound(ply)
end
local stopplayurlCommand = MK.commands.Add("fun", "stopplayurl", stopplayurl)
stopplayurlCommand:DefaultRank("user")

local function allstopplayurl(ply)
	for k,v in pairs(player.GetAll()) do
		v:MK_GameData_Set("spamurl", false)
	end
	MK.sound.StopSound(player.GetAll())
end
local stopplayurlCommand = MK.commands.Add("fun", "allstopplayurl", allstopplayurl)
stopplayurlCommand:DefaultRank("superadmin")

local function stopsounds(ply)
	MK.sound.StopSound(ply)
end
local stopsoundsCommand = MK.commands.Add("fun", "stopsounds", stopsounds)
stopsoundsCommand:DefaultRank("superadmin")