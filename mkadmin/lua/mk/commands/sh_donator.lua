// RAWR!

local function donate(ply)
	if (Prometheus) then
		ply:ConCommand("Prometheus")
		return
	end
	ply:MK_SendChat(Color(100,255,100), "[MK-D] ", Color(255,255,255), "Currently not accepting donations, sorry! Check back tomorrow.")
end
local donateCommand = MK.commands.Add("donations", "donate", donate)
donateCommand:DefaultRank("user")

local function setdonator(ply, targets, days, global)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		target:MK_Rank_SetDonator(days, global)
		table.insert(affectedTargets, target)
	end
	
	MK.administration.Broadcast(ply, " gave donator to ", affectedTargets, " ", {global, "globally", "locally"}, " for ", days, " days!")
end
local setdonatorCommand = MK.commands.Add("donations", "setdonator", setdonator)
setdonatorCommand:AddParam(MK.commands.args.Player, "target", true, {})
setdonatorCommand:AddParam(MK.commands.args.Amount, "length", false, {default = 0})
setdonatorCommand:AddParam(MK.commands.args.Boolean, "global", false, {default = false})
setdonatorCommand:DefaultRank("dev")

local function setdonatorid(ply, steam, days, global)
	MK.players.external.GetPlayer(steam, function(target)
		target:MK_Rank_SetDonator(days, global)
		MK.administration.Broadcast(ply, " gave donator to ", target, " ", {global, "globally", "locally"}, " for ", days, " days!")
	end)
end
local setdonatoridCommand = MK.commands.Add("donations", "setdonatorid", setdonatorid)
setdonatoridCommand:AddParam(MK.commands.args.String, "target", true, {})
setdonatoridCommand:AddParam(MK.commands.args.Amount, "length", false, {default = 0})
setdonatoridCommand:AddParam(MK.commands.args.Boolean, "global", false, {default = false})
setdonatoridCommand:DefaultRank("dev")

local function isdonator(ply, targets, days, global)
	local affectedTargets = {}
	ply:MK_SendChat(Color(100,255,100), "[MK-D] ", Color(255,255,255), "Displaying users' donation status.")
	for k,target in pairs(targets) do
		local status = target:MK_Rank_GetDonator()
		if (isnumber(status)) then
			ply:MK_SendChat(Color(100,255,100), "[MK-D] ", target, Color(255,255,255), ": ", Color(255, 255, 255), MK.util.GetTimeleftByString(status))
		else
			ply:MK_SendChat(Color(100,255,100), "[MK-D] ", target, Color(255,255,255), ": ", Color(255, 255, 255), tostring(status))
		end
		table.insert(affectedTargets, target)
	end
	
	//MK.administration.Broadcast(ply, {bool, " froze ", " unfroze "}, affectedTargets)
end
local isdonatorCommand = MK.commands.Add("donations", "isdonator", isdonator)
isdonatorCommand:AddParam(MK.commands.args.Players, "targets", true, {})
isdonatorCommand:DefaultRank("user")