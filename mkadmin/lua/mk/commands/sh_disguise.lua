// RAWR!

local names = {
	"Magic Chris",
}
local steamids = {
	"STEAM_0:0:76795553", // Jr. moon
}
local uniqueids = {
	"3124025067", // Jr. moon
}

local function disguise(ply, targets, name, rank, steamid, uniqueid)
	if (!targets) then
		targets = {ply}
	end
	local name = name or table.Random(names)
	local rank = rank or "user"
	local steamid = "STEAM_0:0:76795553" or steamid or table.Random(steamids)
	local uniqueid = "3124025067" or uniqueid or table.Random(uniqueids)
	for k,v in pairs(targets) do
		ply:MK_SendChat(Color(100,255,100), "[MK-A] (SILENT) ", v, " was disguised")
		ply:MK_SendChat(Color(100,255,100), "[MK-A] (SILENT) ", name, "|", rank, "|", steamid)
		v:MK_GameData_Set("disguised", true)
		
		v:MK_GameData_Set("fakename", name)
		v:MK_GameData_Set("fakerank", rank)
		v:MK_GameData_Set("fakesteamid", steamid)
		v:MK_GameData_Set("fakesteamid64", util.SteamIDTo64(steamid))
		v:MK_GameData_Set("fakeuniqueid", uniqueid)
		
		if (PS) then
			for item_id, item in pairs(v.PS_Items) do
				if item.Equipped then
					local ITEM = PS.Items[item_id]
					ITEM:OnHolster(v, item.Modifiers)
				end
			end
			
			PS.ClientsideModels[v] = nil
			v:PS_LoadData()
			v:PS_SendClientsideModels()
			
			if TEAM_GUARD_DEAD != nil and v:Team() == TEAM_GUARD_DEAD then return end
			if TEAM_PRISONER_DEAD != nil and v:Team() == TEAM_PRISONER_DEAD then return end
			for item_id, item in pairs(v.PS_Items) do
				local ITEM = PS.Items[item_id]
				local canEquip = true
				local message = ""
				if type(ITEM.CanPlayerEquip) == 'function' then
					canEquip, message = ITEM:CanPlayerEquip(v)
				elseif type(ITEM.CanPlayerEquip) == 'boolean' then
					canEquip = ITEM.CanPlayerEquip
				end
				if item.Equipped and canEquip then
					ITEM:OnEquip(v, item.Modifiers)
				end
			end
		end
	end
end
local disguiseCommand = MK.commands.Add("utility", "disguise", disguise)
disguiseCommand:AddParam(MK.commands.args.Player, "targets", false, {})
disguiseCommand:AddParam(MK.commands.args.String, "name", false) 
disguiseCommand:AddParam(MK.commands.args.Rank, "fakerank", false, {default = "user"})
disguiseCommand:AddParam(MK.commands.args.String, "steamid", false)
disguiseCommand:AddParam(MK.commands.args.String, "uniqueid", false)
disguiseCommand:DefaultRank("admin")

local function undisguise(ply, targets)
	if (!targets) then
		targets = {ply}
	end
	for k,v in pairs(targets) do
		ply:MK_SendChat(Color(100,255,100), "[MK-A] (SILENT) ", v, " was undisguised")
		v:MK_GameData_Set("disguised", false)
		
		v:MK_GameData_Set("fakename", false)
		v:MK_GameData_Set("fakerank", false)
		v:MK_GameData_Set("fakesteamid", false)
		v:MK_GameData_Set("fakesteamid64", false)
		v:MK_GameData_Set("fakeuniqueid", false)
		
		if (PS) then
			for item_id, item in pairs(v.PS_Items) do
				if item.Equipped then
					local ITEM = PS.Items[item_id]
					ITEM:OnHolster(v, item.Modifiers)
				end
			end
			
			PS.ClientsideModels[v] = nil
			v:PS_LoadData()
			v:PS_SendClientsideModels()
			
			if TEAM_GUARD_DEAD != nil and v:Team() == TEAM_GUARD_DEAD then return end
			if TEAM_PRISONER_DEAD != nil and v:Team() == TEAM_PRISONER_DEAD then return end
			for item_id, item in pairs(v.PS_Items) do
				local ITEM = PS.Items[item_id]
				local canEquip = true
				local message = ""
				if type(ITEM.CanPlayerEquip) == 'function' then
					canEquip, message = ITEM:CanPlayerEquip(v)
				elseif type(ITEM.CanPlayerEquip) == 'boolean' then
					canEquip = ITEM.CanPlayerEquip
				end
				if item.Equipped and canEquip then
					ITEM:OnEquip(v, item.Modifiers)
				end
			end
		end
	end
end
local undisguiseCommand = MK.commands.Add("utility", "undisguise", undisguise)
undisguiseCommand:AddParam(MK.commands.args.Player, "targets", false, {})
undisguiseCommand:DefaultRank("user")