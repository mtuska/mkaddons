// RAWR!

local function noclip(ply, targets)
	if (!targets) then
		targets = {ply}
	end
	if (ulx) then return end
	local affectedTargets = {}
	for k,target in pairs(targets) do
		if (target:GetMoveType() == MOVETYPE_WALK) then
			target:SetMoveType(MOVETYPE_NOCLIP)
			table.insert(affectedTargets, target)
		elseif (target:GetMoveType() == MOVETYPE_NOCLIP) then
			target:SetMoveType(MOVETYPE_WALK)
			table.insert(affectedTargets, target)
		else -- Ignore if they're an observer
			//ULib.tsayError( calling_ply, target:Nick() .. " can't be noclipped right now.", true )
		end
	end
	
	MK.administration.Broadcast(ply, " changed the noclip state of ", affectedTargets)
end
local noclipCommand = MK.commands.Add("player", "noclip", noclip)
noclipCommand:AddParam(MK.commands.args.Players, "targets", false, {})
noclipCommand:DefaultRank("admin")