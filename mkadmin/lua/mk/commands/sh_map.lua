// RAWR!

local function map(ply, map, gamemode)
	MK.administration.Broadcast(ply, " changed the map to ", map, {(gamemode&&gamemode != ""), " with gamemode "..gamemode, ""})
	if (gamemode and gamemode != "") then
		game.ConsoleCommand("gamemode "..gamemode.."\n")
	end
	game.ConsoleCommand("changelevel "..map.."\n")
end
local mapCommand = MK.commands.Add("server", "map", map, {"changelevel"})
mapCommand:AddParam(MK.commands.args.String, "map", true, {})
mapCommand:AddParam(MK.commands.args.String, "gamemode", false, {})
mapCommand:DefaultRank("admin")