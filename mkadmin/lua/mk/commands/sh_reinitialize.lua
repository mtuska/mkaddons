// RAWR!

local function reinitialize(ply, dir)
	if (!dir) then
		MK.Initialize()
		MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName)
		MK.util.IncludeInternalDir("maps/"..game.GetMap())
		BroadcastLua('MK.Initialize()')
		BroadcastLua('MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName)')
		BroadcastLua('MK.util.IncludeInternalDir("maps/"..game.GetMap())')
	else
		MK.util.IncludeInternalDir(dir)
		BroadcastLua('MK.util.IncludeInternalDir("'..dir..'")')
	end

	MK.DisplayVersions()

	if (IsValid(ply)) then
		ply:MK_DisplayVersions()
	end
end
local reinitializeCommand = MK.commands.Add("server", "reinitialize", reinitialize)
reinitializeCommand:AddParam(MK.commands.args.String, "directory", false, {})
reinitializeCommand:DefaultRank("dev")
