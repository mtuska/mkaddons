// RAWR!

function respawn(ply, targets, bool)
	local affectedTargets = {}
	for k,v in pairs(targets) do
		if (!v:Alive()||bool) then
			v:Spawn()
			table.insert(affectedTargets, v)
		end
	end
	if (#affectedTargets < 1) then
		ply:MK_SendChat(Color(100,255,100), "[MK-A] ", Color(255,255,255), "Targets are all alive!")
	else
		MK.administration.Broadcast(ply, " respawned ", affectedTargets)
	end
end
local respawnCommand = MK.commands.Add("player", "respawn", respawn)
respawnCommand:AddParam(MK.commands.args.Players, "targets", true, {})
respawnCommand:AddParam(MK.commands.args.Boolean, "respawn dead", false, { default = false})
respawnCommand:DefaultRank("operator")