// RAWR!

MK.duelpending = MK.duelpending or {}
MK.duels = MK.duels or {}

function duel(ply, targets)
	//if (true) then return end
	if (MK.duels[ply:SteamID()]) then
		ply:MK_SendChat(Color(255,255,255), "Already in a duel!")
		return
	end
	if (ply:Team() == TEAM_GUARD||ply:Team() == TEAM_GUARD_DEAD) then
		ply:MK_SendChat(Color(255,255,255), "Can't duel as guard!")
		return
	end
	if (!ply:MK_Alive()) then
		ply:MK_SendChat(Color(255,255,255), "Can't duel a dead player!")
		return
	end
	local affectedTargets = {}
	for k,v in pairs(targets) do
		if (v:Team() == TEAM_GUARD||v:Team() == TEAM_GUARD_DEAD) then
			ply:MK_SendChat(Color(255,255,255), "Can't duel a guard!")
			return
		end
		if (!v:MK_Alive()) then
			ply:MK_SendChat(Color(255,255,255), "Can't duel a dead player!")
			return
		end
		if (MK.duelpending[ply:SteamID()] == v:SteamID()) then
			MK.duelpending[ply:SteamID()] = nil
			local duel = {}
			duel.bets = {}
			duel.shouldRespawn = true
			duel.active = false
			duel.opposer = v:SteamID()
			MK.duels[ply:SteamID()] = duel
			local duel2 = {}
			duel2.bets = {}
			duel2.shouldRespawn = true
			duel2.active = false
			duel2.opposer = ply:SteamID()
			MK.duels[v:SteamID()] = duel2
			MK.administration.Broadcast(ply, " has accepted ", v, "'s challenge to a duel! Duel starts in 5 seconds!")
			MK.administration.Broadcast("Type !duelbet <player> <points> to place a bet!")
			timer.Simple(5, function()
				MK.administration.Broadcast(ply, "'s duel against ", v, " has started!")
				MK.duels[ply:SteamID()].active = true
				MK.duels[v:SteamID()].active = true
			end)
		else
			if (MK.duels[v:SteamID()]) then
				ply:MK_SendChat(Color(255,255,255), "Already in a duel!")
				return
			end
			MK.duelpending[v:SteamID()] = ply:SteamID()
			MK.administration.Broadcast(ply, " challenges ", v, " to a duel!")
			v:MK_SendChat(Color(255,255,255), "Use !acceptduel to accept the challenge!")
		end
	end
end
local duelCommand = MK.commands.Add("fun", "duel", duel)
duelCommand:AddParam(MK.commands.args.Player, "targets", true, {})
duelCommand:DefaultRank("user")

function acceptduel(ply)
	if (!MK.duelpending[ply:SteamID()]) then
		v:MK_SendChat(Color(255,255,255), "No one has challenged you!")
		return
	end
	local target = player.GetBySteamID(MK.duelpending[ply:SteamID()])
	if (target) then
		duel(ply, {target})
	end
end
local acceptduelCommand = MK.commands.Add("fun", "acceptduel", acceptduel)
acceptduelCommand:DefaultRank("user")

hook.Add("PlayerShouldTakeDamage", "PlayerShouldTakeDamage.MK_Duel", function(victim, attacker)
	if (victim:IsPlayer()&&attacker:IsPlayer()&&MK.duels[victim:SteamID()]&&attacker:SteamID()==MK.duels[victim:SteamID()].opposer&&MK.duels[victim:SteamID()].active) then
		return true
	end

	//return false
end)

hook.Add("PlayerDeath", "PlayerDeath.MK_Duel", function(victim, inflictor, attacker)
	if (victim:IsPlayer()) then
		if (MK.duels[victim:SteamID()]) then
			MK.administration.Broadcast(victim, " lost the duel!")
			if (MK.duels[victim:SteamID()].shouldRespawn&&attacker:IsPlayer()&&attacker:SteamID()==MK.duels[victim:SteamID()].opposer) then
				timer.Simple(1, function()
					if (IsValid(victim)) then
						victim.jb_ForceAlive = true
						victim:Spawn()
					end
				end)
			end
			if (#MK.duels[victim:SteamID()].bets > 0||#MK.duels[MK.duels[victim:SteamID()].opposer].bets > 0) then
				local amount = 0
				local loosers = {}
				if (#MK.duels[victim:SteamID()].bets > 0) then
					for k,v in pairs(MK.duels[victim:SteamID()].bets) do
						amount = amount + v
						local ply = player.GetBySteamID(k)
						if (ply) then
							table.insert(loosers, ply)
						end
					end
				else
					loosers = "NO ONE"
				end
				local distribute = amount/#MK.duels[MK.duels[victim:SteamID()].opposer].bets
				local winners = {}
				if (#MK.duels[MK.duels[victim:SteamID()].opposer].bets > 0) then
					for k,v in pairs(MK.duels[MK.duels[victim:SteamID()].opposer].bets) do
						local ply = player.GetBySteamID(k)
						if (ply) then
							table.insert(winners, ply)
							ply:PS_GivePoints(v+distribute)
						end
					end
				else
					winners = "NO ONE"
				end
				MK.administration.Broadcast(loosers, " lost ", amount, " to ", winners, " on ", victim, "!")
			end
			MK.duels[MK.duels[victim:SteamID()].opposer] = nil
			MK.duels[victim:SteamID()] = nil
		end
	end
end)

function duelbet(ply, targets, amount)
	if (true) then
		ply:MK_SendChat(Color(255,255,255), "Betting currently disabled!")
		return
	end
	if (amount < 10) then
		ply:MK_SendChat(Color(255,255,255), "Amount can't be less then 10!")
		return
	end
	for k,v in pairs(targets) do
		if (MK.duels[v:SteamID()]&&!MK.duels[v:SteamID()].bets[ply:SteamID()]&&!MK.duels[MK.duels[v:SteamID()].opposer].bets[ply:SteamID()]) then
			MK.administration.Broadcast(ply, " has bet ", amount, " on ", v, "!")
			MK.duels[v:SteamID()].bets[ply:SteamID()] = tonumber(amount)
			ply:PS_TakePoints(tonumber(amount))
		elseif (MK.duels[v:SteamID()]&&(MK.duels[v:SteamID()].bets[ply:SteamID()]||MK.duels[MK.duels[v:SteamID()].opposer].bets[ply:SteamID()])) then
			ply:MK_SendChat(Color(255,255,255), "You already bet on this duel!")
		else
			ply:MK_SendChat(v, Color(255,255,255), " is not in a duel!")
		end
	end
end
local duelbetCommand = MK.commands.Add("fun", "duelbet", duelbet)
duelbetCommand:AddParam(MK.commands.args.Player, "targets", true, {})
duelbetCommand:AddParam(MK.commands.args.Amount, "points", true, {})
duelbetCommand:DefaultRank("user")