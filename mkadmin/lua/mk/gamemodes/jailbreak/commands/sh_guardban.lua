// RAWR!

function guardban(ply, targets, bool)
	local affectedTargets = {}
	for k,v in pairs(targets) do
		if ((v:Team() == TEAM_GUARD||v:Team() == TEAM_GUARD_DEAD)&&bool) then
			v:SetTeam(TEAM_PRISONER_DEAD)
			v:KillSilent()
		end
		v:MK_GameData_Set("guardBanned", true)
	end
	MK.administration.Broadcast(ply, " banned ", affectedTargets, " from being a guard.")
end
local guardbanCommand = MK.commands.Add("player", "guardban", guardban)
guardbanCommand:AddParam(MK.commands.args.Player, "targets", true, {})
guardbanCommand:AddParam(MK.commands.args.Boolean, "banned", false, {default = true})
guardbanCommand:DefaultRank("operator")

local Player = FindMetaTable("Player")
Player.old_PlayerCanBeGuard = Player.PlayerCanBeGuard
function Player:PlayerCanBeGuard()
	return self:MK_GameData_Get("guardBanned", false) or Player.old_PlayerCanBeGuard(self)
end

hook.Add("PlayerSpawn", "JB.PlayerSpawn.GuardBanned", function(ply)
	if (ply:Team() == TEAM_GUARD&&ply:MK_GameData_Get("guardBanned", false)) then
		ply:SetTeam(TEAM_PRISONER)
		ply.jb_ForceAlive = true
		ply:Spawn()
	end
end)
