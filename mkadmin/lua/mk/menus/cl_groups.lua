// RAWR!

local CATEGORY = {}
	CATEGORY.name = "Groups"
	CATEGORY.adminOnly = true

	function CATEGORY:Layout(panel)
		panel:AddHeader("Group")

		local choose = panel:Add("DComboBox")
			choose:Dock(TOP)
			choose:DockMargin(4, 4, 4, 4)
			choose:SetValue("Choose a Group")
		panel.choose = choose

		local first = true
		local UpdateGroupAccess

		local content = panel:Add("DListLayout")
			content:Dock(TOP)
			content:SetDrawBackground(true)
			content:DockMargin(4, 4, 4, 4)
		panel.content = content

		local lastY = panel:AddHeader("Parent", content):GetTall() + 4

		local parent = content:Add("DListView")
			local line = {}
			local delta

			parent:Dock(TOP)
			parent:SetTall(120)
			parent:DockMargin(4, 0, 4, 4)
			parent.choices = {}
			parent:AddColumn("Group")
			parent:AddColumn("Is Parent")
			parent.Think = function(this)
				for k, v in pairs(MK.ranks.list) do
					if (!IsValid(line[k]) and MK.menus.admin.lastGroup != k) then
						line[k] = this:AddLine(v.displayName, "")
						line[k].group = k

						this:SortByColumn(1)
					elseif (IsValid(line[k]) and MK.menus.admin.lastGroup == k) then
						this:RemoveLine(line[k]:GetID())
						this:SortByColumn(1)

						line[k] = nil
					end

					if (IsValid(line[k]) and MK.menus.admin.lastGroup) then
						local groupTable = MK.ranks.Get(MK.menus.admin.lastGroup)

						line[k]:SetColumnText(2, groupTable.inherit == k and "✔" or "")
					end
				end
			end
			parent.OnClickLine = function(this, line, selected)
				if (!MK.menus.admin.lastGroup) then return end

				local groupTable = MK.ranks.Get(MK.menus.admin.lastGroup)
				local otherGroupTable = MK.ranks.Get(line.group)

				if (selected and line.group != groupTable.inherit) then
					//moderator.UpdateGroup(MK.menus.admin.lastGroup, "inherit", line.group)
					net.Start("MK_RankUpdate")
						net.WriteString(MK.menus.admin.lastGroup)
						net.WriteString("Inherit")
						net.WriteString(line.group)
					net.SendToServer()

					timer.Simple(0.1, function()
						UpdateGroupAccess(MK.menus.admin.lastGroup)
					end)
				end
			end
		panel.parent = parent

		panel:AddHeader("Display Name", content)

		local name = content:Add("DTextEntry")
			name:Dock(TOP)
			name:SetTall(24)
			name:DockMargin(4, 0, 4, 4)
		panel.name = name

		panel:AddHeader("Icon", content)

		local icons = content:Add("DIconBrowser")
			icons:SetTall(256)
			icons:DockMargin(4, 0, 4, 4)
			icons:Dock(TOP)
		panel.icons = icons

		panel:AddHeader("Permissions", content)

		local permissions = content:Add("DListView")
		permissions:Dock(TOP)
		permissions:DockMargin(4, 0, 4, 4)
		permissions:SetTall(256)
		permissions:AddColumn("Name")
		permissions:AddColumn("Allowed")

		function UpdateGroupAccess(data)
			permissions:Clear()

			for k,v in SortedPairs(MK.permissions.list) do
				local hasAccess = MK.permissions.Rank(data, k)
				local line = permissions:AddLine(k, hasAccess and "✔" or "")
				line.permission = k
				line.Think = function(this)
					//if (data != "owner" and moderator.groups[data].access) then
						line:SetColumnText(2, MK.permissions.Rank(data, line.permission) and "✔" or "")
					//end
				end
			end
		end

		panel.choices = {}

		choose.OnSelect = function(this, index, value, data)
			if (panel.delete) then
				panel.delete:Remove()
			end

			if (!data or value == "New...") then
				Derma_StringRequest("New Group", "Enter a group identifier for this new group.", "", function(text)
					if (#text < 1 or !text:find("%S")) then
						//return moderator.Notify("You did not provide a valid group identifier.")
					end

					//moderator.UpdateGroup(text, "create")
					net.Start("MK_RankUpdate")
						net.WriteString(text:lower())
						net.WriteString("New")
					net.SendToServer()
				end)

				return
			end

			MK.menus.admin.lastGroup = data

			local groupTable = MK.ranks.Get(data)
			local lastName = ""

			name:SetText(groupTable.displayName)
			name.OnEnter = function(this)
				local value = this:GetText()
				if (lastName == value) then return end

				net.Start("MK_RankUpdate")
					net.WriteString(data)
					net.WriteString("Name")
					net.WriteString(value)
				net.SendToServer()
				choose.Choices[panel.choices[data]] = value
				choose:SetValue(value)
				lastName = value
			end

			icons:SelectIcon("icon16/"..(groupTable.icon or "user")..".png")
			icons:ScrollToSelected()
			icons.OnChange = function(this)
				//moderator.UpdateGroup(data, "icon", this:GetSelectedIcon():sub(8, -5))
				net.Start("MK_RankUpdate")
					net.WriteString(data)
					net.WriteString("Icon")
					net.WriteString(this:GetSelectedIcon():sub(8, -5))
				net.SendToServer()
			end

			permissions.OnClickLine = function(this, line)
				if (data != "owner") then
					groupTable.permissions = groupTable.permissions or {}

					if (MK.permissions.Rank(data, line.permission)) then
						groupTable.permissions[line.permission] = false
					else
						groupTable.permissions[line.permission] = true
					end
					net.Start("MK_RankUpdate")
						net.WriteString(data)
						net.WriteString("Permission")
						net.WriteString(line.permission)
						net.WriteBool(groupTable.permissions[line.permission])
					net.SendToServer()
				end
			end

			UpdateGroupAccess(data)

			if (LocalPlayer():MK_Permission_Has("mk editranks")) then
				local delete = content:Add("DButton")
					delete:Dock(TOP)
					delete:SetTall(24)
					delete:DockMargin(4, 4, 4, 0)
					delete:SetText("Delete Group")
					delete:SetImage("icon16/delete.png")
					delete.DoClick = function(this)
						Derma_Query("Are you sure you want to delete this group? It can NOT be undone!", "Delete Group",
						"Yes", function()
							MK.menus.admin.lastGroup = nil
							net.Start("MK_RankUpdate")
								net.WriteString(data)
								net.WriteString("Delete")
							net.SendToServer()
						end,
						"No", function() end)
					end
				panel.delete = delete
			end

			local found

			for k, v in pairs(MK.ranks.list) do
				if (groupTable == v) then continue end

				if (groupTable.inherit == k) then
					if (parent.choices[k]) then
						found = true
						parent:ChooseOptionID(parent.choices[k])

						break
					end
				end
			end

			if (!found and parent.choices.none) then
				parent:ChooseOptionID(parent.choices.none)
			end
		end

		for k, v in pairs(MK.ranks.list) do
			if (!MK.menus.admin.lastGroup) then
				panel.choices[k] = choose:AddChoice(v.displayName, k, first)
				first = false
			elseif (MK.menus.admin.lastGroup == k) then
				panel.choices[k] = choose:AddChoice(v.displayName, k, true)
			else
				panel.choices[k] = choose:AddChoice(v.displayName, k)
			end
		end

		local groupTable = MK.ranks.Get(MK.ranks.defaultRank)

		choose:AddChoice("New...")

		return true
	end

	function CATEGORY:ShouldDisplay()
		return LocalPlayer():MK_Permission_Has("mk editranks")
	end
MK.menus.list.groups = CATEGORY
