// RAWR!

local CATEGORY = {}
	CATEGORY.name = "Bans"
	CATEGORY.adminOnly = true

	function CATEGORY:Layout(panel)
		panel.Think = function(this)
			if (MK.bans.menuUpdate) then
				this:Clear(true)
				self:Layout(panel)
				MK.bans.menuUpdate = nil
			end
		end

		if (!MK.bans or table.Count(MK.bans.list) == 0) then
			return panel:ShowMessage("There are currently no bans to view.")
		end

		local function PaintHeader(this, w, h)
			surface.SetDrawColor(255, 255, 255, 5)
			surface.DrawRect(0, 0, w, h)
		end

		panel.bans = panel:Add("DListView")
		panel.bans:Dock(FILL)
		panel.bans:DockMargin(4, 4, 4, 0)
		panel.bans:AddColumn("Date")
		panel.bans:AddColumn("Name")
		panel.bans:AddColumn("Admin")
		panel.bans:AddColumn("Expires In")
		//panel.bans:SetHeaderHeight(28)
		//panel.bans:SetDataHeight(24)
		panel.bans:SetHeight(panel:GetTall() - 2)
		
		local lines

		local function PopulateList()
			panel.bans:Clear()

			lines = {}
			panel.lines = {}

			for k, v in SortedPairsByMemberValue(MK.bans.list, "banDate") do
				local line = panel.bans:AddLine(os.date("%c", v.banDate), v.steam, v.adminSteam, string.NiceTime((v.unbanDate) - MK.time()))
				line:SetToolTip("Reason: "..v.reason.."\nSteamID: "..k.."\nLength: "..string.NiceTime(v.length))
				line.ban = v
				line.steamID = k
				
				MK.players.external.GetPlayer(v.steam, function(target)
					if (IsValid(line)) then
						line:SetColumnText(2, target:Name())
					end
				end)
				
				MK.players.external.GetPlayer(v.adminSteam, function(target)
					if (IsValid(line)) then
						line:SetColumnText(3, target:Name())
					end
				end)

				if (v.length > 0) then
					lines[line] = {
						date = v.banDate,
						length = v.length
					}
				else
					line:SetColumnText(4, "Never")
				end

				panel.lines[k] = line:GetID()
			end

			local nextUpdate = 0

			panel.bans.OnRowRightClick = function(this, index, line)
				local menu = DermaMenu()
					local steamID64 = util.SteamIDTo64(line.steamID)

					menu:AddOption("Open Steam Profile", function()
						gui.OpenURL("http://steamcommunity.com/profiles/"..steamID64)
					end):SetImage("icon16/world.png")
					menu:AddSpacer()
					menu:AddOption("Change Reason", function()
						Derma_StringRequest("Change Reason", "Enter the new ban reason.", line.ban.reason, function(text)
							if (text == "") then
								text = "no reason"
							end

							//moderator.AdjustBan(line.steamID, "reason", text)
						end)
					end):SetImage("icon16/tag_orange.png")
					menu:AddOption("Adjust Time", function()
						Derma_StringRequest("Change Time", "Enter the new ban time.", "1m", function(text)
							if (text == "") then
								text = "1m"
							end

							//moderator.AdjustBan(line.steamID, "length", text)
						end)
					end):SetImage("icon16/clock.png")
					
					local remove = menu:AddSubMenu("Remove Ban")
					remove:AddOption("Confirm", function()
						MK.commands.Parse(LocalPlayer(), {"unbanid", line.steamID})
					end):SetImage("icon16/delete.png")
				menu:Open()
			end
			panel.bans.Think = function()
				if (nextUpdate <= CurTime()) then
					nextUpdate = CurTime() + 1

					for k, v in pairs(lines) do
						local difference = (v.date + v.length) - MK.time()
						k:SetColumnText(4, string.NiceTime(difference))

						if (difference <= 0) then
							MK.bans.list[k.steamID] = nil
							MK.bans.menuUpdate = true
						end
					end
				end
			end
		end

		PopulateList()
	end
MK.menus.list.bans = CATEGORY