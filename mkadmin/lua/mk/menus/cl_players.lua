// RAWR!

local CATEGORY = {}
	CATEGORY.name = "Players"
	CATEGORY.autoSelect = true
	
	//moderator.selected = {}
	//moderator.list = moderator.list or {}
	
	local function PaintButton(this, w, h)
		surface.SetDrawColor(255, 255, 255, 80)
		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(255, 255, 255, 120)
		surface.DrawOutlinedRect(1, 1, w - 2, h - 2)

		surface.SetDrawColor(0, 0, 0, 60)
		surface.DrawOutlinedRect(0, 0, w, h)

		if (this.entered) then
			surface.SetDrawColor(0, 0, 0, 25)
			surface.DrawRect(0, 0, w, h)
		end
	end
	
	function CATEGORY:Layout(panel)
		panel.search = panel:Add("DTextEntry")
		panel.search:Dock(TOP)
		panel.search:DockMargin(4, 4, 4, 0)
		panel.search.OnTextChanged = function(this)
			local text = this:GetText()

			if (text == "") then
				self:ListPlayers(panel.scroll)
			else
				local players = {}

				for k, v in ipairs(player.GetAll()) do
					if (MK.util.StringMatches(v:Name(), text) or v:SteamID():match(text)) then
						players[k] = v
					end
				end

				self:ListPlayers(panel.scroll, players)
			end
		end
		panel.search:SetToolTip("Search for players by their name/SteamID.")

		panel.contents = panel:Add("DPanel")
		panel.contents:DockMargin(4, 4, 4, 4)
		panel.contents:Dock(TOP)
		panel.contents:SetTall(panel:GetTall() - 31)

		panel.actions = panel.contents:Add("DScrollPanel")
		panel.actions:Dock(RIGHT)
		panel.actions:SetWide(panel:GetWide() * 0.335)
		panel.actions.Paint = PaintButton
		panel.actions:DockMargin(0, 4, 4, 4)
		panel.actions:DockPadding(0, 1, 0, 0)

		self:ListCommands(panel.actions, table.Count(MK.menus.selected))

		panel.scroll = panel.contents:Add("DScrollPanel")
		panel.scroll:Dock(FILL)
		panel.scroll:SetWide(panel:GetWide() * 0.6 - 11)

		self.deltaCount = #player.GetAll()
		self.deltaSelectedCount = table.Count(MK.menus.selected)

		panel.Think = function()
			local count = #player.GetAll()

			if (self.deltaCount != count) then
				local players = player.GetAll()
				local text = (IsValid(panel.search) and panel.search:GetText() or "")

				if (text != "") then
					players = {}

					for k, v in ipairs(player.GetAll()) do
						if (MK.util.StringMatches(v:Name(), text)) then
							players[k] = v
						end
					end
				end

				self:ListPlayers(panel.scroll, players)
			end

			self.deltaCount = count
			
			local count = table.Count(MK.menus.selected)
			
			if (self.deltaSelectedCount != count) then
				self:ListCommands(panel.actions, count)
			end
			
			self.deltaSelectedCount = count
		end

		self:ListPlayers(panel.scroll)

		return true
	end
	
	function CATEGORY:ListCommands(scroll, players)
		if (!IsValid(scroll)) then
			return
		end
		
		scroll:Clear()
		
		for k, v in SortedPairsByMemberValue(MK.commands.list, "cmd") do
			//if (v.hidden or !moderator.HasPermission(k)) then
			if (v.hidden or !LocalPlayer():MK_Permission_Has(MK.permissions.prefix..v.cmd)) then
				continue
			end
			
			if (table.Count(v.args) > 1) then
				continue
			end
			
			if ((!players or players == 0) && table.Count(v.args) > 0) then
				continue
			end
			if (players && players > 0) then
				if (table.Count(v.args) != 1 || (v.args[1][4].name != MK.commands.args.Player.name && v.args[1][4].name != MK.commands.args.Players.name)) then
					continue
				end
			end

			local button = scroll:Add("DButton")
			button:Dock(TOP)
			button:SetTall(28)
			button.Paint = PaintButton
			button:DockMargin(2, 2, 2, 0)
			button.OnCursorEntered = function(this)
				this.entered = true
			end
			button.OnCursorExited = function(this)
				this.entered = nil
			end
			button:SetText(v.cmd)
			button:SetTextColor(Color(0, 0, 0, 230))
			button.DoClick = function()
				local targets = {}

				for k, v in pairs(MK.menus.selected) do
					table.insert(targets, k)
				end
				
				MK.commands.Parse(LocalPlayer(), {v.cmd, targets})
			end

			if (v.tip) then
				button:SetToolTip(v.tip)
			end

			if (v.icon) then
				button:SetImage("icon16/"..v.icon..".png")
			end
		end
	end
	
	function CATEGORY:ListPlayers(scroll, players)
		if (!IsValid(scroll)) then
			return
		end

		MK.menus.players = {}

		scroll:Clear()
		players = players or player.GetAll()

		for k, v in SortedPairs(players) do
			local item = scroll:Add("mk_Player")
			item:Dock(TOP)
			item:SetPlayer(v)
			item.index = #MK.menus.players + 1

			MK.menus.players[item.index] = item
		end
	end
MK.menus.list.players = CATEGORY

concommand.Add("mk_clearselected", function()
	MK.menus.selected = {}
end)