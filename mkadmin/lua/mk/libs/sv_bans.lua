// RAWR!

util.AddNetworkString("MK_BanSync")

file.CreateDir("mk/administration")
if (!file.Exists("mk/administration/bans.txt", "DATA")) then
	file.Write("mk/administration/bans.txt", util.TableToJSON({}))
end

hook.Add("Initialize", "MK_Initialize.Bans", function()
	MK.bans.Load()
end)

hook.Add("OnReloaded", "MK_OnReloaded.Bans", function()
	MK.bans.Load()
end)

function MK.bans.Load()
	local bans = util.JSONToTable(file.Read("mk/administration/bans.txt", "DATA") or "[]")

	if (bans) then
		for k, v in pairs(bans) do
			if (v.length > 0 and v.unbanDate <= os.time()) then
				bans[k] = nil
			end
		end

		MK.bans.list = bans
		MK.bans.Save()
	end

	MK.db.query("SELECT * FROM banData WHERE server = '"..MK.server.address.."' or global = '1'", function(data)
		if (data and data[1]) then
			MK.bans.list = {}
			for k,v in pairs(data) do
				local tbl = table.Copy(v)
				tbl.server = nil
				tbl.banDate = tonumber(tbl.banDate)
				tbl.unbanDate = tonumber(tbl.unbanDate)
				tbl.length = tonumber(tbl.length)
				local steamid = util.SteamIDFrom64(tbl.steam)
				MK.bans.list[steamid] = tbl
			end
			MK.bans.Save()
		end
		MK.dev.Log(5, "Loaded 'banData' from database")
	end, "banData_Select")
end

function MK.bans.Save()
	for k, v in pairs(MK.bans.list) do
		if (v.length > 0 and v.unbanDate <= os.time()) then
			MK.bans.list[k] = nil
		end
		MK.players.external.GetPlayer(v.steam)
	end

	net.Start("MK_BanSync")
		net.WriteTable(MK.bans.list)
	net.Broadcast()

	file.Write("mk/administration/bans.txt", util.TableToJSON(MK.bans.list))
end

function MK.bans.Ban(steam, name, length, reason, adminSteam, global)
	if (!steam||!length) then return end
	if (global) then
		global = 1
	else
		global = 0
	end

	local steamid = util.SteamIDFrom64(steam)
	local name = ""
	local ply = player.GetBySteamID(steamid)
	if (ply) then
		name = ply:Name()
	end

	local unbanDate = os.time() + length
	local reason = reason

	MK.db.insertTable({
		steam = steam,
		adminSteam = adminSteam,
		banDate = os.time(),
		unbanDate = unbanDate,
		length = length,
		reason = reason,
		global = global,
		server = MK.server.address or "",
	}, nil, "banData", "insertBanData_"..steam)

	MK.bans.list[steamid] = {
		steam = steam,
		adminSteam = adminSteam,
		banDate = os.time(),
		unbanDate = unbanDate,
		length = length,
		reason = reason,
	}

	MK.bans.Save()
end

function MK.bans.Unban(steam, delete)
	local steamid = util.SteamIDFrom64(steam)
	if (MK.bans.list[steamid]) then
		local length = os.time() - MK.bans.list[steamid].banDate
		if (length == 0) then
			MK.bans.list[steamid].unbanDate = os.time() + 1
			MK.bans.list[steamid].length = MK.bans.list[steamid].unbanDate - MK.bans.list[steamid].banDate + 1
		else
			MK.bans.list[steamid].unbanDate = os.time()
			MK.bans.list[steamid].length = MK.bans.list[steamid].unbanDate - MK.bans.list[steamid].banDate
		end
		MK.db.updateTable({
			unbanDate = MK.bans.list[steamid].unbanDate,
			length = MK.bans.list[steamid].length,
		}, nil, "steam = "..steam, "banData", "updatePlayerBan_"..steam)

		MK.bans.Save()
	end
end

function MK.bans.IsBanned(var)
	var = type(var):lower() == "Player" and var:MK_RealSteamID() or var
	if (ULib&&ULib.bans&&ULib.bans[var]) then
		return true
	end
	if (MK.bans.list[var]&&(MK.bans.list[var].length == 0||MK.bans.list[var].unbanDate >= os.time())) then
		return true
	end
	return false
end

function MK.bans.GetReason(var)
	local reason = "Banned from server"
	var = type(var):lower() == "Player" and var:MK_RealSteamID() or var
	if (ULib&&ULib.bans&&ULib.bans[var]) then
		reason = ULib.bans[var].reason
	end
	if (MK.bans.list[var]&&(MK.bans.list[var].length == 0||MK.bans.list[var].unbanDate >= os.time())) then
		reason = MK.bans.list[var].reason
	end
	reason = reason.." "..MK.config.Get("administration_banSuffix", "")
	return reason
end
