// RAWR!

MK.permissions = MK.permissions or {}
MK.permissions.list = MK.permissions.list or {}
MK.permissions.prefix = "mk "

if (SERVER) then
	MK.permissions.list = MK.data.GetData("administration/permissions.txt", nil, {})
end

function MK.permissions.Save()
	if (SERVER) then
		MK.data.SetData("administration/permissions.txt", nil, MK.permissions.list)
	end
end

function MK.permissions.Get(permission)
	return MK.permissions.list[permission] or nil
end

function MK.permissions.Register(permission, rank, force, from_cami)
	local name = permission
	if (!from_cami) then
		permission = MK.permissions.prefix..permission
	end
	if (MK.permissions.list[permission]) then
		if (!force) then
			MK.dev.Log(7, "Permission '"..permission.."' already exists and isn't being set! Leaving value '"..MK.permissions.list[permission].."'")
			return false
		end
		MK.dev.Log(7, "Resetting Permission: "..permission..":"..rank)
	else
		MK.dev.Log(7, "Adding Permission: "..permission..":"..rank)
	end
	MK.permissions.list[permission] = rank
	if (!from_cami) then
		// When the privilege system is less fucked with CAMI, we'll use it
		//CAMI.RegisterPrivilege({Name=permission, MinAccess=rank})
	end
	MK.permissions.Save()
	return true
end

function MK.permissions.Unregister(permission, from_cami)
	MK.permissions.list[permission] = nil
	if (!from_cami) then
		CAMI.UnregisterPrivilege(permission)
	end
	MK.permissions.Save()
end

function MK.permissions.Query(permission, plys, default)
	if (type(plys) != "table") then
		plys = {plys}
	end
	local results = {}
	for _,ply in pairs(plys) do
		if (!IsValid(ply)) then continue end
		results[ply:MK_RealSteamID()] = default or false
		local permissions = ply:MK_Permission_Get() -- Player specific variables override rank variables
		if (permissions[permission] != nil) then
			results[ply:MK_RealSteamID()] = permissions[permission]
			continue
		end

		local group = ply:MK_Rank_GetRealRank()
		for _,rank in pairs({group, unpack(MK.ranks.GetParents(group))}) do
			local data = MK.ranks.Get(rank)
			if (data&&data.permissions&&data.permissions[permission] != nil) then
				results[ply:MK_RealSteamID()] = data.permissions[permission]
				continue
			end
		end

		if (MK.permissions.list[permission]&&MK.ranks.IsRank(group, MK.permissions.list[permission])) then
			results[ply:MK_RealSteamID()] = true
			continue
		end
	end
	return results
end

function MK.permissions.Rank(group, permission, default)
	for _,rank in pairs({group, unpack(MK.ranks.GetParents(group))}) do
		local data = MK.ranks.Get(rank)
		if (data&&data.permissions&&data.permissions[permission] != nil) then
			return data.permissions[permission]
		end
	end
	if (MK.permissions.list[permission]&&MK.ranks.IsRank(group, MK.permissions.list[permission])) then
		return true
	end
	return false
end
