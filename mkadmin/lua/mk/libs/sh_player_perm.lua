// RAWR!

function player.GetByRank(rank)
	local players = {}
	for k,v in pairs(player.GetAll()) do
		if (v:CheckGroup(rank, true)) then
			table.insert(players, v)
		end
	end
	return players
end

function player.GetByPermission(permission)
	local players = {}
	for k,v in pairs(player.GetAll()) do
		if (v:MK_Permission_Has(permission)) then
			table.insert(players, v)
		end
	end
	return players
end