// RAWR!

MK.commands = MK.commands or {}
MK.commands.cats = MK.commands.cats or {}
MK.commands.list = MK.commands.list or {}
MK.commands.concmds = MK.commands.concmds or {}

function MK.commands.Initialize()
	MK.util.IncludeInternalDir("commands")

	// After commands have been loaded
	local concmds = {}
	for cmd,cmdTbl in pairs(MK.commands.list) do
		for _,concmd in pairs(cmdTbl.concmd) do
			table.insert(concmds, concmd)
		end
	end
	local skip = {}
	for k,concmd in pairs(MK.commands.concmds) do
		if (!table.HasValue(concmds, concmd)) then
			concommand.Remove(concmd)
		else
			table.insert(skip, concmd)
		end
	end
	MK.commands.concmds = concmds
	for _,concmd in pairs(MK.commands.concmds) do
		if (!table.HasValue(skip, concmd)) then
			concommand.Add(concmd, function(ply, cmd, args, argStr)
				MK.commands.Parse(ply, args)
			end, MK.commands.AutoComplete)
		end
	end
end

function MK.commands.AutoComplete(concmd, stringargs)
	local args = MK.util.ExtractArgs(stringargs)
	local possibilities = {}

	local commands = table.Copy(MK.commands.list)
	for cmd,cmdTbl in pairs(commands) do
		if (!table.HasValue(cmdTbl.concmd, concmd)) then
			commands[cmd] = nil
			continue
		end
		if (table.Count(args) >= 1) then
			if (!cmd:lower():find(args[1]:lower())) then // Find what commands we could be attempting to use
				commands[cmd] = nil
			end
			if (cmd:lower() == args[1]:lower()) then
				commands = {}
				commands[cmd] = cmdTbl
			end
		end
	end

	// We know the possible commands, if there is only one, we continue to the arguments
	if (table.Count(commands) == 1) then
		for cmd,cmdTbl in SortedPairsByMemberValue(commands, "cmd", false) do
			local text = " "..cmd
			for k,arg in pairs(cmdTbl.args) do
				text = text.." \""..arg[1].."\""
			end
			table.insert(possibilities, concmd..text)
		end
	// We have multiple possiblities of commands, just list them
	else
		for cmd,cmdTbl in SortedPairsByMemberValue(commands, "cmd", false) do
			local text = " "..cmd
			table.insert(possibilities, concmd..text)
		end
	end

	return possibilities
end

function MK.commands.Add(cat, cmd, func, aliases, prefix, concmd)
	if (!func||!isfunction(func)) then
		ErrorNoHalt("The command '"..cmd.."' has no function!")
		return
	end

	MK.commands.list[cmd] = nil
	for cat,cmds in pairs(MK.commands.cats) do
		for k, cmdTbl in pairs(cmds) do
			if (cmdTbl.cmd == cmd) then
				MK.commands.cats[cat][k] = nil
				if (table.Count(MK.commands.cats[cat]) <= 0) then
					MK.commands.cats[cat] = nil
				end
			end
		end
	end

	MK.dev.Log(7, "Adding command '"..cmd.."'")
	local command = MK.commands.list[cmd] or {}
	if (!aliases) then
		command.aliases = {}
	elseif (type(aliases) == "table") then
		command.aliases = aliases
	else
		command.aliases = {aliases}
	end
	cat = string.lower(cat)
	command.cat = cat
	command.cmd = cmd
	if (!concmd) then
		command.concmd = {"mk"}
	elseif (istable(concmd)) then
		command.concmd = concmd
	else
		command.concmd = {concmd or "mk"}
	end
	command.func = func
	command.args = {}
	command.defaultRank = "superadmin"
	command.help = "Don't do something stupid"
	command.prefix = prefix or MK.config.Get("administration_commandChatPrefix")

	local rank = MK.permissions.Get(cmd)
	if (!rank||!MK.ranks.Get(rank)) then
		if (!MK.ranks.Get(rank)) then
			MK.permissions.Unregister(cmd)
		end
		MK.permissions.Register(cmd, command.defaultRank)
		command.canResetRank = true
	else
		command.canResetRank = false
		command.defaultRank = rank
	end

	setmetatable(command, MK.meta.Command)
	MK.commands.list[cmd] = command

	MK.commands.cats[cat] = MK.commands.cats[cat] or {}
	table.insert(MK.commands.cats[cat], MK.commands.list[cmd])

	return command
end

if (CLIENT) then
	function MK.commands.Parse(ply, args, isChat)
		net.Start("MK_SendCommand")
			net.WriteTable(args or {})
			net.WriteBool(isChat or false)
		net.SendToServer()
	end
end

if (SERVER) then
	util.AddNetworkString("MK_SendCommand")

	net.Receive("MK_SendCommand", function(len, ply)
		MK.commands.Parse(ply, net.ReadTable(), net.ReadBool())
	end)

	function MK.commands.Parse(ply, args, isChat)
		if (type(args)!="table") then
			ErrorNoHalt("Command args expected to be a table")
			return false
		end

		if (!args[1]) then
			ErrorNoHalt("Arguments seem to be missing?")
			return false
		end

		local justincase = args[1]
		local phrase = args[1]:lower()
		table.remove(args, 1)

		local cmd = nil
		local data = nil
		local found = false
		local opposite = false

		for index,value in pairs(MK.commands.list) do
			local text = phrase
			if (isChat&&value.prefix != "") then
				if (value.prefix != text:sub(1, 1)) then
					if (value.prefix != "!") then
						//print(index.." failed prefix check for '"..value.prefix.."'")
					end
					continue
				else
					text = text:sub(2)
				end
			end
			if (value.prefix == ""&&text:sub(1,1) == index) then // Quick fix for asay(@ command in chat)
				cmd = index
				data = value
				found = true
				table.insert(args, 1, justincase:sub(2))
				break
			end
			if (text == index) then
				cmd = index
				data = value
				found = true
				break
			end
			if (value.opposite&&text == value.opposite[1]) then
				cmd = index
				data = value
				found = true
				opposite = true
				break
			end
			if (table.HasValue(value.aliases, text)) then
				cmd = index
				data = value
				found = true
				break
			end
		end
		if (!found) then
			if (isConsole) then
				if (IsValid(ply)) then
					ply:MK_SendChat(Color(255,255,255), "Couldn't find the specified command!")
				else
					print("Couldn't find the specificed command!")
				end
			end
			//ply:SendLua("print('Couldn't find the specified command')")
			return false -- We couldn't find the specified command
		end

		local permission = MK.permissions.prefix..cmd
		if (!IsValid(ply)||ply:IsListenServerHost()||ply:MK_Permission_Has(permission, false)) then
			required = 0
			for k,v in pairs(data.args) do
				if (v[2]) then
					required = required + 1
				else
					break
				end
			end
			if (table.Count(args) >= required) then
				local newArgs = {}
				local run = true
				for k,v in pairs(data.args) do
					if (opposite&&data.opposite[2][k] != nil) then
						table.insert(newArgs, data.opposite[2][k])
						continue
					end
					local arg = args[k]
					if (v[4].name == MK.commands.args.String.name&&#data.args==k) then
						//print("last arg is string")
						arg = table.concat(args, " ", k, #args)
					end
					local success, result = v[4].validation(ply, arg or nil, v[3], v[2])
					if (success) then
						table.insert(newArgs, result)
					else
						run = false
						if (IsValid(ply)) then
							ply:MK_SendChat(Color(255,255,255), result)
						else
							print(result)
						end
						break
					end
				end
				if (run) then
					local results = {MK.commands.list[cmd].func(ply, unpack(newArgs or {}))}
				end
			else
				local text = "That command requires more arguments!"
				if (IsValid(ply)) then
					ply:MK_SendChat(Color(255,255,255), text)
				else
					print(text)
				end
			end
			//local results = {MK.commands.list[cmd].func(ply, unpack(args or {}))}
		elseif (IsValid(ply)) then
			ply:MK_SendChat(Color(255,255,255), "You don't have permission to that command!")
			//print(ply, "Tried to use permission: '"..permission.."'")
		end
		return true -- We found the specified command
	end
end

MK.commands.args = {}
local function stringParsePlayer(str, ply)
	if (str == "") then
		return {}
	end
	if (str == "*") then
		return player.GetAll()
	end
	if (str == "^") then
		return {ply}
	end
	if (str == "@") then
		local trace = ply:GetEyeTrace()
		if (trace.Entity&&trace.Entity:IsPlayer()) then
			return {trace.Entity}
		end
	end
	if (str == "#last") then
		if (ply.mk.latest.targets) then
			return ply.mk.latest.targets
		end
	end
	local target = player.GetBySteamID(str:upper())
	if (IsValid(target)) then
		return {target}
	end
	local target = player.GetBySteamID64(str)
	if (IsValid(target)) then
		return {target}
	end
	local targets = MK.util.FindPlayer(str, ply)
	return targets
end
local function removeUnableTargets(ply, param, possibleTargets)
	local targets = {}
	for k,target in pairs(possibleTargets) do
		if ((!IsValid(ply)||(MK.ranks.CanTarget(ply:MK_Rank_GetRealRank(), target:MK_Rank_GetRealRank())||param.canTargetHigherRank))&&!table.HasValue(targets, target)) then
			table.insert(targets, target)
		end
	end
	return targets
end

MK.commands.args.Player = {
	name = "player",
	prep = function(param)
		param.canTargetHigherRank = param.canTargetHigherRank or false
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end
		if (!isstring(str)) then
			local tbl = istable(str) and str or {str}
			local possibleTargets = {}
			for k,v in pairs(tbl) do
				if (v && isentity(v) && IsValid(v) && v:IsPlayer()) then
					table.insert(possibleTargets, v)
				end
			end
			if (table.Count(possibleTargets) == 0) then
				return false, "Players were not provided"
			end
			if (table.Count(possibleTargets) > 1) then
				return false, "This command doesn't support multiple targets."
			end
			local targets = removeUnableTargets(ply, param, possibleTargets)
			if (table.Count(targets) != 1) then
				return false, "The target(s) are immune to you."
			end
			return true, targets
		end
		local str = str:lower()

		if (str == "") then
			return false, "Argument not provided!"
		end

		local args = string.Explode("&", str)
		local bool, possibleTargets = false, {}
		if (table.Count(args) <= 1) then
			possibleTargets = stringParsePlayer(str, ply)
		else
			BroadcastLua("print('args multi: "..table.concat(args, " | ").."')")
			for k,arg in pairs(args) do
				local targets = stringParsePlayer(arg, ply)
				for k,target in pairs(targets) do
					table.insert(possibleTargets, target)
				end
			end
		end

		local targets = removeUnableTargets(ply, param, possibleTargets)
		if (table.Count(targets) != 1) then
			if (table.Count(possibleTargets) > 1) then
				return false, "This command doesn't support multiple targets."
			end
			if (table.Count(possibleTargets) != 0) then
				return false, "The target(s) are immune to you."
			end
			return false, "Couldn't find the specified player(s)."
		end
		if (IsValid(ply)) then
			ply.mk.latest.targets = targets
		end
		return true, targets
	end,
}
MK.commands.args.Players = {
	name = "players",
	prep = function(param)
		param.canTargetHigherRank = param.canTargetHigherRank or false
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end
		if (!isstring(str)) then
			local tbl = istable(str) and str or {str}
			local possibleTargets = {}
			for k,v in pairs(tbl) do
				if (v && isentity(v) && IsValid(v) && v:IsPlayer()) then
					table.insert(possibleTargets, v)
				end
			end
			if (table.Count(possibleTargets) == 0) then
				return false, "Players were not provided"
			end
			local targets = removeUnableTargets(ply, param, possibleTargets)
			if (table.Count(targets) == 0) then
				return false, "The target(s) are immune to you."
			end
			return true, targets
		end
		local str = str:lower()
		if (str == "") then
			return false, "Argument not provided!"
		end

		local args = string.Explode("&", str)
		local bool, possibleTargets = false, {}
		if (table.Count(args) <= 1) then
			possibleTargets = stringParsePlayer(str, ply)
		else
			BroadcastLua("print('args multi: "..table.concat(args, " | ").."')")
			for k,arg in pairs(args) do
				local targets = stringParsePlayer(arg, ply)
				for k,target in pairs(targets) do
					table.insert(possibleTargets, target)
				end
			end
		end
		local targets = removeUnableTargets(ply, param, possibleTargets)
		if (table.Count(targets) == 0) then
			if (table.Count(possibleTargets) != 0) then
				return false, "The target(s) are immune to you."
			end
			return false, "Couldn't find the specified player(s)."
		end
		if (IsValid(ply)) then
			ply.mk.latest.targets = targets
		end
		return true, targets
	end,
}
MK.commands.args.Rank = {
	name = "rank",
	prep = function(param)
		param.canTargetHigherRank = param.canTargetHigherRank or false
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end
		local str = str:lower()
		local rank = param.default or nil
		if (MK.ranks.list[str]) then
			rank = str
		end
		for k,v in pairs(MK.ranks.list) do
			if (string.find(k:lower(), str)||string.find(v.displayName:lower(), str)) then
				rank = k
				break
			end
		end
		if (rank) then
			if (ply:IsPlayer()&&!ply:IsListenServerHost()&&!MK.ranks.CanTarget(ply:MK_Rank_GetRealRank(), rank)&&!param.canTargetHigherRank) then
				return false, "Specified rank is higher then you."
			end
			return true, rank
		end
		return false, "Couldn't find specified rank '"..str.."'."
	end,
}
MK.commands.args.String = {
	name = "string",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		return true, str or param.default or nil
	end,
}
MK.commands.args.Time = {
	name = "time",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		return true, str or param.default or nil
	end,
}
MK.commands.args.Amount = {
	name = "amount",
	prep = function(param)
		param.default = param.default or 0
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default
		end
		str = tonumber(str)
		if (isnumber(str)) then
			if (!param.min||str >= param.min) then
				if (!param.max||str <= param.max) then
					return true, str
				else
					return false, "Number must be smaller then "..param.max.."."
				end
			else
				return false, "Number must be greater then "..param.min.."."
			end
		else
			return false, "No number found."
		end
	end,
}
MK.commands.args.Boolean = {
	name = "boolean",
	prep = function(param)
		param.default = param.default or false
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default
		end
		if (isnumber(str)) then
			if (str=="0") then
				return true, false
			elseif (str=="1") then
				return true, true
			else
				return false, "Number form must be 0(false) or 1(true)."
			end
		else
			str = str:lower()
			if (str=="false"||str=="f") then
				return true, false
			elseif (str=="true"||str=="t") then
				return true, true
			else
				return false, "String form must be 'true' or 'false'."
			end
		end
		return false, "Unknown failure"
	end,
}
MK.commands.args.Bool = MK.commands.args.Boolean
