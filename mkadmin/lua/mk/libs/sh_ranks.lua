// RAWR!

MK.ranks = MK.ranks or {}
MK.ranks.defaultRank = MK.ranks.defaultRank or "user"
MK.ranks.list = MK.ranks.list or {}
MK.ranks.children = MK.ranks.children or {}
MK.ranks.parents = MK.ranks.parents or {}

function MK.ranks.Initialize()
	MK.permissions.Register("editranks", "superadmin")
end

if (SERVER) then
	MK.ranks.list = MK.data.GetData("administration/ranks.txt", nil, {})

	util.AddNetworkString("MK_RankUpdate")

	net.Receive("MK_RankUpdate", function(len, ply)
		if (!ply:MK_Permission_Has("mk editranks")) then return end
		local rank = net.ReadString()
		local UpdateType = net.ReadString()
		if (!MK.ranks.list[rank] && UpdateType != "New") then return end
		if (UpdateType == "New") then
			// Fuck you
			local data = MK.ranks.Add(rank:lower(), {})
			if (!data) then return end
			net.Start("MK_RankUpdate")
				net.WriteString(rank)
				net.WriteString("New")
				net.WriteTable(data)
			net.Broadcast()
			MK.administration.Broadcast(ply, " created the new rank ", rank)
		elseif (UpdateType == "Delete") then
			// Fuck you
			MK.ranks.list[rank] = nil
			net.Start("MK_RankUpdate")
				net.WriteString(rank)
				net.WriteString("Delete")
			net.Broadcast()
			MK.administration.Broadcast(ply, " deleted the rank ", rank)
		elseif (UpdateType == "Name") then
			local name = net.ReadString()
			if (MK.ranks.list[rank].displayName == name) then return end
			MK.administration.Broadcast(ply, " changed the rank name ", MK.ranks.list[rank].color, MK.ranks.list[rank].displayName, " to ", MK.ranks.list[rank].color, name)
			MK.ranks.list[rank].displayName = name
			net.Start("MK_RankUpdate")
				net.WriteString(rank)
				net.WriteString("Name")
				net.WriteString(name)
			net.Broadcast()
		elseif (UpdateType == "Icon") then
			local icon = net.ReadString()
			MK.ranks.list[rank].icon = icon
			net.Start("MK_RankUpdate")
				net.WriteString(rank)
				net.WriteString("Icon")
				net.WriteString(icon)
			net.Broadcast()
		elseif (UpdateType == "Inherit") then
			local rank1 = net.ReadString()
			if (!MK.ranks.list[rank1]) then return end
			MK.ranks.children = {}
			MK.ranks.parents = {}
			if (MK.ranks.list[rank].inherit == nil&&MK.ranks.list[rank].inherit != rank1) then
				MK.ranks.list[rank].inherit = rank1
				net.Start("MK_RankUpdate")
					net.WriteString(rank)
					net.WriteString("Inherit")
					net.WriteString(rank1)
				net.Broadcast()
			end
		elseif (UpdateType == "Permission") then
			local perm = net.ReadString()
			local bool = net.ReadBool()
			MK.ranks.list[rank].permissions[perm] = nil
			local test = MK.permissions.Rank(data, perm)
			if (test != bool) then
				MK.ranks.list[rank].permissions[perm] = bool
			end
			MK.administration.Broadcast(ply, " ", {bool, "gave", "removed"}, " permission ", Color(84, 214, 453), perm, " ", {bool, "to", "from"}," rank ", MK.ranks.list[rank].color, MK.ranks.list[rank].displayName)

			net.Start("MK_RankUpdate")
				net.WriteString(rank)
				net.WriteString("Permission")
				net.WriteString(perm)
				net.WriteBool(bool)
			net.Broadcast()
		end
		MK.data.SetData("administration/ranks.txt", rank, MK.ranks.list[rank], true)
	end)
else
	net.Receive("MK_RankUpdate", function(len)
		local rank = net.ReadString()
		local UpdateType = net.ReadString()
		if (!MK.ranks.list[rank]) then return end
		MK.dev.Log(7, "Rank '"..rank.."' received an update for '"..UpdateType.."'")
		if (UpdateType == "New") then
			MK.ranks.list[rank] = net.ReadTable()
		elseif (UpdateType == "Delete") then
			MK.ranks.list[rank] = nil
		elseif (UpdateType == "Name") then
			local name = net.ReadString()
			MK.ranks.list[rank].displayName = name
		elseif (UpdateType == "Icon") then
			local icon = net.ReadString()
			MK.ranks.list[rank].icon = icon
		elseif (UpdateType == "Inherit") then
			MK.ranks.children = {}
			MK.ranks.parents = {}
			local rank1 = net.ReadString()
			MK.ranks.list[rank].inherit = rank1
		elseif (UpdateType == "Permission") then
			local perm = net.ReadString()
			local bool = net.ReadBool()
			MK.dev.Log(7, "Received rank perm update for: "..perm..": "..tostring(bool))
			MK.ranks.list[rank].permissions[perm] = bool
		end
	end)
end

function MK.ranks.Add(rank, data, override)
	if (MK.ranks.list[rank]&&!override) then
		MK.dev.Log(7, "Rank '"..rank.."' already exists! Aborting add rank.")
		return nil
	end
	if (!data||type(data)!="table") then
		data = {}
	end
	data.rank = rank
	data.donator = data.donator or false
	data.displayName = data.displayName or rank
	data.color = data.color or Color(255,255,255)
	data.color.a = 255
	data.icon = data.icon or "user"
	data.inherit = data.inherit or nil
	data.universal = data.universal or false
	data.requirePassword = data.requirePassword or true
	data.clearOtherRanks = data.clearOtherRanks or true
	data.canTarget = data.canTarget or "*"
	if (type(data.canTarget) == "string") then
		data.canTarget = {data.canTarget}
	end
	data.permissions = data.permissions or {}
	MK.ranks.list[rank] = data
	hook.Run("MK_Ranks.OnRegisterRank", rank, data)
	if (SERVER) then
		MK.data.SetData("administration/ranks.txt", rank, data, true)
	end
	MK.ranks.children = {}
	MK.ranks.parents = {}
	//MK.ranks.list[rank] = setmetatable(data, MK.meta.Rank)

	return data
end

function MK.ranks.Remove(rank)
	if table.HasValue({"superadmin", "admin", "user"}, rank) then return end
	MK.ranks.list[rank] = nil
	MK.ranks.children[rank] = nil
	MK.ranks.parents[rank] = nil
	hook.Run("MK_Ranks.OnUnregisterRank", rank, data)
end

function MK.ranks.Get(rank)
	return MK.ranks.list[rank] or nil
end

function MK.ranks.GetChildren(rank, inner)
	if (!MK.ranks.list[rank]) then return {} end
	if (!MK.ranks.children[rank]||inner) then
		local children = {}
		for k,v in pairs(MK.ranks.list) do
			if (v.inherit == rank) then
				table.Add(children, MK.ranks.GetChildren(k, true))
				table.insert(children, k)
			end
		end
		if (inner) then
			return children
		end
		MK.ranks.children[rank] = children
	end
	return table.Copy(MK.ranks.children[rank])
end

function MK.ranks.GetParent(rank)
	local rankData = MK.ranks.Get(rank)
	if (rankData&&rankData.inherit) then
		return rankData.inherit
	end
	return false
end

function MK.ranks.GetParents(rank, inner)
	if (!MK.ranks.list[rank]) then return {} end
	if (!MK.ranks.parents[rank]||inner) then
		local parents = {}
		local parent = MK.ranks.GetParent(rank)
		if (parent) then
			table.Add(parents, MK.ranks.GetParents(parent, true))
			table.insert(parents, parent)
			/*for k,v in pairs(MK.ranks.list) do
				if (MK.ranks.GetParent(rank) == k) then
					table.Add(parents, MK.ranks.GetParents(k, true))
					table.insert(parents, k)
					if (inner) then
						return parents
					end
				end
			end*/
		end
		if (inner) then
			return parents
		end
		MK.ranks.parents[rank] = parents
	end
	return table.Copy(MK.ranks.parents[rank])
end

function MK.ranks.IsRank(rank1, rank2)
	if (!MK.ranks.list[rank1]||!MK.ranks.list[rank2]) then
		return false
	end
	local ranks = MK.ranks.GetParents(rank1)
	table.insert(ranks, rank1)
	return table.HasValue(ranks, rank2) or (MK.config.Get("development") and rank1 == "dev") or false
end

function MK.ranks.Message(rank, ...)
	for k,v in pairs(player.GetByRank(rank)) do
		v:MK_SendChat(...)
	end
end

function MK.ranks.CanTarget(group1, group2)
	if (!MK.ranks.list[group1]||!MK.ranks.list[group2]) then
		return false
	end
	local rank1 = MK.ranks.list[group1]
	local rank2 = MK.ranks.list[group2]
	for _,target in pairs(rank1.canTarget) do
		if (target == "!"..group2) then
			return false
		end
		if (target == group2) then
			return true
		end
		local test = string.Replace(target, "!", "")
		if (table.HasValue(MK.ranks.GetParents(group2), test)) then
			return false
		end
		if (table.HasValue(MK.ranks.GetParents(group2), group2)) then
			return true
		end
	end
	return true
end

MK.ranks.Add("none", {
	displayName = "None",
	color = Color(255, 255, 255),
})

MK.ranks.Add("user", {
	displayName = "User",
	color = Color(255, 255, 255),
	canTarget = "!operator",
})
MK.ranks.defaultRank = "user"
MK.ranks.Add("operator", {
	displayName = "Operator",
	color = Color(255, 255, 255),
	inherit = "user",
	canTarget = "!admin",
})
MK.ranks.Add("admin", {
	displayName = "Admin",
	color = Color(50, 0, 255),
	inherit = "operator",
	canTarget = "!superdmin",
})
MK.ranks.Add("superadmin", {
	donator = true,
	displayName = "Super Admin",
	color = Color(255, 0, 0),
	universal = true,
	noRequirePassword = true,
	inherit = "admin",
})
