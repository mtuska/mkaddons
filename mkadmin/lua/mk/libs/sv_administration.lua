// RAWR!

MK.administration = MK.administration or {}

function MK.administration.Kick(target, reason, ply)
	if (!target||!IsValid(target)) then return end
	if (ply!=nil&&!ply.lower) then
		local nick = ply:IsValid() and string.format( "%s(%s)", ply:Nick(), ply:SteamID() ) or "Console"
		target:Kick(string.format("[MK-A] Kicked by %s\n%s", nick, reason or "Kicked from server"))
	else
		target:Kick(reason and "[MK-A] Kicked from server:\n"..reason or "[MK-A] Kicked from server")
	end
end

function MK.administration.Ban(target, length, reason, admin, global)
	if (length&&type(length) == "string") then
		length = MK.util.GetTimeByString(length)
	elseif (length&&type(length) == "number") then
		length = length * 60
	end
	local adminSteam = "Console"
	if (admin&&!admin.lower&&IsValid(admin)) then
		adminSteam = admin:SteamID64()
	end
	MK.bans.Ban(target:SteamID64(), target:Name(), tonumber(length) or 0, reason, adminSteam, global)
	local lenders = target:MK_Data_Get("game_lenders", {})
	for k,steam in pairs(lenders) do
		MK.bans.Ban(steam, tonumber(length) or 0, "An account was banned using your game", adminSteam)
	end
	if (IsValid(target)&&target:IsPlayer()) then
		MK.administration.Kick(target, reason, admin)
	end
	return length
end