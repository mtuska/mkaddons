// RAWR!

local META = MK.meta.Rank or {}
META.__index = META
META.permissions = {}

function META:SetPermission(permission, value)
	self.permissions[permission] = value
end

MK.meta.Rank = META