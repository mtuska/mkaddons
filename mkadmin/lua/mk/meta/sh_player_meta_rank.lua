// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_Rank_SetRank(rank, noSignalRemoveRank)
		if (MK.ranks.list[rank]) then
			if (MK.ranks.list[rank].clearOtherRanks) then
				self:MK_Rank_RemoveRank(true, true)
			end
			if (!noSignalRemoveRank) then
				hook.Run("MK_Ranks.OnPlayerSetRank", self, self:MK_Rank_GetRank(), rank)
			end
			if (MK.ranks.list[rank].universal) then
				self:MK_Data_Set("rank", rank)
			else
				self:MK_Data_Gamemode_Set("rank", rank)
			end
			return true
		end
		return false
	end

	function Player:MK_Rank_RemoveRank(noSignalRemoveRank, isGettingNewRank)
		if (!noSignalRemoveRank) then
			hook.Run("MK_Ranks.OnPlayerRemoveRank", self, self:MK_Rank_GetRank())
		end
		if (self:MK_Data_Get("rank", false)) then
			self:MK_Data_Set("rank", false, nil, isGettingNewRank)
		end
		if (self:MK_Data_Gamemode_Get("rank", false)) then
			self:MK_Data_Gamemode_Set("rank", false, nil, isGettingNewRank)
		end
		return true
	end
end

Player.old_GetUserGroup = Player.GetUserGroup
function Player:MK_Rank_GetRank()
	local rank = self:MK_GameData_Get("fakerank", false) or self:MK_Data_Gamemode_Get("rank", false) or self:MK_Data_Get("rank", false) or MK.ranks.defaultRank
	if (!MK.ranks.Get(rank)) then
		ErrorNoHalt("User '"..self:Name().."' has invalid rank '"..rank.."'!")
		return MK.ranks.defaultRank
	end
	return rank
end

function Player:MK_Rank_GetRealRank()
	local rank = self:MK_Data_Gamemode_Get("rank", false) or self:MK_Data_Get("rank", false) or MK.ranks.defaultRank
	if (!MK.ranks.Get(rank)) then
		ErrorNoHalt("User '"..self:Name().."' has invalid rank '"..rank.."'!")
		return MK.ranks.defaultRank
	end
	return rank
end

Player.old_IsUserGroup = Player.IsUserGroup
function Player:MK_Rank_IsUserGroup(rank)
	if (self:MK_Rank_GetRank() == rank) then
		return true
	end
	return false
end

Player.old_CheckGroup = Player.CheckGroup
function Player:MK_Rank_CheckGroup(rank, real)
	local plyrank = real and self:MK_Rank_GetRealRank() or self:MK_Rank_GetRank()
	if (MK.ranks.IsRank(plyrank, rank)) then
		return true
	end
	return false
end

Player.old_IsAdmin = Player.IsAdmin
function Player:MK_Rank_IsAdmin()
	if (MK.ranks.IsRank(self:MK_Rank_GetRank(), "admin")) then
		return true
	end
	return false
end

Player.old_IsSuperAdmin = Player.IsSuperAdmin
function Player:MK_Rank_IsSuperAdmin()
	if (MK.ranks.IsRank(self:MK_Rank_GetRank(), "superadmin")) then
		return true
	end
	return false
end