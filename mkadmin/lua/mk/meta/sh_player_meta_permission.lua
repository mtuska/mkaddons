// RAWR!

local Player = FindMetaTable("Player")

local function getIndex()
	local index = "permissions"
	return index
end

if (SERVER) then
	function Player:MK_Permission_Set(permission, bool, gmonly)
		if (gmonly) then
			local permissions = self:MK_Data_Gamemode_Get(getIndex(), {})
			permissions[permission] = bool
			self:MK_Data_Gamemode_Set(getIndex(), permissions)
		else
			local permissions = self:MK_Data_Get(getIndex(), {})
			permissions[permission] = bool
			self:MK_Data_Set(getIndex(), permissions)
		end
	end
end

function Player:MK_Permission_Get()
	local permissions = {}
	table.Merge(permissions, self:MK_Data_Get(getIndex(), {}))
	table.Merge(permissions, self:MK_Data_Gamemode_Get(getIndex(), {})) -- Gamemode specific variables override globals
	return permissions
end

function Player:MK_Permission_Has(permission, default)
	return MK.permissions.Query(permission, self, default or false)[self:MK_RealSteamID()]
end