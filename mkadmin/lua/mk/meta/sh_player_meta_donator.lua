// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_Rank_SetDonator(length, global)
		local data = {days = length, startDate = os.time()}
		if (global) then
			if (length == 0) then
				self:MK_Data_Set("donator", false)
			else
				self:MK_Data_Set("donator", data)
			end
		else
			if (length == 0) then
				self:MK_Data_Gamemode_Set("donator", false)
			else
				self:MK_Data_Gamemode_Set("donator", data)
			end
		end
	end
end

function Player:MK_Rank_GetDonator(notGroup)
	if (!notGroup&&MK.ranks.list[self:MK_Rank_GetRank()]&&MK.ranks.list[self:MK_Rank_GetRank()].donator) then
		return true
	else
		local global = self:MK_Data_Get("donator", false)
		local gm = self:MK_Data_Gamemode_Get("donator", false)
		if (global&&(global.days == true||os.time()<(global.startDate+(global.days*24*60*60)))) then
			return global.days == true and true or ((global.startDate+(global.days*24*60*60)) - os.time())
		elseif (gm&&(gm.days == true||os.time()<(gm.startDate+(gm.days*24*60*60)))) then
			return gm.days == true and true or ((gm.startDate+(gm.days*24*60*60)) - os.time())
		end
	end
	return false
end

function Player:MK_Rank_IsDonator(notGroup)
	if (!notGroup&&MK.ranks.list[self:MK_Rank_GetRank()]&&MK.ranks.list[self:MK_Rank_GetRank()].donator) then
		return true
	else
		local global = self:MK_Data_Get("donator", false)
		local gm = self:MK_Data_Gamemode_Get("donator", false)
		if (global&&(global.days == true||os.time()<(global.startDate+(global.days*24*60*60)))) then
			return true
		elseif (gm&&(gm.days == true||os.time()<(gm.startDate+(gm.days*24*60*60)))) then
			return true
		end
	end
	return false
end
Player.IsDonator = Player.MK_Rank_IsDonator
Player.MK_Rank_IsVIP = Player.MK_Rank_IsDonator
Player.IsVIP = Player.MK_Rank_IsDonator