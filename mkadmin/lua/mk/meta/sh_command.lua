// RAWR!

local META = MK.meta.Command or {}
META.__index = META
META.__type = "mkcommand"

function META:AddParam(arg, hint, required, args)
	if (arg) then
		table.insert(self.args, {hint, required, arg.prep(args or {}), arg})
	end
end

function META:SetOpposite(cmd, args)
	self.opposite = {cmd, args}
end

function META:DefaultRank(rank)
	if (MK.ranks.Get(rank)) then
		self.defaultRank = rank
		MK.permissions.Register(self.cmd, rank, self.canResetRank)
	else
		if (SERVER) then
			MK.dev.Log(6, "Rank \""..rank.."\" does not exist! Defaulting to superadmin!")
		end
		self.defaultRank = "superadmin"
		MK.permissions.Register(self.cmd, "superadmin", self.canResetRank)
	end
end

function META:SetHelp(str)
	self.help = str
end

MK.meta.Command = META
