// RAWR!

local Player = FindMetaTable("Player")

Player.CheckGroup = Player.old_CheckGroup
Player.GetUserGroup = Player.old_GetUserGroup
Player.IsUserGroup = Player.old_IsUserGroup
Player.IsAdmin = Player.old_IsAdmin
Player.IsSuperAdmin = Player.old_IsSuperAdmin

MK.config.Add("administration_enabled", true)

MK.config.Add("administration_useranking", true, function(newValue, oldValue)
	if (newValue) then
		print("MKBase Administration is EXPERIMENTAL and may NOT function properly!")
		Player.CheckGroup = Player.MK_Rank_CheckGroup
		Player.GetUserGroup = Player.MK_Rank_GetRank
		Player.IsUserGroup = Player.MK_Rank_IsUserGroup
		Player.IsAdmin = Player.MK_Rank_IsAdmin
		Player.IsSuperAdmin = Player.MK_Rank_IsSuperAdmin
	else
		Player.CheckGroup = Player.old_CheckGroup
		Player.GetUserGroup = Player.old_GetUserGroup
		Player.IsUserGroup = Player.old_IsUserGroup
		Player.IsAdmin = Player.old_IsAdmin
		Player.IsSuperAdmin = Player.old_IsSuperAdmin
	end
end)

MK.config.Add("administration_commandChatPrefix", "!")
/*
MK.config.Add("administration_commandConsolePrefix", false, function(newValue, oldValue)
	if (oldValue) then
		if (istable(oldValue)) then
			for k,v in pairs(oldValue) do
				if (ulx&&v == "ulx") then continue end
				concommand.Remove(v)
			end
		else
			if (ulx&&v == "ulx") then return end
			concommand.Remove(oldValue)
		end
	end
	if (newValue) then
		if (istable(newValue)) then
			for k,v in pairs(newValue) do
				concommand.Add(v, function(ply, cmd, args, argStr)
					MK.commands.Parse(ply, args)
				end, MK.commands.AutoComplete)
			end
		else
			concommand.Add(newValue, function(ply, cmd, args, argStr)
				MK.commands.Parse(ply, args)
			end, MK.commands.AutoComplete)
		end
	end
end)
MK.config.Hook("administration_enabled", "administration_commandConsolePrefix.Update", function(newValue, oldValue)
	if (newValue) then
		if (!ulx) then
			MK.config.Set("administration_commandConsolePrefix", {"mk", "ulx"})
		else
			MK.config.Set("administration_commandConsolePrefix", "mk")
		end
	else
		MK.config.Set("administration_commandConsolePrefix", false)
	end
end)
*/
MK.config.Add("administration_banSuffix", " ~ Appeal at AsylumCentral.com")

/*
MK.ranks.Add("user", {
	displayName = "User",
	color = Color(255, 255, 255),
	canTarget = "!operator",
}, true)
MK.ranks.Add("operator", {
	displayName = "Operator",
	color = Color(0, 255, 0),
	inherit = "user",
	canTarget = "!basicadmin",
})
MK.ranks.Add("basicadmin", {
	displayName = "Basic Admin",
	color = Color(200, 200, 0),
	inherit = "operator",
	canTarget = "!admin",
})
MK.ranks.Add("admin", {
	displayName = "Regular Admin",
	color = Color(50, 0, 255),
	inherit = "basicadmin",
	canTarget = "!trustedadmin",
}, true)
MK.ranks.Add("trustedadmin", {
	donator = true,
	displayName = "Trusted Admin",
	color = Color(0, 0, 0),
	universal = true,
	noRequirePassword = true,
	inherit = "admin",
	canTarget = "!superadmin",
})
MK.ranks.Add("superadmin", {
	donator = true,
	displayName = "Super Admin",
	color = Color(255, 0, 0),
	universal = true,
	noRequirePassword = true,
	inherit = "trustedadmin",
}, true)
*/

//Guest < VIP < Super VIP < Trial Moderator < Moderator < Admin < Super Admin < Owner(s)
/*
MK.ranks.Add("user", {
	displayName = "User",
	color = Color(255, 255, 255),
	canTarget = "!operator",
}, true)
MK.ranks.Add("regular", {
	displayName = "Regular",
	color = Color(255, 255, 255),
	inherit = "user",
	canTarget = "!operator",
}, true)
MK.ranks.Add("operator", {
	displayName = "Operator",
	color = Color(0, 255, 0),
	inherit = "user",
	canTarget = "!moderator",
})
MK.ranks.Add("moderator", {
	displayName = "Moderator",
	color = Color(200, 200, 0),
	inherit = "operator",
	canTarget = "!admin",
})
MK.ranks.Add("admin", {
	displayName = "Admin",
	color = Color(50, 0, 255),
	inherit = "moderator",
	canTarget = "!superadmin",
}, true)
MK.ranks.Add("superadmin", {
	donator = true,
	displayName = "Super Admin",
	color = Color(255, 0, 0),
	universal = true,
	noRequirePassword = true,
	inherit = "admin",
	canTarget = "!servermanager",
}, true)
MK.ranks.Add("servermanager", {
	donator = true,
	displayName = "Server Manager",
	color = Color(255, 255, 255),
	noRequirePassword = true,
	inherit = "superadmin",
	canTarget = {"!owner", "!dev"},
})
MK.ranks.Add("dev", {
	donator = true,
	displayName = "Developer",
	color = Color(120, 0, 175),
	universal = true,
	noRequirePassword = true,
	inherit = "servermanager",
})
MK.ranks.Add("owner", {
	donator = true,
	displayName = "Founder",
	color = Color(204, 0, 204),
	universal = true,
	noRequirePassword = true,
	inherit = "dev",
})
MK.ranks.defaultRank = "user"
*/
