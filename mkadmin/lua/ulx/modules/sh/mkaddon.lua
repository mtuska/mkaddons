// RAWR!

local Player = FindMetaTable("Player")

if (MK&&MK.config.Get("administration_enabled", false)) then
	local update = false
	if (Player.GetUserGroup!=Player.MK_Rank_GetRank) then
		Player.old_GetUserGroup = Player.GetUserGroup
		update = true
	end
	if (Player.IsUserGroup!=Player.MK_Rank_IsUserGroup) then
		Player.old_IsUserGroup = Player.IsUserGroup
		update = true
	end
	if (Player.CheckGroup!=Player.MK_Rank_CheckGroup) then
		Player.old_CheckGroup = Player.CheckGroup
		update = true
	end
	if (Player.IsAdmin!=Player.MK_Rank_IsAdmin) then
		Player.old_IsAdmin = Player.IsAdmin
		update = true
	end
	if (Player.IsSuperAdmin!=Player.MK_Rank_IsSuperAdmin) then
		Player.old_IsSuperAdmin = Player.IsSuperAdmin
		update = true
	end
	if (update) then
		MK.config.Set("administration_enabled", true, false, true)
	end
end