// RAWR!

MK.data = MK.data or {}

file.CreateDir(MK.folderName)

function MK.data.Append(handle, text)
	local folders = string.Explode("/", handle)
	folders[#folders] = nil
	local folder = MK.folderName
	for k,v in pairs(folders) do
		folder = folder.."/"..v
		file.CreateDir(folder)
	end
	local text = text.."\n"
	local handle = MK.folderName.."/"..handle
	if (file.Exists(handle, "DATA")) then
		file.Append(handle, text)
	else
		file.Write(handle, text)
	end
end

function MK.data.SetData(handle, index, value, append)
	local folders = string.Explode("/", handle)
	folders[#folders] = nil
	local folder = MK.folderName
	for k,v in pairs(folders) do
		folder = folder.."/"..v
		file.CreateDir(folder)
	end
	local arr = {}
	if (append) then
		arr = MK.data.GetData(handle, nil, {})
	else
		arr = MK.data.GetData(handle, index, {})
	end
	if (!index) then
		arr = value
	else
		arr[index] = value
	end
	file.Write(MK.folderName.."/"..handle, util.TableToJSON(arr))
end

function MK.data.GetData(handle, index, default)
	if (file.Exists(MK.folderName.."/"..handle, "DATA")) then
		local arr = util.JSONToTable(file.Read(MK.folderName.."/"..handle, "DATA") or "[]") or {}
		if (!istable(arr)) then
			return default
		end
		if (!index) then
			return arr
		else
			return arr[index] or default
		end
	end
	return default
end
