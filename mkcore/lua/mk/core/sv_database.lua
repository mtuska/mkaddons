// RAWR!

MK.db = MK.db or {}
MK.db.list = MK.db.list or {}
MK.db.queue = MK.db.queue or {}

function MK.db.register(str, data)
	if (!data.query) then
		ErrorNoHalt("Expected query function for database type '"..str.."'!\n")
		return
	end
	data.isReady = data.isReady or function()
		return true
	end
	data.title = data.title or "Unknown"
	MK.db.list[str] = MK.db.list[str] or {}
	MK.db.list[str].data = data
	setmetatable(MK.db.list[str], data)
end

function MK.db.get(str)
	return MK.db.list[str]
end

function MK.db.connect(type, connectParams, callback)
	if (!type||!connectParams) then return false end
	if (!MK.db.list[type]) then return false end
	if (MK.db.list[type]:isConnected()) then return false end
	if (!MK.db.disconnect()) then return false end

	MK.db.list[type]:connect(connectParams, function(object)
		hook.Run("MK_Database.Ready", MK.db.list[type])
		if (callback) then
			callback(object)
		end
	end)
end

local function getMeta(mod)
	local mod = mod or MK.config.Get("database_module", "sqlite")
	return MK.db.list[mod]
end

function MK.db.disconnect(meta)
	if (!getMeta(meta)||!getMeta(meta).disconnect) then return true end
	local result = getMeta(meta):disconnect()
	if (!result) then return true end
	return false
end

function MK.db.isConnected(meta)
	return getMeta(meta) && getMeta(meta):isConnected()
end

function MK.db.isReady(meta)
	return getMeta(meta) && getMeta(meta):isReady()
end

function MK.db.ping(meta)
	if (!getMeta(meta)) then return false end
	return getMeta(meta):ping()
end

function MK.db.convertDataType(value, meta)
	if (type(value) == "string") then
		return "'"..MK.db.escape(value, meta).."'"
	elseif (type(value) == "table") then
		return "'"..MK.db.escape(util.TableToJSON(value), meta).."'"
	end

	return value
end

function MK.db.escape(value, meta)
	if (!getMeta(meta)) then return value end
	return getMeta(meta):escape(value)
end

function MK.db.query(query, callback, queue, meta)
	if (!MK.config.Get("database_module", false)) then
		if (queue) then
			MK.db.queue[queue] = {query, callback, meta}
			MK.dev.Log(5, "Query Queued 1: "..queue.." : '"..query.."'")
		end
		return false
	end
	if (!MK.db.isReady(meta)) then
		if (queue) then
			MK.db.queue[queue] = {query, callback, meta}
			MK.dev.Log(5, "Query Queued 2: "..queue.." : '"..query.."'")
		end
		return false
	end
	local function errorCallback(queue, callback, query, fault)
		if (fault == "Lost connection to MySQL server during query") then
			MK.db.ping()
			MK.db.query(query, callback, queue)
		end
	end
	hook.Run("MK_Database.Query", query, meta)
	if (MK.config.Get("development")) then
		getMeta(meta):query(query, function(data, lastInsert, obj)
			if (queue) then
				MK.dev.Log(5, "Queued Query Callback: "..queue)
			end
			if (callback) then
				callback(data, lastInsert, obj)
			end
		end, function(query, fault)
			if (queue) then
				errorCallback(queue, callback, query, fault)
			end
		end)
	else
		getMeta(meta):query(query, callback, function(query, fault)
			if (queue) then
				errorCallback(queue, callback, query, fault)
			end
		end)
	end
end

function MK.db.insertTable(value, callback, tableName, queue, meta)
	local query = "INSERT INTO "..(tableName or "playerData").." ("
	local keys = {}
	local values = {}

	for k, v in pairs(value) do
		keys[#keys + 1] = k
		values[#keys] = MK.db.convertDataType(v)
	end

	query = query..table.concat(keys, ", ")..") VALUES ("..table.concat(values, ", ")..")"
	MK.db.query(query, callback, queue, meta)
end

function MK.db.updateTable(value, callback, condition, tableName, queue, meta)
	local query = "UPDATE "..(tableName or "playerData").." SET "
	local changes = {}
	for k, v in pairs(value) do
		changes[#changes + 1] = k.." = "..MK.db.convertDataType(v)
	end

	query = query..table.concat(changes, ", ")..((condition and " WHERE "..condition) or "")
	MK.db.query(query, callback, queue, meta)
end

function MK.db.insertOrUpdateTable(value, callback, tableName, queue, meta)
	local query = ""
	local condition = nil
	local keys = {}
	local values = {}

	for k, v in pairs(value) do
		keys[#keys + 1] = k
		values[#keys] = MK.db.convertDataType(v)
	end

	local meta = meta or MK.config.Get("database_module", "sqlite")
	if (meta == "sqlite") then
		query = "UPDATE "..(tableName or "playerData").." SET "
		local changes = {}
		for k, v in pairs(value) do
			changes[#changes + 1] = k.." = "..MK.db.convertDataType(v)
			if (k == "vid") then
				condition = "vid = "..MK.db.convertDataType(v)
			end
		end
		query = query..table.concat(changes, ",")..((condition and " WHERE "..condition) or "")
		query = query..";INSERT INTO "..(tableName or "playerData").." ("
		query = query..table.concat(keys, ",")..") "
		query = query.."SELECT "..table.concat(values, ",").." WHERE (Select Changes() = 0)"
	else
		query = "INSERT INTO "..(tableName or "playerData").." ("
		query = query..table.concat(keys, ",")..") VALUES ("..table.concat(values, ",")..")"
		query = query.." ON DUPLICATE KEY UPDATE "
		local changes = {}
		for k,v in pairs(keys) do
			changes[#changes + 1] = v.."=VALUES("..v..")"
		end
		query = query..table.concat(changes, ",")
	end
	MK.db.query(query, callback, queue, meta)
end

hook.Add("MK_Database.Ready", "MK_Database.Ready.Log", function(meta)
	MK.dev.Log(5, "<DATABASE> MKCore has connected to the database via '"..meta.title.."'!")
end)

hook.Add("MK_Database.Ready", "MK_Database.Ready.SendQueue", function(meta)
	if (meta == "none") then return end
	print("MKCore has connected to the database!")
	for k,v in pairs(MK.db.queue) do
		local query, callback, meta = v[1], v[2], v[3]
		MK.db.queue[k] = nil
		MK.db.query(query, callback, k, meta)
	end
end)

hook.Add("MK_Database.Query", "MK_Database.Query.Backup", function(query, meta)
	if (meta == "none") then return end
	query = query or "Umm, query is gone?"
	meta = meta or MK.config.Get("database_module", "unknown")
	MK.dev.Log(5, "<DATABASE> '"..meta.."' Query: "..query)
end)
