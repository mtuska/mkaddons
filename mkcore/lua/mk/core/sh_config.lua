// RAWR!

MK.config = MK.config or {}
MK.config.list = MK.config.list or {}
MK.config.buffer = MK.config.list

if (!MK.meta.Config) then
	MK.util.IncludeInternal("meta/sh_config.lua")
	if (MK.meta.Config.SetValue) then
		print("Config Meta")
	else
		print("No Config Meta")
	end
end

if (SERVER) then
	util.AddNetworkString("MK_ConfigInitial")
	util.AddNetworkString("MK_ConfigUpdate")
end

function MK.config.Get(parameter, default)
	return MK.config.list[parameter] and MK.config.list[parameter]:Get() or default
end

function MK.config.Set(parameter, value, save, force, data)
	if (!MK.config.list[parameter]) then
		ErrorNoHalt("Couldn't find config parameter: "..parameter..":"..tostring(value).."\n")
		return
	end
	MK.dev.Log(7, "Updating parameter: "..parameter..":"..tostring(value))
	if (data) then
		MK.config.list[parameter]:SetData(data)
	end
	for k,v in pairs(MK.config.list[parameter].hooks) do
		v(MK.config.list[parameter]:GetValue(), value)
	end
	MK.config.list[parameter]:SetValue(value, force)
	if (SERVER) then
		if (save) then
			MK.data.SetData("data/configurations.txt", parameter, MK.config.list[parameter], true)
		end
		if (!MK.config.list[parameter].noNetworking) then
			net.Start("MK_ConfigUpdate")
				net.WriteTable({parameter, value, false, force, data})
			net.Broadcast()
		end
	end
	if (CLIENT) then
		if (save && MK.config.list[parameter]:GetData().clientMalleable) then
			MK.data.SetData("data/configurations.txt", parameter, MK.config.list[parameter], true)
		end
	end
end

function MK.config.Add(parameter, value, callback, data, noNetworking)
	if (MK.config.list[parameter]) then
		if (SERVER) then
			error("Configuration parameter already created! Use Set to update its value! '"..parameter.."'")
		end
		return
	end
	MK.dev.Log(7, "Adding config: "..parameter..":"..tostring(value))
	if (parameter==nil||value==nil) then
		ErrorNoHalt("Couldn't add configuration, missing parameter or value! '"..parameter.."'")
		return
	end
	local config = {}
	config.value = value
	config.default = value
	config.callback = callback or function() return true end
	config.data = data or {}
	config.noNetworking = noNetworking or false
	config.hooks = {}
	setmetatable(config, MK.meta.Config)
	config:SetValue(value, true)
	MK.config.list[parameter] = config
end

function MK.config.Hook(parameter, uid, callback)
	if (!MK.config.list[parameter]) then
		error("Couldn't find config parameter: '"..parameter.."' to create hook '"..uid.."'\n")
	end
	callback(MK.config.list[parameter]:GetValue())
	MK.config.list[parameter].hooks[uid] = callback
end

function MK.config.RemoveHook(parameter, uid)
	if (!MK.config.list[parameter]) then
		error("Couldn't find config parameter: "..parameter..":"..tostring(value).."\n")
	end
	MK.config.list[parameter].hooks[uid] = nil
end

if (CLIENT) then
	net.Receive("MK_ConfigInitial", function()
		local configs = net.ReadTable()
		for k,tbl in pairs(configs) do
			MK.config.Set(tbl[1], tbl[2], tbl[3], tbl[4], tbl[5])
		end
		hook.Run("MK_Configuration.Loaded")
	end)
	net.Receive("MK_ConfigUpdate", function()
		local tbl = net.ReadTable()
		MK.config.Set(tbl[1], tbl[2], tbl[3], tbl[4], tbl[5])
	end)
end

hook.Add("MK_Configuration.Loaded", "MK_Configuration.Loaded.LoadConfigData", function()
	local configurations = MK.data.GetData("data/configurations.txt", nil, {})
	for parameter,config in pairs(configurations) do
		MK.config.Set(parameter, config.value, false, true, config.data)
	end
end)
