// RAWR!

function MK.time() // Return a universal time stamp
	if (SERVER) then
		return os.time()
	else
		return os.time() - (MK.timeDifference or 0)
	end
end

if (SERVER) then
	util.AddNetworkString("MK_TimeSync")
	hook.Add("MK_Player.Ready", "MK_Player.Ready.Time", function(ply)
		net.Start("MK_TimeSync")
			net.WriteInt(os.time(), 32)
		net.Send(ply)
	end)
	timer.Create("MK_TimeSync", 150, 0, function()
		net.Start("MK_TimeSync")
			net.WriteInt(os.time(), 32)
		net.Broadcast()
	end)
else
	net.Receive("MK_TimeSync", function(len)
		MK.serverTime = net.ReadInt(32)
		MK.receiveTime = os.time()
		MK.timeDifference = MK.receiveTime - MK.serverTime
	end)
end