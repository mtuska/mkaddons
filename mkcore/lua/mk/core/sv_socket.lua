// RAWR!

if (true) then return end

if (!pcall(require, "bromsock")) then
	if (system.IsWindows()) then
		//error("Server is missing VC++ redistributables!")
	else
		//error("Server is missing binaries for bromsock!")
	end
	return false
end

MK.socket = MK.socket or {}
MK.socket.SiteIP = MK.socket.SiteIP or {"208.146.35.5", ""}
MK.socket.SecurityKey = MK.socket.SecurityKey or ""
MK.socket.Server = MK.socket.Server or BromSock()
MK.socket.Server:Listen(1337)

MK.socket.Server:SetCallbackAccept(function(ServerSock, ClientSock)
	ClientSock:SetCallbackReceive(function(Sock, Packet)
		IP = Sock:GetIP()

		local JSON = string.TrimRight(Packet:ReadStringAll(), "\1")

		MK.dev.Log(7, "{Socket} Received confirmed payload from " .. IP .. " Containing: " .. JSON)

		Sock:Disconnect()

		MK.socket.Receive(JSON, IP)
	end)
	IP = ClientSock:GetIP()
	if (!table.HasValue(MK.socket.SiteIP, IP)) then
		MK.dev.Log(2, "{Socket} Received payload from a different IP(" .. IP .. ") than what the web IP is set, please make sure your IP is set correctly in web side general settings, if it is, this is probably an attempted hack.")
		ClientSock:Close()
	else
		ClientSock:SetTimeout(2000)
		ClientSock:ReceiveUntil("\1")
	end
	ServerSock:Accept()
end)
MK.socket.Server:Accept()

function MK.socket.Receive(JSON, IP)
	local T = util.JSONToTable(JSON)

	if !istable(T) then
		MK.dev.Log(2, "{Socket} Received a payload that wasn't proper JSON, possible attempt to hack, IP: " .. IP)
		return
	end

	if T.skey != MK.socket.SecurityKey then
		MK.dev.Log(2, "{Socket} Received a payload that has an incorrect security key, possible attempt to hack, IP: " .. IP)
		return
	end

	//Neutron.Commands.Run(JSON, T.type)
end
