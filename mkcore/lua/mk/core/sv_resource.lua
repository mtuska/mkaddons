// RAWR!

local function addDir(dir, resDir)
	local f, d = file.Find(dir..'/'..resDir..'/*', 'GAME')

	for k, v in pairs(f) do
		resource.AddSingleFile(resDir..'/'..v)
	end

	for k, v in pairs(d) do
		addDir(dir, resDir..'/'..v)
	end
end

local files, folders = file.Find("addons/mk*", "GAME")
for _,dir in pairs(folders) do
	addDir("addons/"..dir, "materials")
	addDir("addons/"..dir, "models")
	addDir("addons/"..dir, "particles")
	addDir("addons/"..dir, "resource")
	addDir("addons/"..dir, "sound")
end
