// RAWR!

MK.dev = MK.dev or {}

// Verbosity Levels
// 1 Fatal
// 2 Error
// 3 Warning
// 4 idk
// 5 Log
// 6 Verbose
// 7 VeryVerbose
// 8 VeryVeryVerbose (include files, resources)

function MK.dev.Log(verbosity, text)
	//if (!MK.config.Get("development", false)) then return end

	if (verbosity <= MK.config.Get("development_verbosity", 2)) then
		MsgC(Color(100,255,100), "[MK] ", Color(255,0,0), os.date("%X"), Color(255,255,255), ": ", Color(125,125,125), verbosity, Color(255,255,255), " >> ", text, "\n")
		local log = ""..os.date("%X")..": "..verbosity.." >> "
		log = log..text

		MK.data.Append("devlog/"..os.date("%m-%d-%Y")..".txt", log)
		if (verbosity < 2) then
			//error(text)
			ErrorNoHalt(text)
		end
	end
end
