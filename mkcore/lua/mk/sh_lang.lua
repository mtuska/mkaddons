// RAWR!

MK.lang = MK.lang or {}
MK.lang.buffer = MK.lang.buffer or {}

function MK.lang.Add(key, value, language)
	language = language or "english"
	
	MK.lang.buffer[language] = MK.lang.buffer[language] or {}
	MK.lang.buffer[language][key] = value
end

function MK.lang.GetAll()
	return MK.lang.buffer
end

function MK.lang.Get(key, ...)
	local language = MK.config.language or "english"
	if (MK.lang.buffer[language] and MK.lang.buffer[language][key]) then
		return string.format(MK.lang.buffer[language][key], ...)
	else
		return "<missing "..key..">"
	end
end

function MK.lang.GetLower(key, ...)
	return string.lower(MK.lang.Get(key, ...))
end