// RAWR!

function MK.nextbots.TraceAirNode(pos)
    if (!util.IsInWorld(pos)) then return false end

	local trace = {}
	trace.start = pos
	trace.endpos = trace.start + Vector(0, 0, 64)
	trace.mask = MASK_SOLID
	local tr = util.TraceLine(trace)

    //BroadcastLua("print('Did hit? 1')")
	if (tr.Hit || tr.HitNoDraw || tr.HitSky || tr.HitWorld) then return false end
	//if (tr.Fraction != 0) then return false end

	return true
end

function MK.nextbots.TryAirNode(pos)
    //info_node_air
    if (!util.IsInWorld(pos)) then return false end

    if (!MK.nextbots.TraceAirNode(pos)) then
        return false
    end
    table.insert(MK.nextbots.airnodes, pos)
    //table.insert(MK.nextbots.nodes, 0)
    MK.nextbots.TryAirNode(pos+Vector(0,0,64)) // Add 64, info_node_air must have at least 64 units of empty space above them to be valid
    return true
end
/*
hook.Add("InitPostEntity", "InitPostEntity.MK_Nextbot_Flying", function()
    if (navmesh.IsLoaded()) then
		//for k,area in pairs(navmesh.GetAllNavAreas()) do
            //MK.nextbots.TryAirNode(area:GetCenter()+Vector(0,0,64))
        //end
	end
end)
*/
concommand.Add("mk_airnodes", function(ply)
    table.Empty(MK.nextbots.airnodes)
	for k,area in RandomPairs(navmesh.GetAllNavAreas()) do
        MK.nextbots.TryAirNode(area:GetCenter()+Vector(0,0,64))
    end
    ply:ChatPrint(table.Count(MK.nextbots.airnodes)..' nodes!')
    /*
    local ent = ents.Create("mk_info_node_air") // https://developer.valvesoftware.com/wiki/Info_node_air
    if (!IsValid(ent)) then return end
    ent:SetPos(ply:GetPos())
    ent:Spawn()

    ply:ChatPrint('Spawned node!')*/

    /*for k,v in pairs(ents.FindByClass("mk_info_node_air")) do
        if (IsValid(v)) then
            v:Remove()
            ply:ChatPrint('Removed node!')
        end
    end*/
end)
