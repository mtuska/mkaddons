// RAWR!

hook.Add("MK_Player.SetupVars", "MK_Player.SetupVars.Initial", function(ply)
	//----------------------------------------------------------------------------------------------------\\

	// index, default, save, noNetwork, broadcast
	ply:MK_GameData_Create("fallbackDatabase", nil, false, true, false)
	ply:MK_GameData_Create("clientInitialized", false, false, true, false)
	ply:MK_GameData_Create("loadedData", false, false, true, false)

	//----------------------------------------------------------------------------------------------------\\

	// index, default, noNetwork, broadcast
	ply:MK_Data_Create(GAMEMODE.FolderName, {}, false, true)
	ply:MK_Data_Create("aliases", {}, false, true)
	ply:MK_Data_Create("ipaddresses", {}, true, false)
end)

hook.Add("MK_Player.SetData", "MK_Player.SetData.DevLog", function(ply, index, value)
	local text = value
	if (type(value) == "table") then
		text = util.TableToJSON(value)
	end
	MK.dev.Log(6, string.format("<MK_Player.SetData> %s | %s has updated data: %s >> %s", ply:Nick(), ply:MK_RealSteamID(), index, text))
end)

hook.Add("MK_Player.SetGameData", "MK_Player.SetGameData.DevLog", function(ply, index, value, save)
	local text = value
	if (type(value) == "table") then
		text = util.TableToJSON(value)
	end
	MK.dev.Log(6, string.format("<MK_Player.SetGameData> %s | %s has updated game data: %s >> %s", ply:Nick(), ply:MK_RealSteamID(), index, text))
end)
