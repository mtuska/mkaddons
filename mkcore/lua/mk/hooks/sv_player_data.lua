// RAWR!

util.AddNetworkString("MK_Player.Ready")

hook.Add("MK_Player.Connected", "MK_Player.Connected.LoadData", function(name, steamid, address, userid, bot, index)
	//MK.players.external.GetPlayer(steamid, function(target)
	//
	//end)
end)

hook.Add("PlayerInitialSpawn", "MK_PlayerInitialSpawn.LoadData", function(ply)
	ply:MK_DB_LoadData(false) // Load player's data from fallback database and then external database
end)

local function networkPlayer(ply)
	for k,v in pairs(ply.mk.data) do
		ply:MK_Data_Set(k,v.value,nil,true,true)
	end
	for k,v in pairs(ply.mk.gameData) do
		ply:MK_GameData_Set(k,v.value,nil,true,true)
	end

	if (ply:MK_GameData_Get("fallbackDatabase", false) && MK.config.Get("database_fallbackModule", "sqlite") != "none") then
		ply:MK_SendChat(Color(100,255,100), "[MK-Data] ", Color(255,255,255), "Data was not loaded from proper database, using local data. Any changes will not be saved")
	end

	hook.Run("MK_Player.Ready", ply)
	net.Start("MK_Player.Ready")
		net.WriteEntity(ply)
	net.Broadcast()
end

hook.Add("MK_Player.LoadedData", "MK_Player.LoadedData", function(ply, newUser)
	ply:MK_GameData_Set("loadedData", true)

	if (ply:MK_GameData_Get("clientInitialized", false)) then
		networkPlayer(ply)
	end

	if (ply:IsBot()) then
		MK.dev.Log(7, string.format("<MK_Player.LoadedData> %s bot has joined", ply:Nick()))
	elseif (newUser) then
		MK.dev.Log(7, string.format("<MK_Player.LoadedData> %s | %s has created their player data: %s", ply:Nick(), ply:MK_RealSteamID(), util.TableToJSON(ply:MK_GameData_Get()) or ""))
	else
		MK.dev.Log(7, string.format("<MK_Player.LoadedData> %s | %s has loaded their player data: %s", ply:Nick(), ply:MK_RealSteamID(), util.TableToJSON(ply:MK_GameData_Get()) or ""))
	end
end)

hook.Add("MK_Player.ClientInitialized", "MK_Player.ClientInitialized.SendData", function(ply)
	ply:MK_GameData_Set("clientInitialized", true)

	local configs = {}
	for k,v in pairs(MK.config.list) do
		if (v.noNetworking) then continue end
		table.insert(configs, {k, v:GetValue(), false, true, v.data})
	end
	net.Start("MK_ConfigInitial")
		net.WriteTable(configs)
	net.Send(ply)

	if (ply:MK_GameData_Get("loadedData", false)) then
		networkPlayer(ply)
	end
end)
