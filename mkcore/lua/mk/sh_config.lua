// RAWR!

MK.config.Add("development", false)
MK.config.Add("development_verbosity", 2)

if (CLIENT) then
	MK.config.Add("show_versions", false, nil, {clientMalleable = true}, true)
end

if (SERVER) then
	MK.config.Add("steamKey", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", nil, nil, true)

	MK.config.Add("database_module", "none", function(newValue, oldValue, data)
		if (!newValue||newValue=="none") then return true end
		if (oldValue) then
			MK.db.disconnect(oldValue)
		end
		MK.db.connect(newValue, data or {})
	end, {
		host = "",
		port = 3306,
		database = "",
		username = "",
		password = "",
	}, true)

	MK.config.Add("generatePassword", false, function(newValue, oldValue)
		if (newValue) then
			local pass = MK.util.GenPassword()
			RunConsoleCommand("sv_password",pass)
			MsgN("Changing the server's Password to: \""..pass.."\"")
		end
	end, nil, true)
	//RunConsoleCommand("hostname", "MKBase - Hostname has not been reset!")
end
