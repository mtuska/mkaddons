// RAWR!

local Player = FindMetaTable("Player")

function Player:MK_Data_Create(index, default, noNetwork, broadcast)
	if (self.mk.data[index]) then
		ErrorNoHalt("The specified MK Data '"..index.."' already exists for this player!")
		debug.Trace()
		return
	end
	self.mk.data[index] = self.mk.data[index] or {noNetwork = noNetwork or false, broadcast = broadcast or false}
	self.mk.data[index].value = default
end

if (SERVER) then
	util.AddNetworkString("MK_Player.SetData")
	function Player:MK_Data_Set(index, value, noNetwork, noHook, noMerge)
		self.mk = self.mk or {}
		self.mk.data = self.mk.data or {}
		if (!self.mk.data[index]) then
			error("The specified MK Data '"..index.."' does not exist for '"..self:Name().."'")
		end
		local difftable = {}
		local merge = false
		if (type(self.mk.data[index].value) == "table" && type(value) == "table" && !self:IsBot()) then
			difftable = table.GetDifference(self.mk.data[index].value, value, "_nil")
			if (table.Count(difftable) < 1) then
				return // Don't waste bandwidth on no data
			end
			if (!noMerge) then
				merge = true
			end
		end

		self.mk.data[index].value = value

		if (!noNetwork&&!self.mk.data[index].noNetwork) then
			net.Start("MK_Player.SetData")
				if (merge) then
					net.WriteString(util.TableToJSON({index, difftable}))
				else
					net.WriteString(util.TableToJSON({index, value}))
				end
				net.WriteEntity(self)
				net.WriteBool(merge)
			if (self.mk.data[index].broadcast) then
				net.Broadcast()
			else
				net.Send(self)
			end
		end
		if (!noHook) then
			hook.Run("MK_Player.SetData", self, index, value)

			local savetable = {}
			if (table.Count(difftable) > 0) then
				savetable = difftable
			else
				savetable[index] = value
			end
			if (!self:IsBot() && table.Count(savetable) > 0) then
				local typ = 0 // type 0 is data
				local function prepareQueryTable(tbl, index)
					local response = {}
					if (type(tbl) != "table") then
						response[index] = tbl
					else
						for k,v in pairs(tbl) do
							local id = index..";"..k
							if (tonumber(k) != nil) then
								response[index] = tbl
							elseif (type(v) == "table") then
								table.Merge(response, prepareQueryTable(v, id))
							elseif (v == "_nil") then
								continue
							else
								response[id] = v
							end
						end
					end
					return response
				end
				local tbl = prepareQueryTable(savetable, self:MK_RealSteamID64()..";"..typ..";"..index)
				local queryTemplate = {}
				queryTemplate["steam"] = self:MK_RealSteamID64()
				queryTemplate["type"] = typ
				for k,v in pairs(tbl) do
					local query = table.Copy(queryTemplate)
					query["vid"] = k
					query["value"] = v
					MK.db.insertOrUpdateTable(query, nil, "playerData", "updatePlayerData_"..k.."_"..self:MK_RealSteamID64())
					MK.db.insertOrUpdateTable(query, nil, "playerData", "updatePlayerData_"..k.."_"..self:MK_RealSteamID64().."_fallback", MK.config.Get("database_fallbackModule", "sqlite"))
				end
			end
		end
	end

	function Player:MK_Data_Gamemode_Set(index, value)
		local data = self:MK_Data_Get(gmod.GetGamemode().FolderName, {})
		data[index] = value
		self:MK_Data_Set(gmod.GetGamemode().FolderName, data)
	end
end

if (CLIENT) then
	net.Receive("MK_Player.SetData", function(len)
		local tab = util.JSONToTable(net.ReadString())
		local ply = net.ReadEntity() or LocalPlayer()
		local merge = net.ReadBool()
		if (!tab||!IsValid(ply)) then return end
		ply.mk = ply.mk or {}
		ply.mk.data = ply.mk.data or {}
		if (merge) then
			if (!ply.mk.data[tab[1]]) then
				ply.mk.data[tab[1]] = {}
				ply.mk.data[tab[1]].value = {}
			end
			table.Merge(ply.mk.data[tab[1]].value, tab[2])
			local function nilValues(tbl, nilType)
				for k,v in pairs(tbl) do
					if (type(v)=="table") then
						nilValues(v, nilType)
					elseif (v == nilType) then
						tbl[k] = nil
					end
				end
			end
			nilValues(ply.mk.data[tab[1]].value, "_nil")
		elseif (!tab[2]) then
			ply.mk.data[tab[1]] = nil
		else
			ply.mk.data[tab[1]] = {}
			ply.mk.data[tab[1]].value = tab[2]
		end
		hook.Run("MK_Player.SetData", ply, tab[1], tab[2])
		//print("MKData: "..tab[1])
	end)
end

function Player:MK_Data_Get(index, default)
	if (self.mk&&self.mk.data) then
		if (!index) then
			return table.Copy(self.mk.data)
		end
		if (self.mk.data[index]&&self.mk.data[index].value) then
			if (type(self.mk.data[index].value) == "table") then
				return table.Copy(self.mk.data[index].value)
			end
			return self.mk.data[index].value
		end
	end
	return default
end

function Player:MK_Data_Gamemode_Get(index, default)
	local data = self:MK_Data_Get(gmod.GetGamemode().FolderName, {})
	if (data&&data[index]) then
		return data[index]
	end
	return default
end
