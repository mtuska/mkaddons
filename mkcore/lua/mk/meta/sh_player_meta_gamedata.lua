// RAWR!

local Player = FindMetaTable("Player")

function Player:MK_GameData_Create(index, default, save, noNetwork, broadcast)
	if (self.mk.gameData[index]) then
		ErrorNoHalt("The specified MK Gamedata '"..index.."' already exists for this player!")
		debug.Trace()
		return
	end
	self.mk.gameData[index] = self.mk.gameData[index] or {save = save or false, noNetwork = noNetwork or false, broadcast = broadcast or false}
	self.mk.gameData[index].value = default
end

if (SERVER) then
	util.AddNetworkString("MK_Player.SetGameData")
	function Player:MK_GameData_Set(index, value, noNetwork, noHook, noMerge)
		self.mk = self.mk or {}
		self.mk.gameData = self.mk.gameData or {}
		if (!self.mk.gameData[index]) then
			if (index == "locationThink") then return end
			error("The specified MK Gamedata '"..index.."' does not exist for '"..self:Name().."'")
		end
		local difftable = {}
		local merge = false
		if (type(self.mk.gameData[index].value) == "table" && type(value) == "table" && !self:IsBot()) then
			difftable = table.GetDifference(self.mk.gameData[index].value, value, "_nil")
			if (table.Count(difftable) < 1) then
				return // Don't waste bandwidth on no data
			end
			if (!noMerge) then
				merge = true
			end
		end

		self.mk.gameData[index].value = value

		if (!noNetwork&&!self.mk.gameData[index].noNetwork) then
			net.Start("MK_Player.SetGameData")
				if (merge) then
					net.WriteString(util.TableToJSON({index, difftable}))
				else
					net.WriteString(util.TableToJSON({index, value}))
				end
				net.WriteEntity(self)
				net.WriteBool(merge)
			if (self.mk.gameData[index].broadcast) then
				net.Broadcast()
			else
				net.Send(self)
			end
		end
		if (!noHook) then
			hook.Run("MK_Player.SetGameData", self, index, value, self.mk.gameData[index].save)

			local savetable = {}
			if (self.mk.gameData[index].save) then
				if (table.Count(difftable) > 0) then
					savetable = difftable
				else
					savetable[index] = value
				end
			end
			if (!self:IsBot() && table.Count(savetable) > 0) then
				local typ = 1 // type 0 is data
				local function prepareQueryTable(tbl, index)
					local response = {}
					if (type(tbl) != "table") then
						response[index] = tbl
					else
						for k,v in pairs(tbl) do
							local id = index..";"..k
							if (tonumber(k) != nil) then
								response[index] = tbl
							elseif (type(v) == "table") then
								table.Merge(response, prepareQueryTable(v, id))
							elseif (v == "_nil") then
								continue
							else
								response[id] = v
							end
						end
					end
					return response
				end
				local tbl = prepareQueryTable(savetable, self:MK_RealSteamID64()..";"..typ..";"..index)
				local queryTemplate = {}
				queryTemplate["steam"] = self:MK_RealSteamID64()
				queryTemplate["type"] = typ
				for k,v in pairs(tbl) do
					local query = table.Copy(queryTemplate)
					query["vid"] = k
					query["value"] = v
					MK.db.insertOrUpdateTable(query, nil, "playerData", "updatePlayerGameData_"..k.."_"..self:MK_RealSteamID64())
					MK.db.insertOrUpdateTable(query, nil, "playerData", "updatePlayerGameData_"..k.."_"..self:MK_RealSteamID64().."_fallback", MK.config.Get("database_fallbackModule", "sqlite"))
				end
			end
		end
	end

	function Player:MK_GameData_Gamemode_Set(index, value)
		local data = self:MK_GameData_Get(gmod.GetGamemode().FolderName, {})
		data[index] = value
		self:MK_GameData_Set(gmod.GetGamemode().FolderName, data)
	end
end

if (CLIENT) then
	net.Receive("MK_Player.SetGameData", function(len)
		local tab = util.JSONToTable(net.ReadString())
		local ply = net.ReadEntity() or LocalPlayer()
		local merge = net.ReadBool()
		if (!tab||!IsValid(ply)) then return end
		ply.mk = ply.mk or {}
		ply.mk.gameData = ply.mk.gameData or {}
		if (merge) then
			if (!ply.mk.gameData[tab[1]]) then
				ply.mk.gameData[tab[1]] = {}
				ply.mk.gameData[tab[1]].value = {}
			end
			table.Merge(ply.mk.gameData[tab[1]].value, tab[2])
			local function nilValues(tbl, nilType)
				for k,v in pairs(tbl) do
					if (type(v)=="table") then
						nilValues(v, nilType)
					elseif (v == nilType) then
						tbl[k] = nil
					end
				end
			end
			nilValues(ply.mk.gameData[tab[1]].value, "_nil")
		elseif (!tab[2]) then
			ply.mk.gameData[tab[1]] = nil
		else
			ply.mk.gameData[tab[1]] = {}
			ply.mk.gameData[tab[1]].value = tab[2]
		end
		hook.Run("MK_Player.SetGameData", ply, tab[1], tab[2])
		//print("MKGameData: "..tab[1])
	end)
end

function Player:MK_GameData_Get(index, default)
	if (self.mk&&self.mk.gameData) then
		if (!index) then
			return table.Copy(self.mk.gameData)
		end
		if (self.mk.gameData[index]&&self.mk.gameData[index].value) then
			if (type(self.mk.gameData[index].value) == "table") then
				return table.Copy(self.mk.gameData[index].value)
			end
			return self.mk.gameData[index].value
		end
	end
	return default
end

function Player:MK_GameData_Gamemode_Get(index, default)
	local data = self:MK_GameData_Get(gmod.GetGamemode().FolderName, {})
	if (data&&data[index]) then
		return data[index]
	end
	return default
end
