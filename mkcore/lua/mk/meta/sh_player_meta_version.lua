// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_DisplayVersions()
		self:SendLua("LocalPlayer():MK_DisplayVersions()")
	end
else
	function Player:MK_DisplayVersions()
		if (MK.config.Get("show_versions", false)) then
			self:MK_SendChat(Color(100,255,100), "[MK-VC] ", Color(255,255,255), "MKCore version "..MK.version.." loaded!")

			for k,v in pairs(MK) do
				if (istable(v)&&v.version) then
					local title = v.title or k:sub(1,1):upper()..k:sub(2)
					self:MK_SendChat(Color(100,255,100), "[MK-VC] ", Color(255,255,255), "MK "..title.." "..v.version.." loaded!")
				end
			end
		end
	end
end
