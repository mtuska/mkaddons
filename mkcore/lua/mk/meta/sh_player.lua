// RAWR!

MK.meta.Player = MK.meta.Player or {}
MK.meta.Player.__index = MK.meta.Player
MK.meta.Player.__type = "MK_Player"

local Player = FindMetaTable("Player")
for k,v in pairs(Player) do
	if (isfunction(v)&&k:find("MK_")) then
		MK.meta.Player[k] = v
	end
end
Player = nil
Player = MK.meta.Player

function Player:__tostring()
	return "Player ["..(self.steamid or 0).."]["..self.name or "".."]"
end

function Player:__eq(other)
	return self:SteamID64() == other:SteamID64()
end

function Player:IPAddress()
	return self.ip or ""
end

function Player:IsBot()
	return false
end

function Player:IsPlayer()
	return false
end

function Player:SteamID()
	return self.steamid or ""
end
Player.MK_RealSteamID = Player.SteamID

function Player:SteamID64()
	return self.steam or ""
end
Player.MK_RealSteamID64 = Player.SteamID64

function Player:Name()
	return self.name or ""
end
Player.old_Nick = Player.Name
Player.GetName = Player.Name
Player.Nick = Player.Name

MK.meta.Player = Player