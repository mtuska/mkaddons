// RAWR!

local META = MK.meta.Version or {}
META.__index = META
META.minor = 0
META.major = 0

META.__tostring = function(obj) return "v"..obj.major.."."..obj.minor end

META.__eq = function(a, b) return a.major == b.major && a.minor == b.minor end
META.__lt = function(a, b) return a.major <= b.major && a.minor < b.minor end
META.__le = function(a, b) return a.major <= b.major && a.minor <= b.minor end

MK.meta.Version = META

MK.CreateVersion = function(major, minor)
	local obj = {}
	obj.major = major
	obj.minor = minor
	setmetatable(obj, META)
	return obj
end