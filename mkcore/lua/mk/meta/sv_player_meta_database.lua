// RAWR!

local Player = FindMetaTable("Player")

function Player:MK_DB_LoadData(noFallback, callback)
	if (!self.mk) then
		self:MK_InitializePlayer() // Run before the player's data is loaded
	end

	if (self:IsBot()) then
		hook.Run("MK_Player.LoadedData", self)
		return
	end

	if (!noFallback) then

	end

	local sid = MK.db.convertDataType(self:MK_RealSteamID64())
	local query = "SELECT type, vid, value FROM playerData WHERE steam = "..sid

	local function playerData(data)
		if (IsValid(self) and data) then
			for k,v in pairs(data) do
				local vid = string.Split(v.vid, ";")
				table.remove(vid, 1) // steamid64
				table.remove(vid, 1) // type
				local function buildTable(strtbl, value)
					local tbl = {}
					local id = strtbl[1]
					table.remove(strtbl, 1)
					tbl[id] = tbl[id] or {}
					if (table.Count(strtbl) > 0) then
						tbl[id] = buildTable(strtbl, value)
					else
						tbl[id] = util.JSONToTable(value)
						if (tbl[id] == nil) then
							if (tonumber(value) ~= nil) then
								tbl[id] = tonumber(value)
							else
								tbl[id] = value
							end
						end
					end
					return tbl
				end
				if (tonumber(v.value) ~= nil) then
					v.value = tonumber(v.value)
				end
				v.type = tonumber(v.type) // SQLite isn't nice and won't do it for us
				local id = vid[1]
				table.remove(vid, 1)
				if (v.type == 0 && self.mk.data[id]) then
					if (table.Count(vid) > 0) then
						if (type(self.mk.data[id].value) == "table") then
							table.Merge(self.mk.data[id].value, buildTable(vid, v.value))
						else
							MK.dev.Log(2, string.format("Failed to add '%s' to %s's data! Default value is not table!", id, self:MK_RealSteamID()))
						end
					else
						//self.mk.data[id].value = v.value
						self.mk.data[id].value = util.JSONToTable(v.value)
						if (self.mk.data[id].value == nil) then
							if (tonumber(v.value) ~= nil) then
								self.mk.data[id].value = tonumber(v.value)
							else
								self.mk.data[id].value = v.value
							end
						end
					end
				elseif (v.type == 1 && self.mk.gameData[id]) then
					if (table.Count(vid) > 0) then
						if (type(self.mk.gameData[id].value) == "table") then
							table.Merge(self.mk.gameData[id].value, buildTable(vid, v.value))
						else
							MK.dev.Log(2, string.format("Failed to add '%s' to %s's gameData! Default value is not table!", id, self:MK_RealSteamID()))
						end
					else
						self.mk.gameData[id].value = util.JSONToTable(v.value)
						if (self.mk.gameData[id].value == nil) then
							if (tonumber(v.value) ~= nil) then
								self.mk.gameData[id].value = tonumber(v.value)
							else
								self.mk.gameData[id].value = v.value
							end
						end
					end
				else
					MK.dev.Log(1, string.format("Failed loading data! Data Id: %s; Data Type: %d", id, v.type))
				end
			end
			hook.Run("MK_Player.LoadedData", self)
		else
			hook.Run("MK_Player.LoadedData", self, true)
		end

		local ipaddress = string.Explode(":", self:IPAddress())[1]
		local ipaddresses = self:MK_Data_Get("ipaddress", {})
		if (!table.HasValue(ipaddresses, ipaddress)) then
			table.insert(ipaddresses, ipaddress)
			shouldUpdate = true
		end
		self:MK_Data_Set("ipaddresses", ipaddresses, true)

		local aliases = self:MK_Data_Get("aliases", {})
		if (!table.HasValue(aliases, self:Name())) then
			table.insert(aliases, self:Name())
		end
		self:MK_Data_Set("aliases", aliases, true)

		if (callback) then
			callback(self)
		end
	end

	MK.db.query(query, function(data)
		if (self:MK_GameData_Get("clientInitialized", false)&&!self:MK_GameData_Get("fallbackDatabase", false)&&MK.config.Get("database_fallbackModule", "sqlite") != "none") then
			self:MK_SendChat(Color(100,255,100), "[MK-D] ", Color(255,255,255), "Proper database was connected and your data has been loaded.")
		end
		self:MK_GameData_Set("fallbackDatabase", false)
		MK.dev.Log(6, "Received live database info for "..self:Name())
		playerData(data)
	end, "getPlayerData_"..self:MK_RealSteamID64())
	MK.db.query(query, playerData, "getPlayerData_"..self:MK_RealSteamID64().."_fallback", MK.config.Get("database_fallbackModule", "sqlite"))

	local tbl = {}
	tbl["steam"] = self:MK_RealSteamID64()
	tbl["steamid"] = self:MK_RealSteamID()
	tbl["name"] = self:Name()
	tbl["ip"] = string.Explode(":", self:IPAddress())[1]
	tbl["firstJoined"] = os.time()
	tbl["status"] = 1
	tbl["server"] = MK.server.address
	MK.db.insertOrUpdateTable(tbl, nil, "players", "insertPlayerData_"..self:MK_RealSteamID64())
	MK.db.insertOrUpdateTable(tbl, nil, "players", "insertPlayerData_"..self:MK_RealSteamID64().."_fallback", MK.config.Get("database_fallbackModule", "sqlite"))
end
