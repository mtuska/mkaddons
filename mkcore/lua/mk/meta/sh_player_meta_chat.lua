// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	util.AddNetworkString("MK_Player_SendChat")
else
	local function sendChat(len)
		local tbl = net.ReadTable()
		if (!LocalPlayer().MK_SendChat) then return end
		LocalPlayer():MK_SendChat(unpack(tbl))
	end
	
	net.Receive("MK_Player_SendChat", sendChat)
end

function Player:MK_SendChat(...)
	local tbl = {...}
	if (table.Count(tbl) < 1) then return end
	if (SERVER) then
		net.Start("MK_Player_SendChat")
			net.WriteTable(tbl)
		net.Send(self)
	else
		chat.AddText(unpack(tbl))
	end
end