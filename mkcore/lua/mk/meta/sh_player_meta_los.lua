// RAWR!

local Player = FindMetaTable("Player")

function Player:CanSee(ent)
    local trace = {}
    trace.start = self:EyePos()
    trace.endpos = ent:EyePos() or ent:GetPos()
    trace.filter = self
    local tr = util.TraceLine(trace)

    if (tr.Fraction == 1.0) then
        return true
    end
    return false
end

function Player:CanSeeVec(pos)
    local trace = {}
    trace.start = self:EyePos()
    trace.endpos = pos
    trace.filter = self
    local tr = util.TraceLine(trace)

    if (tr.Fraction == 1.0) then
        return true
    end
    return false
end
