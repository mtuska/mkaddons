// RAWR!

local Player = FindMetaTable("Player")

function Player:MK_InitializePlayer()
	MK.dev.Log(7, "<MK_Player.SetupVars> Setup player variables for "..tostring(self))
	self.mk = {}
	self.mk.data = {}
	self.mk.gameData = {}
	self.mk.latest = {}
	self.mk.latest.deaths = {}

	hook.Run("MK_Player.SetupVars", self)
end
