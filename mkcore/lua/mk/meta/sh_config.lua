// RAWR!

local META = MK.meta.Config or {}
META.__index = META
META.permissions = {}

function META:Get()
	return self:GetValue() or self:GetDefault()
end

function META:GetDefault()
	return self.default
end

function META:GetValue()
	return self.value
end

function META:Set(value, data)
	self:SetValue(value)
	self:SetData(data)
end

function META:SetValue(value, force)
	if (self.value == value&&!force) then
		return
	end
	if (self.callback(value, self.value, self.data) != false||force) then
		self.value = value
	end
end

function META:SetData(data)
	self.data = data
end

function META:GetData()
	return table.Copy(self.data)
end

MK.meta.Config = META
