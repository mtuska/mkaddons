// RAWR!

local Player = FindMetaTable("Player")

Player.old_SteamID = Player.old_SteamID or Player.SteamID
function Player:MK_RealSteamID()
	return self:old_SteamID()
end

function Player:SteamID()
	return self:MK_GameData_Get("fakesteamid", false) or self:MK_RealSteamID()
end

Player.old_SteamID64 = Player.old_SteamID64 or Player.SteamID64
function Player:MK_RealSteamID64()
	return self:old_SteamID64()
end

function Player:SteamID64()
	return self:MK_GameData_Get("fakesteamid64", false) or self:MK_RealSteamID64()
end
Player.MK_RealUniqueID = Player.UniqueID
/*
Player.old_UniqueID = Player.old_UniqueID or Player.UniqueID
function Player:MK_RealUniqueID()
	return self:old_UniqueID()
end

function Player:UniqueID()
	return self:MK_GameData_Get("fakeuniqueid", false) or self:MK_RealUniqueID()
end
*/
/*
Player.old_SetPData = Player.old_SetPData or Player.SetPData
function Player:SetPData(key, value)
	if (!SERVER) then
		return Player:old_SetPData(key, value)
	end
	if (self:MK_GameData_Get("disguised", false)&&self:MK_Rank_CheckGroup("superadmin", true)) then
		self:SendLua([[error("Not allowed to set other player's data. That's rude.")]])
		return
	end
	return Player:old_SetPData(key, value)
end
*/