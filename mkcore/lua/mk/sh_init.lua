// RAWR!

MK = MK or {}
MK.meta = MK.meta or {}
MK.version = "2.0.1"
MK.folderName = "mk"

MK.internaldirs = {}
include("sh_util.lua")
if (SERVER) then
	AddCSLuaFile("sh_util.lua")
end

function MK.DisplayVersions()
	MsgC(Color(100,255,100), "[MK-VC] ", Color(255,255,255), "MKCore "..MK.version.." loaded!\n")

	for k,v in pairs(MK) do
		if (istable(v)&&v.version) then
			local title = v.title or k:sub(1,1):upper()..k:sub(2)
			MsgC(Color(100,255,100), "[MK-VC] ", Color(255,255,255), "MK "..title.." "..v.version.." loaded!\n")
		end
	end
end

function MK.Initialize()
	MK.util.IncludeInternalDir("core")
	MK.util.IncludeInternalDir("database")
	MK.util.IncludeInternal("sh_config.lua")
	MK.util.IncludeInternalDir("libs")
	MK.util.IncludeInternalDir("meta")
	MK.util.IncludeInternalDir("hooks")
	MK.util.IncludeInternalDir("config")
	hook.Run("MK_Configuration.Loaded")
	MK.util.IncludeInternalDir("addon")

	// Library Initialization
	for k,v in pairs(MK) do
		if (istable(v)&&v.Initialize) then
			v.Initialize()
		end
	end

	MK.DisplayVersions()
end

hook.Add("OnReloaded", "OnReloaded.MK_Initialize", function()
	MK.Initialize()
	MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName)
	MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName.."/hooks")
	MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName.."/config")
	MK.util.IncludeInternalDir("maps/"..game.GetMap())

	// Library Gamemode Initialization
	for k,v in pairs(MK) do
		if (istable(v)&&v.GMInitialize) then
			v.GMInitialize()
		end
	end
end)

hook.Add("Initialize", "Initialize.MK_Gamemode", function()
	if (file.Exists("gamemodes/mkbase", "GAME")&&!MK.version) then
		MK = {}
		local Player = FindMetaTable("Player")
		for k,v in pairs(Player) do
			if (isfunction(v)&&k:find("MK_")) then
				Player[k] = nil
			end
		end
		error("MKBase MUST be removed from gamemodes! We dumped the metatables to try and preserve our data!")
	end
	MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName)
	MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName.."/hooks")
	MK.util.IncludeInternalDir("gamemodes/"..GAMEMODE.FolderName.."/config")
	MK.util.IncludeInternalDir("maps/"..game.GetMap())

	// Library Gamemode Initialization
	for k,v in pairs(MK) do
		if (istable(v)&&v.GMInitialize) then
			v.GMInitialize()
		end
	end
end)

MK.Initialize()
