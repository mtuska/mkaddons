// RAWR!

local function ThrowQueryFault(query, fault)
	MsgC(Color(255, 0, 0), "* "..query.."\n")
	MsgC(Color(255, 0, 0), fault.."\n")
end

local db = {}
db.calls = 0

function db.connect()
	
end

function db.isConnected()
	return false
end

function db.call(post, callback, URL, version)
	db.calls = db.calls + 1
	local apiCallId = db.calls
	version = version or MK.config.db.api.version or "*"
	URL = URL or MK.config.db.api.host or false
	if (URL&&post) then
		local action = post.action or MK.lang.Get("unknown")
		//Msg(MK.lang.Get("api_call", apiCallId, URL, action).."\n")
		post.version = version
		post.key = MK.config.api.key or ""
		post.key_secret = MK.config.db.api.skey or ""
		postData = {data = util.TableToJSON(post)}
		http.Post(
			URL,
			postData,
			function(responseText, contentLength, responseHeaders, statusCode)
				post.key = "***HIDDEN***"
				post.key_secret = "***HIDDEN***"
				post.sid = "***HIDDEN***"
				//FRS.util.Log("api", "API Call "..apiCallId..": SUCCEEDED", "RESPONSE: "..statusCode.." "..FRS.httpHeaders[statusCode], "SENT: "..util.TableToJSON(post), "DATA: "..responseText)
				
				if (statusCode < 500) then
					//Msg(FRS.lang.Get("api_callback", apiCallId, URL).."\n")
					if (statusCode >= 400) then
						//Msg(FRS.lang.GetError('api_warning_statusCode', statusCode, FRS.httpHeaders[statusCode]).."\n")
					end
					if (callback) then
						callback({response = util.JSONToTable(responseText) or {}, headers = responseHeaders, statusCode = statusCode})
					end
				else
					//error(FRS.lang.GetError('api_error_statusCode', statusCode, FRS.httpHeaders[statusCode]).."\n")
				end
			end,
			function(errorMessage)
				post.key = "***HIDDEN***"
				post.key_secret = "***HIDDEN***"
				post.sid = "***HIDDEN***"
				//FRS.util.Log("api", "API Call "..apiCallId..": FAILED", "SENT: "..util.TableToJSON(post), "ERROR: "..errorMessage)
				//error(FRS.lang.GetError('api_error_connection', errorMessage))
			end
		)
	else
		//FRS.util.Log("api", "API Call "..apiCallId..": FAILED", "ERROR: "..FRS.lang.GetError("api_error_missing"))
		//Msg(FRS.lang.GetError("api_error_missing").."\n")
	end
end

//MK.db.register("api", db)