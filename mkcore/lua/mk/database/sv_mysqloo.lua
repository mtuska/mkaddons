// RAWR!

local function ThrowQueryFault(query, fault)
	MK.dev.Log(2, "<DATABASE> 'MySQLoo' Query fault: "..fault)
end

local function ThrowConnectionFault(fault)
	MK.dev.Log(2, "<DATABASE> 'MySQLoo' Failed to connect to the database: "..fault)
	Msg("MKBase has failed to connect to the database.\n")
	error(fault)
end

local db = {}
db.__index = db
db.title = "MySQLoo"

local CREATE_TABLES = [[
CREATE TABLE IF NOT EXISTS `players` (
	`steam` char(20) NOT NULL,
	`steamid` char(25) NOT NULL,
	`name` varchar(255) NOT NULL,
	`ip` varchar(255) NOT NULL,
	`firstJoined` varchar(15) NOT NULL,
	`status` tinyint(1) unsigned NOT NULL,
	`server` varchar(255) NOT NULL,
	UNIQUE KEY `steam` (`steam`),
	UNIQUE KEY `steamid` (`steamid`),
	KEY `name` (`name`)
);
CREATE TABLE `playerData` (
	`steam` char(20) NOT NULL,
	`type` int(4) NOT NULL,
	`vid` char(100) NOT NULL,
	`value` text NOT NULL,
	UNIQUE KEY `vid` (`vid`)
);
ALTER TABLE `playerData` ADD CONSTRAINT `playerData_steam` FOREIGN KEY (`steam`) REFERENCES `players` (`steam`);
CREATE TABLE IF NOT EXISTS `banData` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`steam` char(255) NOT NULL,
	`adminSteam` char(255) NOT NULL,
	`banDate` varchar(15) NOT NULL,
	`unbanDate` varchar(15) NOT NULL,
	`length` varchar(15) NOT NULL,
	`reason` text NOT NULL,
	`server` varchar(255) NOT NULL,
	`global` int(1) NOT NULL,
	PRIMARY KEY (`id`)
);
ALTER TABLE `banData` ADD CONSTRAINT `banData_steam` FOREIGN KEY (`steam`) REFERENCES `players` (`steam`);
ALTER TABLE `banData` ADD CONSTRAINT `banData_steam` FOREIGN KEY (`adminSteam`) REFERENCES `players` (`steam`);
CREATE TABLE IF NOT EXISTS `serverData` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`address` varchar(255) NOT NULL,
	`name` varchar(255) NOT NULL,
	`gamemode` varchar(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `address` (`address`)
);
]]

function db:connect(connectParams, callback)
	if (!pcall(require, "mysqloo")) then
		if (system.IsWindows()) then
			error("Server is missing VC++ redistributables!")
		else
			error("Server is missing binaries for mysqloo!")
		end
		return false
	end

	local host = connectParams.host
	local username = connectParams.username
	local password = connectParams.password
	local database = connectParams.database
	local port = connectParams.port or 3306
	self.object = mysqloo.connect(host, username, password, database, port)

	function self.object:onConnected()
		MK.dev.Log(5, "<DATABASE> 'MySQLOO' Attempting to create tables...")
		MK.db.query(CREATE_TABLES, function()
			if (callback) then
				callback(self)
			end
		end)
	end

	function self.object:onConnectionFailed(fault)
		ThrowConnectionFault(fault)
	end

	self.object:connect()
end

function db:isConnected()
	if (self.object&&self.object:status() == mysqloo.DATABASE_CONNECTED) then
		return true
	end
	return false
end

function db:isReady()
	if (!self.object) then
		return false
	elseif (self.object:status() == mysqloo.DATABASE_CONNECTING) then
		return false
	elseif (self.object:status() == mysqloo.DATABASE_NOT_CONNECTED) then
		return false
	elseif (self.object:status() == mysqloo.DATABASE_INTERNAL_ERROR) then
		return false
	end
	return true
end

function db:ping()
	return self.object:ping()
end

function db:escape(value)
	if (self.object) then
		return self.object:escape(value)
	else
		return sql.SQLStr(value, true)
	end
end

function db:query(query, callback, errorCallback)
	if (!self.object) then return end
	local queryObject = self.object:query(query)
	queryObject:start()

	if (callback) then
		function queryObject:onSuccess(data)
			callback(data, self:lastInsert(), self)
		end
	end

	function queryObject:onError(fault, query)
		ThrowQueryFault(query, fault)
		errorCallback(query, fault)
	end
end

MK.db.register("mysqloo", db)
