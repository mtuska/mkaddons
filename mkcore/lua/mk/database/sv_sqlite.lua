// RAWR!

local function ThrowQueryFault(query, fault)
	MK.dev.Log(2, "<DATABASE> 'sqlite' Query fault: "..fault)
end

local function ThrowConnectionFault(fault)
	MK.dev.Log(2, "<DATABASE> 'sqlite' Failed to connect to the database: "..fault)
	Msg("MKBase has failed to connect to the database.\n")
	error(fault)
end

local db = {}
db.__index = db
db.title = "SQLite"

local CREATE_TABLES = [[
CREATE TABLE IF NOT EXISTS `players` (
	`steam` INTEGER UNIQUE NOT NULL,
	`steamid` TEXT,
	`name` TEXT,
	`ip` TEXT,
	`firstJoined` INTEGER,
	`status` TINYINT,
	`server` TEXT
);
CREATE TABLE IF NOT EXISTS `playerData` (
	`vid` TEXT,
	`steam` INTEGER,
	`type` INTEGER,
	`value` TEXT
);
CREATE TABLE IF NOT EXISTS `banData` (
	`id` INTEGER PRIMARY KEY,
	`steam` INTEGER,
	`adminSteam` INTEGER,
	`banDate` INTEGER,
	`unbanDate` INTEGER,
	`length` INTEGER,
	`reason` TEXT,
	`global` TINYINT,
	`server` TEXT
);
CREATE TABLE IF NOT EXISTS `serverData` (
	`id` INTEGER PRIMARY KEY,
	`address` TEXT UNIQUE NOT NULL,
	`name` TEXT,
	`gamemode` TEXT
);
]]

function db:connect(connectParams, callback)
	self.connected = true
	if (callback) then
		callback()
	end
	if (!sql.TableExists("players")) then
		sql.Query("DROP TABLE `playerData`")
		sql.Query("DROP TABLE `serverData`")
		sql.Query("DROP TABLE `logData`")
	end
	MK.dev.Log(5, "<DATABASE> 'SQLite' Attempting to create tables...")
	self:query(CREATE_TABLES)
end

function db:disconnect()
	self.connected = false
end

function db:isConnected()
	return self.connected or false
end

function db:isReady()
	return true
end

function db:ping()
	return true
end

function db:escape(value)
	return sql.SQLStr(value, true)
end

function db:query(query, callback)
	local data = sql.Query(query)
	local fault = sql.LastError()

	if (data == false) then
		ThrowQueryFault(query, fault)
	end

	if (callback) then
		local lastID = tonumber(sql.QueryValue("SELECT last_insert_rowid()"))

		callback(data, lastID)
	end
end

MK.db.register("sqlite", db)
