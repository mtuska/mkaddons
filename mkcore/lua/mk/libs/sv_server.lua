// RAWR!

MK.server = MK.server or {}
MK.server.finished = MK.server.finished or false
MK.server.ip = MK.server.ip or game.GetIP() or "client"
MK.server.port = MK.server.port or GetConVarNumber("hostport")
MK.server.address = MK.server.address or MK.server.ip..":"..MK.server.port
MK.server.data = MK.server.data or {}
MK.server.settings = MK.server.settings or {}

local function serverData()
	MK.server.finished = true
	MK.db.query("SELECT * FROM serverData WHERE address = '"..MK.server.address.."'", function(data)
		if (data and data[1] and data[1].data) then
			MK.server.data = util.JSONToTable(data[1].data)
			MK.server.settings = util.JSONToTable(data[1].settings)
			MK.db.updateTable({
				name = GetConVarString("hostname"),
				gamemode = gmod.GetGamemode().FolderName,
			}, null, "address = '"..MK.server.address.."'", "serverData", "serverData_Update")
		else
			MK.db.insertTable({
				address = MK.server.address,
				name = GetConVarString("hostname"),
				gamemode = gmod.GetGamemode().FolderName,
			}, nil, "serverData", "serverData_Insert")
		end
		MK.dev.Log(5, "ServerData indexed server "..MK.server.address)
		hook.Run("MK_Server.Indexed")
		MK.db.updateTable({
			status = 0,
		}, nil, "server = '"..MK.server.address.."' AND status != '0'", "players", "updatePlayerDisconnected_"..MK.server.address)
	end, "serverData_Select")
end

hook.Add("Initialize", "MK_ServerData.Initialize", function()
	MK.server.GetIP()
end)

function MK.server.GetIP()
	local ip = game.GetIP()
	if (ip != "127.0.0.1") then
		MK.server.ip = ip
		MK.server.address = MK.server.ip..":"..MK.server.port

		serverData()
	else
		http.Fetch("http://whatismyip.akamai.com/", function(content, length, headers, code)
			MK.server.ip = content
			MK.server.address = MK.server.ip..":"..MK.server.port

			serverData()
		end)
	end
end

function MK.server.GetIPAddress()
	return MK.server.address
end

function MK.server.SetData(index, value)
	MK.server.data[index] = value
	MK.db.updateTable({
		data = util.TableToJSON(MK.server.data),
	}, null, "address = '"..MK.server.address.."'", "serverData", "serverData_UpdateData_"..index)
end

function MK.server.GetData(index, default)
	return MK.server.data[index] or default
end

function MK.server.SetSetting(index, value)
	MK.server.settings[index] = value
	MK.db.updateTable({
		settings = util.TableToJSON(MK.server.settings),
	}, null, "address = '"..MK.server.address.."'", "serverData", "serverData_UpdateSettings_"..index)
end

function MK.server.GetSetting(index, default)
	return MK.server.settings[index] or default
end
