// RAWR!

MK.nextbots = MK.nextbots or {}
MK.nextbots.list = MK.nextbots.list or {}
MK.nextbots.airnodes = MK.nextbots.airnodes or {}

MK.nextbots.navareas = MK.nextbots.navareas or {} // Each entity can create a new index(with their class) and then store all valid navareas here, to ease the wander process!
	// This means 1) Don't touch this 2) Each newly spawned nextbot will use this indexing(or create it)

function MK.nextbots.TestSpawn(pos, los)
    local c = util.PointContents(pos)
    if (bit.band(c, CONTENTS_SOLID) == CONTENTS_SOLID) then return false end

    local trace = {}
    trace.start = pos
    trace.endpos = pos + Vector(0, 0, 64)
    trace.mins = Vector(-16, -16, 0)
    trace.maxs = Vector(16, 16, 1)
    local tr = util.TraceHull(trace)
    if (tr.Hit) then return false end

    if (los) then
        for _, v in pairs(player.GetAll()) do
            if (v:VisibleVec(pos)) then return false end
            if (v:CanSeeVec(pos)) then return false end
            if (v:CanSeeVec(pos + Vector(0, 0, 72))) then return false end
        end
    end

    return true
end

function MK.nextbots.Add(ent)
    if (!IsValid(ent)) then return end
    MK.nextbots.list[ent:EntIndex()] = ent
end

function MK.nextbots.GetNexbots()
    return MK.nextbots.list
end
