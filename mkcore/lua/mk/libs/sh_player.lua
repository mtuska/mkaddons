// RAWR!

MK.players = MK.players or {}
MK.players.external = MK.players.external or {}
MK.players.external.list = MK.players.external.list or {}
MK.players.external.requests = MK.players.external.requests or {}

net.WriteVars[TYPE_DAMAGEINFO] = function(t,v) net.WriteUInt( t, 8 ) end
net.ReadVars[TYPE_DAMAGEINFO] = function(t,v) return nil end

if (SERVER) then
	util.AddNetworkString("MK_ExternalPlayersRequest")

	net.Receive("MK_ExternalPlayersRequest", function(len, ply)
		if (!ply:IsAdmin()) then return end // Avoid spammy cheaters
		local function callback(Player)
			if (IsValid(ply)) then
				net.Start("MK_ExternalPlayersRequest")
					net.WriteTable(Player)
				net.Send(ply)
			end
		end
		MK.players.external.GetPlayer(net.ReadString(), callback, false, true)
	end)

	local function getPlayerExternalData(Player)
		Player.mk.data = {}
		Player.mk.gameData = {}
		
		local sid = MK.db.convertDataType(self:MK_RealSteamID64())
		if (MK.config.Get("database_module", "sqlite") == "mysqloo") then
			MK.db.query("SELECT name, ip FROM players WHERE steam = "..sid..";SELECT type, vid, value FROM playerData WHERE steam = "..sid, function(data, lastInsert, obj)
				while(obj:hasMoreResults()) do
					if (data && data.name && data.ip) then
						Player.ip = data.ip
						Player.name = data.name
					elseif (data) then
						for k,v in pairs(data) do
							local vid = string.Split(v.vid, ";")
							local function buildTable(strtbl, value)
								local tbl = {}
								local id = strtbl[1]
								table.remove(strtbl, 1)
								tbl[id] = tbl[id] or {}
								if (table.Count(strtbl) > 0) then
									tbl[id] = buildTable(strtbl, value)
								else
									tbl[id] = value
								end
								return tbl
							end
							if (tonumber(v.value) ~= nil) then
								v.value = tonumber(v.value)
							end
							if (v.type == 0 && Player.mk.data[vid[1]]) then
								local id = vid[1]
								table.remove(vid, 1)
								if (table.Count(vid) > 0) then
									if (type(Player.mk.data[id].value) == "table") then
										table.Merge(Player.mk.data[id].value, buildTable(vid, v.value))
									else
										MK.dev.Log(2, string.format("Failed to add '%s' to %s's data! Default value is not table!", id, Player:MK_RealSteamID()))
									end
								else
									Player.mk.data[id].value = v.value
								end
							elseif (v.type == 1 && self.mk.gameData[vid[1]]) then
								local id = vid[1]
								table.remove(vid, 1)
								if (table.Count(vid) > 0) then
									if (type(Player.mk.gameData[id].value) == "table") then
										table.Merge(Player.mk.gameData[id].value, buildTable(vid, v.value))
									else
										MK.dev.Log(2, string.format("Failed to add '%s' to %s's gameData! Default value is not table!", id, Player:MK_RealSteamID()))
									end
								else
									Player.mk.gameData[id].value = v.value
								end
							end
						end
					end
					self:getNextResults()
				end
				MK.players.external.list[Player:SteamID()] = Player
				for k,callback in pairs(MK.players.external.requests[Player:SteamID()]) do
					callback(Player)
					MK.players.external.requests[k] = nil
				end
			end, "getPlayerData_"..Player:SteamID64())
		else
			MK.db.query("SELECT type, vid, value FROM playerData WHERE steam = "..sid, function(data, lastInsert, obj)
				if (data) then
					for k,v in pairs(data) do
						local vid = string.Split(v.vid, ";")
						local function buildTable(strtbl, value)
							local tbl = {}
							local id = strtbl[1]
							table.remove(strtbl, 1)
							tbl[id] = tbl[id] or {}
							if (table.Count(strtbl) > 0) then
								tbl[id] = buildTable(strtbl, value)
							else
								tbl[id] = value
							end
							return tbl
						end
						if (tonumber(v.value) ~= nil) then
							v.value = tonumber(v.value)
						end
						if (v.type == 0 && Player.mk.data[vid[1]]) then
							local id = vid[1]
							table.remove(vid, 1)
							if (table.Count(vid) > 0) then
								if (type(Player.mk.data[id].value) == "table") then
									table.Merge(Player.mk.data[id].value, buildTable(vid, v.value))
								else
									MK.dev.Log(2, string.format("Failed to add '%s' to %s's data! Default value is not table!", id, Player:MK_RealSteamID()))
								end
							else
								Player.mk.data[id].value = v.value
							end
						elseif (v.type == 1 && self.mk.gameData[vid[1]]) then
							local id = vid[1]
							table.remove(vid, 1)
							if (table.Count(vid) > 0) then
								if (type(Player.mk.gameData[id].value) == "table") then
									table.Merge(Player.mk.gameData[id].value, buildTable(vid, v.value))
								else
									MK.dev.Log(2, string.format("Failed to add '%s' to %s's gameData! Default value is not table!", id, Player:MK_RealSteamID()))
								end
							else
								Player.mk.gameData[id].value = v.value
							end
						end
					end
					Player.ip = Player:MK_Data_Get("aliases", {})[1]
					Player.name = Player:MK_Data_Get("ipaddresses", {})[1]
				end
				MK.players.external.list[Player:SteamID()] = Player
				for _,callback in pairs(MK.players.external.requests[Player:SteamID()]) do
					callback(Player)
					MK.players.external.requests[_] = nil
				end
			end, "getPlayerData_"..Player:SteamID64())
		end
	end

	function MK.players.external.GetPlayer(data, callback, notPly, forceData)
		if (!data||!callback) then return end
		local Player = {
			steam = "",
			steamid = "",
			mk = {data = {}, gameData = {}}
		}
		setmetatable(Player, MK.meta.Player)
		if (data:lower() == "console") then // Fuck console
			Player.steam = "Console"
			Player.steamid = "Console"
			Player.name = "Console"
			Player.ip = "Console"
			MK.players.external.list[Player:SteamID()] = Player
			if (callback) then
				callback(Player)
			end
			return true
		elseif (type(data)=="Player") then // Just setup to have the steamid for the rest
			Player.steam = data:SteamID64()
			Player.steamid = data:SteamID()
		elseif (data:find("STEAM_")) then
			Player.steam = util.SteamIDTo64(data)
			Player.steamid = data
		else
			Player.steam = data
			Player.steamid = util.SteamIDFrom64(data)
		end
		local target = player.GetBySteamID(Player:SteamID())
		if (IsValid(target)) then
			data = target
		end
		if (type(data)=="Player"&&!notPly) then
			Player.steam = data:SteamID64()
			Player.steamid = data:SteamID()
			Player.ip = data:IPAddress()
			Player.name = data:Name()
			Player.mk.data = data.mk.data
			Player.mk.gameData = data.mk.gameData
			MK.players.external.list[Player:SteamID()] = Player
			if (callback) then
				if (forceData) then
					callback(Player)
				else
					callback(data)
				end
			end
			return true
		end
		if (MK.players.external.list[Player:SteamID()]) then
			if (callback) then
				callback(MK.players.external.list[Player:SteamID()])
			end
			return true
		end
		if (MK.players.external.requests[Player:SteamID()]) then
			table.insert(MK.players.external.requests[Player:SteamID()], callback)
		else
			MK.players.external.requests[Player:SteamID()] = {}
			table.insert(MK.players.external.requests[Player:SteamID()], callback)
			getPlayerExternalData(Player)
		end
		return false
	end
else
	net.Receive("MK_ExternalPlayersRequest", function(len)
		local Player = net.ReadTable()
		setmetatable(Player, MK.meta.Player)
		for _,callback in pairs(MK.players.external.requests[Player.steamid]) do
			callback(Player)
			MK.players.external.requests[_] = nil
		end
		MK.players.external.list[Player:SteamID()] = Player
	end)

	function MK.players.external.GetPlayer(data, callback)
		if (!data||!callback) then return end
		local Player = {
			steam = "",
			steamid = "",
			mk = {data = {}, gameData = {}}
		}
		setmetatable(Player, MK.meta.Player)
		if (data:lower() == "console") then // Fuck console
			Player.steam = "Console"
			Player.steamid = "Console"
			Player.name = "Console"
			Player.ip = "Console"
			MK.players.external.list[Player:SteamID()] = Player
			if (callback) then
				callback(Player)
			end
			return true
		elseif (type(data)=="Player") then
			Player.steam = data:SteamID64()
			Player.steamid = data:SteamID()
		elseif (data:find("STEAM_")) then
			Player.steam = util.SteamIDTo64(data)
			Player.steamid = data
		else
			Player.steam = data
			Player.steamid = util.SteamIDFrom64(data)
		end
		if (MK.players.external.list[Player:SteamID()]) then
			callback(MK.players.external.list[Player:SteamID()])
			return true
		end
		if (MK.players.external.requests[Player:SteamID()]) then
			table.insert(MK.players.external.requests[Player:SteamID()], callback)
		else
			MK.players.external.requests[Player:SteamID()] = {}
			table.insert(MK.players.external.requests[Player:SteamID()], callback)
			net.Start("MK_ExternalPlayersRequest")
				net.WriteString(Player:SteamID())
			net.SendToServer()
		end
		return false
	end
end
