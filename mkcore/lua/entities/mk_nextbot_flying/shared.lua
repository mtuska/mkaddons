// RAWR!

AddCSLuaFile()

ENT.Base            = "base_anim"
ENT.Type			= "anim"
ENT.PrintName		= "Madkiller Max's Nextbot Flying Base"

function ENT:SetupDataTables()
	self:InstallDataTable()
	self:NetworkVar("String", 0, "BotType")
	self:NetworkVar("String", 1, "DisplayName")

	self:NetworkVar("Entity", 0, "Target")

	self:NetworkVar("Int", 0, "FlySpeed")

	self:NetworkVar("Vector", 0, "DebugLocation")
end

if (SERVER) then
	AddCSLuaFile("sh_functions.lua")
	AddCSLuaFile("cl_nextbot.lua")
	AddCSLuaFile("cl_animations.lua")
	include("sh_functions.lua")
	include("sv_animations.lua")
	include("sv_nextbot.lua")
	include("sv_enemy.lua")
	include("sv_pathing.lua")
	include("sv_flypath.lua")
else
	include("sh_functions.lua")
	include("cl_nextbot.lua")
	include("cl_animations.lua")
end
