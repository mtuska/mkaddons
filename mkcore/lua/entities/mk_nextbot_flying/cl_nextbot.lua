// RAWR!

function ENT:Initialize()
	self:MKInitialize() // used to help with the child hierarchy
end

function ENT:MKInitialize()
	//PrintTable(self:GetSequenceList())

	self.MKBaseClass = scripted_ents.Get("mk_nextbot_flying")
end

function ENT:Draw()
    self:DrawModel()

    //self:BodyUpdate()
	local maxs, mins = self:GetCollisionBounds()
	render.DrawWireframeBox(self:GetPos(), Angle(), mins, maxs, Color(255,255,255), true)

    render.DrawSphere(self:GetDebugLocation(), 20, 50, 50, Color( 255, 255, 255 ) )

	render.DrawLine(self:GetDebugLocation() + Vector(0,0,5), self:GetDebugLocation() - Vector(0,0,5), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetDebugLocation() + Vector(0,5,0), self:GetDebugLocation() - Vector(0,5,0), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetDebugLocation() + Vector(5,0,0), self:GetDebugLocation() - Vector(5,0,0), Color( 255, 0, 0 ), true)

	render.DrawLine(self:GetPos() + Vector(0,0,5), self:GetPos() - Vector(0,0,5), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetPos() + Vector(0,5,0), self:GetPos() - Vector(0,5,0), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetPos() + Vector(5,0,0), self:GetPos() - Vector(5,0,0), Color( 255, 0, 0 ), true)
    //render.DrawWireframeBox(self:GetDebugLocation(), Angle( 0, 0, 0 ), Vector(-32,-32,-32), Vector(32,32,32), Color( 255, 255, 255 ), true )
end
