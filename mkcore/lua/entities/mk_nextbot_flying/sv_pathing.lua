// RAWR!

function ENT:FindNewDestination() // Calculate end location of a wander
	if (self.ResumePos) then
		return self.ResumePos
	end
	local radius = radius or 2000

	local loc = nil
	while (!util.IsInWorld(loc)) do
		local i = 0
		loc = nil
		for k,pos in RandomPairs(MK.nextbots.airnodes) do
			if (self:TestPVS(pos) && self:VisibleVec(pos)) then
				i = i + 1
				//if (self:GetPos():DistToSqr(pos) <= radius) then
					//return pos
					loc = pos
					break
				//end
			end
		end
		BroadcastLua("print('"..i.."')")
		loc = loc or self:GetPos() + Vector(math.random(-250,250),math.random(-250,250),math.random(-250,250))
	end
	return loc
end

function ENT:StopMovingToPos()
	self._AllowedToMove = false
	self:SetVelocity(-self:GetVelocity()) // Negate the velocity
end

function ENT:FlyToPos(pos, options)
	self:SetDebugLocation(pos)

	local options = options or {}

	local path = {}
	setmetatable(path,FlyPathMeta)

	path:SetMinLookAheadDistance(options.lookahead or 300)
	path:SetGoalTolerance(options.tolerance or 20)

	path:Compute(self, pos or self:GetPos())

	if (!path:IsValid()) then return "failed" end

    self._AllowedToMove = true
	self.LastPos = {self:GetPos()}

    while (path:IsValid() && self._AllowedToMove) do
        path:Update(self)

		if (self:CustomStuck(pos) == "stuck") then
			return "stuck"
		end

		-- Draw the path (only visible on listen servers or single player)
		if ( options.draw ) then
			path:Draw()
		end
		-- If we're stuck then call the HandleStuck function and abandon
		/*if ( self.loco:IsStuck() ) then
			self:HandleStuck();
			return "stuck"
		end*/
		-- If they set maxage on options then make sure the path is younger than it
		if ( options.maxage ) then
			if ( path:GetAge() > options.maxage ) then return "timeout" end
		end
		-- If they set repath then rebuild the path every x seconds
		if ( options.repath ) then
			if ( path:GetAge() > options.repath ) then path:Compute( self, pos ) end
		end

		coroutine.yield()
    end
end
