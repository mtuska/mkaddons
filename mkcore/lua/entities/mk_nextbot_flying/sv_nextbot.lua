// RAWR!

ENT.ViewDistance = 1000
ENT.LooseDistance = 2000

function ENT:Initialize()
	self:MKInitialize() // used to help with the child hierarchy
end

function ENT:MKInitialize()
	self.MKBaseClass = scripted_ents.Get("mk_nextbot_flying")

    self:SetMoveType(MOVETYPE_FLY)
    //self:SetCollisionGroup(COLLISION_GROUP_PASSABLE_DOOR)

    self:SetFlySpeed(50)

    self.SpawnTime = CurTime()
	self.SpawnPos = self:GetPos()
    self.ViewDistanceSqr = self.ViewDistance^2
	self.LooseDistanceSqr = self.LooseDistance^2
    self.LastPos = {self:GetPos()}
	self.EnemyMemory = {}
	self._AllowedToMove = true

    //self:SetVelocity(Vector(0,0,10))
    self:BehaveStart()
end

function ENT:Think()
	self:BodyUpdate()
    self:BehaveUpdate()
	if (CurTime() > (self.m_UpdateEnemy or 0)) then
		self.m_UpdateEnemy = CurTime() + 2
		if (IsValid(self:GetEnemy())) then
			if (!self:CheckTarget(self:GetEnemy())) then
				self:SetEnemy(nil)
				self:OnLooseEnemy()
			end
		else
			self:SetEnemy(nil)
			self:OnLooseEnemy()
		end
	end
end

function ENT:BehaveStart()
	self.BehaveThread = coroutine.create( function() self:RunBehaviour() end )
end

function ENT:BehaveUpdate( fInterval )
	if (self.m_Locked) then return end
	if (self.m_Frozen) then return end
	if ( !self.BehaveThread ) then return end

	--
	-- Give a silent warning to developers if RunBehaviour has returned
	--
	if ( coroutine.status( self.BehaveThread ) == "dead" ) then
		self.BehaveThread = nil
		Msg( self, " Warning: ENT:RunBehaviour() has finished executing\n" )

		return
	end

	--
	-- Continue RunBehaviour's execution
	--
	local ok, message = coroutine.resume( self.BehaveThread )
	if ( ok == false ) then
		self.BehaveThread = nil
		ErrorNoHalt( self, " Error: ", message, "\n" )
	end
end

function ENT:RunBehaviour()
	self:Lock()
	error("Child nextbot should override ENT:RunBehaviour()")
end

function ENT:CustomStuck(pos)
	if (!self.Door) then // Doors will delay us, so don't add positions during that time
		table.insert(self.LastPos, 1, self:GetPos())
	end

	local count = #self.LastPos
	local minCheck = 10
	if count > minCheck then
		self.LastPos[minCheck+1] = nil
		local sum = 0
		for i=1, minCheck do
			sum = sum + (self:GetPos() - self.LastPos[i]):Length2DSqr()
		end
		self.LastPos = {self:GetPos()}
		print("CustomStuck: "..(sum/minCheck))
		if (sum/minCheck) < 200 then
			//BroadcastLua("print('math: "..(sum/minCheck).."')")
			self:HandleCustomStuck()

			return "stuck"
		end
	end
end

function ENT:HandleCustomStuck()

end

function ENT:OnLooseEnemy()

end

function ENT:OnSetEnemy()

end
