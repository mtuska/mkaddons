// RAWR!

function ENT:PlaySequence(name, speed)
	self.m_PlayingSequence = true
	local len = self:SetSequence( name )
	speed = speed or 1

	self:ResetSequenceInfo()
	self:SetCycle( 0 )
	self:SetPlaybackRate( speed )
	local time = len/speed

	timer.Simple(time, function()
		if (!IsValid(self)) then return end
		self.m_PlayingSequence = false
		//self:StartActivity(self:GetActivity())
	end)
	return time
end

function ENT:LoopSequence(name, speed)
	if (self.m_LoopSeqName == name) then return end
	if (name == "") then
		self.m_PlayingSequence = false
	end
	self.m_LoopSeqName = name
	self.m_LoopSeqSpeed = speed or 1
	self.m_UpdateSeqLoop = CurTime()
end

function ENT:BodyUpdate()
    self:CalcMainActivity(self:GetVelocity())

    self:FrameAdvance()
end

function ENT:CalcMainActivity(velocity)
    if (self.m_LoopSeqName && self.m_LoopSeqName != "") then
		if (!self.m_PlayingSequence && CurTime() >= self.m_UpdateSeqLoop) then
			self.m_UpdateSeqLoop = CurTime() + self:PlaySequence(self.m_LoopSeqName, self.m_LoopSeqSpeed)
		end
	end
end
