// RAWR!

local function isOutside(pos)
	local trace = {}
	trace.start = pos + Vector(0,0,10)
	trace.endpos = trace.start + Vector(0, 0, 32768)
	local tr = util.TraceLine(trace)

	if (tr.HitSky or tr.HitNoDraw) then return true end

	return false
end

function ENT.BasicCanUseNavArea(area)
	if (area:HasAttributes(NAV_MESH_TRANSIENT)) then // Transient is meant to be areas you shouldn't stay in
		return false
	elseif (area:HasAttributes(NAV_MESH_AVOID)) then // Areas you ought to avoid(i.e. streets)
		return false
	elseif (area:IsUnderwater()) then // We don't belong under the sea, we're not fish!
		return false
	elseif (!isOutside(area:GetCenter())) then
		return false
	end
	return true
end

function ENT:CanUseNavArea(area) // Can we use this area as an end location
	if (!self:IsOutside(area:GetRandomPoint())) then
		return false
	elseif (!self.loco:IsAreaTraversable(area)) then
		return false
	end
	return true
end

function ENT:FindNewDestination(radius, notForward, backward) // Calculate end location of a wander
	if (self.ResumePos) then
		return self.ResumePos
	end

	if (!navmesh.IsLoaded()) then
		error("No navmesh is loaded! MK Nextbots require a nvamesh to function!")
		self:Remove()
		return self:GetPos()
	end

	local radius = radius or 3000
	local pos = self.SpawnPos
	if (!self.StayNearSpawn) then
		pos = self:GetPos()
		if (!notForward) then
			pos = pos+self:GetForward()*radius
		elseif (backward) then
			pos = pos+self:GetForward()*(-radius)
		end
	end
	if (MK.civs && table.Count(MK.nextbots.navareas[self.ClassName]) >= 1) then
		local radiussqr = radius^2

		local areas = table.Copy(MK.nextbots.navareas[self.ClassName])
		local rnd = table.Random(areas)
		local area = nil

		for _,testarea in pairs(areas) do
			local dist = pos:DistToSqr(testarea:GetCenter())
			if (dist <= radiussqr) then
				area = testarea
				break
			end
		end

		if (!area) then
			if (rnd) then
				//ErrorNoHalt("Using a random position")
				return rnd:GetRandomPoint()
			end
			return self:GetPos()
		end
		return area:GetRandomPoint()
	end

	if (MK.civs && table.Count(MK.nextbots.navareas[self.ClassName]) < 1) then
		ErrorNoHalt("No navareas in MK.nextbots.navareas?")
	end

	local area = nil

	local areas = navmesh.Find(pos, radius, self.loco:GetDeathDropHeight(), self.loco:GetJumpHeight())

	for _,testarea in pairs(areas) do
		if (self:CanUseNavArea(testarea)) then
			area = testarea
			break
		end
	end
	if (!area) then
		local testarea = navmesh.GetNearestNavArea(pos, false, radius, false, true, TEAM_ANY)
		if (self:CanUseNavArea(testarea)) then
			area = testarea
		end
	end
	if (!area) then
		for _,testarea in pairs(navmesh.GetAllNavAreas()) do
			if (self:CanUseNavArea(testarea)) then
				area = testarea
				break
			end
		end
	end

	if (!area) then
		area = navmesh.GetNavArea(self:GetPos(), 100)
		if (!area) then
			ErrorNoHalt("Could not find a suitable location for wander! Forcing NextBot to stay put!")
			//BroadcastLua('ErrorNoHalt("Could not find a suitable location for wander! Forcing NextBot to stay put!")')
			return self:GetPos()
		else
			ErrorNoHalt("Could not find a suitable location for wander! Using current NavArea")
			//BroadcastLua('ErrorNoHalt("Could not find a suitable location for wander! Using current NavArea")')
			return area:GetRandomPoint()
		end
		return self:GetPos()
	end
	return area:GetRandomPoint()
end

function ENT:ComputePath(path, pos) // Find the path to take to the end location
	if (istable(pos)) then PrintTable(pos) end
	if (!isvector(pos)) then error("expected vector got "..type(pos)) end
	path:Compute(self, pos, function(area, fromArea, ladder, elevator, length)
		if (!IsValid(fromArea)) then
			// first area in path, no cost
			return 0
		else
			if (!fromArea:IsConnected(area)) then
				return -1
			end

			if (!self.loco:IsAreaTraversable(area)) then
				// our locomotor says we can't move here
				return -1
			end

			// compute distance traveled along path so far
			local dist = 0

			if (IsValid(ladder)) then
				dist = ladder:GetLength()
			elseif (length > 0) then
				// optimization to avoid recomputing length
				dist = length
			else
				dist = (area:GetCenter() - fromArea:GetCenter()):Length()
			end

			local cost = dist + fromArea:GetCostSoFar()

			// START OUR CUSTOM CODE
			local doorOpenPenalty = 10
			local doorClosedPenalty = 20
			local _ents = ents.FindInBox(area:GetCorner(0), area:GetCorner(2) + Vector(0,0,100))
			for k,v in pairs(_ents) do
				if (v:GetClass():find("door")) then
					if (self:GetDoorState(v) == DOOR_STATE_CLOSED) then
						cost = cost + doorClosedPenalty
					else
						cost = cost + doorOpenPenalty
					end
					//return -1
				elseif (v:GetClass():find("breakable")) then
					// check if has weapon
					if (self:GetActiveWeapon()) then
						return -1
					end
					return -1
				end
			end

			local avoidPenalty = 1.2
			if area:HasAttributes(NAV_MESH_AVOID) then
				local chance = math.Rand(0,1)
				if (self.m_UseAvoid || (self.m_JWalk && chance <= self.m_JWalk) || chance <= MK.civs.config.jwalk) then //if we're in a hurry, allow movement through avoid areas.
					cost = cost + avoidPenalty * dist
				else
					return -1
				end
			end

			local waterPenalty = 50 // Seriously, please avoid the water unless you HAVE to...
			if (area:IsUnderwater()) then
				cost = cost + avoidPenalty * dist
			end
			// END OUR CUSTOM CODE

			// check height change
			local deltaZ = fromArea:ComputeAdjacentConnectionHeightChange(area)
			if (deltaZ >= self.loco:GetStepHeight()) then
				if (deltaZ >= self.loco:GetMaxJumpHeight()) then
					// too high to reach
					return -1
				end

				// jumping is slower than flat ground
				local jumpPenalty = 5
				cost = cost + jumpPenalty * dist
			elseif (deltaZ < -self.loco:GetDeathDropHeight()) then
				// too far to drop
				return -1
			end

			return cost
		end
	end)
end

function ENT:StopMovingToPos()
	self._AllowedToMove = false
end

function ENT:Chase(options)
	local options = options or {}
	options.repath = options.repath or 0.1

	// path:Chase has curious issues...
	//local path = Path("Chase")
	local path = Path("Follow")
	path:SetMinLookAheadDistance( options.lookahead or 300 )
	path:SetGoalTolerance( options.tolerance or 20 )
	path:Compute(self, self:GetEnemy():GetPos())

	if (!path:IsValid()) then return "failed" end

	self._AllowedToMove = true

	while (path:IsValid() and self:HasEnemy() and self._AllowedToMove) do
		if (IsValid(self:GetEnemy()) && self:IsEnemy(self:GetEnemy())) then
			self:AttackBehaviour()
		else
			self:StopMovingToPos()
			break
		end

		// has curious issues...
		//path:Chase(self, self:GetEnemy())
		path:Update(self)

		-- Draw the path (only visible on listen servers or single player)
		if ( options.draw ) then
			path:Draw()
		end
		-- If we're stuck then call the HandleStuck function and abandon
		if ( self.loco:IsStuck() ) then
			self:HandleStuck();
			return "stuck"
		end
		-- If they set maxage on options then make sure the path is younger than it
		if ( options.maxage ) then
			if ( path:GetAge() > options.maxage ) then return "timeout" end
		end
		-- If they set repath then rebuild the path every x seconds
		if ( options.repath ) then
			if ( path:GetAge() > options.repath ) then path:Compute( self, self:GetEnemy():GetPos() ) end
		end

		coroutine.yield()
	end

	return "ok"
end

function ENT:ChaseAdv(options)
	local options = options or {}
	options.repath = options.repath or 0.1

	// path:Chase has curious issues...
	//local path = Path("Chase")
	local path = Path("Follow")
	path:SetMinLookAheadDistance(options.lookahead or 300)
	path:SetGoalTolerance(options.tolerance or 50)

	path:Compute(self, self:GetEnemy():GetPos())

	if (!path:IsValid()) then return "failed" end

	self._AllowedToMove = true

	while (path:IsValid() and self:HasEnemy() and self._AllowedToMove) do

		if (IsValid(self:GetEnemy()) && self:IsEnemy(self:GetEnemy())) then
			self:AttackBehaviour()
		else
			self:StopMovingToPos()
			break
		end

		// has curious issues...
		//path:Chase(self, self:GetEnemy())
		path:Update(self)

		if (self:TracePath(pos) == "stuck") then
			return "stuck"
		end

		-- Draw the path (only visible on listen servers or single player)
		if ( options.draw ) then
			path:Draw()
		end
		-- If we're stuck then call the HandleStuck function and abandon
		if ( self.loco:IsStuck() ) then
			self:HandleStuck();
			return "stuck"
		end
		-- If they set maxage on options then make sure the path is younger than it
		if ( options.maxage ) then
			if ( path:GetAge() > options.maxage ) then return "timeout" end
		end
		-- If they set repath then rebuild the path every x seconds
		if ( options.repath ) then
			if ( path:GetAge() > options.repath ) then path:Compute( self, self:GetEnemy():GetPos() ) end
		end

		coroutine.yield()
	end

	return "ok"
end

function ENT:MoveToAdvWithWeapons(pos, options)
	local options = options or {}
	options.repath = options.repath or 0.1

	// path:Chase has curious issues...
	//local path = Path("Chase")
	local path = Path("Follow")
	path:SetMinLookAheadDistance(options.lookahead or 300)
	path:SetGoalTolerance(options.tolerance or 50)

	path:Compute(self, pos or self:GetPos())

	if (!path:IsValid()) then return "failed" end

	self._AllowedToMove = true
	self.LastPos = {self:GetPos()}

	while (path:IsValid() and self._AllowedToMove) do

		if (IsValid(self:GetEnemy()) && self:IsEnemy(self:GetEnemy())) then
			self:AttackBehaviour()
		elseif (self.Investigate && IsValid(self.LastEnemy) && self:IsEnemy(self.LastEnemy)) then
			self:AttackBehaviour()
		else
			self:StopMovingToPos()
			break
		end

		// has curious issues...
		//path:Chase(self, self:GetEnemy())
		path:Update(self)
		
		if (self:TracePath(pos) == "stuck") then
			return "stuck"
		end

		-- Draw the path (only visible on listen servers or single player)
		if ( options.draw ) then
			path:Draw()
		end
		-- If we're stuck then call the HandleStuck function and abandon
		if ( self.loco:IsStuck() ) then
			self:HandleStuck();
			return "stuck"
		end
		-- If they set maxage on options then make sure the path is younger than it
		if ( options.maxage ) then
			if ( path:GetAge() > options.maxage ) then return "timeout" end
		end
		-- If they set repath then rebuild the path every x seconds
		if ( options.repath ) then
			if ( path:GetAge() > options.repath ) then path:Compute( self, pos ) end
		end

		coroutine.yield()
	end

	return "ok"
end

function ENT:MoveToPos(pos, options)

	local options = options or {}

	local path = Path( "Follow" )
	path:SetMinLookAheadDistance( options.lookahead or 300 )
	path:SetGoalTolerance( options.tolerance or 50 )

	path:Compute(self, pos or self:GetPos())

	if ( !path:IsValid() ) then return "failed" end
	-- We just called this function so of course we want to move
	self._AllowedToMove = true
	self.LastPos = {self:GetPos()}
	-- While the path is still valid and the bot is allowed to move
	while ( path:IsValid() and self._AllowedToMove ) do
		path:Update( self )

		if (self:CustomStuck(pos) == "stuck") then
			return "stuck"
		end

		-- Draw the path (only visible on listen servers or single player)
		if ( options.draw ) then
			path:Draw()
		end
		-- If we're stuck then call the HandleStuck function and abandon
		if ( self.loco:IsStuck() ) then
			self:HandleStuck();
			return "stuck"
		end
		-- If they set maxage on options then make sure the path is younger than it
		if ( options.maxage ) then
			if ( path:GetAge() > options.maxage ) then return "timeout" end
		end
		-- If they set repath then rebuild the path every x seconds
		if ( options.repath ) then
			if ( path:GetAge() > options.repath ) then path:Compute( self, pos ) end
		end

		coroutine.yield()
	end
	return "ok"

end

function ENT:MoveToPosAdv(pos, options)
	local pos = pos or self:GetPos()
	local options = options or {}

	local path = Path( "Follow" )

	path:SetMinLookAheadDistance( options.lookahead or 100 )
	path:SetGoalTolerance( options.tolerance or 30 )

	self:ComputePath(path, pos) --compute path with no doors.

	if ( !path:IsValid() ) then return "failed" end

	-- local last = self:GetPos()
	-- for k,v in ipairs(path:GetAllSegments())do
		-- local trace = { start = last, endpos = v.pos+Vector(0,0,12), filter = self, mask=MASK_NPCSOLID}
		-- local res = util.TraceLine(trace)
		-- last = v.pos+Vector(0,0,1)
		-- if res.Hit then
			-- return "blocked"
		-- end
	-- end
	self._AllowedToMove = true
	self.LastPos = {self:GetPos()}

	while (path:IsValid() and self._AllowedToMove) do

		path:Update(self)								-- This function moves the bot along the path

		if (self:CustomStuck(pos) == "stuck") then
			return "stuck"
		end
		if (self:TracePath(pos) == "stuck") then
			return "stuck"
		end

		-- Draw the path (only visible on listen servers or single player)
		if ( options.draw ) then
			path:Draw()
		end
		-- If we're stuck then call the HandleStuck function and abandon
		if ( self.loco:IsStuck() ) then
			self:HandleStuck();
			return "stuck"
		end
		-- If they set maxage on options then make sure the path is younger than it
		if ( options.maxage ) then
			if ( path:GetAge() > options.maxage ) then return "timeout" end
		end
		-- If they set repath then rebuild the path every x seconds
		if ( options.repath ) then
			if ( path:GetAge() > options.repath ) then self:ComputePath(path, pos) end
		end

		coroutine.yield()
	end

	return "ok"

end

function ENT:FindSpots( tbl )
	local tbl = tbl or {}

	tbl.pos			= tbl.pos			or self:WorldSpaceCenter()
	tbl.radius		= tbl.radius		or 1000
	tbl.stepdown	= tbl.stepdown		or 20
	tbl.stepup		= tbl.stepup		or 20
	tbl.type		= tbl.type			or 'hiding'

	-- Use a path to find the length
	local path = Path( "Follow" )

	-- Find a bunch of areas within this distance
	local areas = navmesh.Find( tbl.pos, tbl.radius, tbl.stepdown, tbl.stepup )

	local found = {}

	-- In each area
	for _, area in pairs( areas ) do
		-- get the spots
		local spots

		if ( tbl.type == 'hiding' ) then spots = area:GetHidingSpots() end

		for k, vec in pairs( spots ) do
			-- Work out the length, and add them to a table
			path:Invalidate()

			path:Compute( self, vec, 1 ) -- TODO: This is bullshit - it's using 'self.pos' not tbl.pos

			table.insert( found, { vector = vec, distance = path:GetLength() } )
		end
	end

	return found
end

function ENT:FindSpot( type, options )
	local spots = self:FindSpots( options )
	if ( !spots || #spots == 0 ) then return end

	if ( type == "near" ) then
		table.SortByMember( spots, "distance", true )
		return spots[1].vector
	end

	if ( type == "far" ) then
		table.SortByMember( spots, "distance", false )
		return spots[1].vector
	end

	-- random
	return spots[ math.random( 1, #spots ) ].vector
end
