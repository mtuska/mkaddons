// RAWR!

function ENT:IsDoor(ent)
	if (!IsValid(ent)) then return false end
	local class = ent:GetClass()
	
	if (class:find("door")||class:find("func_movelinear")) then
		return true
	end
	return false
end

DOOR_STATE_CLOSED = 0;
DOOR_STATE_OPENING = 1;
DOOR_STATE_OPEN = 2;
DOOR_STATE_CLOSING = 3;
DOOR_STATE_AJAR = 4;

function ENT:GetDoorState(ent)
	return ent:GetSaveTable().m_eDoorState or DOOR_STATE_CLOSED;
end

function ENT:IsDoorLocked(ent)
	return ent:GetSaveTable().m_bLocked == true
end

function ENT:OpenDoor(ent, delay, bUnlock, bSound)
	if (self:IsDoor(ent)) then
		if (bUnlock) then
			ent:Fire("Unlock", "", delay)
			delay = delay + 0.025
			
			if (bSound) then
				ent:EmitSound("physics/wood/wood_box_impact_hard3.wav")
			end
		end
		
		if (ent:GetClass() == "prop_dynamic") then
			ent:Fire("SetAnimation", "open", delay)
			ent:Fire("SetAnimation", "close", delay + 5)
		elseif (string.lower(ent:GetClass()) == "prop_door_rotating") then
			local target = ents.Create("info_target")
				target:SetName(tostring(target))
				target:SetPos(self:GetPos())
				target:Spawn()
			ent:Fire("OpenAwayFrom", tostring(target), delay)
			
			timer.Simple(delay + 1, function()
				if (IsValid(target)) then
					target:Remove()
				end
			end)
		else
			ent:Fire("Open", "", delay)
		end
	end
end