// RAWR!

function ENT:CheckLOS(ent)
	// Can't use IsLineOfSightClear, it'll hit NPCs
	//if (!self:IsLineOfSightClear(ent)) then return false end
	if (!IsValid(ent)) then return false end
	local pos1 = (ent.EyePos and ent:EyePos()) or ent
	local pos2 = (ent.GetPos and ent:GetPos()) or ent
	local tr = util.TraceLine({
			start = self:EyePos(),
			endpos = pos1,
			mask = MASK_NPCSOLID_BRUSHONLY,
	})
	if (tr.HitWorld) then return false end
	if (!self:InView(pos2)) then return false end
	return true
end

function ENT:FindEnemy()
	if (GetConVarNumber("ai_disabled") == 1) then return false end
	if (CurTime() < (self.m_NextFindEnemy or 0)) then return IsValid(self:GetEnemy()) end
	self.m_NextFindEnemy = CurTime() + 0.5
	if (self:HasEnemy()) then return true end // We already have an enemy, return true
	//local _ents = ents.FindInSphere(self:GetPos(), self.ViewDistance)
	local closestTarget, closestDist
	//local _ents = ents.FindInSphere(self:GetPos(), self.ViewDistance)
	local _ents = {}
	table.Add(_ents,player.GetAll())
	table.Add(_ents,MK.nextbots.GetNexbots())
	//local _ents = ents.GetAll()
	for k,v in pairs(_ents) do
		if (!v||!IsValid(v)) then continue end
		local dist = self:EyePos():DistToSqr(v:EyePos())
		if (self:CanTarget(v, dist) && (!closestDist || dist <= closestDist)) then
			if (!self:IsEnemy(v)) then continue end
			closestDist = dist
			closestTarget = v
		end
	end
	if (closestTarget) then
		self:SetEnemy(closestTarget)
		return true // We found someone!
	end
	return false
end

function ENT:CanTarget(ent, dist)
	if (IsValid(ent)) then
		if (ent == self) then return false end
		local dist = dist or self:EyePos():DistToSqr(ent:EyePos())
		if (dist <= self.ViewDistanceSqr && self:CheckLOS(ent)) then
			return true
		end
	else
		local pos = ent
		local dist = dist or self:EyePos():DistToSqr(pos)
		if (dist <= self.ViewDistanceSqr && self:CheckLOS(pos)) then
			return true
		end
	end
	return false
end

function ENT:CheckTarget(ent, dist)
	if (ent == self) then return false end
	// Can't use IsLineOfSightClear, it'll hit NPCs
	//if (!self:IsLineOfSightClear(ent)) then return false end
	local tr = util.TraceLine({
			start = self:EyePos(),
			endpos = ent:EyePos(),
			mask = MASK_BLOCKLOS,
	})
	if (tr.HitWorld) then return false end
	local dist = dist or self:EyePos():DistToSqr(ent:EyePos())
	if (dist > self.LooseDistanceSqr) then return false end
	if (!self:IsEnemy(ent)) then return false end

	self:UpdateEnemyMemory(ent, ent:GetPos())
	self.Investigate = ent:GetPos()
	return true
end

function ENT:IsEnemy(ent)
	if (GetConVarNumber("ai_disabled") == 1) then return false end
	if (!ent || !IsValid(ent)) then return false end
	if ((ent.IsPlayer && ent:IsPlayer())||(ent.IsBot && ent:IsBot())) then
		if (ent:IsPlayer() && GetConVarNumber("ai_ignoreplayers") == 1) then return false end
		if (ent:GetMoveType() == MOVETYPE_NOCLIP) then return false end
		if (!ent:Alive()) then return false end
		if (TEAM_ADMIN && ent:Team() == TEAM_ADMIN) then return false end
		return true
	end
	if ((ent.IsNPC && ent:IsNPC())||(ent.IsMKNextBot && ent:IsMKNextBot())) then
		if (ent:Health() <= 0) then return false end
		return true
	end
	return false
end

function ENT:HasEnemy()
	if (self:GetEnemy()) then
		if (IsValid(self:GetEnemy()) && self:IsEnemy(self:GetEnemy())) then
			return true
		end
		self:SetEnemy(nil)
	end
	return false
end

function ENT:SetEnemy(ent)
	if (self.Enemy == ent) then return false end
	if (IsValid(self.Enemy)) then
		self.LastEnemy = self.Enemy
		self.Investigate = self.Enemy:GetPos()
	end
	self:StopMovingToPos() // Stop any current movement so the logic can(probably) move on to ChaseEnemy
	self.Enemy = ent
	self.EnemySetTime = CurTime()
	self:SetTarget(ent) // Used to network to client
	self:OnSetEnemy()
end

function ENT:GetEnemy()
	if (GetConVarNumber("ai_disabled") == 1) then return nil end
	return self.Enemy
end

// Force the NPC to update information on the supplied enemy, as if it had line of sight to it.
function ENT:UpdateEnemyMemory(ent, pos)
	self.EnemyMemory[ent:EntIndex()] = pos
end
