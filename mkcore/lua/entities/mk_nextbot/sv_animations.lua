// RAWR!

function ENT:GetYawPitch(vec)
	--This gets the offset from 0,2,0 on the entity to the vec specified as a vector
	local yawAng=vec-self:EyePos()
	--Then converts it to a vector on the entity and makes it an angle ("local angle")
	local yawAng=self:WorldToLocal(self:GetPos()+yawAng):Angle()

	--Same thing as above but this gets the pitch angle. Since the turret's pitch axis and the turret's yaw axis are seperate I need to do this seperately.
	local pAng=vec-self:LocalToWorld((yawAng:Forward()*8)+Vector(0,0,50))
	local pAng=self:WorldToLocal(self:GetPos()+pAng):Angle()

	--Y=Yaw. This is a number between 0-360.
	local y=yawAng.y
	--P=Pitch. This is a number between 0-360.
	local p=pAng.p

	--Numbers from 0 to 360 don't work with the pose parameters, so I need to make it a number from -180 to 180
	if y>=180 then y=y-360 end
	if p>=180 then p=p-360 end
	if y<-60 || y>60 then return false end
	if p<-80 || p>50 then return false end
	--Returns yaw and pitch as numbers between -180 and 180
	return y,p
end

--This grabs yaw and pitch from ENT:GetYawPitch.
--This function sets the facing direction of the turret also.
function ENT:Aim(vec)
	local y,p=self:GetYawPitch(vec)
	if y==false then
		return false
	end
	self:SetPoseParameter("aim_yaw",y)
	self:SetPoseParameter("aim_pitch",p)
	return true
end

function ENT:PlaySequenceAndWait( name, speed )
	local time = self:PlaySequence(name, speed)
	coroutine.wait(time)
end

function ENT:PlaySequence(name, speed)
	self.m_PlayingSequence = true
	local len = self:SetSequence( name )
	speed = speed or 1

	self:ResetSequenceInfo()
	self:SetCycle( 0 )
	self:SetPlaybackRate( speed )
	local time = len/speed

	timer.Simple(time, function()
		if (!IsValid(self)) then return end
		if (self.m_LoopSeqName == name) then return end
		self.m_PlayingSequence = false
		self:StartActivity(self:GetActivity())
	end)
	return time
end

function ENT:LoopSequence(name, speed)
	if (self.m_LoopSeqName == name) then return end
	if (name == "") then
		self.m_PlayingSequence = false
	else
		self.m_PlayingSequence = true
	end
	self.m_LoopSeqName = name
	self.m_LoopSeqSpeed = speed or 1
	self.m_UpdateSeqLoop = CurTime()
end

function ENT:SequenceUpdate(velocity)
	if (self.m_LoopSeqName && self.m_LoopSeqName != "") then
		if (CurTime() >= self.m_UpdateSeqLoop) then
			self.m_UpdateSeqLoop = CurTime() + self:PlaySequence(self.m_LoopSeqName, self.m_LoopSeqSpeed)
		end
	end
end

function ENT:BodyUpdate()
	if (self.m_Locked) then return end

	if (self.m_PlayingSequence) then
		self:SequenceUpdate(self.loco:GetVelocity())
	else
		local newact = self:CalcMainActivity( self.loco:GetVelocity() )

		if self:GetActivity() == newact then
			//self:SetAnimation( newact )
		else
			self:StartActivity( newact )
		end
	end

	self:FrameAdvance()
end

function ENT:CalcIdleActivity()
	return self:IsNPC() and ACT_IDLE or ACT_MP_STAND_IDLE
end

function ENT:CalcWalkActivity()
	return self:IsNPC() and ACT_WALK or ACT_MP_WALK
end

function ENT:CalcRunActivity()
	return self:IsNPC() and ACT_RUN or ACT_MP_RUN
end

function ENT:CalcJumpActivity()
	return self:IsNPC() and ACT_RUN or ACT_MP_RUN
end

function ENT:CalcMainActivity( velocity )

	self.CalcIdeal = self:CalcIdleActivity()

	self:HandlePlayerLanding( velocity, self.m_bWasOnGround );

	if ( self:HandlePlayerNoClipping( velocity ) ||
		self:HandlePlayerVaulting( velocity ) ||
		self:HandlePlayerJumping( velocity ) ||
		self:HandlePlayerDucking( velocity ) ||
		self:HandlePlayerSwimming( velocity ) ) then

	else

		local len2d = velocity:Length2D()
		if ( len2d > 150 ) then
			self.CalcIdeal = self:CalcRunActivity()
		elseif ( len2d > 0.5 ) then
			self.CalcIdeal = self:CalcWalkActivity()
		end

		if (len2d > 0.5) then
			self:BodyMoveXY()
		end
	end
	if (IsValid(self:GetActiveWeapon())) then
		if (IsValid(self:GetEnemy()) && self:CheckLOS(self:GetEnemy())) then
			self.loco:FaceTowards( self:GetEnemy():GetPos() )
			self:Aim(self:GetEnemy():LocalToWorld(self:GetEnemy():OBBCenter()))
		else
			self:SetPoseParameter("aim_yaw",0)
			self:SetPoseParameter("aim_pitch",0)
		end
	end

	self.m_bWasOnGround = self:IsOnGround()
	self.m_bWasNoclipping = ( self:GetMoveType() == MOVETYPE_NOCLIP && !self:InVehicle() )

	local res = self:TranslateActivity( self.CalcIdeal )
	if (!res) then
		ErrorNoHalt("No proper animation was provided!")
		return self:CalcIdleActivity()
	end
	return res

end

function ENT:HandlePlayerJumping( velocity )

	if ( self:GetMoveType() == MOVETYPE_NOCLIP ) then
		self.m_InAir = true;
		return
	end

	-- airwalk more like hl2mp, we airwalk until we have 0 velocity, then it's the jump animation
	-- underwater we're alright we airwalking
	if ( !self.m_InAir && !self:OnGround() && self:WaterLevel() <= 0 ) then

		if ( !self.m_fGroundTime ) then

			self.m_fGroundTime = CurTime()

		elseif (CurTime() - self.m_fGroundTime) > 0 && velocity:Length2D() < 0.5 then

			self.m_InAir = true
			self.m_bFirstJumpFrame = false
			self.m_InAirStart = 0

		end
	end

	if self.m_InAir then

		if self.m_bFirstJumpFrame then

			self.m_bFirstJumpFrame = false
			self:AnimRestartMainSequence()

		end

		if ( self:WaterLevel() >= 2 ) ||	( (CurTime() - self.m_InAirStart) > 0.2 && self:OnGround() ) then

			self.m_InAir = false
			self.m_fGroundTime = nil
			self:AnimRestartMainSequence()

		end

		if self.m_InAir then
			self.CalcIdeal = self:IsNPC() and ACT_JUMP or ACT_MP_JUMP
			return true
		end
	end

	return false
end

function ENT:HandlePlayerDucking( velocity )

	if ( !self:Crouching() ) then return false end

	if ( velocity:Length2D() > 0.5 ) then
		self.CalcIdeal = self:IsNPC() and ACT_CROUCH or ACT_MP_CROUCHWALK
	else
		self.CalcIdeal = self:IsNPC() and ACT_CROUCHIDLE or ACT_MP_CROUCH_IDLE
	end

	return true

end

function ENT:HandlePlayerNoClipping( velocity )

	if ( self:GetMoveType() != MOVETYPE_NOCLIP || self:InVehicle() ) then

		if ( self.m_bWasNoclipping ) then

			self.m_bWasNoclipping = nil
			self:AnimResetGestureSlot( GESTURE_SLOT_CUSTOM )
			if ( CLIENT ) then self:SetIK( true ); end

		end

		return

	end

	if ( !self.m_bWasNoclipping ) then

		self:AnimRestartGesture( GESTURE_SLOT_CUSTOM, ACT_GMOD_NOCLIP_LAYER, false )
		if ( CLIENT ) then self:SetIK( false ); end

	end


	return true

end

function ENT:HandlePlayerVaulting( velocity )

	if ( velocity:Length() < 1000 ) then return end
	if ( self:IsOnGround() ) then return end

	self.CalcIdeal = self:IsNPC() and ACT_SWIM or ACT_MP_SWIM
	return true

end

function ENT:HandlePlayerSwimming( velocity )

	if ( self:WaterLevel() < 2 ) then
		self.m_bInSwim = false
		return false
	end

	if ( velocity:Length2D() > 10 ) then
		self.CalcIdeal = self:IsNPC() and ACT_SWIM or ACT_MP_SWIM
	else
		self.CalcIdeal = self:IsNPC() and ACT_SWIM_IDLE or ACT_MP_SWIM_IDLE
	end

	self.m_bInSwim = true
	return true

end

function ENT:HandlePlayerLanding( velocity, WasOnGround )

	if ( self:GetMoveType() == MOVETYPE_NOCLIP ) then return end

	if ( self:IsOnGround() && !WasOnGround ) then
		self:RestartGesture( ACT_LAND );
	end

end

local IdleActivity = ACT_HL2MP_IDLE
local IdleActivityTranslate = {}
	IdleActivityTranslate [ ACT_MP_STAND_IDLE ] 				= IdleActivity
	IdleActivityTranslate [ ACT_MP_WALK ] 						= IdleActivity+1
	IdleActivityTranslate [ ACT_MP_RUN ] 						= IdleActivity+2
	IdleActivityTranslate [ ACT_MP_CROUCH_IDLE ] 				= IdleActivity+3
	IdleActivityTranslate [ ACT_MP_CROUCHWALK ] 				= IdleActivity+4
	IdleActivityTranslate [ ACT_MP_ATTACK_STAND_PRIMARYFIRE ] 	= IdleActivity+5
	IdleActivityTranslate [ ACT_MP_ATTACK_CROUCH_PRIMARYFIRE ]	= IdleActivity+5
	IdleActivityTranslate [ ACT_MP_RELOAD_STAND ]		 		= IdleActivity+6
	IdleActivityTranslate [ ACT_MP_RELOAD_CROUCH ]		 		= IdleActivity+6
	IdleActivityTranslate [ ACT_MP_JUMP ] 						= ACT_HL2MP_JUMP_SLAM
	IdleActivityTranslate [ ACT_MP_SWIM_IDLE ] 					= ACT_MP_SWIM_IDLE
	IdleActivityTranslate [ ACT_MP_SWIM ] 						= ACT_MP_SWIM
	IdleActivityTranslate [ ACT_LAND ] 							= ACT_LAND

-- it is preferred you return ACT_MP_* in CalcMainActivity, and if you have a specific need to not tranlsate through the weapon do it here
function ENT:TranslateActivity( act )

	//local newact = act
	local newact = self:TranslateWeaponActivity( act )

	-- a bit of a hack because we're missing ACTs for a couple holdtypes
	if ( act == ACT_MP_CROUCH_IDLE ) then
		local wep = self:GetActiveWeapon()

		if ( IsValid(wep) ) then
			BroadcastLua("print('"..wep.HoldType.."')")
			-- there really needs to be a way to get the holdtype set in sweps with SWEP.SetWeaponHoldType
			-- people just tend to use wep.HoldType because that's what most of the SWEP examples do
			if wep.HoldType == "knife" or wep:GetHoldType() == "knife" then
				newact = ACT_HL2MP_IDLE_CROUCH_KNIFE
			end
		end
	end

	-- select idle anims if the weapon didn't decide
	if (act == newact && !self:IsNPC()) then
		return IdleActivityTranslate[ act ] or act
	end

	return newact

end

--[[
---------------Player Animations sequences---------------
{
  0 = ragdoll
  1 = reference
  2 = idle_all_01
  3 = idle_all_02
  4 = idle_all_angry
  5 = idle_all_scared
  6 = idle_all_cower
  7 = cidle_all
  8 = swim_idle_all
  9 = sit
  10 = menu_walk
  11 = menu_combine
  12 = menu_gman
  13 = walk_all
  14 = cwalk_all
  15 = run_all_01
  16 = run_all_02
  17 = run_all_panicked_01
  18 = run_all_panicked_02
  19 = run_all_protected
  20 = run_all_charging
  21 = swimming_all
  22 = walk_suitcase
  23 = aimlayer_magic_delta_ref
  24 = aimlayer_magic_delta
  25 = soldier_Aim_9_directions
  26 = weapons_Aim_9_directions
  27 = weapons_Aim_9_directions_Alert
  28 = aimlayer_ar2
  29 = aimlayer_camera
  30 = aimlayer_camera_crouch
  31 = aimlayer_crossbow
  32 = aimlayer_dual
  33 = aimlayer_dual_crouch
  34 = aimlayer_dual_walk
  35 = aimlayer_dual_run
  36 = aimlayer_knife
  37 = aimlayer_knife_crouch
  38 = aimlayer_knife_walk
  39 = aimlayer_magic
  40 = aimlayer_magic_crouch
  41 = aimlayer_melee
  42 = aimlayer_melee2
  43 = aimlayer_melee2_crouch
  44 = aimlayer_physgun
  45 = aimlayer_physgun_crouch
  46 = aimlayer_physgun_run
  47 = aimlayer_pistol
  48 = aimlayer_pistol_crouch
  49 = aimlayer_pistol_walk
  50 = aimlayer_pistol_run
  51 = aimlayer_rpg
  52 = aimlayer_rpg_crouch
  53 = aimlayer_revolver
  54 = aimlayer_revolver_walk
  55 = aimlayer_revolver_run
  56 = aimlayer_slam
  57 = aimlayer_slam_crouch
  58 = aimlayer_shotgun
  59 = aimlayer_smg
  60 = cidle_ar2
  61 = cidle_crossbow
  62 = cidle_camera
  63 = cidle_dual
  64 = cidle_fist
  65 = cidle_grenade
  66 = cidle_knife
  67 = cidle_magic
  68 = cidle_melee
  69 = cidle_melee2
  70 = cidle_passive
  71 = cidle_physgun
  72 = cidle_pistol
  73 = cidle_revolver
  74 = cidle_rpg
  75 = cidle_shotgun
  76 = cidle_slam
  77 = cidle_smg1
  78 = cwalk_ar2
  79 = cwalk_camera
  80 = cwalk_crossbow
  81 = cwalk_dual
  82 = cwalk_fist
  83 = cwalk_knife
  84 = cwalk_magic
  85 = cwalk_melee2
  86 = cwalk_passive
  87 = cwalk_pistol
  88 = cwalk_physgun
  89 = cwalk_revolver
  90 = cwalk_rpg
  91 = cwalk_shotgun
  92 = cwalk_smg1
  93 = cwalk_grenade
  94 = cwalk_melee
  95 = cwalk_slam
  96 = idle_ar2
  97 = idle_camera
  98 = idle_crossbow
  99 = idle_dual
  100 = idle_knife
  101 = idle_grenade
  102 = idle_magic
  103 = idle_melee
  104 = idle_melee2
  105 = idle_passive
  106 = idle_pistol
  107 = idle_physgun
  108 = idle_revolver
  109 = idle_rpg
  110 = idle_shotgun
  111 = idle_slam
  112 = idle_smg1
  113 = jump_ar2
  114 = jump_camera
  115 = jump_crossbow
  116 = jump_dual
  117 = jump_fist
  118 = jump_grenade
  119 = jump_knife
  120 = jump_magic
  121 = jump_melee
  122 = jump_melee2
  123 = jump_passive
  124 = jump_pistol
  125 = jump_physgun
  126 = jump_revolver
  127 = jump_rpg
  128 = jump_shotgun
  129 = jump_slam
  130 = jump_smg1
  131 = run_ar2
  132 = run_camera
  133 = run_crossbow
  134 = run_dual
  135 = run_fist
  136 = run_knife
  137 = run_magic
  138 = run_melee2
  139 = run_passive
  140 = run_physgun
  141 = run_revolver
  142 = run_rpg
  143 = run_shotgun
  144 = run_smg1
  145 = run_grenade
  146 = run_melee
  147 = run_pistol
  148 = run_slam
  149 = sit_ar2
  150 = sit_camera
  151 = sit_crossbow
  152 = sit_duel
  153 = sit_fist
  154 = sit_grenade
  155 = sit_knife
  156 = sit_melee
  157 = sit_melee2
  158 = sit_pistol
  159 = sit_shotgun
  160 = sit_smg1
  161 = sit_physgun
  162 = sit_rpg
  163 = sit_passive
  164 = sit_slam
  165 = swim_idle_ar2
  166 = swim_idle_camera
  167 = swim_idle_crossbow
  168 = swim_idle_duel
  169 = swim_idle_fist
  170 = swim_idle_gravgun
  171 = swim_idle_grenade
  172 = swim_idle_knife
  173 = swim_idle_magic
  174 = swim_idle_melee
  175 = swim_idle_melee2
  176 = swim_idle_passive
  177 = swim_idle_pistol
  178 = swim_idle_revolver
  179 = swim_idle_rpg
  180 = swim_idle_shotgun
  181 = swim_idle_slam
  182 = swim_idle_smg1
  183 = swimming_ar2
  184 = swimming_camera
  185 = swimming_crossbow
  186 = swimming_duel
  187 = swimming_fist
  188 = swimming_gravgun
  189 = swimming_magic
  190 = swimming_melee2
  191 = swimming_passive
  192 = swimming_revolver
  193 = swimming_rpg
  194 = swimming_shotgun
  195 = swimming_smg1
  196 = swimming_grenade
  197 = swimming_knife
  198 = swimming_melee
  199 = swimming_pistol
  200 = swimming_slam
  201 = walk_ar2
  202 = walk_camera
  203 = walk_crossbow
  204 = walk_dual
  205 = walk_fist
  206 = walk_knife
  207 = walk_magic
  208 = walk_melee2
  209 = walk_passive
  210 = walk_physgun
  211 = walk_revolver
  212 = walk_rpg
  213 = walk_shotgun
  214 = walk_smg1
  215 = walk_grenade
  216 = walk_melee
  217 = walk_pistol
  218 = walk_slam
  219 = death_01
  220 = death_02
  221 = death_03
  222 = death_04
  223 = idle_melee_angry
  224 = idle_suitcase
  225 = sit_zen
  226 = pose_standing_01
  227 = pose_standing_02
  228 = pose_standing_03
  229 = pose_standing_04
  230 = pose_ducking_01
  231 = pose_ducking_02
  232 = seq_cower
  233 = seq_preskewer
  234 = seq_baton_swing
  235 = seq_meleeattack01
  236 = seq_throw
  237 = seq_wave_smg1
  238 = idle_layer
  239 = idle_layer_alt
  240 = idle_layer_alt_nofeetlock
  241 = idle_layer_lock_right
  242 = gmod_breath_layer
  243 = gmod_breath_layer_lock_right
  244 = gmod_breath_layer_lock_hands
  245 = gmod_breath_layer_sitting
  246 = gmod_breath_noclip_layer
  247 = gmod_jump_delta
  248 = jump_land
  249 = gesture_voicechat
  250 = gesture_agree_original
  251 = gesture_agree_pelvis_layer
  252 = gesture_agree_base_layer
  253 = gesture_agree
  254 = gesture_bow_original
  255 = gesture_bow_pelvis_layer
  256 = gesture_bow_base_layer
  257 = gesture_bow
  258 = gesture_becon_original
  259 = gesture_becon_pelvis_layer
  260 = gesture_becon_base_layer
  261 = gesture_becon
  262 = gesture_disagree_original
  263 = gesture_disagree_pelvis_layer
  264 = gesture_disagree_base_layer
  265 = gesture_disagree
  266 = gesture_salute_original
  267 = gesture_salute_pelvis_layer
  268 = gesture_salute_base_layer
  269 = gesture_salute
  270 = gesture_wave_original
  271 = gesture_wave_pelvis_layer
  272 = gesture_wave_base_layer
  273 = gesture_wave
  274 = gesture_item_drop_original
  275 = gesture_item_drop_pelvis_layer
  276 = gesture_item_drop_base_layer
  277 = gesture_item_drop
  278 = gesture_item_give_original
  279 = gesture_item_give_pelvis_layer
  280 = gesture_item_give_base_layer
  281 = gesture_item_give
  282 = gesture_item_place_original
  283 = gesture_item_place_pelvis_layer
  284 = gesture_item_place_base_layer
  285 = gesture_item_place
  286 = gesture_item_throw_original
  287 = gesture_item_throw_pelvis_layer
  288 = gesture_item_throw_base_layer
  289 = gesture_item_throw
  290 = gesture_signal_forward_original
  291 = gesture_signal_forward_pelvis_layer
  292 = gesture_signal_forward_base_layer
  293 = gesture_signal_forward
  294 = gesture_signal_halt_original
  295 = gesture_signal_halt_pelvis_layer
  296 = gesture_signal_halt_base_layer
  297 = gesture_signal_halt
  298 = gesture_signal_group_original
  299 = gesture_signal_group_pelvis_layer
  300 = gesture_signal_group_base_layer
  301 = gesture_signal_group
  302 = taunt_cheer_base
  303 = taunt_cheer
  304 = taunt_dance_base
  305 = taunt_dance
  306 = taunt_laugh_base
  307 = taunt_laugh
  308 = taunt_muscle_base
  309 = taunt_muscle
  310 = taunt_robot_base
  311 = taunt_robot
  312 = taunt_persistence_base
  313 = taunt_persistence
  314 = shotgun_pump
  315 = fist_block
  316 = range_ar2
  317 = range_crossbow
  318 = range_dual_r
  319 = range_dual_l
  320 = range_gravgun
  321 = range_grenade
  322 = range_knife
  323 = range_melee
  324 = range_melee2_b
  325 = range_pistol
  326 = range_revolver
  327 = range_rpg
  328 = range_shotgun
  329 = range_slam
  330 = range_smg1
  331 = reload_ar2_original
  332 = reload_ar2_pelvis_layer
  333 = reload_ar2_base_layer
  334 = reload_ar2
  335 = reload_pistol_original
  336 = reload_pistol_pelvis_layer
  337 = reload_pistol_base_layer
  338 = reload_pistol
  339 = reload_smg1_original
  340 = reload_smg1_pelvis_layer
  341 = reload_smg1_base_layer
  342 = reload_smg1
  343 = reload_smg1_alt_original
  344 = reload_smg1_alt_pelvis_layer
  345 = reload_smg1_alt_base_layer
  346 = reload_smg1_alt
  347 = reload_dual_original
  348 = reload_dual_pelvis_layer
  349 = reload_dual_base_layer
  350 = reload_dual
  351 = reload_revolver_original
  352 = reload_revolver_pelvis_layer
  353 = reload_revolver_base_layer
  354 = reload_revolver
  355 = reload_shotgun_original
  356 = reload_shotgun_pelvis_layer
  357 = reload_shotgun_base_layer
  358 = reload_shotgun
  359 = range_melee_shove_2hand_original
  360 = range_melee_shove_2hand_pelvis_layer
  361 = range_melee_shove_2hand_base_layer
  362 = range_melee_shove_2hand
  363 = range_melee_shove_1hand_original
  364 = range_melee_shove_1hand_pelvis_layer
  365 = range_melee_shove_1hand_base_layer
  366 = range_melee_shove_1hand
  367 = range_melee_shove
  368 = flinch_01
  369 = flinch_02
  370 = flinch_back_01
  371 = flinch_head_01
  372 = flinch_head_02
  373 = flinch_phys_01
  374 = flinch_phys_02
  375 = flinch_shoulder_l
  376 = flinch_shoulder_r
  377 = flinch_stomach_01
  378 = flinch_stomach_02
  379 = idle_fist
  380 = idle_fist_layer
  381 = idle_fist_layer_weak
  382 = idle_fist_layer_rt
  383 = range_fists_r
  384 = range_fists_l
  385 = drive_pd
  386 = sit_rollercoaster
  387 = drive_airboat
  388 = drive_jeep
  389 = zombie_anim_fix_pelvis_layer
  390 = zombie_anim_fix_base_layer
  391 = zombie_anim_fix
  392 = zombie_crouch_layer_pelvis
  393 = zombie_idle_01
  394 = zombie_cidle_01
  395 = zombie_cidle_02
  396 = zombie_walk_01
  397 = zombie_walk_02
  398 = zombie_walk_03
  399 = zombie_walk_04
  400 = zombie_walk_05
  401 = zombie_cwalk_01
  402 = zombie_cwalk_02
  403 = zombie_cwalk_03
  404 = zombie_cwalk_04
  405 = zombie_cwalk_05
  406 = zombie_walk_06
  407 = zombie_run_upperbody_layer
  408 = zombie_run
  409 = zombie_run_fast
  410 = zombie_attack_01_original
  411 = zombie_attack_01_pelvis_layer
  412 = zombie_attack_01_base_layer
  413 = zombie_attack_01
  414 = zombie_attack_02_original
  415 = zombie_attack_02_pelvis_layer
  416 = zombie_attack_02_base_layer
  417 = zombie_attack_02
  418 = zombie_attack_03_original
  419 = zombie_attack_03_pelvis_layer
  420 = zombie_attack_03_base_layer
  421 = zombie_attack_03
  422 = zombie_attack_04_original
  423 = zombie_attack_04_pelvis_layer
  424 = zombie_attack_04_base_layer
  425 = zombie_attack_04
  426 = zombie_attack_05_original
  427 = zombie_attack_05_pelvis_layer
  428 = zombie_attack_05_base_layer
  429 = zombie_attack_05
  430 = zombie_attack_06_original
  431 = zombie_attack_06_pelvis_layer
  432 = zombie_attack_06_base_layer
  433 = zombie_attack_06
  434 = zombie_attack_07_original
  435 = zombie_attack_07_pelvis_layer
  436 = zombie_attack_07_base_layer
  437 = zombie_attack_07
  438 = zombie_attack_special_original
  439 = zombie_attack_special_pelvis_layer
  440 = zombie_attack_special_base_layer
  441 = zombie_attack_special
  442 = zombie_attack_frenzy_original
  443 = zombie_attack_frenzy_pelvis_layer
  444 = zombie_attack_frenzy_base_layer
  445 = zombie_attack_frenzy
  446 = taunt_zombie_original
  447 = taunt_zombie_pelvis_layer
  448 = taunt_zombie_base_layer
  449 = taunt_zombie
  450 = menu_zombie_01
  451 = zombie_climb_start
  452 = zombie_climb_loop
  453 = zombie_climb_end
  454 = zombie_leap_start
  455 = zombie_leap_mid
  456 = zombie_slump_idle_02
  457 = zombie_slump_rise_02_fast
  458 = zombie_slump_rise_02_slow
  459 = zombie_slump_idle_01
  460 = zombie_slump_rise_01
  461 = body_rot_z
  462 = head_rot_z
  463 = head_rot_y
}

---------------NPC Animations sequences---------------
{
  0 = ragdoll
  1 = dummyattachment
  2 = idle_subtle
  3 = idle_angry
  4 = idlenoise
  5 = LineIdle01
  6 = LineIdle02
  7 = LineIdle03
  8 = Crouch_idleD
  9 = layer_crouch_walk_no_weapon
  10 = layer_crouch_run_no_weapon
  11 = walk_all_Moderate
  12 = walk_all
  13 = run_all
  14 = Crouch_walk_all
  15 = crouch_run_all_delta
  16 = crouchRUNALL1
  17 = Stand_to_crouch
  18 = Crouch_to_stand
  19 = Open_door_away
  20 = Open_door_towards_right
  21 = Open_door_towards_left
  22 = turnleft
  23 = turnright
  24 = gesture_turn_left_45default
  25 = gesture_turn_left_45inDelta
  26 = gesture_turn_left_45outDelta
  27 = gesture_turn_left_45
  28 = gesture_turn_left_90default
  29 = gesture_turn_left_90inDelta
  30 = gesture_turn_left_90outDelta
  31 = gesture_turn_left_90
  32 = gesture_turn_right_45default
  33 = gesture_turn_right_45inDelta
  34 = gesture_turn_right_45outDelta
  35 = gesture_turn_right_45
  36 = gesture_turn_right_90default
  37 = gesture_turn_right_90inDelta
  38 = gesture_turn_right_90outDelta
  39 = gesture_turn_right_90
  40 = gesture_turn_left_45_flatdefault
  41 = gesture_turn_left_45_flatinDelta
  42 = gesture_turn_left_45_flatoutDelta
  43 = gesture_turn_left_45_flat
  44 = gesture_turn_left_90_flatdefault
  45 = gesture_turn_left_90_flatinDelta
  46 = gesture_turn_left_90_flatoutDelta
  47 = gesture_turn_left_90_flat
  48 = gesture_turn_right_45_flatdefault
  49 = gesture_turn_right_45_flatinDelta
  50 = gesture_turn_right_45_flatoutDelta
  51 = gesture_turn_right_45_flat
  52 = gesture_turn_right_90_flatdefault
  53 = gesture_turn_right_90_flatinDelta
  54 = gesture_turn_right_90_flatoutDelta
  55 = gesture_turn_right_90_flat
  56 = photo_react_blind
  57 = photo_react_startle
  58 = ThrowItem
  59 = Heal
  60 = Wave
  61 = Wave_close
  62 = crouchidlehide
  63 = Fear_Reaction
  64 = Fear_Reaction_Idle
  65 = cower
  66 = cower_Idle
  67 = layer_run_protected
  68 = run_protected_all
  69 = layer_run_panicked2
  70 = run_panicked_2_all
  71 = layer_run_panicked
  72 = run_panicked__all
  73 = layer_run_panicked3
  74 = run_panicked3__all
  75 = layer_walk_panicked
  76 = walk_panicked_all
  77 = crouch_panicked
  78 = podpose
  79 = deathpose_front
  80 = deathpose_back
  81 = deathpose_right
  82 = deathpose_left
  83 = Handshake_Alyx_Female
  84 = injured1
  85 = injured1postidle
  86 = injured2
  87 = base_cit_medic_anim
  88 = base_cit_medic_postanim
  89 = silo_sit
  90 = Idle_Angry_SMG1
  91 = Idle_Angry_Shotgun
  92 = Idle_SMG1_Aim
  93 = Idle_SMG1_Aim_Alert
  94 = idle_angry_Ar2
  95 = idle_ar2_aim
  96 = shoot_smg1
  97 = shoot_ar2
  98 = shoot_ar2_alt
  99 = shoot_shotgun
  100 = shoot_rpg
  101 = reload_smg1
  102 = reload_shotgun1
  103 = reload_ar2
  104 = gesture_reload_smg1spine
  105 = gesture_reload_smg1arms
  106 = gesture_reload_smg1
  107 = gesture_reload_ar2spine
  108 = gesture_reload_ar2arms
  109 = gesture_reload_ar2
  110 = gesture_shoot_ar2
  111 = gesture_shoot_smg1
  112 = gesture_shoot_shotgun
  113 = gesture_shoot_rpg
  114 = IdleAngryToShootDelta
  115 = IdleAngryToShoot
  116 = ShootToIdleAngryDelta
  117 = ShootToIdleAngry
  118 = IdleAngry_AR2_ToShootDelta
  119 = IdleAngry_AR2_ToShoot
  120 = Shoot_AR2_ToIdleAngryDelta
  121 = Shoot_AR2_ToIdleAngry
  122 = CrouchDToShoot
  123 = ShootToCrouchD
  124 = CrouchDToStand
  125 = StandToCrouchD
  126 = CrouchDToCrouchShoot
  127 = CrouchShootToCrouchD
  128 = Cover_R
  129 = Cover_L
  130 = CoverLow_R
  131 = CoverLow_L
  132 = Cover_LToShootSMG1
  133 = Cover_RToShootSMG1
  134 = CoverLow_LToShootSMG1
  135 = CoverLow_RToShootSMG1
  136 = ShootSMG1ToCover_L
  137 = ShootSMG1ToCover_R
  138 = ShootSMG1ToCoverLow_L
  139 = ShootSMG1ToCoverLow_R
  140 = crouch_shoot_smg1
  141 = crouch_aim_smg1
  142 = crouch_reload_smg1
  143 = shootp1
  144 = reloadpistol
  145 = Pistol_idle
  146 = Pistol_idle_aim
  147 = gesture_shootp1
  148 = layer_Aim_p_all
  149 = layer_run_p_aiming
  150 = run_aiming_p_all
  151 = layer_walk_p_aiming
  152 = walk_aiming_p_all
  153 = throw1
  154 = swing
  155 = MeleeAttack01
  156 = pickup
  157 = gunrack
  158 = smgdraw
  159 = Fear_Reaction_gesturespine
  160 = Fear_Reaction_gesturearms
  161 = Fear_Reaction_gesture
  162 = Wave_SMG1
  163 = layer_Aim_all
  164 = layer_Aim_AR2_all
  165 = layer_Aim_Alert_all
  166 = layer_Aim_AR2_Alert_all
  167 = layer_runAim_all
  168 = layer_runAim_ar2_all
  169 = layer_walkAlertAim_all
  170 = layer_walkAlertAim_AR2_all
  171 = layer_walk_aiming
  172 = layer_walk_holding
  173 = layer_run_aiming
  174 = layer_crouch_run_aiming
  175 = layer_crouch_walk_aiming
  176 = layer_walk_AR2_aiming
  177 = layer_walk_AR2_holding
  178 = layer_run_AR2_aiming
  179 = layer_walk_alert_holding
  180 = walkAlertHOLDALL1
  181 = layer_walk_alert_aiming
  182 = walkAlertAimALL1
  183 = layer_walk_alert_holding_AR2
  184 = walkAlertHOLD_AR2_ALL1
  185 = layer_walk_alert_aiming_AR2
  186 = walkAlertAim_AR2_ALL1
  187 = layer_run_alert_holding
  188 = run_alert_holding_all
  189 = layer_run_alert_aiming
  190 = run_alert_aiming_all
  191 = layer_run_alert_holding_AR2
  192 = run_alert_holding_AR2_all
  193 = layer_run_alert_aiming_ar2
  194 = run_alert_aiming_ar2_all
  195 = walkAIMALL1
  196 = walkHOLDALL1
  197 = walkAIMALL1_ar2
  198 = walkHOLDALL1_ar2
  199 = layer_run_holding
  200 = run_holding_all
  201 = run_aiming_all
  202 = layer_run_holding_ar2
  203 = run_holding_ar2_all
  204 = run_aiming_ar2_all
  205 = layer_crouch_run_holding
  206 = crouchRUNHOLDINGALL1
  207 = crouchRUNAIMINGALL1
  208 = layer_crouch_walk_holding
  209 = Crouch_walk_holding_all
  210 = Crouch_walk_aiming_all
  211 = Man_Gun_Aim_all
  212 = Man_Gun
  213 = Idle_SMG1_Relaxed
  214 = layer_walk_holding_SMG1_Relaxed
  215 = walk_SMG1_Relaxed_all
  216 = layer_run_holding_SMG1_Relaxed
  217 = run_SMG1_Relaxed_all
  218 = Idle_AR2_Relaxed
  219 = layer_walk_holding_AR2_Relaxed
  220 = walk_AR2_Relaxed_all
  221 = layer_run_holding_AR2_Relaxed
  222 = run_AR2_Relaxed_all
  223 = Idle_RPG_Relaxed
  224 = Idle_Angry_RPG
  225 = Idle_RPG_Aim
  226 = Crouch_Idle_RPG
  227 = StandToShootRPGfake
  228 = ShootToStandRPGfake
  229 = CrouchToShootRPGfake
  230 = ShootToCrouchRPGfake
  231 = layer_walk_holding_RPG
  232 = walk_holding_RPG_all
  233 = layer_run_holding_RPG
  234 = run_holding_RPG_all
  235 = layer_crouch_walk_holding_RPG
  236 = Crouch_walk_holding_RPG_all
  237 = layer_crouch_run_holding_RPG
  238 = crouch_run_holding_RPG_all
  239 = layer_Aim_RPG_all
  240 = layer_walk_holding_RPG_Relaxed
  241 = walk_RPG_Relaxed_all
  242 = layer_run_holding_RPG_Relaxed
  243 = run_RPG_Relaxed_all
  244 = layer_walk_holding_package
  245 = walk_holding_package_all
  246 = idle_noise
  247 = Idle_Relaxed_SMG1_1
  248 = Idle_Relaxed_SMG1_2
  249 = Idle_Relaxed_SMG1_3
  250 = Idle_Relaxed_SMG1_4
  251 = Idle_Relaxed_SMG1_5
  252 = Idle_Relaxed_SMG1_6
  253 = Idle_Relaxed_AR2_1
  254 = Idle_Relaxed_AR2_2
  255 = Idle_Relaxed_AR2_3
  256 = Idle_Relaxed_AR2_4
  257 = Idle_Relaxed_AR2_5
  258 = Idle_Relaxed_AR2_6
  259 = Idle_Relaxed_AR2_7
  260 = Idle_Relaxed_AR2_8
  261 = Idle_Relaxed_AR2_9
  262 = Idle_Alert_SMG1_1
  263 = Idle_Alert_SMG1_2
  264 = Idle_Alert_SMG1_3
  265 = Idle_Alert_SMG1_4
  266 = Idle_Alert_SMG1_5
  267 = Idle_Alert_SMG1_6
  268 = Idle_Alert_AR2_1
  269 = Idle_Alert_AR2_2
  270 = Idle_Alert_AR2_3
  271 = Idle_Alert_AR2_4
  272 = Idle_Alert_AR2_5
  273 = Idle_Alert_AR2_6
  274 = Idle_Alert_AR2_7
  275 = Idle_Alert_AR2_8
  276 = Idle_Alert_AR2_9
  277 = Idle_Relaxed_Shotgun_1
  278 = Idle_Relaxed_Shotgun_2
  279 = Idle_Relaxed_Shotgun_3
  280 = Idle_Relaxed_Shotgun_4
  281 = Idle_Relaxed_Shotgun_5
  282 = Idle_Relaxed_Shotgun_6
  283 = Idle_Alert_Shotgun_1
  284 = Idle_Alert_Shotgun_2
  285 = Idle_Alert_Shotgun_3
  286 = Idle_Alert_Shotgun_4
  287 = Idle_Alert_Shotgun_5
  288 = Idle_Alert_Shotgun_6
  289 = jump_holding_jump
  290 = jump_holding_glide
  291 = jump_holding_land
  292 = Seafloor_Poses
  293 = Idle_to_Lean_Left
  294 = Lean_Left
  295 = Lean_Left_to_Idle
  296 = Idle_to_Lean_Back
  297 = Lean_Back
  298 = Lean_Back_to_Idle
  299 = Idle_to_Sit_Ground
  300 = Sit_Ground
  301 = Sit_Ground_to_Idle
  302 = Idle_to_Sit_Chair
  303 = Sit_Chair
  304 = Sit_Chair_to_Idle
  305 = body_rot_z
  306 = spine_rot_z
  307 = neck_trans_x
  308 = head_rot_z
  309 = head_rot_y
  310 = head_rot_x
  311 = sitchair1
  312 = sitchairtable1
  313 = sitcouchfeet1
  314 = sitcouchknees1
  315 = sitcouch1
  316 = sitccouchtv1
  317 = laycouch1
  318 = takepackage
  319 = idlepackage
  320 = plazaidle1
  321 = plazaidle2
  322 = plazaidle3
  323 = plazaidle4
  324 = plazastand1
  325 = plazastand2
  326 = plazastand3
  327 = plazastand4
  328 = Lying_Down
  329 = d1_t01_Clutch_Chainlink_Entry
  330 = d1_t01_Clutch_Chainlink_Idle
  331 = Idle_to_d1_t01_BreakRoom_Sit01
  332 = d1_t02_Plaza_Sit01_Idle
  333 = d1_t01_BreakRoom_Sit01_to_Idle
  334 = d1_t01_BreakRoom_Sit02_Entry
  335 = d1_t02_Plaza_Sit02
  336 = d1_t01_BreakRoom_Sit02_Exit
  337 = d1_t03_LookOutDoor_Entry
  338 = d1_t03_LookOutDoor
  339 = d1_t03_LookOutDoor_Exit
  340 = d1_t03_LookOutWindow
  341 = d1_t03_Sit_Bed_Entry
  342 = d1_t03_Sit_Bed
  343 = d1_t03_Sit_Bed_Response
  344 = d1_t03_Sit_Bed_Exit
  345 = d1_t03_Sit_couch
  346 = canals_mary_preidle
  347 = canals_mary_wave
  348 = canals_mary_postidle
  349 = stopwomanpre
  350 = stopwoman
  351 = checkmalepost
  352 = checkmale
  353 = canals_arlene_tinker
  354 = canals_arlene_pourgas
  355 = canals_arlene_candown
  356 = turnwheel
  357 = d1_town05_Wounded_Idle_1
  358 = d1_town05_Jacobs_Heal_Entry
  359 = d1_town05_Jacobs_Heal
  360 = d1_town05_Jacobs_Heal_Entry_NoGun
  361 = d1_town05_Nurse_Open_Door
  362 = idle_alert_01
  363 = idle_alert_02
  364 = d2_coast03_PreBattle_Scan_Skies
  365 = d2_coast03_PreBattle_Scan_Skies02
  366 = d2_coast03_PreBattle_Scan_Skies03
  367 = d2_coast03_PreBattle_Scan_Skies_Respond
  368 = d2_coast03_PostBattle_Idle01_Entry
  369 = d2_coast03_PostBattle_Idle01
  370 = d2_coast03_PostBattle_Idle02_Entry
  371 = d2_coast03_PostBattle_Idle02
  372 = pullrope1idle
  373 = droprope1
  374 = cheer1
  375 = reference
  376 = g_scan_IDapexArms
  377 = g_scan_IDapexSpine
  378 = g_scan_IDapexDelta
  379 = g_scan_IDloopArms
  380 = g_scan_IDloopSpine
  381 = g_scan_IDaccentDelta
  382 = g_scan_ID
  383 = urgenthandsweepapexArms
  384 = urgenthandsweepapexSpine
  385 = urgenthandsweepapexDelta
  386 = urgenthandsweeploopArms
  387 = urgenthandsweeploopSpine
  388 = urgenthandsweepaccentDelta
  389 = urgenthandsweep
  390 = urgenthandsweepcrouchapexArms
  391 = urgenthandsweepcrouchapexSpine
  392 = urgenthandsweepcrouchapexDelta
  393 = urgenthandsweepcrouchloopArms
  394 = urgenthandsweepcrouchloopSpine
  395 = urgenthandsweepcrouchaccentDelta
  396 = urgenthandsweepcrouch
  397 = broadsweepdownapexArms
  398 = broadsweepdownapexSpine
  399 = broadsweepdownapexDelta
  400 = broadsweepdownloopArms
  401 = broadsweepdownloopSpine
  402 = broadsweepdownaccentDelta
  403 = broadsweepdown
  404 = holdhandsapexArms
  405 = holdhandsapexSpine
  406 = holdhandsapexDelta
  407 = holdhandsloopArms
  408 = holdhandsloopSpine
  409 = holdhandsaccentDelta
  410 = holdhands
  411 = giveapexArms
  412 = giveapexSpine
  413 = giveapexDelta
  414 = giveloopArms
  415 = giveloopSpine
  416 = giveaccentDelta
  417 = give
  418 = g_display_leftapexArms
  419 = g_display_leftapexSpine
  420 = g_display_leftapexDelta
  421 = g_display_leftloopArms
  422 = g_display_leftloopSpine
  423 = g_display_leftaccentDelta
  424 = g_display_left
  425 = g_waveapexArms
  426 = g_waveapexSpine
  427 = g_waveapexDelta
  428 = g_waveloopArms
  429 = g_waveloopSpine
  430 = g_waveaccentDelta
  431 = g_wave
  432 = g_right_openhandapexArms
  433 = g_right_openhandapexSpine
  434 = g_right_openhandapexDelta
  435 = g_right_openhandloopArms
  436 = g_right_openhandloopSpine
  437 = g_right_openhandaccentDelta
  438 = g_right_openhand
  439 = g_left_openhandapexArms
  440 = g_left_openhandapexSpine
  441 = g_left_openhandapexDelta
  442 = g_left_openhandloopArms
  443 = g_left_openhandloopSpine
  444 = g_left_openhandaccentDelta
  445 = g_left_openhand
  446 = g_arlene_postidle_headupapexArms
  447 = g_arlene_postidle_headupapexSpine
  448 = g_arlene_postidle_headupapexDelta
  449 = g_arlene_postidle_headuploopArms
  450 = g_arlene_postidle_headuploopSpine
  451 = g_arlene_postidle_headupaccentDelta
  452 = g_arlene_postidle_headup
  453 = g_arrest_clenchapexArms
  454 = g_arrest_clenchapexSpine
  455 = g_arrest_clenchapexDelta
  456 = g_arrest_clenchloopArms
  457 = g_arrest_clenchloopSpine
  458 = g_arrest_clenchaccentDelta
  459 = g_arrest_clench
  460 = g_d1_t03_Sit_Bed_ResponseinDelta
  461 = g_d1_t03_Sit_Bed_ResponseinFrameArms
  462 = g_d1_t03_Sit_Bed_ResponseinFrameSpine
  463 = g_d1_t03_Sit_Bed_ResponseinLoopDelta
  464 = g_d1_t03_Sit_Bed_ResponseloopFrameArms
  465 = g_d1_t03_Sit_Bed_ResponseloopFrameSpine
  466 = g_d1_t03_Sit_Bed_ResponseloopDelta
  467 = g_d1_t03_Sit_Bed_ResponseoutLoopDelta
  468 = g_d1_t03_Sit_Bed_ResponseOutFrameArms
  469 = g_d1_t03_Sit_Bed_ResponseOutFrameSpine
  470 = g_d1_t03_Sit_Bed_ResponseoutDelta
  471 = g_d1_t03_Sit_Bed_Response
  472 = g_Clutch_Chainlink_HandtoChestapexArms
  473 = g_Clutch_Chainlink_HandtoChestapexSpine
  474 = g_Clutch_Chainlink_HandtoChestapexDelta
  475 = g_Clutch_Chainlink_HandtoChestloopArms
  476 = g_Clutch_Chainlink_HandtoChestloopSpine
  477 = g_Clutch_Chainlink_HandtoChestaccentDelta
  478 = g_Clutch_Chainlink_HandtoChest
  479 = g_d2_coast03_PostBattle_Idle01apexArms
  480 = g_d2_coast03_PostBattle_Idle01apexSpine
  481 = g_d2_coast03_PostBattle_Idle01apexDelta
  482 = g_d2_coast03_PostBattle_Idle01loopArms
  483 = g_d2_coast03_PostBattle_Idle01loopSpine
  484 = g_d2_coast03_PostBattle_Idle01accentDelta
  485 = g_d2_coast03_PostBattle_Idle01
  486 = g_d2_coast03_PostBattle_Idle02apexArms
  487 = g_d2_coast03_PostBattle_Idle02apexSpine
  488 = g_d2_coast03_PostBattle_Idle02apexDelta
  489 = g_d2_coast03_PostBattle_Idle02loopArms
  490 = g_d2_coast03_PostBattle_Idle02loopSpine
  491 = g_d2_coast03_PostBattle_Idle02accentDelta
  492 = g_d2_coast03_PostBattle_Idle02
  493 = hg_nod_rightdefault
  494 = hg_nod_rightapexDelta
  495 = hg_nod_rightloopDelta
  496 = hg_nod_right
  497 = hg_nod_leftdefault
  498 = hg_nod_leftapexDelta
  499 = hg_nod_leftloopDelta
  500 = hg_nod_left
  501 = hg_puncuate_downdefault
  502 = hg_puncuate_downapexDelta
  503 = hg_puncuate_downloopDelta
  504 = hg_puncuate_down
  505 = hg_headshakedefault
  506 = hg_headshakeapexDelta
  507 = hg_headshakeloopDelta
  508 = hg_headshake
  509 = G_puncuateapexArms
  510 = G_puncuateapexSpine
  511 = G_puncuateapexDelta
  512 = G_puncuateloopArms
  513 = G_puncuateloopSpine
  514 = G_puncuateaccentDelta
  515 = G_puncuate
  516 = b_OverHere_Leftdefault
  517 = b_OverHere_LeftapexDelta
  518 = b_OverHere_LeftloopDelta
  519 = b_OverHere_Left
  520 = b_OverHere_Rightdefault
  521 = b_OverHere_RightapexDelta
  522 = b_OverHere_RightloopDelta
  523 = b_OverHere_Right
  524 = b_head_backdefault
  525 = b_head_backapexDelta
  526 = b_head_backloopDelta
  527 = b_head_back
  528 = b_head_forwarddefault
  529 = b_head_forwardapexDelta
  530 = b_head_forwardloopDelta
  531 = b_head_forward
  532 = b_accent_fwddefault
  533 = b_accent_fwdapexDelta
  534 = b_accent_fwdloopDelta
  535 = b_accent_fwd
  536 = b_accent_backdefault
  537 = b_accent_backapexDelta
  538 = b_accent_backloopDelta
  539 = b_accent_back
  540 = b_accent_fwd2default
  541 = b_accent_fwd2apexDelta
  542 = b_accent_fwd2loopDelta
  543 = b_accent_fwd2
  544 = b_accent_fwd_UpperBodydefault
  545 = b_accent_fwd_UpperBodyapexDelta
  546 = b_accent_fwd_UpperBodyloopDelta
  547 = b_accent_fwd_UpperBody
  548 = initialize
  549 = P_Chainlink_Respondloop
  550 = P_Chainlink_Respondin
  551 = P_Chainlink_Respondout
  552 = P_Chainlink_Respond
  553 = P_shiftrightloop
  554 = P_shiftrightin
  555 = P_shiftrightout
  556 = P_shiftright
  557 = p_shiftleftloop
  558 = p_shiftleftin
  559 = p_shiftleftout
  560 = p_shiftleft
  561 = Scan_Skies_Respondloop
  562 = Scan_Skies_Respondin
  563 = Scan_Skies_Respondout
  564 = Scan_Skies_Respond
  565 = p_arlene_postidleloop
  566 = p_arlene_postidlein
  567 = p_arlene_postidleout
  568 = p_arlene_postidle
  569 = p_arrest_Uprightloop
  570 = p_arrest_Uprightin
  571 = p_arrest_Uprightout
  572 = p_arrest_Upright
  573 = p_bouncingloop
  574 = p_bouncingin
  575 = p_bouncingout
  576 = p_bouncing
  577 = p_stepleftloop
  578 = p_stepleftin
  579 = p_stepleftout
  580 = p_stepleft
  581 = p_jumpuploop
  582 = p_jumpupin
  583 = p_jumpupout
  584 = p_jumpup
  585 = p_balance_toesloop
  586 = p_balance_toesin
  587 = p_balance_toesout
  588 = p_balance_toes
  589 = p_bendoverloop
  590 = p_bendoverin
  591 = p_bendoverout
  592 = p_bendover
  593 = p_stepinloop
  594 = p_stepinin
  595 = p_stepinout
  596 = p_stepin
}
--]]
