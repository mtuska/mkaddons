// RAWR!

ENT.NoCollide = false
ENT.ViewDistance = 1000
ENT.LooseDistance = 2000
ENT.WeaponAttachment = "anim_attachment_RH"

function ENT:Initialize()
	self:MKInitialize() // used to help with the child hierarchy
end

function ENT:MKInitialize()
	MK.nextbots.Add(self)
	self.MKBaseClass = scripted_ents.Get("mk_nextbot")

	self:SetSolidMask(self.NoCollide and MASK_NPCSOLID_BRUSHONLY or MASK_NPCSOLID)
	self:SetUseType(SIMPLE_USE)
	//self:SetHullType(HULL_HUMAN)

	self:SetMaxHealth(100)
	self:SetHealth(self:GetMaxHealth())

	self:SetName("MKNextBot_"..self:EntIndex())
	self:SetBotType("MKNextBot_Base")
	self:SetDisplayName("MK NextBot Base")

	self:SetCrouching(false)
	self:SetAllowFullRotation(false)
	self:SetAllowWeaponsInVehicle(false)
	self:SetAvoidPlayers(true)
	self:SetCanWalk(true)
	self:SetCanZoom(false)
	self:SetNoCollideWithTeammates(false)

	self:SetArmor(0)
	self:SetCrouchedWalkSpeed(0)
	self:SetDeaths(0)
	self:SetFrags(0)
	self:SetJumpPower(0)
	self:SetMaxSpeed(500)
	self:SetRunSpeed(500)
	self:SetStepSize(32)
	self:SetTeam(0)
	self:SetWalkSpeed(100)

	self:SetDuckSpeed(0)
	self:SetUnDuckSpeed(0)

	self:SetPlayerColor(Vector(1,1,1))
	self:SetCurrentViewOffset(Vector(0,0,0))
	self:SetViewOffset(Vector(0,0,0))
	self:SetViewOffsetDucked(Vector(0,0,0))
	self:SetWeaponColor(Vector(1,1,1))

	self:SetViewPunchAngles(Angle(0,0,0))

	self.loco:SetStepHeight(32)

	self.SpawnTime = CurTime()
	self.SpawnPos = self:GetPos()
	self.SpawnRot = self:GetAngles()
	//self.SpawnLookAt = self:GetEyeTrace()
	self.StayNearSpawn = false
	self.ViewDistanceSqr = self.ViewDistance^2
	self.LooseDistanceSqr = self.LooseDistance^2
	self.Weapons = {}
	self.WeaponAmmo = {}
	self.LastPos = {self:GetPos()}
	self.EnemyMemory = {}
	self._AllowedToMove = true
	self.m_UseAvoid = false // Should we use avoid navareas?

	if (!MK.nextbots.navareas[self.ClassName]) then
		BroadcastLua("print('Preparing nav areas for "..self.ClassName.."')")
		MK.nextbots.navareas[self.ClassName] = {}
		for _,area in pairs(navmesh.GetAllNavAreas()) do
			if (self.BasicCanUseNavArea(area)) then
				table.insert(MK.nextbots.navareas[self.ClassName], area)
			end
		end
	end
end

function ENT:AddCoffeeCup()
	local shootpos = self:GetAttachment(self:LookupAttachment("anim_attachment_LH"))

	local prop = ents.Create("prop_physics")
	prop:SetModel(Model("models/props/cs_office/coffee_mug.mdl"))
	prop:SetOwner(self)

	prop:Spawn()

	prop:SetSolid(SOLID_NONE)
	prop:SetParent(self)

	prop:Fire("setparentattachment", "anim_attachment_LH")
	prop:SetLocalAngles(Angle(0, 90, 0))

	self.CoffeeCup = prop
end

function ENT:Think()
	//self:SetPlayerColor(Vector(math.random(), math.random(), math.random()))
	if (CurTime() > (self.m_UpdateEnemy or 0)) then
		self.m_UpdateEnemy = CurTime() + 2
		if (IsValid(self:GetEnemy())) then
			if (!self:CheckTarget(self:GetEnemy())) then
				self:SetEnemy(nil)
				self:OnLooseEnemy()
			end
		else
			self:SetEnemy(nil)
			self:OnLooseEnemy()
		end
	end
end

function ENT:BehaveStart()
	self.BehaveThread = coroutine.create( function() self:RunBehaviour() end )
end

function ENT:BehaveUpdate( fInterval )
	if (self.m_Locked) then return end
	if (self.m_Frozen) then return end
	if ( !self.BehaveThread ) then return end

	--
	-- Give a silent warning to developers if RunBehaviour has returned
	--
	if ( coroutine.status( self.BehaveThread ) == "dead" ) then
		self.BehaveThread = nil
		Msg( self, " Warning: ENT:RunBehaviour() has finished executing\n" )

		return
	end

	--
	-- Continue RunBehaviour's execution
	--
	local ok, message = coroutine.resume( self.BehaveThread )
	if ( ok == false ) then
		self.BehaveThread = nil
		ErrorNoHalt( self, " Error: ", message, "\n" )
	end
end

function ENT:RunBehaviour()
	self:Lock()
	error("Child nextbot should override ENT:RunBehaviour()")
end

function ENT:OnLeaveGround()
	self.m_InAir = true
	self.m_InAirStart = CurTime()
end

function ENT:OnLandOnGround()
	self.m_InAir = false
	self.m_Jumping = false
end

function ENT:StopMovingToPos()
	self._AllowedToMove = false
end

function ENT:CustomStuck(pos)
	if (!self.Door) then // Doors will delay us, so don't add positions during that time
		table.insert(self.LastPos, 1, self:GetPos())
	end

	local count = #self.LastPos
	local minCheck = 25
	if count > minCheck then
		self.LastPos[minCheck+1] = nil
		local sum = 0
		for i=1, minCheck do
			sum = sum + (self:GetPos() - self.LastPos[i]):Length2DSqr()
		end
		self.LastPos = {self:GetPos()}
		if (sum/minCheck) < 50 then
			//BroadcastLua("print('math: "..(sum/minCheck).."')")
			self:HandleCustomStuck()

			return "stuck"
		end
	end
end

function ENT:TracePath(pos)
	// Search in traces
	local mins, maxs = self:GetCollisionBounds()
	local diff1 = (self:GetSafeShootPos() - self:GetPos()).z // Subtract from the shoot position, it'll always be higher
	maxs.z = 0
	mins.z = (-diff1)

	local trace = {}
	trace.start		= self:GetSafeShootPos()
	trace.endpos	= self:GetSafeShootPos()+self:GetAimVector()*20
	trace.filter	= {self, self:GetActiveWeapon()}
	trace.mins		= mins
	trace.maxs		= maxs
	//trace.mask	= MASK_BLOCKLOS_AND_NPCS
	trace.mask		= MASK_NPCSOLID

	local tr = util.TraceHull(trace)

	if (tr.Entity && IsValid(tr.Entity)) then
		local ent = tr.Entity
		if (self.Door) then
			if (self.Door == ent) then
				if (self:IsDoorLocked(ent)) then
					self.LastPos = {self:GetPos()}
					self:HandleDoorLocked()

					return "stuck"
				elseif (self:GetDoorState(ent) == DOOR_STATE_CLOSED) then
					self.LastPos = {self:GetPos()}
					self:HandleStuckOnDoor()

					return "stuck"
				elseif (self:GetDoorState(ent) == DOOR_STATE_OPENING && CurTime() >= (self.StartDoor + 1)) then
					self.ResumePos = pos
					self.LastPos = {self:GetPos()}
					self:HandleStuckOnDoor()

					return "stuck"
				end
			else
				self.Door = nil
			end
		end
		if (self.Prop) then
			if (self.Prop == ent) then
				// We can probably assume we're being prop blocked because people are dickheads...
				//self.LastPos = {self:GetPos()}
				//self:HandleStuckOnProp()

				//return "stuck"
			else
				self.Prop = nil
			end
		end
		if (self.Ent) then
			if (self.Ent == ent) then
				// We can probably assume we're being prop blocked because people are dickheads...
				//self.LastPos = {self:GetPos()}
				//self:HandleStuckOnProp()

				//return "stuck"
			else
				self.Ent = nil
			end
		end
		if (ent:GetClass() == "prop_door_rotating"||ent:GetClass() == "func_door") then
			if (!self.Door && self:GetDoorState(ent) == DOOR_STATE_CLOSED) then
				self.Door = ent
				self.StartDoor = CurTime()

				if (!self:IsDoorLocked(ent)) then
					self:OpenDoor(ent, 0, false, false, self:GetPos())
				end
				coroutine.wait(0.5) // Just a small delay to look like we're opening the door
				if (self:IsDoorLocked(ent)) then
					// Well fuck you too door!
					// This door is locked, lets move on with our lives
					self.LastPos = {self:GetPos()}
					self:HandleDoorLocked()

					return "stuck"
				end
			end
		elseif (ent:GetClass():find("prop")) then
			if (!self.Prop) then
				self.Prop = ent
				self.StartProp = CurTime()
			end
		else
			if (self.Ent) then
				self.Ent = ent
				self.StartEnt = CurTime()
			end
		end
	else
		self.Door = nil
		self.Prop = nil
		self.Ent = nil
	end
end

function ENT:HandleStuck()
	BroadcastLua("print('Stuck')")
	//self.ResumePos = nil
	//self:WalkTo(self:FindNewDestination(500, true, true))

	local isvisible = false
	for k,v in pairs(player.GetAll()) do
		if (v:Visible(self)) then
			isvisible = true
			break
		end
	end
	if (!isvisible) then
		local found = false
		for _,navarea in RandomPairs(navmesh.GetAllNavAreas()) do
			if (!self:CanUseNavArea(navarea)) then continue end
			local pos = navarea:GetRandomPoint()
			local visiblevec = false
			for k,v in pairs(player.GetAll()) do
				if (v:VisibleVec(pos)) then
					visiblevec = true
					break
				end
			end
			if (!visiblevec) then
				self:SetPos(pos)
				found = true
				break
			end
		end
		if (!found) then
			self:Remove() // This is really awkward... and it will probably cause more issues to keep this guy around
		end
	else
		local area = navmesh.GetNearestNavArea(self:GetPos(), false, 500, true, true)
		local pos = area and area:GetRandomPoint() or nil
		if (area && pos) then
			self:SetPos(pos)
		else
			self:Remove() // We don't want to try and trust just randomly setting position without navmesh
		end
	end

	self.loco:ClearStuck()
end

function ENT:HandleCustomStuck()
	//BroadcastLua("print('CustomStuck')")
	self.ResumePos = nil
	self:MoveToPos(self:FindNewDestination(100, true, true))

	self.loco:ClearStuck()
end

function ENT:HandleStuckOnDoor()
	BroadcastLua("print('Stuck on door')")
	//self:WalkTo(self:GetPos()-(self:GetForward()*20))
	//self:SimpleMoveToPos(self:FindNewDestination(10, true, true))

	//self.loco:ClearStuck()

	self:SetPos(self:GetPos()-(self:GetForward()*20)) // Move them back, the door is probably opening toward us
	self.loco:ClearStuck()
end

function ENT:HandleStuckOnProp()

end

function ENT:HandleDoorLocked()
	BroadcastLua("print('Door locked!')")
	self.ResumePos = nil
	local pos = self:FindNewDestination(100, true, true)
	self:FaceTowardsAndWait(pos)
	self:MoveToPos(pos)

	self.loco:ClearStuck()
end

function ENT:OnNavAreaChanged(old, new)
    /*if new:HasAttributes( NAV_MESH_JUMP ) then
        self:Jump()
    end*/
    if (!self:Crouching() and new:HasAttributes(NAV_MESH_CROUCH)) then
		self:Crouch()
	end
    if (self:Crouching() and !new:HasAttributes(NAV_MESH_CROUCH)) then
		self:UnCrouch()
	end
end

function ENT:IsJumping()
	return self.m_Jumping
end

function ENT:Jump()
	if (self.m_Jumping) then return end

	self.loco:Jump()
	self.m_Jumping = true
	self.m_bFirstJumpFrame = true
end

function ENT:Crouch()
	self:SetCollisionBounds(self:GetHullDuck())
	self.m_Crouching = true
	self:SetCrouching(true)
end

function ENT:UnCrouch()
	self:SetCollisionBounds(self:GetHull())
	self.m_Crouching = false
	self:SetCrouching(false)
end

function ENT:FaceTowardsAndWait(pos)
	while (!self:InView(pos)) do
		self.loco:FaceTowards(pos)
		coroutine.yield()
	end
end

function ENT:OnStuck()

end

function ENT:OnUnStuck()

end

function ENT:OnLooseEnemy()

end

function ENT:OnSetEnemy()

end

function ENT:OnContact(ent)

end

function ENT:OnIgnite()

end

function ENT:OnInjured(dmginfo)
	local pos = dmginfo:GetDamagePosition()
	local hitgroup = 0
	local dist_to_hitgroups = {}

	for hitbox,hitgroup in pairs(self.HitBoxToHitGroup) do
		local bone = self:GetHitBoxBone(hitbox, 0)
		if bone then
			local bonepos, boneang = self:GetBonePosition(bone)

			table.insert(dist_to_hitgroups, {bonename = self:GetBoneName(bone), hitgroup = hitgroup, dist = pos:Distance(bonepos)})
		end
	end

	table.SortByMember(dist_to_hitgroups, "dist", true)

	hitgroup = dist_to_hitgroups[1].hitgroup

	hook.Call("BDScaleNextbotDamage", GAMEMODE, self, hitgroup, dmginfo)
end

function ENT:OnOtherKilled(victim, dmginfo)

end

function ENT:OnKilled( dmginfo )
	//hook.Run( "OnNPCKilled", self, dmginfo:GetAttacker(), dmginfo:GetInflictor() )
	//self:CreateRagdoll()
	for k,v in pairs(self.Weapons) do
		if (IsValid(v)) then
			v:Remove()
		end
	end

	local attacker = dmginfo:GetAttacker()
	if (IsValid(attacker) && attacker:IsPlayer()) then
		attacker:AddFrags(1)
	end
	if (IsValid(self)) then
		self:Remove()
	end
end

function ENT:Use(activator, caller, useType, value)

end

// https://maurits.tv/data/garrysmod/wiki/wiki.garrysmod.com/index8f77.html
function ENT:VoiceSound(tbl, overlap, force)
	if ((self.VoiceTime or 0) > CurTime() && !force && !overlap) then return end
	if ((self.VoiceTime or 0) > CurTime() && !overlap) then
		self:StopSound(self.LastSoundFile)
	end
	self.VoiceTime = CurTime() + 1.5

	local tbl = istable(tbl) and tbl or {tbl}

	local snd = table.Random(tbl)
	self.LastSoundFile = snd
	self:EmitSound(snd, SNDLVL_TALKING, 100, 1, CHAN_VOICE)
end
