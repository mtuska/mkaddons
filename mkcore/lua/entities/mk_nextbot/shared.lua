// RAWR!

AddCSLuaFile()

ENT.Base 			= "base_anim"
ENT.Type			= "nextbot"
ENT.PrintName		= "Madkiller Max's Nextbot Base"
ENT.Author			= "Madkiller Max"
ENT.Contact			= "Message someone who cares"
ENT.Purpose			= "To make creating nextbots more efficent and optimized"
ENT.Instructions	= "Create a new nextbot and override it's RunBehavior! Do whatever you want, I don't care."
ENT.RenderGroup		= RENDERGROUP_OPAQUE

ENT.HitBoxToHitGroup = {
	[0] = HITGROUP_HEAD,
	[16] = HITGROUP_CHEST,
	[15] = HITGROUP_STOMACH,
	[5] = HITGROUP_RIGHTARM,
	[2] = HITGROUP_LEFTARM,
	[12] = HITGROUP_RIGHTLEG,
	[8] = HITGROUP_LEFTLEG
}

function ENT:SetupDataTables()
	self:MKSetupDataTables()
end

function ENT:MKSetupDataTables()
	self:InstallDataTable()
	self:NetworkVar("String", 0, "BotType")
	self:NetworkVar("String", 1, "DisplayName")

	self:NetworkVar("Entity", 0, "Target")
	self:NetworkVar("Entity", 1, "ActiveWeapon")
	self:NetworkVar("Entity", 2, "RagdollEntity")

	self:NetworkVar("Bool", 0, "Crouching")
	self:NetworkVar("Bool", 1, "AllowFullRotation")
	self:NetworkVar("Bool", 2, "AllowWeaponsInVehicle")
	self:NetworkVar("Bool", 3, "AvoidPlayers")
	self:NetworkVar("Bool", 4, "CanWalk")
	self:NetworkVar("Bool", 5, "CanZoom")
	self:NetworkVar("Bool", 6, "NoCollideWithTeammates")

	self:NetworkVar("Int", 0, "Armor")
	self:NetworkVar("Int", 1, "CrouchedWalkSpeed")
	self:NetworkVar("Int", 2, "Deaths")
	self:NetworkVar("Int", 3, "Frags")
	self:NetworkVar("Int", 4, "JumpPower")
	self:NetworkVarNotify("JumpPower", self.OnVarChanged)
	self:NetworkVar("Int", 5, "MaxSpeed")
	self:NetworkVar("Int", 6, "RunSpeed")
	self:NetworkVar("Int", 7, "StepSize")
	self:NetworkVarNotify("StepSize", self.OnVarChanged)
	self:NetworkVar("Int", 8, "Team")
	self:NetworkVar("Int", 9, "WalkSpeed")

	self:NetworkVar("Float", 0, "DuckSpeed")
	self:NetworkVar("Float", 1, "UnDuckSpeed")

	self:NetworkVar("Vector", 0, "PlayerColor")
	self:NetworkVar("Vector", 1, "CurrentViewOffset")
	self:NetworkVar("Vector", 2, "ViewOffset")
	self:NetworkVar("Vector", 3, "ViewOffsetDucked")
	self:NetworkVar("Vector", 4, "WeaponColor")
	self:NetworkVarNotify("WeaponColor", self.OnVarChanged)

	self:NetworkVar("Angle", 0, "ViewPunchAngles")
end

function ENT:OnVarChanged(name, old, new)
	if (name == "StepSize") then
		self.loco:SetStepHeight(new)
	elseif (name == "WeaponColor") then
	elseif (name == "JumpPower") then
	end
end

if (SERVER) then
	AddCSLuaFile("sh_functions.lua")
	AddCSLuaFile("cl_functions.lua")
	AddCSLuaFile("cl_nextbot.lua")
	AddCSLuaFile("cl_animations.lua")
	AddCSLuaFile("sh_mk.lua")

	include("sh_functions.lua")
	include("sh_mk.lua")

	include("sv_functions.lua")

	include("sv_nextbot.lua")
	include("sv_animations.lua")

	include("sv_pathing.lua")
	include("sv_weapons.lua")
	include("sv_enemy.lua")
	include("sv_doors.lua")
else
	include("sh_functions.lua")
	include("sh_mk.lua")

	include("cl_functions.lua")

	include("cl_nextbot.lua")
	include("cl_animations.lua")
end
