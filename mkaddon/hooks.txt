// libs/sh_rank.lua
hook.Run("MK_Ranks.OnRegisterRank", rank, data)
hook.Run("MK_Ranks.OnUnregisterRank", rank, data)

// hooks/sv_location.lua
hook.Run("PlayerChangeLocation", ply, loc, oldloc)

// hooks/sv_player_name.lua
hook.Run("MK_Player.NameChanged", ply, oldName, newName)

// hooks/sv_player_playtime.lua
hook.Run("MK_Player.UpdatePlayTime_GM", ply, seconds)
hook.Run("MK_Player.UpdatePlayTime", ply, seconds)

// derma/cl_hud.lua
hook.Run("HUDShouldDraw", "MK_HUDPaint_XP")
hook.Run("HUDShouldDraw", "MK_HUDPaint_Player")
hook.Run("HUDShouldDraw", "MK_HUDPaint_Intro")
