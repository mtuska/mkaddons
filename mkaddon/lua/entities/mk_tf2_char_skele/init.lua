// RAWR!

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
    self.BaseClass.Initialize(self)

    self:SetModel("models/bots/skeleton_sniper/skeleton_sniper.mdl")
    self:SetSkin(1)
end

--[[
misc/halloween/skeletons/skelly_giant_01.wav
misc/halloween/skeletons/skelly_giant_02.wav
misc/halloween/skeletons/skelly_giant_03.wav
misc/halloween/skeletons/skelly_medium_01.wav
misc/halloween/skeletons/skelly_medium_02.wav
misc/halloween/skeletons/skelly_medium_03.wav
misc/halloween/skeletons/skelly_medium_04.wav
misc/halloween/skeletons/skelly_medium_05.wav
misc/halloween/skeletons/skelly_medium_06.wav
misc/halloween/skeletons/skelly_medium_07.wav
misc/halloween/skeletons/skelly_small_01.wav
misc/halloween/skeletons/skelly_small_02.wav
misc/halloween/skeletons/skelly_small_03.wav
misc/halloween/skeletons/skelly_small_04.wav
misc/halloween/skeletons/skelly_small_05.wav
misc/halloween/skeletons/skelly_small_06.wav
misc/halloween/skeletons/skelly_small_07.wav
misc/halloween/skeletons/skelly_small_08.wav
misc/halloween/skeletons/skelly_small_09.wav
misc/halloween/skeletons/skelly_small_10.wav
misc/halloween/skeletons/skelly_small_11.wav
misc/halloween/skeletons/skelly_small_12.wav
misc/halloween/skeletons/skelly_small_13.wav
misc/halloween/skeletons/skelly_small_14.wav
misc/halloween/skeletons/skelly_small_15.wav
misc/halloween/skeletons/skelly_small_16.wav
misc/halloween/skeletons/skelly_small_17.wav
misc/halloween/skeletons/skelly_small_18.wav
misc/halloween/skeletons/skelly_small_19.wav
misc/halloween/skeletons/skelly_small_20.wav
misc/halloween/skeletons/skelly_small_21.wav
misc/halloween/skeletons/skelly_small_22.wav

{
  0 = ref
  1 = ragdoll
  2 = MELEE_aimmatrix_run
  3 = run_MELEE
  4 = spawn01
  5 = spawn02
  6 = spawn03
  7 = spawn04
  8 = spawn05
  9 = spawn06
  10 = spawn07
  11 = armslayer_MELEE_swing
  12 = bodylayer_MELEE_swing
  13 = MELEE_swing
  14 = armslayer_MELEE_swing2
  15 = bodylayer_MELEE_swing2
  16 = MELEE_swing2
  17 = armslayer_MELEE_swing5
  18 = bodylayer_MELEE_swing5
  19 = MELEE_swing5
  20 = armslayer_MELEE_swing3
  21 = bodylayer_MELEE_swing3
  22 = MELEE_swing3
  23 = armslayer_MELEE_swing4
  24 = bodylayer_MELEE_swing4
  25 = MELEE_swing4
  26 = a_flinch01
  27 = user_ref
  28 = ragdollspawn
  29 = r_handposes
  30 = r_armposes
  31 = PRIMARY_aimmatrix_idle
  32 = PRIMARY_aimmatrix_run
  33 = PRIMARY_aimmatrix_crouch_idle
  34 = PRIMARY_aimmatrix_deployed
  35 = PRIMARY_crouch_aimmatrix_deployed
  36 = AttackStand_PRIMARY
  37 = AttackCrouch_PRIMARY
  38 = AttackSwim_PRIMARY
  39 = AttackCrouch_PRIMARY_DEPLOYED
  40 = AttackSwim_PRIMARY_DEPLOYED
  41 = ReloadStand_PRIMARY_DEPLOYED
  42 = ReloadCrouch_PRIMARY_DEPLOYED
  43 = ReloadSwim_PRIMARY_DEPLOYED
  44 = ReloadStand_PRIMARY
  45 = ReloadCrouch_PRIMARY
  46 = ReloadSwim_PRIMARY
  47 = attackstand_Primary_deployed_fire
  48 = layer_placeSapper_ArmL
  49 = PRIMARY_placeSapper
  50 = SECONDARY_aimmatrix_idle
  51 = SECONDARY_aimmatrix_run
  52 = SECONDARY_aimmatrix_crouch_idle
  53 = AttackStand_SECONDARY
  54 = AttackCrouch_SECONDARY
  55 = AttackSwim_SECONDARY
  56 = layer_reload_standing_arms_SECONDARY
  57 = ReloadStand_SECONDARY
  58 = ReloadAirwalk_SECONDARY
  59 = ReloadCrouch_SECONDARY
  60 = ReloadSwim_SECONDARY
  61 = MELEE_aimmatrix_idle
  62 = MELEE_aimmatrix_crouch_idle
  63 = armslayer_melee_Crouch_swing
  64 = bodylayer_Melee_Crouch_Swing
  65 = Melee_Crouch_Swing
  66 = AttackSwim_MELEE
  67 = layer_melee_swing_swimS
  68 = MELEE_swim_swing
  69 = MELEE_ALLCLASS_aimmatrix_idle
  70 = MELEE_ALLCLASS_aimmatrix_run
  71 = MELEE_ALLCLASS_aimmatrix_crouch_idle
  72 = armslayer_melee_allclass_swing
  73 = bodylayer_Melee_allclass_Swing
  74 = Melee_allclass_Swing
  75 = armslayer_melee_allclass_Crouch_swing
  76 = bodylayer_Melee_allclass_Crouch_Swing
  77 = Melee_allclass_Crouch_Swing
  78 = AttackSwim_MELEE_ALLCLASS
  79 = layer_MELEE_ALLCLASS_swing_swimS
  80 = MELEE_ALLCLASS_swim_swing
  81 = Item1_aimmatrix_idle
  82 = Item1_aimmatrix_run
  83 = Item1_aimmatrix_crouch_idle
  84 = armslayer_ITEM1_fire
  85 = bodylayer_ITEM1_fire
  86 = ITEM1_fire
  87 = armslayer_ITEM1_Crouch_fire
  88 = bodylayer_ITEM1_Crouch_fire
  89 = ITEM1_Crouch_fire
  90 = layer_ITEM1_WepGrip
  91 = ITEM2_aimmatrix_idle
  92 = ITEM2_aimmatrix_run
  93 = ITEM2_aimmatrix_crouch_idle
  94 = ITEM2_aimmatrix_deployed
  95 = ITEM2_aimmatrix_deployed_walk
  96 = ITEM2_crouch_aimmatrix_deployed
  97 = ITEM2_fire
  98 = ITEM2_reload
  99 = ITEM2_crouch_fire
  100 = ITEM2_crouch_reload
  101 = layer_gesture_primary_go_armL
  102 = gesture_primary_go
  103 = layer_gesture_primary_cheer_armL
  104 = gesture_primary_cheer
  105 = layer_gesture_primary_help_armL
  106 = gesture_primary_help
  107 = layer_gesture_primary_positive_armL
  108 = gesture_primary_positive
  109 = layer_gesture_secondary_go_armL
  110 = gesture_secondary_go
  111 = layer_gesture_secondary_cheer_armL
  112 = gesture_secondary_cheer
  113 = layer_gesture_secondary_help_armL
  114 = gesture_secondary_help
  115 = layer_gesture_secondary_positive_armL
  116 = gesture_secondary_positive
  117 = layer_gesture_melee_go_armL
  118 = gesture_melee_go
  119 = layer_gesture_melee_cheer_armL
  120 = gesture_melee_cheer
  121 = layer_gesture_primary_help_runN_armL
  122 = layer_gesture_melee_help_armL
  123 = gesture_melee_help
  124 = layer_gesture_melee_positive_armL
  125 = gesture_melee_positive
  126 = primary_death_backstab
  127 = primary_death_headshot
  128 = primary_death_burning
  129 = layer_dieviolent
  130 = dieviolent
  131 = layer_PRIMARY_Stun_begin
  132 = layer_PRIMARY_stun_middle
  133 = layer_PRIMARY_Stun_end
  134 = PRIMARY_Stun_begin
  135 = PRIMARY_stun_middle
  136 = PRIMARY_stun_end
  137 = SelectionMenu_StartPose
  138 = SelectionMenu_Anim01
  139 = SelectionMenu_Idle
  140 = stand_PRIMARY
  141 = stand_SECONDARY
  142 = stand_MELEE
  143 = stand_MELEE_ALLCLASS
  144 = stand_LOSER
  145 = stand_ITEM1
  146 = stand_ITEM2
  147 = crouch_PRIMARY
  148 = crouch_SECONDARY
  149 = crouch_MELEE
  150 = crouch_MELEE_ALLCLASS
  151 = crouch_LOSER
  152 = crouch_ITEM1
  153 = crouch_ITEM2
  154 = crouch_ITEM2_DEPLOYED
  155 = a_jumpstart_PRIMARY
  156 = a_jumpfloat_PRIMARY
  157 = jumplandPose_PRIMARY
  158 = jumpland_PRIMARY
  159 = a_jumpstart_SECONDARY
  160 = a_jumpfloat_SECONDARY
  161 = jumplandPose_SECONDARY
  162 = jumpland_SECONDARY
  163 = a_jumpstart_MELEE
  164 = a_jumpfloat_MELEE
  165 = jumplandPose_MELEE
  166 = jumpland_MELEE
  167 = a_jumpstart_MELEE_ALLCLASS
  168 = a_jumpfloat_MELEE_ALLCLASS
  169 = jumplandPose_MELEE_ALLCLASS
  170 = jumpland_MELEE_ALLCLASS
  171 = a_jumpstart_LOSER
  172 = a_jumpfloat_LOSER
  173 = jumplandPose_LOSER
  174 = jumpland_LOSER
  175 = a_jumpstart_ITEM1
  176 = a_jumpfloat_ITEM1
  177 = jumplandPose_ITEM1
  178 = jumpland_ITEM1
  179 = a_jumpstart_ITEM2
  180 = a_jumpfloat_ITEM2
  181 = jumplandPose_ITEM2
  182 = jumpland_ITEM2
  183 = PRIMARY_aimmatrix_swim
  184 = s_swimAlign_PRIMARY
  185 = swim_PRIMARY
  186 = SECONDARY_aimmatrix_swim
  187 = s_swimAlign_SECONDARY
  188 = swim_SECONDARY
  189 = MELEE_aimmatrix_swim
  190 = s_swimAlign_MELEE
  191 = swim_MELEE
  192 = MELEE_ALLCLASS_aimmatrix_swim
  193 = s_swimAlign_MELEE_ALLCLASS
  194 = swim_MELEE_ALLCLASS
  195 = ITEM1_aimmatrix_swim
  196 = s_swimAlign_ITEM1
  197 = swim_ITEM1
  198 = ITEM2_aimmatrix_swim
  199 = s_swimAlign_ITEM2
  200 = swim_ITEM2
  201 = PRIMARY_DEPLOYED_aimmatrix_swim
  202 = s_swimAlign_PRIMARY_DEPLOYED
  203 = swim_PRIMARY_DEPLOYED
  204 = s_swimAlign_LOSER
  205 = swim_LOSER
  206 = crouch_deployed_PRIMARY
  207 = stand_deployed_PRIMARY
  208 = PRIMARY_deployed_movement
  209 = deployed_crouch_walk_PRIMARY
  210 = run_PRIMARY
  211 = crouch_walk_PRIMARY
  212 = run_SECONDARY
  213 = crouch_walk_SECONDARY
  214 = crouch_walk_MELEE
  215 = run_MELEE_ALLCLASS
  216 = crouch_walk_MELEE_ALLCLASS
  217 = run_LOSER
  218 = run_ITEM1
  219 = crouch_walk_ITEM1
  220 = run_ITEM2
  221 = crouch_walk_ITEM2
  222 = crouch_deployed_ITEM2
  223 = stand_deployed_ITEM2
  224 = ITEM2_deployed_movement
  225 = crouch_walk_ITEM2_deployed
  226 = airwalk_PRIMARY
  227 = SECONDARY_aimmatrix_airwalk
  228 = airwalk_SECONDARY
  229 = airwalk_MELEE
  230 = airwalk_MELEE_ALLCLASS
  231 = airwalk_LOSER
  232 = airwalk_ITEM1
  233 = airwalk_ITEM2
  234 = a_grapple_pull_start
  235 = a_grapple_pull_idle
  236 = a_grapple_wall_idle
  237 = layer_grapple_SHOOT
  238 = a_grapple_SHOOT
  239 = armslayer_throw_fire
  240 = throw_fire
  241 = layer_taunt01
  242 = taunt01
  243 = taunt02
  244 = melee_taunt
  245 = taunt03
  246 = layer_taunt04
  247 = taunt04
  248 = taunt06
  249 = layer_taunt_replay
  250 = taunt_replay
  251 = taunt_brutallegendinterior
  252 = taunt_brutallegendin
  253 = taunt_brutallegendout
  254 = taunt_brutallegendinblend
  255 = taunt_brutallegendoutblend
  256 = taunt_brutallegend
  257 = taunt_highFiveStart
  258 = layer_taunt_highFiveSuccess
  259 = taunt_highFiveSuccess
  260 = layer_taunt_highFiveSuccessFull
  261 = taunt_highFiveSuccessFull
  262 = layer_taunt_laugh
  263 = taunt_laugh
  264 = taunt_conga
  265 = layer_taunt_dosido_dance
  266 = taunt_dosido_dance
  267 = layer_taunt_dosido_intro
  268 = taunt_dosido_intro
  269 = taunt_flip_start
  270 = layer_taunt_flip_success_initiator
  271 = taunt_flip_success_initiator
  272 = layer_taunt_flip_success_receiver
  273 = taunt_flip_success_receiver
  274 = taunt_RPS_start
  275 = layer_taunt_rps_rock
  276 = taunt_rps_rock
  277 = layer_taunt_rps_paper
  278 = taunt_rps_paper
  279 = layer_taunt_rps_scissors
  280 = taunt_rps_scissors
  281 = layer_taunt_rps_rock_win
  282 = taunt_rps_rock_win
  283 = layer_taunt_rps_paper_win
  284 = taunt_rps_paper_win
  285 = layer_taunt_rps_scissors_win
  286 = taunt_rps_scissors_win
  287 = layer_taunt_rps_rock_lose
  288 = taunt_rps_rock_lose
  289 = layer_taunt_rps_paper_lose
  290 = taunt_rps_paper_lose
  291 = layer_taunt_rps_scissors_lose
  292 = taunt_rps_scissors_lose
  293 = layer_taunt_headbutt_start
  294 = taunt_headbutt_start
  295 = layer_taunt_headbutt_success
  296 = taunt_headbutt_success
  297 = layer_taunt_i_see_you_primary
  298 = taunt_i_see_you_primary
  299 = a_steer_matrix
  300 = a_vehicle_NS_leans
  301 = kart_idle
  302 = kart_dash
  303 = kart_impact
  304 = kart_impact_big
  305 = a_kart_jump_start
  306 = a_kart_jump_float
  307 = kart_jumpland
  308 = layer_kart_cheer
  309 = kart_cheer
  310 = layer_kart_lose
  311 = kart_lose
  312 = a_steer_taunt_allclass_matrix
  313 = layer_taunt_vehicle_allclass_spawn
  314 = taunt_vehicle_allclass_start_layer
  315 = taunt_vehicle_allclass_start
  316 = layer_taunt_vehicle_allclass_honk
  317 = taunt_vehicle_allclass_honk
  318 = layer_taunt_vehicle_allclass_end
  319 = taunt_vehicle_allclass_end
  320 = taunt_russian
  321 = taunt_aerobic_A
  322 = PASSTIME_aimmatrix_idle
  323 = PASSTIME_aimmatrix_run
  324 = PASSTIME_aimmatrix_crouch_idle
  325 = layer_PASSTIME_throw_begin
  326 = layer_PASSTIME_throw_middle
  327 = layer_PASSTIME_throw_end
  328 = layer_PASSTIME_throw_cancel
  329 = PASSTIME_throw_begin
  330 = PASSTIME_throw_middle
  331 = PASSTIME_throw_end
  332 = PASSTIME_throw_cancel
  333 = stand_PASSTIME
  334 = run_PASSTIME
  335 = crouch_walk_PASSTIME
  336 = taunt_aerobic_B
  337 = a_aerobic_turns
  338 = taunt_aerobic_A_START
  339 = taunt_aerobic_B_START
  340 = competitive_loserstate_idle
  341 = competitive_winnerstate_idle
}
]]--
