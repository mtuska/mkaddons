// RAWR!

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

ENT.Damage = {1, 10} // Damage min and max
ENT.m_Sounds = {
	miss = {
		Sound("npc/zombie/claw_miss1.wav"),
		Sound("npc/zombie/claw_miss2.wav")
	},
	hit = {
		Sound("npc/zombie/claw_strike1.wav"),
		Sound("npc/zombie/claw_strike2.wav"),
		Sound("npc/zombie/claw_strike3.wav")
	},
	idle = {
		Sound("npc/zombie/zombie_voice_idle1.wav"),
		Sound("npc/zombie/zombie_voice_idle2.wav"),
		Sound("npc/zombie/zombie_voice_idle3.wav"),
		Sound("npc/zombie/zombie_voice_idle4.wav"),
		Sound("npc/zombie/zombie_voice_idle5.wav"),
		Sound("npc/zombie/zombie_voice_idle6.wav"),
		Sound("npc/zombie/zombie_voice_idle7.wav"),
		Sound("npc/zombie/zombie_voice_idle8.wav"),
		Sound("npc/zombie/zombie_voice_idle9.wav"),
		Sound("npc/zombie/zombie_voice_idle10.wav"),
		Sound("npc/zombie/zombie_voice_idle11.wav"),
		Sound("npc/zombie/zombie_voice_idle12.wav"),
		Sound("npc/zombie/zombie_voice_idle13.wav"),
		Sound("npc/zombie/zombie_voice_idle14.wav")
	},
	alert = {
		Sound("npc/zombie/zombie_alert1.wav"),
		Sound("npc/zombie/zombie_alert2.wav"),
		Sound("npc/zombie/zombie_alert3.wav")
	}
}

function ENT:Initialize()
    self:MKInitialize()

    local model, name = table.Random(player_manager.AllValidModels())
	self:SetModel(model)
	--self:SetModel("models/Humans/Group01/Female_01.mdl")

	self:SetBotType("MKNextBot_Zombie")
	self:SetDisplayName("Zombie")

    self.m_JustSpawned = true

    self:SetHealth(500)
    self:SetMaxHealth(500)
    self:SetWalkSpeed(50)
    self:SetRunSpeed(75)
end

function ENT:Think()
    self.MKBaseClass.Think(self)
    self:FindEnemy()
end

function ENT:RunBehaviour()
    while(true) do
        if (self.m_JustSpawned) then
            self.m_JustSpawned = false
            self:VoiceSound(self.m_Sounds.alert)
            self:PlaySequenceAndWait("taunt_zombie_original", 1)
        end

        if (self:FindEnemy()) then
			self.Investigate = nil
            self.loco:SetDesiredSpeed(self:GetRunSpeed())
            self:Chase()
		elseif (self.Investigate) then
			self.loco:SetDesiredSpeed(self:GetWalkSpeed())
			self:VoiceSound(self.m_Sounds.idle)
			local pos = self.Investigate
			self:MoveToPos(self.Investigate)
			if (pos == self.Investigate) then
				self.Investigate = nil
			end
        else
            self.loco:SetDesiredSpeed(self:GetWalkSpeed())
    		self.loco:SetJumpHeight( 40 )
    		//self.loco:SetDeathDropHeight( self.StepHeight )

            self:VoiceSound(self.m_Sounds.idle)

    		self:MoveToPos(self:FindNewDestination(math.random(200, 400)))

			coroutine.wait(math.random(1.5))

	        if (math.random(100) <= 33) then
				self:VoiceSound(self.m_Sounds.idle)
			end
        end

        coroutine.yield()
    end
end

function ENT:OnSetEnemy()
    self:StopMovingToPos()
    self:VoiceSound(self.m_Sounds.alert)
end

function ENT:OnInjured(dmginfo)
	self.MKBaseClass.OnInjured(self, dmginfo)
	self:SetEnemy(dmginfo:GetAttacker())
end

function ENT:OnKilled(dmginfo)
	self.MKBaseClass.OnKilled(self, dmginfo)
end

function ENT:AttackBehaviour()
	//BroadcastLua("print('Behave attack')")

	if (math.random(100) <= 1) then
		self:VoiceSound(self.m_Sounds.idle)
	end

	// Do a distance check in squares, since this will actually be run A LOT
	if (self:GetPos():DistToSqr(self:GetEnemy():GetPos()) > 10000) then // 100^2
		return
	end

    if (CurTime() < (self.m_NextAttack or 0)) then return end
    self.m_NextAttack = CurTime() + 2

	local enemy = self:GetEnemy() // We check if enemy is valid before calling this function

	if (!self.m_Reviving) then
		self.m_Attacking = true
		//self:RestartGesture(ACT_GMOD_GESTURE_RANGE_ZOMBIE, true, true)
		local attack = math.random(1, 7)
		local layer = self:AddGestureSequence(self:LookupSequence("zombie_attack_0"..attack), true)
		self:SetLayerPlaybackRate(layer,0.6)
		self:SetPlaybackRate(0.1)

		local damage = math.random(unpack(self.Damage))
		timer.Simple(0.9, function()
			if (!IsValid(self)) then return end
			if (!self:Alive()) then return end

			if (!IsValid(enemy)) then
				self:VoiceSound(self.m_Sounds.miss)
				return
			end

			if (!enemy:Alive()) then
				self:VoiceSound(self.m_Sounds.miss)
				return
			end

			if (self:GetRangeTo(enemy) < 55) then
				self:VoiceSound(self.m_Sounds.hit)
				enemy:TakeDamage(damage, self)
				if (enemy:IsPlayer()) then
					enemy:ViewPunch(Angle(math.random(-1, 1)*damage, math.random(-1, 1)*damage, math.random(-1, 1)*damage))
				end
				local bleed = ents.Create("info_particle_system")
				bleed:SetKeyValue("effect_name", "blood_impact_red_01")
				bleed:SetPos(enemy:GetPos() + Vector(0,0,70))
				bleed:Spawn()
				bleed:Activate()
				bleed:Fire("Start", "", 0)
				bleed:Fire("Kill", "", 0.2)

				local moveAdd=Vector(0,0,150)
				if (!enemy:IsOnGround()) then
					moveAdd=Vector(0,0,0)
				end
				enemy:SetVelocity(moveAdd+((enemy:GetPos()-self:GetPos()):GetNormal()*100)) -- apply the velocity
			else
				self:VoiceSound(self.m_Sounds.miss)
			end
			self.m_Attacking = false
		end)
	end
end

function ENT:IsEnemy(ent)
	//return false
	if (!ent||!IsValid(ent)) then return end
	local b = self.MKBaseClass.IsEnemy(self, ent)
	if (ent.IsZombie and ent:IsZombie()) then
		return false
	end
	return b
	//return ent.SteamID and ent:SteamID() == "STEAM_0:1:7102488"
end

function ENT:IsZombie()
	return true
end

function ENT:CalcIdleActivity()
	return ACT_HL2MP_IDLE_ZOMBIE
end

function ENT:CalcWalkActivity()
	return ACT_HL2MP_WALK_ZOMBIE_01
end

function ENT:CalcRunActivity()
	return ACT_HL2MP_RUN_ZOMBIE
end

--[[
npc/zombie/claw_miss1.wav
npc/zombie/claw_miss2.wav
npc/zombie/claw_strike1.wav
npc/zombie/claw_strike2.wav
npc/zombie/claw_strike3.wav
npc/zombie/foot1.wav
npc/zombie/foot2.wav
npc/zombie/foot3.wav
npc/zombie/foot_slide1.wav
npc/zombie/foot_slide2.wav
npc/zombie/foot_slide3.wav
npc/zombie/moan_loop1.wav
npc/zombie/moan_loop2.wav
npc/zombie/moan_loop3.wav
npc/zombie/moan_loop4.wav
npc/zombie/zombie_alert1.wav
npc/zombie/zombie_alert2.wav
npc/zombie/zombie_alert3.wav
npc/zombie/zombie_die1.wav
npc/zombie/zombie_die2.wav
npc/zombie/zombie_die3.wav
npc/zombie/zombie_hit.wav
npc/zombie/zombie_pain1.wav
npc/zombie/zombie_pain2.wav
npc/zombie/zombie_pain3.wav
npc/zombie/zombie_pain4.wav
npc/zombie/zombie_pain5.wav
npc/zombie/zombie_pain6.wav
npc/zombie/zombie_pound_door.wav
npc/zombie/zombie_voice_idle1.wav
npc/zombie/zombie_voice_idle10.wav
npc/zombie/zombie_voice_idle11.wav
npc/zombie/zombie_voice_idle12.wav
npc/zombie/zombie_voice_idle13.wav
npc/zombie/zombie_voice_idle14.wav
npc/zombie/zombie_voice_idle2.wav
npc/zombie/zombie_voice_idle3.wav
npc/zombie/zombie_voice_idle4.wav
npc/zombie/zombie_voice_idle5.wav
npc/zombie/zombie_voice_idle6.wav
npc/zombie/zombie_voice_idle7.wav
npc/zombie/zombie_voice_idle8.wav
npc/zombie/zombie_voice_idle9.wav
npc/zombie/zo_attack1.wav
npc/zombie/zo_attack2.wav
]]--
