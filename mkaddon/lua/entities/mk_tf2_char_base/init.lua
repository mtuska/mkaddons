// RAWR!

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

ENT.WeaponAttachment = "weapon_bone"

function ENT:Initialize()
    self:MKInitialize()
end

local blueeyes = {EyeType = "eye", ColorR = 27, ColorG = 235, ColorB = 221}
local yelloweyes = {EyeType = "eye", ColorR = 255, ColorG = 170, ColorB = 0}
local redeyes = {EyeType = "eye", ColorR = 229, ColorG = 28, ColorB = 35}
function ENT:Think()
    self.MKBaseClass.Think(self)

    if (!self.eyeparticle1) then
        if (self:GetSkin() == 0) then
            self:GiveRobotEyeGlow(redeyes)
        elseif (self:GetSkin() == 1) then
            self:GiveRobotEyeGlow(blueeyes)
        else
            self:GiveRobotEyeGlow(yelloweyes)
        end
    end
end

function ENT:GiveRobotEyeGlow(Data)
	local eyetype = Data.EyeType
	local colorr = Data.ColorR
	local colorg = Data.ColorG
	local colorb = Data.ColorB

	local ID = self:LookupAttachment(eyetype.."_1")
	local Attachment = self:GetAttachment( ID )
	if (Attachment == nil) then return end

	self.eyeparticle1 = ents.Create( "info_particle_system" )
	self.eyeparticle1:SetPos( Attachment.Pos )
	self.eyeparticle1:SetAngles( Attachment.Ang )
	self:DeleteOnRemove(self.eyeparticle1)

	PrecacheParticleSystem("alt_bot_eye_glow")
	self.eyeparticle1:SetKeyValue( "effect_name", "alt_bot_eye_glow" )
	self.eyeparticle1:SetKeyValue( "start_active", "1")

	local colorcontrol = ents.Create( "info_particle_system" )
	colorcontrol:SetPos( Vector(colorr,colorg,colorb) )
	self.eyeparticle1:DeleteOnRemove(colorcontrol)
	colorcontrol:SetKeyValue( "effect_name", "alt_bot_eye_glow" )
	//colorcontrol:SetKeyValue( "globalname", "colorcontrol_".. self.eyeparticle1:EntIndex())
	colorcontrol:SetName("colorcontrol_".. self.eyeparticle1:EntIndex())
	colorcontrol:Spawn()

	self.eyeparticle1:SetParent(self)
	self.eyeparticle1:Fire("setparentattachment", eyetype.. "_1", 0.01)
	self.eyeparticle1:SetKeyValue( "cpoint1", "colorcontrol_".. self.eyeparticle1:EntIndex() ) //the color is controlled by the position of this entity -
										 			     //if the colorcontroller's position is 255, 255, 255,
											 		     //the color of the particle becomes white (255 255 255)
	self.eyeparticle1:Spawn()
	self.eyeparticle1:Activate()

	//now for eye two
	local ID = self:LookupAttachment( eyetype.. "_2" )
	local Attachment = self:GetAttachment( ID )
	if (Attachment != nil) then
		self.eyeparticle2 = ents.Create( "info_particle_system" )
		self.eyeparticle2:SetPos( Attachment.Pos )
		self.eyeparticle2:SetAngles( Attachment.Ang )
		self.eyeparticle1:DeleteOnRemove(self.eyeparticle2)
		self.eyeparticle2:SetKeyValue( "effect_name", "alt_bot_eye_glow" )
		self.eyeparticle2:SetKeyValue( "start_active", "1")
		self.eyeparticle2:SetParent(self)
		self.eyeparticle2:Fire("setparentattachment", eyetype.. "_2", 0.01)
		self.eyeparticle2:SetKeyValue( "cpoint1", "colorcontrol_".. self.eyeparticle1:EntIndex() )
		self.eyeparticle2:Spawn()
		self.eyeparticle2:Activate()
	end
end

--[[
{
  1:
  {
    name = head
    id = 1
  }
  2:
  {
    name = eye_2
    id = 2
  }
  3:
  {
    name = eye_1
    id = 3
  }
  4:
  {
    name = eye_boss_2
    id = 4
  }
  5:
  {
    name = eye_boss_1
    id = 5
  }
  6:
  {
    name = weapon_bone_L
    id = 6
  }
  7:
  {
    name = weapon_bone
    id = 7
  }
  8:
  {
    name = weapon_bone_1
    id = 8
  }
  9:
  {
    name = weapon_bone_2
    id = 9
  }
  10:
  {
    name = weapon_bone_3
    id = 10
  }
  11:
  {
    name = weapon_bone_4
    id = 11
  }
  12:
  {
    name = flag
    id = 12
  }
  13:
  {
    name = boss_eye_2
    id = 13
  }
  14:
  {
    name = boss_eye_1
    id = 14
  }
}

http://badgercode.co.uk/sounds.php

vo/halloween_boo1.wav
vo/halloween_boo2.wav
vo/halloween_boo3.wav
vo/halloween_boo4.wav
vo/halloween_boo5.wav
vo/halloween_boo6.wav
vo/halloween_boo7.wav
vo/halloween_haunted1.wav
vo/halloween_haunted2.wav
vo/halloween_haunted3.wav
vo/halloween_haunted4.wav
vo/halloween_haunted5.wav
vo/halloween_moan1.wav
vo/halloween_moan2.wav
vo/halloween_moan3.wav
vo/halloween_moan4.wav
vo/halloween_scream1.wav
vo/halloween_scream2.wav
vo/halloween_scream3.wav
vo/halloween_scream4.wav
vo/halloween_scream5.wav
vo/halloween_scream6.wav
vo/halloween_scream7.wav
vo/halloween_scream8.wav
]]--
