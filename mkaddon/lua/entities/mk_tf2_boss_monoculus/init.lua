// RAWR!

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
    self.BaseClass.Initialize(self)

    self:SetModel("models/props_halloween/halloween_demoeye.mdl")
	self:SetCollisionBounds(Vector(-32,-32,-32), Vector(32,32,32))
	self:SetPos(self:GetPos() + Vector(0,0,32))

	self:LoopSequence("angry", 1)
end

function ENT:Think()
	if (IsValid(self:GetTarget())) then
		self:PointAtEntity(self:GetTarget())
	else
		/*if (self:GetSequence() == self:LookupSequence("angry")) then
			self:PlaySequence("lookaround"..math.random(1,3), 1)
		end*/
	end
	self.BaseClass.Think(self)
end

function ENT:RunBehaviour()
    while(true) do
    	self:FlyToPos(self:FindNewDestination())

        coroutine.wait(5)

        coroutine.yield()
    end
end
--[[
misc/halloween_eyeball/book_exit.wav
misc/halloween_eyeball/book_spawn.wav
misc/halloween_eyeball/vortex_eyeball_died.wav
misc/halloween_eyeball/vortex_eyeball_moved.wav

vo/halloween_eyeball/eyeball01.wav
vo/halloween_eyeball/eyeball02.wav
vo/halloween_eyeball/eyeball03.wav
vo/halloween_eyeball/eyeball04.wav
vo/halloween_eyeball/eyeball05.wav
vo/halloween_eyeball/eyeball06.wav
vo/halloween_eyeball/eyeball07.wav
vo/halloween_eyeball/eyeball08.wav
vo/halloween_eyeball/eyeball09.wav
vo/halloween_eyeball/eyeball10.wav
vo/halloween_eyeball/eyeball11.wav
vo/halloween_eyeball/eyeball_biglaugh01.wav
vo/halloween_eyeball/eyeball_boss_pain01.wav
vo/halloween_eyeball/eyeball_laugh01.wav
vo/halloween_eyeball/eyeball_laugh02.wav
vo/halloween_eyeball/eyeball_laugh03.wav
vo/halloween_eyeball/eyeball_mad01.wav
vo/halloween_eyeball/eyeball_mad02.wav
vo/halloween_eyeball/eyeball_mad03.wav
vo/halloween_eyeball/eyeball_teleport01.wav
]]--
/*
0	=	ref
1	=	angry
2	=	general_noise
3	=	stunned
4	=	firing1
5	=	firing2
6	=	firing3
7	=	arrives
8	=	laugh
9	=	longlaugh
10	=	teleport_out
11	=	teleport_in
12	=	huff
13	=	huff2
14	=	death
15	=	lookaround1
16	=	lookaround2
17	=	lookaround3
18	=	escape
19	=	left_right
20	=	up_down
21	=	anger_pose
22	=	suprised_pose
*/
