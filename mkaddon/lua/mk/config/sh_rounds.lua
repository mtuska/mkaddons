// RAWR!

MK.config.Add("rounds_enabled", false, function(newValue, oldValue)
	MK.rounds.state = 0
	MK.rounds.count = 0
	if (newValue) then
		if (SERVER&&!timer.Exists("MK_Rounds_CheckState")) then
			timer.Create("MK_Rounds_CheckState", 2, 0, function()
				if (MK.config.dev) then
					print("Check if change round state")
				end
				if (MK.config.rounds.types[MK.rounds.state]&&MK.config.rounds.types[MK.rounds.state].shouldChangeState) then
					local result = MK.config.rounds.types[MK.rounds.state].shouldChangeState()
					if (result) then
						MK.rounds.SetState(result)
					end
				end
			end)
		end
	elseif (newValue) then
		if (timer.Exists("MK_Rounds_CheckState")) then
			timer.Destroy("MK_Rounds_CheckState")
		end
		if (timer.Exists("MK_Rounds_Timer")) then
			timer.Destroy("MK_Rounds_Timer")
		end
	end
end)
MK.config.Add("rounds_max", 0)