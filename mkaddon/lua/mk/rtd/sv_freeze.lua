// RAWR!

MK.rtd.Add("freeze", {chance = 50}, function(ply)
	local duration = math.random(5, 10)
	MK.rtd.Broadcast(ply, " has been frozen for ", duration, " seconds!")
	ply:Freeze(true)
	//ply:SetPlayerColor(Vector(0,0,1))
	ply:EmitSound("physics/glass/glass_sheet_break1.wav")
	timer.Simple(duration, function()
		if (!IsValid(ply)) then return end
		ply:Freeze(false)
		MK.rtd.Broadcast(ply, " has been unfrozen!")
		//ply:SetColor(255, 255, 255, 255)
	end)
end)