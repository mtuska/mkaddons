// RAWR!

//lua_run MK.events.Start('zombies')
local EVENT = table.Copy(MK.meta.Event)
EVENT.name = "Zombies!!"
EVENT.notification = "AHHHH, ZOMBIESSS!"
EVENT.length = 120
EVENT.minZombies = 100
EVENT.maxZombies = 200
EVENT.zombieTypes = {"mk_zombie_basic"}
//EVENT.zombieTypes = {"mk_bottest"}
EVENT.zombies = {}
EVENT.spawnAreas = {}

function EVENT:Start()
    local ent = scripted_ents.Get(self.zombieTypes[1])
    for _,area in pairs(navmesh.GetAllNavAreas()) do
        if (ent.BasicCanUseNavArea(area)) then
            table.insert(self.spawnAreas, area)
        end
    end

    self:CheckZombies()
    local delay = 5
    timer.Create("Events_Zombies", delay, 0, function()
        self:CheckZombies()
    end)
end

function EVENT:CheckZombies()
    for k,ent in pairs(self.zombies) do
        if (!IsValid(ent)) then
            self.zombies[k] = nil
        end
    end
    for i=0, (self.minZombies-table.Count(self.zombies)) do
        self:SpawnZombie()
    end
end

function EVENT:SpawnZombie()
    local pos = table.Random(self.spawnAreas):GetRandomPoint()
    if (MK.nextbots.TestSpawn(pos, false)) then
        local ent = ents.Create(table.Random(self.zombieTypes))
        ent:SetPos(pos)
        ent:Spawn()
        table.insert(self.zombies, ent)
    end
end

function EVENT:End()
    for k,ent in pairs(self.zombies) do
        if (!IsValid(ent)) then
            continue
        end
        ent:Remove()
    end
    timer.Destroy("Events_Zombies")
end

function EVENT.Test()
    if (!SW) then return true end
    if (SW.WeatherMode == "rain" || SW.WeatherMode == "storm" || SW.WeatherMode == "fog") then
        return true
    end
    if (SW.Time < 4 || SW.Time >= 20) then
        return true
    end
    return false
end

MK.events.Add("zombies", EVENT)
