// RAWR!

local function getIndex(gmonly, uid)
	local index = "XP"
	if (gmonly) then
		//index = index.."_"..gmod.GetGamemode().FolderName
		index = gmod.GetGamemode().FolderName
	end
	if (uid) then
		//index = index.."_"..uid
		index = uid
	end
	return index
end

function MK.util.XPToLVL(xp)
	local lvl = 0
	for k,v in pairs(MK.config.Get("experience_customLevels", {})) do
		if (v<xp) then
			lvl = k
		elseif (v>xp) then
			return lvl
		end
	end
	
	local xp = math.abs(xp or 1)
	if (xp == 0) then
		xp = 1
	end
	local lvl = (MK.config.Get("experience_multiplier")*(math.log10(xp)))
	return math.floor(lvl)
end
function MK.util.LVLToXP(lvl, max)
	local levels = MK.config.Get("experience_customLevels", {})
	if (levels[lvl]) then return levels[lvl] end
	
	local lvl = math.abs(lvl or 0)
	local max = math.abs(max or MK.config.Get("experience_maxLevel"))
	if (lvl > max) then
		lvl = max
	end
	local xp = (10^(lvl/MK.config.Get("experience_multiplier")))
	return math.ceil(xp)
end

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_XP_Give(amount, gmonly, uid)
		return self:MK_XP_Set(self:MK_XP_Get(gmonly, uid) + math.abs(amount), gmonly, uid)
	end

	function Player:MK_XP_Take(amount, gmonly, uid)
		return self:MK_XP_Set(self:MK_XP_Get(gmonly, uid) - math.abs(amount), gmonly, uid)
	end
	
	function Player:MK_XP_Set(amount, gmonly, uid)
		if (!MK.config.Get("experience_enabled")) then return false end
		if (amount < 0) then
			amount = 0
		end
		local experience = self:MK_GameData_Get("xp", {})
		experience[getIndex(gmonly, uid)] = amount
		return self:MK_GameData_Set("xp", experience)
	end
end

function Player:MK_XP_Get(gmonly, uid)
	if (!MK.config.Get("experience_enabled")) then return false end
	local experience = self:MK_GameData_Get("xp", {})
	return experience[getIndex(gmonly, uid)] or 0
end

function Player:MK_XP_Has(amount, gmonly, uid)
	if (!MK.config.Get("experience_enabled")) then return false end
	return self:MK_XP_Get(gmonly, uid) >= amount
end

function Player:MK_XP_GetLVL(gmonly, uid)
	if (!MK.config.Get("experience_enabled")) then return false end
	return MK.util.XPToLVL(self:MK_XP_Get(gmonly, uid) or 0)
end

function Player:MK_XP_HasLVL(amount, gmonly, uid)
	if (!MK.config.Get("experience_enabled")) then return false end
	return self:MK_XP_GetLVL(gmonly, uid) >= amount
end