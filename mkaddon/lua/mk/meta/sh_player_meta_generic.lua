// RAWR!

local Player = FindMetaTable("Player")

function Player:MK_IsClientLoaded()
	return self:MK_GameData_Get("clientInitialized", false)
end

function Player:MK_Alive()
	if (TEAM_GUARD_DEAD&&self:Team() == TEAM_GUARD_DEAD) then return false end
	if (TEAM_PRISONER_DEAD&&self:Team() == TEAM_PRISONER_DEAD) then return false end
	
	return self:Alive()
end