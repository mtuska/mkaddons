// RAWR!

local function getIndex(gmonly, uid)
	local index = "displayIntro"
	if (gmonly) then
		index = index.."_"..gmod.GetGamemode().FolderName
	end
	if (uid) then
		index = index.."_"..uid
	end
	return index
end

local Player = FindMetaTable("Player")

function Player:MK_CanDisplayIntro()
	return ply:MK_Data_Get(getIndex(true), true)
end

if (SERVER) then
	net.Receive("MK_HUDPaint_Intro", function(len, ply)
		ply:MK_Data_Set(getIndex(true), false, true)
	end)
end