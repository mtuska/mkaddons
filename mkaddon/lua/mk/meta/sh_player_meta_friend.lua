// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	util.AddNetworkString("MK_Player_SendChat")
else
	local function sendChat(len)
		local tbl = net.ReadTable()
		if (!LocalPlayer().MK_SendChat) then return end
		LocalPlayer():MK_SendChat(unpack(tbl))
	end

	net.Receive("MK_Player_SendChat", sendChat)
end

function Player:MK_IsSteamFriend(ply)
	/*if (CLIENT) then // If someone is hacking, they might've messed with this. But not with our custom friends api :P
		if (self == LocalPlayer()) then
			return ply:GetFriendStatus() == "friend"
		elseif (ply == LocalPlayer()) then
			return self:GetFriendStatus() == "friend"
		end
	end*/
	return MK.steamfriends.IsFriends(self, ply)
end

function Player:MK_GetSteamFriends()
	return MK.steamfriends.list[self:SteamID()]
end

function Player:MK_IsPlayingWithSteamFriends()
	return self:MK_CountSteamFriendsOnline() > 0
end

function Player:MK_CountPlayingWithSteamFriends()
	local count = 0
	for k,v in pairs(player.GetAll()) do
		if (self:MK_IsSteamFriend(v)) then
			count = count + 1
		end
	end
	return count
end

function Player:MK_GetPlayingWithSteamFriends()
	local friends = 0
	for k,v in pairs(player.GetAll()) do
		if (self:MK_IsSteamFriend(v)) then
			table.insert(friends, v)
		end
	end
	return friends
end
