// RAWR!

local Player = FindMetaTable("Player")
oldShootPos = oldShootPos or Player.GetShootPos 
function Player:GetShootPos()
    if (self:MK_GameData_Get("thirdperson_enabled", false)) then
        local data = MK.ThirdPerson(self, oldShootPos(self), self:EyeAngles())
		if (data) then
			return data.origin
		else
			return oldShootPos(self)
		end
    else
        return oldShootPos(self)
    end
end
Player.EyePos = Player.GetShootPos