// RAWR!

function MK.util.StackInv(inventory, class, quantity, data)
	local stack, index
	quantity = quantity or 1

	inventory[class] = inventory[class] or {}
	
	for k, v in pairs(inventory[class]) do
		if (data and v.data and MK.util.IsSimilarTable(v.data, data)) then
			stack = v
			index = k

			break
		elseif (!data and !v.data) then
			stack = v
			index = k

			break
		end
	end

	-- Here we see if the item should be added or removed.
	if (!stack and quantity > 0) then
		table.insert(inventory[class], {quantity = quantity, data = data})
	else
		stack = stack or {}
		index = index or table.GetFirstKey(inventory[class])
		-- A stack already exists, so add or take from it.
		stack.quantity = (stack.quantity or 0) + quantity
		
		-- If the quantity is negative, meaning we take from the stack, remove
		-- the stack from the inventory.
		if (stack.quantity <= 0 and inventory[class][index]) then
			inventory[class][index] = nil
		end

		-- If there is nothing completely in the class, remove it from the inventory
		-- completely to reduce data that is saved.
		if (table.Count(inventory[class]) <= 0) then
			inventory[class] = nil
		end
	end

	return inventory
end

local inventory = {}
inventory.buffer = {}

function inventory:Add(class, quantity, data)
	self.buffer = MK.util.StackInv(self.buffer, class, quantity, data)
end

local function getIndex(gmonly, uid)
	local index = "INV"
	if (gmonly) then
		index = index.."_"..gmod.GetGamemode().FolderName
	end
	if (uid) then
		index = index.."_"..uid
	end
	return index
end

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_Inv_Add(gmonly, uid)
		
	end
end

function Player:MK_Inv_Get(gmonly, uid)
	return self:MK_Data_Get(getIndex(gmonly, uid), {})
end