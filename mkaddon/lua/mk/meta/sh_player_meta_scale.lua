// RAWR

local Sizing = {}	
Sizing.Default =
{
	StandingHull =
	{
		Minimum = Vector( -16, -16, 00 ),
		Maximum = Vector(  16,  16, 72 ),
	},
	DuckingHull =
	{
		Minimum = Vector( -16, -16, 00 ),
		Maximum = Vector(  16,  16, 36 ),
	},
	JumpPower = 160,
	StepSize = 18,
	
	--MaxRunSpeed = 1500,
	--MaxWalkSpeed= 1500,
	MaxStepSize = 500,
	MaxHullScale = 3,
	
	RunSpeed = 500,
	WalkSpeed = 250,
	ViewOffset = Vector( 0, 0, 64 ),
	ViewOffsetDuck = Vector( 0, 0, 28 ),
	--AdjustToHeight = true,
}

local EyeAdjustments =
{
	["models/characters/sh/tails.mdl"] = 0.45,
	["models/characters/sh/rouge.mdl"] = 0.6,
	["models/characters/sh/knuckles.mdl"] = 0.63,
	["models/characters/sh/knuckles.mdl"] = 0.6,
	["models/characters/sh/shadow.mdl"] = 0.6,
	["models/characters/sh/sonic.mdl"] = 0.6,
	["models/characters/sh/supershadow.mdl"] = 0.6,
	["models/characters/sh/rouge_sa2.mdl"] = 0.6,
	["models/characters/sh/amy.mdl"] = 0.6,
	["models/player/headcrab.mdl"] = 0.6
}

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_Scale_Set(scale)
		local ply = self
		local scale = scale or 1
		
		local psc = 1.0 --psc = player scale
		if (EyeAdjustments[ply:GetModel()]) then
			psc=EyeAdjustments[ply:GetModel()]
		end
		
		ply:SetModelScale(scale, 1)
		ply:SetHull(Sizing.Default.StandingHull.Minimum*scale, Sizing.Default.StandingHull.Maximum*scale)
		ply:SetHullDuck(Sizing.Default.DuckingHull.Minimum*scale, Sizing.Default.DuckingHull.Maximum*scale)
		ply:SetViewOffset(Sizing.Default.ViewOffset*scale)
		ply:SetViewOffsetDucked(Sizing.Default.ViewOffsetDuck*scale)
		
		ply:SetStepSize(Sizing.Default.StepSize/* * scale*/)

		local hat = ply:LookupBone("ValveBiped.Bip01_Head1")
		if hat then
			ply:ManipulateBoneScale(hat, Vector(1, 1, 1) / scale^(1/3))
		end
		ply:SetPlaybackRate(1 / scale)
		umsg.Start("playerscale")
			umsg.Entity(ply)
			umsg.Float(scale)
		umsg.End()
	end
else
	local function doScale(um)
		local ply = um:ReadEntity()
		local scale = um:ReadFloat()

		if not IsValid(ply) then return end
		ply:SetModelScale(scale, 1)
		ply:SetRenderBounds(Sizing.Default.StandingHull.Minimum*scale, Sizing.Default.StandingHull.Maximum*scale)
		ply:SetHull(Sizing.Default.StandingHull.Minimum*scale, Sizing.Default.StandingHull.Maximum*scale)
		ply:SetHullDuck(Sizing.Default.DuckingHull.Minimum*scale, Sizing.Default.DuckingHull.Maximum*scale)
		ply:SetViewOffset(Sizing.Default.ViewOffset*scale)
		ply:SetViewOffsetDucked(Sizing.Default.ViewOffsetDuck*scale)

		local hat = ply:LookupBone("ValveBiped.Bip01_Head1")
		if hat then
			ply:ManipulateBoneScale(hat, Vector(1, 1, 1) / scale^(1/3))
		end
		ply:SetPlaybackRate(1 / scale)
		
		if (Legs && Legs.EnabledVar && Legs.LegEnt) then
			Legs.LegEnt:SetModelScale(scale, 1)
		end
	end
	usermessage.Hook("playerscale", doScale)
end