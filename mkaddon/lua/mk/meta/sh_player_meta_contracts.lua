// RAWR!

local Player = FindMetaTable("Player")

function Player:MK_Contracts_Get(id)
    return self:MK_GameData_Get("contracts", {})[id] or 0
end

function Player:MK_Contracts_Completed(id, count)
    return self:MK_Contracts_Get(id) >= MK.contracts.Get(id).count
end

if (SERVER) then
    function Player:MK_Contracts_Set(id, count)
        if (self:MK_Contracts_Completed(id)) then
            return
        end
        local contracts = self:MK_GameData_Get("contracts", {})
        contracts[id] = count
        self:MK_GameData_Set("contracts", contracts)
        if (self:MK_Contracts_Completed(id)) then
            MK.contracts.Get(id).onreceive(ply)
        end
    end

    function Player:MK_Contracts_Add(id, count)
        self:MK_Contracts_Set(id, self:MK_Contracts_Get(id) + count)
    end
end
