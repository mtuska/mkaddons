// RAWR!

local function getIndex(gmonly, uid)
	local index = "CURRENCY"
	if (gmonly) then
		index = index.."_"..gmod.GetGamemode().FolderName
	end
	if (uid) then
		index = index.."_"..uid
	end
	return index
end

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_Currency_Give(amount, gmonly, uid)
		return self:MK_Currency_Set(self:MK_Currency_Get(gmonly, uid) + math.abs(amount), gmonly, uid)
	end
	
	function Player:MK_Currency_Take(amount, gmonly, uid)
		return self:MK_Currency_Set(self:MK_Currency_Get(gmonly, uid) - math.abs(amount), gmonly, uid)
	end
	
	function Player:MK_Currency_Set(amount, gmonly, uid)
		return self:MK_Data_Set(getIndex(gmonly, uid), amount, MK.config.currency.types[uid or "default"].shouldSave)
	end
end

function Player:MK_Currency_Get(gmonly, uid)
	return self:MK_Data_Get(getIndex(gmonly, uid), 0)
end

function Player:MK_Currency_Has(amount, gmonly, uid)
	return self:MK_Currency_Get(gmonly, uid) >= amount
end