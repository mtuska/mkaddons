// RAWR!

// This gamemode was modified by Madkiller Max
// This gamemode was created by Madkiller Max

function MK_HUDPaint_Intro()
	local x, y = 0, 0
	local w, h = ScrW(), ScrH()
	
	surface.SetDrawColor(Color(0,0,0,255))
	surface.DrawRect(x, y, w, h)
	
	local text = "Presenting a Madkiller Max Modification"
	if (MK.config.creation) then
		text = "Presenting a Madkiller Max Creation"
	end
	local textW, textH = surface.GetTextSize(text)
	surface.SetTextColor(Color(0,0,0,255))
	surface.SetTextPos(x+(w*0.5)-(textW*0.5), y+(h*0.5)-(textH*0.5))
	surface.DrawText(text)
	
	local text = gmod.GetGamemode().Name
	local textW, textH = surface.GetTextSize(text)
	surface.SetTextColor(Color(0,0,0,255))
	surface.SetTextPos(x+(w*0.5)-(textW*0.5), y+(h*0.75)-(textH*0.5))
	surface.DrawText(text)
end
/*
net.Start("MK_HUDPaint_Intro")
net.SendToServer()
*/