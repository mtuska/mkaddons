// RAWR!

function MK_HUDPaint_XP()
	local gmXP = LocalPlayer():MK_XP_Get(true) or 0
	local gmLevel = LocalPlayer():MK_XP_GetLVL(true) or 0
	local gmLevelXP = MK.util.LVLToXP(gmLevel) or 0
	local gmNextLevel = gmLevel + 1
	local gmNextLevelXP = MK.util.LVLToXP(gmNextLevel)
	
	local x, y = /*math.Max(ScrW()*0.1*/250, ScrH()*0.05
	local width, height = 200, 20
	
	//surface.SetDrawColor(Color(0,0,0,255))
	//surface.DrawRect(x, y, width, height)
	//surface.SetDrawColor(Color(0,0,0,255))
	//surface.DrawOutlinedRect(x, y, width, height)
	
	surface.SetDrawColor(Color(105,105,105,255))
	surface.DrawRect(x, y, width, height)
	surface.SetDrawColor(Color(255,255,255,255))
	local w = gmXP/gmNextLevelXP
	if (gmLevel != 0) then
		w = (gmXP - gmLevelXP)/(gmNextLevelXP - gmLevelXP)
	end
	surface.DrawRect(x + 2, y + 2, (width*w) - 4, height - 4)
	local removeWidth, removeHeight = width*0.15, height*0.15
	//local meth = (math.Clamp(gmXP, 0, gmNextLevelXP) / gmNextLevelXP) * (width-removeWidth)
	//surface.SetDrawColor(Color(100,149,237,255))
	//surface.DrawRect(x+(removeWidth*0.5), y+(removeHeight*5), meth, height-removeHeight)
	
	surface.SetFont("xpSystem_XPDisplay")
	local text = gmXP.." / "..gmNextLevelXP
	local textW, textH = surface.GetTextSize(text)
	surface.SetTextColor(Color(0,0,0,255))
	surface.SetTextPos(x+(width*0.5)-(textW*0.5), y+(height*0.5)-(textH*0.5))
	surface.DrawText(text)
	
	local y = y+height
	local width, height = 50, 25
	
	surface.SetDrawColor(Color(0,0,0,50))
	surface.DrawRect(x, y, width, height)
	surface.SetDrawColor(Color(255,255,255,255))
	surface.DrawOutlinedRect(x, y, width, height)
	
	surface.SetFont("xpSystem_LevelDisplay")
	local text = "LEVEL: "..gmLevel
	local textW, textH = surface.GetTextSize(text)
	surface.SetTextColor(Color(255,255,255,255))
	surface.SetTextPos(x+(width*0.5)-(textW*0.5), y+(height*0.5)-(textH*0.5))
	surface.DrawText(text)
end