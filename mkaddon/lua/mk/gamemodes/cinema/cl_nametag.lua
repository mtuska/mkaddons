// RAWR!

hook.Add("HUDPaint", "HUDPaint.NameTags", function()
	GM:NameTagPaint()
end)

function GM:PaintTag(Name, ToScreen, Percent, Team)
	NameTagCol.a = 150 * Percent
	NameTagTeamCol.a = 150 * Percent

	if Team then Name = "[TEAM] ".. Name end 
	
	surface.SetFont("TabLarge")
	local TWidth, THeight = surface.GetTextSize(Name)
	
	draw.RoundedBox(4, ToScreen.x - (TWidth * 0.5) - 4, ToScreen.y - (THeight * 0.5) - 2, TWidth + 8, THeight + 4, NameTagCol)
	surface.SetTextColor(0, 0, 0, 100)
	surface.SetTextPos(ToScreen.x - (TWidth * 0.5) + 1, ToScreen.y - (THeight * 0.5) + 1)
	surface.DrawText(Name)
	
	surface.SetTextColor(255, 255, 255, 255 * Percent)
	surface.SetTextPos(ToScreen.x - (TWidth * 0.5), ToScreen.y - (THeight * 0.5))
	surface.DrawText(Name)
end

function GM:NameTagPaint()
	//surface.SetFont("TabLarge")
	surface.SetFont("Default")
	local Pos = LocalPlayer():EyePos()
	for k,v in pairs(player.GetAll()) do
		if (IsValid(v) and LocalPlayer() != v) then
			if (v:Alive()) then
				local Distance = v:EyePos():Distance(Pos)
				local Dist = 640
				if(Distance <= Dist) then
					local Eyes = v:LookupAttachment("anim_attachment_head")
					if(Eyes) then
						local Attachment = v:GetAttachment(Eyes)
						if(Attachment) then
							-- local tr = util.TraceLine({start = Pos, endpos = Attachment.Pos, mask = MASK_SOLID_BRUSHONLY})
							-- if(tr.Fraction == 1) then
								self:PaintTag(v:Name(), (Attachment.Pos + NameTagUp):ToScreen(),  math.Min((Dist - Distance + 200) / Dist, 1), LocalPlayer():Team() == v:Team() )
							-- end
						end
					end
				end
			end
		end
	end
end