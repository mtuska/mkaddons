// RAWR!

local function CinemaLoadout(ply)
	if (ply:MK_Rank_GetRank() == "owner") then
		ply:Give("weapon_popcorn_spam")
	else
		ply:Give("weapon_popcorn")
	end
end

hook.Add("PlayerLoadout", "PlayerLoadout.CinemaLoadout", CinemaLoadout)
hook.Add("MK_Player.LoadedData", "MK_Player.LoadedData.CinemaLoadout", CinemaLoadout)

hook.Add("PlayerSetModel", "PlayerSetModel.DefaultModels", function(ply)
	ply:SetModel("models/player/kleiner.mdl")
end)

hook.Add("PlayerChangeLocation", "GivePlayerCrowbar", function(ply, loc, old)
	if !IsValid(ply) then return end

	if (string.find(ply:GetLocationName():lower(), "vip")&&!ply:MK_Rank_IsDonator()) then
		ply:Spawn()
		ply:MK_SendChat(Color(100,255,100), "[MK-D] ", Color(255,255,255), "You don't seem to be a donator. You're not allowed in this area!")
	end
end)
