// RAWR!

hook.Add("PlayerSpawn", "PlayerSpawn.JB_DevMode", function(ply)
	if (ply:MK_GameData_Get("devmode", false)&&(ply:Team() == TEAM_GUARD||ply:Team()==TEAM_PRISONER)) then
		ply:KillSilent()
	end
end)

function MK.JB_PlayerDeath(victim, inflictor, attacker)
	if (IsValid(victim)&&victim:IsPlayer()) then
		local ply = victim
		local comp = ply:MK_Data_Gamemode_Get("competitive", {})
		comp.jb = comp.jb or {}
		if (ply:Team() == TEAM_GUARD||ply:Team() == TEAM_GUARD_DEAD) then
			comp.jb.guards = comp.jb.guards or {}
			comp.jb.guards.deaths = (comp.jb.guards.deaths or 0) + 1
		end
		if (ply:Team() == TEAM_PRISONER||ply:Team() == TEAM_PRISONER_DEAD) then
			comp.jb.prisoners = comp.jb.prisoners or {}
			comp.jb.prisoners.deaths = (comp.jb.prisoners.deaths or 0) + 1
		end
		if (comp != ply:MK_Data_Gamemode_Get("competitive", {})) then
			ply:MK_Data_Gamemode_Set("competitive", comp)
		end
	end
	if (IsValid(attacker)&&attacker:IsPlayer()) then
		local ply = attacker
		local comp = ply:MK_Data_Gamemode_Get("competitive", {})
		comp.jb = comp.jb or {}
		if (ply:Team() == TEAM_GUARD||ply:Team() == TEAM_GUARD_DEAD) then
			comp.jb.guards = comp.jb.guards or {}
			comp.jb.guards.kills = (comp.jb.guards.kills or 0) + 1
		end
		if (ply:Team() == TEAM_PRISONER||ply:Team() == TEAM_PRISONER_DEAD) then
			comp.jb.prisoners = comp.jb.prisoners or {}
			comp.jb.prisoners.kills = (comp.jb.prisoners.kills or 0) + 1
		end
		ply:MK_Data_Gamemode_Set("competitive", comp)
	end
end

hook.Add("PlayerDeath", "MK.JB.PlayerDeath.Competitive", MK.JB_PlayerDeath)
