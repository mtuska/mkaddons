// RAWR!

hook.Add("TroopDestroyed", "EM_Experience_TroopDestroyed", function(victimOverlord, victim, attackerOverlord, attacker)
	if (victim:IsTroop() && attacker:IsTroop() && victim != attacker && attackerOverlord:IsSuperAdmin()) then
		attackerOverlord:MK_XP_Give(1, true)
	end
end)