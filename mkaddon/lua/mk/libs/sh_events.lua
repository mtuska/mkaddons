// RAWR!

MK.events = MK.events or {}
MK.events.version = "0.1.0"
MK.events.list = MK.events.list or {}
MK.events.current = MK.events.current or nil

function MK.events.Initialize()
    MK.util.IncludeInternalDir("events")
end

function MK.events.Add(id, data)
    if (!id) then
        error("Event missing id!")
    end
    local data = data or {}
    MK.events.list[id] = data
end

function MK.events.Get(id)
    return MK.events.list[id] or nil
end

function MK.events.Start(id)
    MK.events.End()
    local event = MK.events.Get(id)
    if (!event) then
        error("Event not found by Id '"..id.."'")
    end
    setmetatable(event, MK.meta.Event)
    event:Start()
    MK.events.current = event

    if (event.length) then
        timer.Create("MK_Events.End", event.length, 1, function()
            if (event) then
                event:End()
            end
        end)
    end
    STNotifications.Send(event.notification)
end

function MK.events.End()
    if (MK.events.current) then
        MK.events.current:End()
        MK.events.current = nil
    end
    timer.Destroy("MK_Events.End")
end
