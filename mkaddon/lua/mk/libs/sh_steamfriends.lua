// RAWR!

MK.steamfriends = MK.steamfriends or {}
MK.steamfriends.list = MK.steamfriends.list or {}

function MK.steamfriends.IsFriendsStrict(ply1, ply2)
	if (MK.steamfriends.list[ply1:SteamID()] && MK.steamfriends.list[ply1:SteamID()][ply2:SteamID()]) then
		return true
	end
	return false
end

function MK.steamfriends.IsFriends(ply1, ply2)
	if (MK.steamfriends.IsFriendsStrict(ply1, ply2) || MK.steamfriends.IsFriendsStrict(ply2, ply1)) then
		return true
	end
	return false
end

function MK.steamfriends.GetDate(ply1, ply2)
	if (MK.steamfriends.IsFriendsStrict(ply1, ply2)) then
		return MK.steamfriends.list[ply1:SteamID()][ply2:SteamID()]
	end
	if (MK.steamfriends.IsFriendsStrict(ply2, ply1)) then
		return MK.steamfriends.list[ply2:SteamID()][ply1:SteamID()]
	end
	return nil
end

function MK.steamfriends.GetLength(ply1, ply2)
	if (MK.steamfriends.IsFriends(ply1, ply2)) then
		local date = MK.steamfriends.GetDate(ply1, ply2)
		return os.time() - date
	end
	return nil
end
