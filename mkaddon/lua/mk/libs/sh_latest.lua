// RAWR!

MK.latest = MK.latest or {}

MK.latest.deaths = MK.latest.deaths or {}

function MK.latest.deaths:Clear()
	self.victim = nil
	self.inflictor = nil
	self.attacker = nil
end

function MK.latest.deaths:IsLatestVictim(ply)
	if (!IsValid(ply)) then return false end
	if (!IsValid(self.victim)) then return false end
	return self.victim == ply
end

function MK.latest.deaths:IsLatestAttacker(ent)
	if (!IsValid(ent)) then return false end
	if (!IsValid(self.attacker)) then return false end
	return self.attacker == ent
end

function MK.latest.deaths:PlayerIsLatestVictim(ply)
	if (!IsValid(ply)||!ply.mk||!ply.mk.latest.deaths.time) then return false end
	if (!IsValid(self.victim)) then return false end
	return (self.victim==ply&&ply.mk.latest.deaths.time==self.time)
end

function MK.latest.deaths:PlayerIsLatestAttacker(ply)
	if (!IsValid(ply)||!ply.mk||!ply.mk.latest.deaths.time) then return false end
	if (!IsValid(self.attacker)) then return false end
	return (self.attacker==ply&&ply.mk.latest.deaths.time==self.time)
end

function MK.latest.deaths:Compare(ply, ply2)
	if (!IsValid(ply)||!ply.mk||!ply.mk.latest.deaths.time) then return false end
	if (!IsValid(ply2)||!ply2.mk||!ply2.mk.latest.deaths.time) then return false end
	if (ply.mk.latest.deaths.time==ply2.mk.latest.deaths.time) then
		if (ply.mk.latest.deaths.victim==ply2||ply.mk.latest.deaths.attacker==ply2) then
			return true
		end
	end
	return false
end