// RAWR!

STNotifications = {}

util.AddNetworkString("STNotification")

function STNotifications.Send(message, Type, plys)
	if type(message) != "string" then
		return
	elseif type(Type) != "number" or Type < 1 then
		Type = NOTIFY_GENERIC
	end
	
	net.Start("STNotification")
		net.WriteString(message)
		net.WriteInt(Type, 8)
	net.Send(plys or player.GetAll())
end


concommand.Add("st_notification", function(ply,cmd,args)
	-- no need to check args as function will handle it
	STNotifications.Send(args[1], args[2], args[3])
end)