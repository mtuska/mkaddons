// RAWR!

MK.weapons = MK.weapons or {}
MK.weapons.replaceList = MK.weapons.replaceList or {}

hook.Add("Initialize","MK_Initialize.ReplaceWeapons",function()
	for k,v in pairs(MK.weapons.replaceList)do
		weapons.Register({Base = v, IsDropped = true}, string.lower(k), false)
	end
end)
/*
if SERVER then
	function CheckWeaponReplacements(ply,entity)
		if (MK.weapons.replaceList[entity:GetClass()]) then
			ply:Give(MK.weapons.replaceList[entity:GetClass()])
			return true
		end
		return false
	end
end
*/
// Quick hack to fix pickup ammo bug
local oldRegister = weapons.Register
function weapons.Register(tab,class)
	if tab and tab.Primary and tab.Base and tab.Base == "weapon_mk_base" then
		tab.Primary.DefaultClip	= tab.Primary.ClipSize or 0
		//JB:DebugPrint("Registered JailBreak weapon: "..class)
	end
	return oldRegister(tab,class)
end