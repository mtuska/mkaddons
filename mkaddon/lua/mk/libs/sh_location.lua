// RAWR!

MK.location = MK.location or {}
MK.location.list = MK.location.list or {}
MK.location.default = "Unknown"

if (SERVER) then
	MK.location.list = MK.data.GetData("data/locations.txt", nil, {})
	
	util.AddNetworkString("MK_LocationSync")
	util.AddNetworkString("MK_LocationAdd")
	net.Receive("MK_LocationAdd", function(len, ply)
		if (!ply:IsSuperAdmin()) then return end
		local min = net.ReadVector()
		local max = net.ReadVector()
		local name = net.ReadString()
		MK.location.Add(name, {min = min, max = max})
	end)
	
	hook.Add("MK_Player.Ready", "MK_Player.Ready.LocationSync", function(ply)
		net.Start("MK_LocationSync")
			net.WriteTable(MK.location.list)
		net.Send(ply)
	end)
else
	net.Receive("MK_LocationSync", function(ply, len)
		MK.location.list = net.ReadTable()
	end)
end

function MK.location.Add(location, data, map)
	map = map or game.GetMap()
	if (!data.min||!data.max) then
		ErrorNoHalt("Location '"..location.."' is missing a max/min vector!")
		return
	end
	BroadcastLua("print('Added "..location.."')")
	OrderVectors(data.min, data.max)
	MK.location.list[map] = MK.location.list[map] or {}
	MK.location.list[map][location] = data
	if (SERVER) then
		MK.data.SetData("data/locations.txt", nil, MK.location.list)
		
		net.Start("MK_LocationSync")
			net.WriteTable(MK.location.list)
		net.Broadcast()
	end
end

function MK.location.Get(location, map)
	map = map or game.GetMap()
	return MK.location.list[map][location] or {}
end

function MK.location.Find(pos)
	for k,v in pairs(MK.location.list[game.GetMap()] or {}) do
		OrderVectors(v.min, v.max)
		//local text = "Loc: "..k.." Pos: "..tostring(pos).." Min: "..tostring(v.min).." Max: "..tostring(v.max).." Bool: "..tostring(pos:WithinAABox(v.min, v.max))
		//BroadcastLua("print('"..text.."')")
		if (pos:WithinAABox(v.min - Vector(1,1,1), v.max + Vector(1,1,1))) then // Make changes to overlap locations
			return k
		end
	end
	
	return MK.location.default
end

function MK.location.GetPlayersInLocation(location)
	local players = {}

	for k,ply in pairs(player.GetAll()) do
		if (ply:GetLocation() == location) then
			table.insert(players, ply)
		end
	end

	return players
end