// RAWR!

MK.rtd = MK.rtd or {}
MK.rtd.version = "1.0.0"
MK.rtd.list = {}
MK.rtd.Colour = Color(255, 255, 0, 255)

function MK.rtd.Initialize()
	MK.util.IncludeInternalDir("rtd")
end

function MK.rtd.Add(action, data, func)
	MK.rtd.list[action] = {}
	MK.rtd.list[action].data = data or {}
	MK.rtd.list[action].func = func
end

function MK.rtd.Broadcast(...)
	MK.administration.Broadcast(MK.rtd.Colour, "[MK-RTD] ", ...)
end

function MK.rtd.Run(ply, str)
	local str = str or false
	if (!MK.config.Get("rtd_enabled", false)) then
		ply:MK_SendChat(MK.rtd.Colour, "[MK-RTD] ", Color(255, 255, 255, 255), "Roll The Dice is currently disabled!")
		return
	end
	if (!ply:MK_Rank_IsDonator()) then
		//ply:MK_SendChat(MK.rtd.Colour, "[MK-RTD] ", Color(255, 255, 255, 255), "Only donators can roll the dice!")
		//return
	end
	if (!ply:MK_Alive()) then
		ply:MK_SendChat(MK.rtd.Colour, "[MK-RTD] ", Color(255, 255, 255, 255), "You can't do that while dead!")
		return
	end
	if ((ply:MK_GameData_Get("lastrtd", 0) + MK.config.Get("rtd_delay", 0)) > os.time()) then
		ply:MK_SendChat(MK.rtd.Colour, "[MK-RTD] ", Color(255, 255, 255, 255), "You need to wait "..MK.util.GetTimeleftByString((ply:MK_GameData_Get("lastrtd", 0) + MK.config.Get("rtd_delay", 0)) - os.time()).."!")
		return
	end
	
	local safeTable = {}
	for k,v in pairs(MK.rtd.list) do
		safeTable[k] = v.data.chance or 100
	end
	local action_id = MK.util.PickWeighted(safeTable)
	if (str) then
		action_id = str
	end
	local action = MK.rtd.list[action_id]
	if (action) then
		action.func(ply)
		ply:MK_GameData_Set("lastrtd", os.time())
	end
end