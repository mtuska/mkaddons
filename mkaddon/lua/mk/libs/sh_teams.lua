// RAWR!

function team.GetAlivePlayers(tm)
	local plys = {}
	for k,v in pairs(team.GetPlayers(tm)) do
		if (v:Alive()) then
			table.insert(plys, v)
		end
	end
	return plys
end

function team.NumAlivePlayers(tm)
	return table.Count(team.GetAlivePlayers(tm))
end