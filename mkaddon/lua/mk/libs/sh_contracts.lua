// RAWR!

MK.contracts = MK.contracts or {}
MK.contracts.version = "0.1.0"
MK.contracts.list = MK.contracts.list or {}

function MK.contracts.Add(id, data)
    if (!id) then
        error("Contract missing id!")
    end
    local data = data or {}
    MK.contracts.list[id] = data
end

function MK.contracts.Get(id)
    return MK.contracts.list[id] or {}
end
