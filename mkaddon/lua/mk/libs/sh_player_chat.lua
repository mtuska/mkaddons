// RAWR!

function player.SendChat(players, ...)
	if (type(players) != "table") then
		players = {players}
	end
	for k,v in pairs(players) do
		if (!IsValid(v)) then
			continue
		end
		v:MK_SendChat(...)
	end
end

function player.BroadcastChat(...)
	player.SendChat(player.GetAll(), ...)
end