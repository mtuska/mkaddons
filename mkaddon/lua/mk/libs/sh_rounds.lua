// RAWR!

MK.rounds = MK.rounds or {}
MK.rounds.types = MK.rounds.types or {}
MK.rounds.state = MK.rounds.state or 0
MK.rounds.count = MK.rounds.count or 0

if (SERVER) then
	function MK.rounds.SetState(state)
		if (MK.config.rounds.types[state]) then
			if (MK.config.Get("development")) then
				print("Change current round state")
			end
			MK.rounds.state = state
			if (MK.config.rounds.types[state].time&&MK.config.rounds.types[state].time!=-1) then
				if (timer.Exists("MK_Rounds_Timer")) then
					timer.Destroy("MK_Rounds_Timer")
				end
				timer.Create("MK_Rounds_Timer", MK.config.rounds.types[state].time, 1, function()
					if (MK.config.rounds.types[state].timeup) then
						MK.config.rounds.types[state].timeup()
					end
				end)
			end
			if (MK.config.rounds.types[state].begin) then
				MK.config.rounds.types[state].begin()
			end
		end
	end
	
	function MK.rounds.SetRoundCount(amount)
		if (MK.config.Get("development")) then
			print("Round Count: "..amount)
		end
		MK.rounds.count = amount
	end
	
	function MK.rounds.AddRoundCount()
		if (MK.config.Get("development")) then
			print("Another round has passed")
		end
		MK.rounds.SetRoundCount(MK.rounds.GetRoundCount() + 1)
	end
	
	hook.Add("MK_Rounds_Continue", "MK_Rounds_Continue", function()
		MK.rounds.AddRoundCount()
	end)
end

function MK.rounds.AddState(uid, title, time, begin, shouldChangeState, timeup)
	MK.rounds.types[uid] = {title = title, time = time, begin = begin, shouldChangeState = shouldChangeState, timeup = timeup}
end

function MK.rounds.GetState()
	return MK.rounds.state
end

function MK.rounds.GetRoundCount()
	return MK.rounds.count
end