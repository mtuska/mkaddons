// RAWR!

hook.Add("MK_Player.LoadedData", "MK_Player.LoadedData.PlayTime", function(ply, newUser)
	if (!ply:IsBot()) then
		local uid = "PlayTimeUpdater_" .. ply:SteamID()
		local time = 300
		timer.Create(uid, time, 0, function()
			if (!IsValid(ply)) then
				timer.Destroy(uid)
				return
			end
			local seconds = ply:MK_Data_Gamemode_Get("playtime", 0) + time
			ply:MK_Data_Gamemode_Set("playtime", seconds)
			hook.Run("MK_Player.UpdatePlayTime_GM", ply, seconds)
			local seconds = ply:MK_Data_Get("playtime", 0) + time
			ply:MK_Data_Set("playtime", seconds)
			hook.Run("MK_Player.UpdatePlayTime", ply, seconds)
		end)
	end
end)
