// RAWR!

if (!timer.Exists("MK_PlayerName")) then
	timer.Create("MK_PlayerName", 1, 0, function()
		for k,v in pairs(player.GetAll()) do
			if (v.MK_GameData_Get) then
				if (!v:MK_GameData_Get("name", false)) then
					v:MK_GameData_Set("name", v:MK_RealName())
				elseif (v:MK_RealName() != v:MK_GameData_Get("name", false)) then
					local aliases = v:MK_Data_Get("aliases", {})
					local shouldUpdate = false
					if (!table.HasValue(aliases, v:MK_RealName())) then
						hook.Run("MK_Player.NameChanged", ply, v:MK_GameData_Get("name", false), v:MK_RealName())
						player.BroadcastChat(player.GetByPermission("see_aliases"), v, Color(255, 255, 255), " has changed their Steam name from '"..v:MK_GameData_Get("name", false).."'!")
						table.insert(aliases, v:MK_RealName())
						shouldUpdate = true
					end
					v:MK_Data_Set("aliases", aliases, shouldUpdate)
					v:MK_GameData_Set("name", v:MK_RealName())
				end
			end
		end
	end)
end
