// RAWR!

MK.joinTable = MK.joinTable or {}
MK.mapStart = MK.mapStart or os.time()
hook.Add("CheckPassword", "MK_CheckPassword", function(steam, ipAddress, svPassword, clPassword, name)
	local steamid = util.SteamIDFrom64(steam)
	MK.joinTable[steamid] = os.time()

	if (table.HasValue(MK.config.Get("noPasswordRequired", {}), steamid)) then
		return true
	end

	MK.dev.Log(6, string.format("<CheckPassword> %s | %s | %s attempted to use password '%s'", name, steam, ipAddress, clPassword))
	if (MK.bans.IsBanned(steamid)) then
		return false, "Your steam account has been banned.\n"..MK.bans.GetReason(steamid)
	end
	if (not svPassword||svPassword == "") then
		return true
	elseif (svPassword == clPassword) then
		return true
	elseif (svPassword != clPassword) then
		return false, "Password is incorrect"
	end
end)

hook.Add("PlayerInitialSpawn", "MK_PlayerInitialSpawn", function(ply)
	local time = os.time() - MK.mapStart
	if (MK.joinTable[ply:MK_RealSteamID()]) then
		time = os.time() - MK.joinTable[ply:MK_RealSteamID()]
	end
	if (ply:IsBot()) then
		time = 0
	end
	MK.dev.Log(6, string.format("<PlayerInitialSpawn> %s | %s | %s (took %s seconds)", ply:Nick(), ply:MK_RealSteamID(), ply:IPAddress(), time))
end)

hook.Add("MK_Player.Ready", "MK_Player.Ready.PointshopLoad", function(ply)
	if (PS) then
		for k,v in pairs(player.GetAll()) do
			v:PS_SendPoints()
			v:PS_SendItems()
			v:PS_SendClientsideModels()
		end
		ply:MK_SendChat(Color(100,255,100), "[MK-PS] ", Color(255,255,255), "Pointshop data has been loaded!")
	end
end)

hook.Add("PlayerSay", "MK_PlayerSay", function(ply, text, teamOnly)
	if (MK.config.Get("administration_enabled", false)) then
		args = MK.util.ExtractArgs(text)

		if (MK.commands.Parse(ply, args, true)) then
			return ""
		end
	end
end)

hook.Add("PlayerDisconnected", "MK_PlayerDisconnected", function(ply)
	/*local cid = table.Count(ply.mk_data[gmod.GetGamemode().FolderName]["connections"])
	if (cid == 0) then
		cid = 1
	end
	ply.mk_data[gmod.GetGamemode().FolderName]["connections"][cid][2] = os.time()*/
	if (!ply:IsBot()) then
		MK.db.updateTable({
			status = 0,
		}, nil, "steam = '"..ply:MK_RealSteamID64().."'", "players", "updatePlayerDisconnected_"..ply:MK_RealSteamID64())
	end
	MK.dev.Log(6, string.format("<PlayerDisconnected> %s | %s | %s", ply:Nick(), ply:MK_RealSteamID(), ply:IPAddress()))
end)

hook.Add("ShutDown", "MK_ShutDown", function()
	for k,ply in pairs(player.GetAll()) do
		if (IsValid(ply)&&!ply:IsBot()) then
			MK.db.updateTable({
				status = 0,
			}, nil, "steam = '"..ply:MK_RealSteamID64().."'", "players", "updatePlayerDisconnected_"..ply:MK_RealSteamID64())
			MK.dev.Log(6, string.format("<PlayerDisconnected> %s | %s | %s", ply:Nick(), ply:MK_RealSteamID(), ply:IPAddress()))
		end
	end
end)
