// RAWR!

if SERVER then
    util.AddNetworkString("ots_on")
    util.AddNetworkString("ots_off")
end

function MK.ThirdPerson(ply, pos, ang, fov, znear, zfar)
	local data = {}
	data.drawviewer = true
	
	local targetpos
	if (ply:MK_GameData_Get("thirdperson_view", 0) == 0) then
		targetpos = pos + ang:Forward() * -40 + ang:Up() * 6
	elseif (ply:MK_GameData_Get("thirdperson_view", 0) == 1) then
		targetpos = pos + ang:Forward() * -36 + ang:Right() * 12 + ang:Up() * 4
	elseif (ply:MK_GameData_Get("thirdperson_view", 0) == 2) then
		targetpos = pos + ang:Forward() * -36 + ang:Right() * -12 + ang:Up() * 4
	else
		targetpos = pos + ang:Forward() * -40 + ang:Up() * 4
	end

	local trace = {}
	trace.start = pos
	trace.endpos = targetpos
	trace.mins = Vector(-5, -5, -5)
	trace.maxs = Vector(5, 5, 5)
	trace.filter = ply
	trace.mask = MASK_VISIBLE
	local tr = util.TraceHull(trace)
	if tr.Hit then
		data.origin = tr.HitPos
	else
		data.origin = targetpos
	end
	return data
end

hook.Add("EntityFireBullets", "EntityFireBullets.MK_ThirdPerson", function(ent, data)
	if ent:IsPlayer() and ent:MK_GameData_Get("thirdperson_enabled", false) then
		local origin = data.Src
		local eyes = ent:EyePos()
		local dir = data.Dir
		local trace = {}
		trace.start = eyes
		trace.endpos = eyes + (dir * 8 * 4096)
		trace.filter = ent
		trace.mask = MASK_SHOT
		local tr = util.TraceLine(trace)
		local finaldir = tr.HitPos - origin
		finaldir:Normalize()
		data.Dir = finaldir
		return true
	end
end)

if (CLIENT) then
	net.Receive("ots_on", function() 
		hook.Add("CalcView", "OverTheShoulderThirdPerson", MK.ThirdPerson)
	end)
	net.Receive("ots_off", function()
		hook.Remove("CalcView", "OverTheShoulderThirdPerson")
	end)
	hook.Add("MK_Player.Ready", "MK_Player.Ready.ThirdPerson", function(ply)
		if (ply == LocalPlayer() && LocalPlayer():MK_GameData_Get("thirdperson_enabled", false)) then
			hook.Add("CalcView", "OverTheShoulderThirdPerson", MK.ThirdPerson)
		end
	end)
end
if (SERVER) then
	hook.Add("PlayerSpawn", "MK.PlayerSpawn.ThirdPerson", function(ply)
		if (ply:MK_GameData_Get("thirdperson_enabled", false)) then
			net.Start("ots_on")
			net.Send(ply)
		end
	end)
	hook.Add("PlayerDeath", "MK.PlayerDeath.ThirdPerson", function(ply)
		if (ply:MK_GameData_Get("thirdperson_enabled", false)) then
			net.Start("ots_off")
			net.Send(ply) // So spectating looks nicer(better)
		end
	end)
end