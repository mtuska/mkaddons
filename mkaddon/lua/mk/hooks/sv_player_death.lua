// RAWR!

hook.Add("PlayerDeath", "PlayerDeath.MK_TryAndSaveForGamemodes", function(victim, inflictor, attacker)
	if (IsValid(victim)&&victim:IsPlayer()&&victim.mk) then
		victim.mk.latest.deaths.attacker = attacker
		MK.latest.deaths.attacker = attacker
		if (attacker:IsPlayer()&&attacker.mk) then
			attacker.mk.latest.deaths = attacker.mk.latest.deaths or {}
			attacker.mk.latest.deaths.victim = victim
			attacker.mk.latest.deaths.time = os.time()
		end
		
		victim.mk.latest.deaths.inflictor = inflictor
		MK.latest.deaths.inflictor = inflictor
		
		local time = os.time()
		victim.mk.latest.deaths.time = time
		MK.latest.deaths.time = time
		MK.latest.deaths.victim = victim
	end
end)