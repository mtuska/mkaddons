// RAWR!

local DebugEnabled = CreateClientConVar( "mk_debug_locations", "0", false, false )

local DebugLocStart = nil
local DebugLocEnd = nil

// we use this so that the bottom of a box will be lower than the player's position
local FootOffset = Vector( 0, 0, -5 )


/*
	Location editing utilities
	
	These two concommands are designed to simplify location creation.
	This requires you to be an admin!
*/

concommand.Add( "mk_loc_start", function( ply, cmd, args )
	if !ply:IsSuperAdmin() then return end
	
	DebugLocStart = LocalPlayer():GetPos() + FootOffset
	
	hook.Add("PostDrawTranslucentRenderables", "CinemaDebugLocation", function()
		Debug3D.DrawBox(DebugLocStart, LocalPlayer():GetPos())
	end)
	
end)

concommand.Add( "mk_loc_end", function ( ply, cmd, args )
	if !ply:IsSuperAdmin() then return end
	if !args[1] then
		MsgN("Requires a name!")
		return
	end
	if MK.location.list[game.GetMap()]&&MK.location.list[game.GetMap()][args[1]] then
		MsgN("Location name already exists!")
		return
	end
	
	DebugLocEnd = LocalPlayer():GetPos() + FootOffset
	hook.Remove("PostDrawTranslucentRenderables", "CinemaDebugLocation")
	
	local min = DebugLocStart
	local max = DebugLocEnd
	
	OrderVectors(min, max)
	
	net.Start("MK_LocationAdd")
		net.WriteVector(min)
		net.WriteVector(max)
		net.WriteString(args[1])
	net.SendToServer()
	
	MsgN("The above location has been copied to your clipboard.")
	
end)

// location visualizer for debugging
hook.Add("PostDrawTranslucentRenderables", "MK_DebugLocations", function()
	if (!DebugEnabled:GetBool()) then return end
	
	for k, v in pairs(MK.location.list[game.GetMap()]) do
		
		local center = (v.min + v.max) / 2
		
		Debug3D.DrawBox(v.min, v.max)
		Debug3D.DrawText(center, k)
		
		if (!v.teleports) then continue end
		
		for _, tele in ipairs(v.teleports) do
			local min = tele + Vector( -20, -20, 0 )
			local max = tele + Vector( 20, 20, 80 )
			local center = ( min + max ) / 2
			
			local text = k .. "\nTeleport"
			
			Debug3D.DrawBox( min, max, Color( 0, 255, 0, 255 ) )
			Debug3D.DrawText( center, text, nil, Color( 50, 255, 50, 255 ), 0.25 )
		end
	end
end)