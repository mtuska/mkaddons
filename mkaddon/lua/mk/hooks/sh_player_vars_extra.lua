// RAWR!

hook.Add("MK_Player.SetupVars", "MK_Player.SetupVars.Addon", function(ply)
	//----------------------------------------------------------------------------------------------------\\

	// index, default, save, noNetwork, broadcast
	ply:MK_GameData_Create("thirdperson_enabled", false, true, false)
	ply:MK_GameData_Create("thirdperson_view", 1, true, true, false)

	ply:MK_GameData_Create("locationThink", 0, false, true, true, false)
	ply:MK_GameData_Create("location", "unknown", false, true, false)

	// Pointshop bullshit
	ply:MK_GameData_Create("pointshopData", {items = {}, points = 0}, true, false, true)
	ply:MK_GameData_Create("knives", {}, true, true, false)

	// RTD
	ply:MK_GameData_Create("lastrtd", 0, false, true, false)

	// Experience
	ply:MK_GameData_Create("xp", {}, true, false, true)

	//----------------------------------------------------------------------------------------------------\\

	// index, default, noNetwork, broadcast
	ply:MK_Data_Create("points", 0, false, true)
	ply:MK_Data_Create("items", {}, false, true)

	ply:MK_Data_Create("playtime", 0, false, true)
end)
