// RAWR!

hook.Add("InitPostEntity", "InitPostEntity.MK_MapSpawn", MK.mapSpawn.Spawn)

hook.Add("EntityRemoved", "EntityRemoved.MK_MapSpawn", function(ent)
	if (ent.MKSpawnedEnt) then
		local uid = ent.MKSpawnedEnt
		local data = MK.mapSpawnEnts[game.GetMap()][uid]
		if (data.respawnDelay) then
			timer.Simple(data.respawnDelay, function()
				MK.mapSpawn.Spawn(uid)
			end)
		end
	end
end)
