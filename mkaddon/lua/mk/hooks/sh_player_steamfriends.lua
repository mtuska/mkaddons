// RAWR!

hook.Add("MK_Player.Ready", "MK_Player.Ready.SteamFriendsUpdate", function(ply)
	local name = ply:Name()
	local steam = ply:SteamID64()
	local steamid = ply:SteamID()
	MK.steamfriends.list[steamid] = {}
	// Localize properties incase the player leaves while we collect their info

	http.Fetch(
		string.format(
			"http://api.steampowered.com/ISteamUser/GetFriendList/v1?key=%s&steamid=%s&relationship=%s",
			"1624394BC543213DCAD2EB4338947037", // <steam_api_key>
			ply:SteamID64(), // <steam_id_64>
			"friend" // <all|friend>
		), function(body)
			local body = util.JSONToTable(body)
			
			if (not body or not body.friendslist or not body.friendslist.friends) then
				error(string.format("Friends: Invalid Steam API response for %s | %s\n", name, steamid))
			end

			local steamfriends = {}
			for _,v in pairs(body.friendslist.friends) do
				steamfriends[util.SteamIDFrom64(v.steamid)] = {v.friend_since}
			end
			MK.steamfriends.list[steamid] = steamfriends
			if (IsValid(ply)) then
				hook.Run("MK_Player.ReceivedSteamFriends", ply)
			end
		end
	)
end)
