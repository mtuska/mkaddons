AddCSLuaFile( 'shared.lua' )
AddCSLuaFile( 'cl_init.lua' )
include( 'shared.lua' )


SWEP.Weight				= 5
SWEP.HoldType			= "smg"
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

local sndEnd = Sound( "Weapon_FlameThrower.FireEnd" )
local sndEmpty = Sound( "Weapon_Pistol.Empty" )


function SWEP:Initialize()

	self:SetWeaponHoldType( self.HoldType )

	self:InitializeParticleSystem()

	if IsValid( self.Owner ) then -- It can happen
		self:InitializeSounds()
	end

	self.Firing = false

end


-- Create the particle system entity.  It is used to create the flame effects.
-- It is turned on every time you fire, and turned off when primary attack is released.
function SWEP:InitializeParticleSystem()

	self.ParticleSystem = ents.Create( "info_particle_system" )
	self.ParticleSystem:SetKeyValue( "effect_name", "flamethrower" )
	self.ParticleSystem:Spawn()
	self.ParticleSystem:Activate()

end


-- These looping sounds are played while firing
function SWEP:InitializeSounds()

	self.sndobjStart	= CreateSound( self.Owner, "Weapon_FlameThrower.FireStart" )
	self.sndobjLoop		= CreateSound( self.Owner, "Weapon_FlameThrower.FireLoop" )
	self.sndobjSizzle	= CreateSound( self.Owner, "Weapon_FlameThrower.FireHit" )
	self.sndobjDraw		= CreateSound( self.Owner, "Weapon_FlameThrower.PilotLoop" )

end


function SWEP:StartShootEffects()

	-- Activate the particle system
	if not IsValid( self.ParticleSystem ) then  -- Idiot proof!
		self:InitializeParticleSystem()
	end

	self.ParticleSystem:Fire( "Start", "", 0 )
	self.ParticleSystem:SetParent( self.Owner:GetViewModel() )
	self.ParticleSystem:Fire( "SetParentAttachment", "muzzle" )

	-- Start the sounds
	self.sndobjStart:Play()
	self.sndobjStart:FadeOut( 2 )
	self.sndobjLoop:Play()

	-- Play attack animation
	self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )

	self.Firing = true

end


function SWEP:StopShootEffects()

	-- Deactivate the particle system
	if IsValid( self.ParticleSystem ) then
		self.ParticleSystem:Fire( "Stop", "", 0 )
	end

	-- End the sounds
	self.sndobjLoop:Stop()
	self.sndobjStart:Stop()
	self.sndobjSizzle:Stop()
	self.Owner:EmitSound( sndEnd )

	-- Play idle animation
	self:SendWeaponAnim( ACT_VM_IDLE )

	self.Firing = false

end


function SWEP:PrimaryAttack()

	self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

	 -- Check the main ammo since this weapon doesn't use clips
	if self:Ammo1() <= 0 or self.Owner:WaterLevel() >= 3 then -- Flamethrower's don't work underwater!

	 	self.Owner:EmitSound( sndEmpty )
 		self:SetNextPrimaryFire( CurTime() + 0.4 )

		if self.Firing then
			self:StopShootEffects()
		end

		return

	end

	-- Start the flame effects and sounds
	if not self.Firing then
		self:StartShootEffects()
	end

	-- Launch an invisible projectile
	local shootpos = self.Owner:GetShootPos()
	local ang = self.Owner:EyeAngles()
	shootpos = shootpos + ang:Right() * 10 - ang:Up() * 8
	ang = ang:Forward()

	local projectile = ents.Create( "sent_tf2_ft_proj" )
	if (projectile == nil) then return end
	projectile:SetOwner( self.Owner )
	projectile:SetPos( shootpos )
	projectile:Spawn()

	projectile:SetDamage( self.Primary.MinDamage, self.Primary.MaxDamage )
	projectile:SetVelocity( ( ang + VectorRand() * 0.07 ):GetNormalized() * 500 + self.Owner:GetVelocity() * 0.88 )

	-- Do a trace to see if we should play the sizzle sound
	local tr = util.TraceLine(
	{
		start = shootpos,
		endpos = shootpos + ang * 240,
		mask = MASK_SHOT,
		filter = self.Owner
	})

	if tr.HitNonWorld and ( tr.Entity:IsNPC() or tr.Entity:IsPlayer() ) then -- It's alive!
		self.sndobjSizzle:Play() -- Make their flesh sizzle
	else
		self.sndobjSizzle:FadeOut( 1 )
	end

	-- Take ammo from the main clip
	self:TakePrimaryAmmo( 1 )

end


function SWEP:SecondaryAttack()

	-- Secondary attack does nothing

end


function SWEP:Think()

	if not self.Firing then return end

	if self.Owner:KeyReleased( IN_ATTACK ) then
		self:StopShootEffects()
	end

end


function SWEP:Reload()

	return false -- no reloading... this weapon doesn't use clips

end


function SWEP:OwnerChanged()

	self:InitializeSounds()

end


function SWEP:Deploy()

	self:SendWeaponAnim( ACT_VM_DRAW )
	self.sndobjDraw:Play()

	return true

end


function SWEP:Holster( wep )

	if self.Firing then
		self:StopShootEffects()
	end

	self.sndobjDraw:Stop()

	return true

end


function SWEP:OnRemove()

	if self.Firing then
		self:StopShootEffects()
	end

	if IsValid( self.ParticleSystem ) then
		self.ParticleSystem:Remove()
	end

end
