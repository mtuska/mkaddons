
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_modelstf/v_flamethrower_pyro.mdl"
SWEP.WorldModel	= "models/weapons/c_models/c_flamethrower/c_flamethrower.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["weapon_bone"] = { scale = Vector(1, 1, 1), pos = Vector(0.317, -17.143, -13.334), angle = Angle(-3.81, 0, 0) }
}


-- Damage is applied every 0.05 seconds for each flame projectile.  Damage decreases the longer the projectile is alive.
SWEP.Primary.MinDamage		= 1
SWEP.Primary.MaxDamage		= 5
SWEP.Primary.Delay			= 0.08

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= 200
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "pistol"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"


