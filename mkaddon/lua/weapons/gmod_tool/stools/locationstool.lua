// RAWR!

AddCSLuaFile()

TOOL.Category = "MKAddon"
TOOL.Name = "Location Stool"
TOOL.Command = nil
TOOL.ConfigName = ""

local minLoc, maxLoc
local lookLoc = "Unknown"

function TOOL:Think()
	if (SERVER) then return end
	
	local tr = self:GetOwner():GetEyeTrace()
	if (tr.HitPos) then
		lookLoc = MK.location.Find(tr.HitPos)
	end
end

function TOOL:DrawToolScreen(width, height)
	if (SERVER) then return end
	//local tr = self:GetOwner():GetEyeTrace()
	//local loc = MK.location.Find(tr.HitPos)
	
	surface.SetDrawColor( Color( 20, 20, 20 ) )
	surface.DrawRect( 0, 0, width, height )

	-- Draw white text in middle
	draw.SimpleText( lookLoc, "DermaLarge", width / 2, height / 2, Color( 200, 200, 200 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
end

function TOOL:Holster()
	if (SERVER) then return end
	minLoc = nil
	maxLoc = nil
	hook.Remove("PostDrawTranslucentRenderables", "MK_WeaponDebugLocation")
	
	return true
end

local pressed1 = false

function TOOL:LeftClick(tr)
	if (SERVER||pressed1) then return end
	pressed1 = true
	timer.Simple(0.1, function() pressed1 = false end)
	if (tr.HitNonWorld&&!input.IsShiftDown()) then
		self:GetOwner():ChatPrint("Hit something, can't use that: "..tostring(tr.Entity))
		return
	end
	minLoc = tr.HitPos
	if (!maxLoc) then
		hook.Add("PostDrawTranslucentRenderables", "MK_WeaponDebugLocation", function()
			Debug3D.DrawBox(minLoc, self:GetOwner():GetEyeTrace().HitPos)
		end)
	else
		hook.Add("PostDrawTranslucentRenderables", "MK_WeaponDebugLocation", function()
			Debug3D.DrawBox(minLoc, maxLoc)
		end)
	end
	
	self:Check()
end

local pressed2 = false

function TOOL:RightClick(tr)
	if (SERVER||pressed2) then return end
	pressed2 = true
	timer.Simple(0.1, function() pressed2 = false end)
	if (tr.HitNonWorld&&!input.IsShiftDown()) then
		self:GetOwner():ChatPrint("Hit something, can't use that: "..tostring(tr.Entity))
		return
	end
	
	maxLoc = tr.HitPos
	if (!minLoc) then
		hook.Add("PostDrawTranslucentRenderables", "MK_WeaponDebugLocation", function()
			Debug3D.DrawBox(maxLoc, self:GetOwner():GetEyeTrace().HitPos)
		end)
	else
		hook.Add("PostDrawTranslucentRenderables", "MK_WeaponDebugLocation", function()
			Debug3D.DrawBox(minLoc, maxLoc)
		end)
	end
	
	self:Check()
end

local pressed3 = false

function TOOL:Reload(tr)
	if (SERVER||pressed3) then return end
	pressed3 = true
	timer.Simple(0.1, function() pressed3 = false end)
	self:Check()
end

function TOOL:Check()
	if (!minLoc || !maxLoc) then return end
	
	Derma_StringRequest(
		"Console Print",
		"Input the string to print to console",
		"",
		function( text )
			if MK.location.list[game.GetMap()]&&MK.location.list[game.GetMap()][text] then
				self:GetOwner():ChatPrint("location name already exists")
				return
			end
			hook.Remove("PostDrawTranslucentRenderables", "MK_WeaponDebugLocation")
			if !self:GetOwner():IsSuperAdmin() then self:GetOwner():ChatPrint("only superadmins") return end
			OrderVectors(minLoc, maxLoc)
			net.Start("MK_LocationAdd")
				net.WriteVector(minLoc)
				net.WriteVector(maxLoc)
				net.WriteString(text)
			net.SendToServer()
			minLoc = nil
			maxLoc = nil
		end,
		function( text ) print( "Cancelled input" ) end
	 )
end