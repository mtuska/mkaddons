SWEP.MuzzleEffect		= "muzzle_shotgun"
SWEP.TracerEffect 		= "bullet_shotgun_tracer01_red"
SWEP.MuzzleAttachment		= "muzzle"

SWEP.Category			= "Team Fortress 2"

SWEP.DrawWeaponInfoBox  	= false

SWEP.Author 			= ""
SWEP.Contact 			= ""
SWEP.Purpose 			= ""
SWEP.Instructions 		= ""

SWEP.Spawnable 			= false
SWEP.AdminSpawnable 		= false

SWEP.Weight 			= 5
SWEP.AutoSwitchTo 		= false
SWEP.AutoSwitchFrom 		= false

SWEP.Primary.Sound 		= Sound("Weapon_AK47.Single")
SWEP.Primary.Recoil 		= 0
SWEP.Primary.Damage 		= 0
SWEP.Primary.NumShots 		= 0
SWEP.Primary.Cone 		= 0
SWEP.Primary.ClipSize 		= 0
SWEP.Primary.Delay 		= 0						
SWEP.Primary.DefaultClip 	= 0
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo 		= "none"

SWEP.Secondary.ClipSize 	= 0
SWEP.Secondary.DefaultClip 	= 0
SWEP.Secondary.Automatic 	= false
SWEP.Secondary.Ammo 		= "none"

SWEP.data 			= {}
SWEP.mode 			= "auto"
SWEP.data.ironsights		= 0

SWEP.MuzzleEffect		= "muzzle_shotgun"
SWEP.TracerEffect 		= "bullet_tracer01_red"
SWEP.TracerCritEffect 		= "bullet_tracer01_red_crit"
SWEP.MuzzleAttachment		= "muzzle"

/*---------------------------------------------------------
IronSight
---------------------------------------------------------*/
function SWEP:IronSight()
end

/*---------------------------------------------------------
Think
---------------------------------------------------------*/
function SWEP:Think()
end

/*---------------------------------------------------------
Initialize
---------------------------------------------------------*/
function SWEP:Initialize()

	if (SERVER) then
		self:SetWeaponHoldType(self.HoldType) 	-- Hold type of the 3rd person animation
	end
	
	PrecacheParticleSystem( self.MuzzleEffect )
	PrecacheParticleSystem( self.TracerEffect )
	PrecacheParticleSystem( self.TracerCritEffect )
	PrecacheParticleSystem( "critgun_weaponmodel_red" )
	PrecacheParticleSystem( "critgun_weaponmodel_blu" )
	PrecacheParticleSystem( "healthlost_red" )
	PrecacheParticleSystem( "healthgained_red" )
	self.CritSoundLoop = CreateSound(self, Sound("TFPlayer.CritBoostOn"))
	self.Reloadaftershoot = CurTime()
end

/*---------------------------------------------------------
Reload
---------------------------------------------------------*/
function SWEP:Reload()
end

/*---------------------------------------------------------
Deploy
---------------------------------------------------------*/
function SWEP:Deploy()
end

/*---------------------------------------------------------
PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
end

/*---------------------------------------------------------
SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()
end

/*---------------------------------------------------------
CanPrimaryAttack
---------------------------------------------------------*/
function SWEP:CanPrimaryAttack()

	if ( self.Weapon:Clip1() <= 0 ) and self.Primary.ClipSize > -1 then
		self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
		self.Weapon:EmitSound("Weapons/ClipEmpty_Pistol.wav")
		return false
	end
	return true
end

/*---------------------------------------------------------
DrawWeaponSelection
---------------------------------------------------------*/
function SWEP:DrawWeaponSelection(x, y, wide, tall, alpha)
end

function SWEP:GetViewModelPosition(pos, ang)
end

/*---------------------------------------------------------
RecoilPower
---------------------------------------------------------*/
function SWEP:RecoilPower()
end

/*
MUZZLE FLASHES
"muzzle_pistol" - Scout/Engineer pistol muzzle flash
"muzzle_bignasty" - Force-a-Nature muzzle
"muzzle_minigun" - Sascha/Natascha muzzle flash
"muzzle_revolver" - Revolver muzzle
"muzzle_sniperrifle" - Sniper rifle muzzle
"muzzle_shotgun" - Shotgun muzzle flash

TRACER EFFECTS
"bullet_tracer01_blue" - Standard blue tracer
"bullet_tracer01_blue_crit" - Standard blue crit tracer
"bullet_tracer01_red" - Standard red tracer
"bullet_tracer01_red_crit" - Standard red crit tracer
"bullet_bignasty_tracer01_blue" - FaN blue tracer
"bullet_bignasty_tracer01_red" -  FaN red tracer
"bullet_bignasty_tracer01_blue_crit" -  FaN blue tracer
"bullet_bignasty_tracer01_red_crit" -  FaN red crit tracer
"bullet_shotgun_tracer01_blue" - Standard blue shotgun tracer
"bullet_shotgun_tracer01_red" - Standard red shotgun tracer
"bullet_shotgun_tracer01_blue_crit" - Standard blue crit shotgun tracer
"bullet_shotgun_tracer01_red_crit" - Standard red crit shotgun tracer
*/

function DoBulletTracer(msg)
local weapon = msg:ReadEntity()
local endpos = msg:ReadVector()
local effect = msg:ReadString()
DoBulletTracer2(weapon, endpos, effect)
end
 
usermessage.Hook("DoBulletTracer",DoBulletTracer)

function DoMuzzleEffect(msg)
local weaponz = msg:ReadEntity()
local effectz = msg:ReadString()
DoMuzzleEffect2(weaponz, effectz)
end

usermessage.Hook("DoMuzzleEffect", DoMuzzleEffect)

function DoMuzzleEffect2(weaponz, effectz)
local trakmuz
if weaponz.Owner==LocalPlayer() then
if weaponz.CModel then trakmuz=weaponz.CModel
else trakmuz=LocalPlayer():GetViewModel() end
else
trakmuz=weaponz
end
if !weaponz:IsValid() then return end
local idmuz = trakmuz:LookupAttachment("muzzle")
local attmuz = trakmuz:GetAttachment(idmuz)
ParticleEffectAttach( effectz, PATTACH_POINT_FOLLOW, trakmuz, idmuz)
end

function DoBonkMuzzleEffect(msg)
local weaponz = msg:ReadEntity()
local effectz = msg:ReadString()
DoBonkMuzzleEffect2(weaponz, effectz)
end

usermessage.Hook("DoBonkMuzzleEffect", DoBonkMuzzleEffect)

function DoBonkMuzzleEffect2(weaponz, effectz)
local trakmuz
if weaponz.Owner==LocalPlayer() then
if weaponz.CModel then trakmuz=weaponz.CModel
else trakmuz=LocalPlayer():GetViewModel() end
else
trakmuz=weaponz
end
if !weaponz:IsValid() then return end
local idmuz = trakmuz:LookupAttachment("drink_spray")
local attmuz = trakmuz:GetAttachment(idmuz)
ParticleEffectAttach( effectz, PATTACH_POINT_FOLLOW, trakmuz, idmuz)
end

function DoBulletTracer2(weapon, endpos, effect)
local trak
if weapon.Owner==LocalPlayer() then
if weapon.CModel then trak=weapon.CModel
else trak=LocalPlayer():GetViewModel() end
else
trak=weapon
end
if !weapon:IsValid() then return end
local id = trak:LookupAttachment("muzzle")
local attz = trak:GetAttachment(id)
util.ParticleTracerEx(effect, attz.Pos, endpos, true, trak:EntIndex(), id)
end

local NotAliveNPCS = {
"npc_turret_floor",
"npc_grenade_frag",
"npc_turret_ceiling",
"npc_turret_ground",
"npc_combine_mine",
"npc_cscanner",
"npc_clawscanner",
"npc_strider",
"npc_helicopter",
"npc_combinedropship",
"npc_combinegunship",
"npc_manhack",
"npc_combine_camera"}

/*---------------------------------------------------------
ShootBullet
---------------------------------------------------------*/
function SWEP:CSShootBulletMG(dmg, recoil, numbul, cone)
	
	numbul 		= numbul or 1
	cone 			= cone or 0.01

	local bullet 	= {}
	bullet.Num  	= 2
	bullet.Src 		= self.Owner:GetShootPos()       					-- Source
	bullet.Dir 		= self.Owner:GetAimVector()      					-- Dir of bullet
	bullet.Spread 	= Vector(cone, cone, 0)     						-- Aim Cone
	bullet.Tracer 	= 0       									-- Show a tracer on every x bullets
	bullet.Force 	= 0.22 * dmg     								-- Amount of force to give to phys objects
	bullet.Damage 	= dmg										-- Amount of damage to give to the bullets
	bullet.Attacker = self.Owner

	self:FireBullets(bullet)					-- Fire the bullets
	self.Owner:SetAnimation(PLAYER_ATTACK1)       			-- 3rd Person Animation

end

/*---------------------------------------------------------
ShootBullet
---------------------------------------------------------*/
function SWEP:CSShootBullet(dmg, recoil, numbul, cone)
	
	numbul 		= numbul or 1
	cone 			= cone or 0.01

	local bullet 	= {}
	bullet.Num  	= numbul
	bullet.Src 		= self.Owner:GetShootPos()       					-- Source
	bullet.Dir 		= self.Owner:GetAimVector()      					-- Dir of bullet
	bullet.Spread 	= Vector(cone, cone, 0)     						-- Aim Cone
	bullet.Tracer 	= 0       									-- Show a tracer on every x bullets
	bullet.Force 	= 0.22 * dmg     								-- Amount of force to give to phys objects
	bullet.Damage 	= dmg										-- Amount of damage to give to the bullets
	bullet.Attacker = self.Owner
	bullet.Callback = function(a, b, c) 
		
		local FireMath = math.random(0,6)
		if a:GetActiveWeapon():GetNWBool("FireUpgrade") and FireMath == 3 and a:WaterLevel() <= 2 then
			local effectdata = EffectData()
			effectdata:SetOrigin(b.HitPos)
			effectdata:SetNormal(b.HitNormal)
			effectdata:SetScale(20)
			util.Effect("tf2_fireupgrade_effect", effectdata)
			util.Decal("FadingScorch", b.HitPos + b.HitNormal, b.HitPos - b.HitNormal)
			if b.Entity:IsNPC() and b.Entity:WaterLevel() <= 2 then
			if (SERVER) then
			b.Entity:Ignite(8)
			end
			end
		end
		
		local LeechMath = math.random(1,3)
		local LeechMath2 = math.random(1,2)
		if a:GetActiveWeapon():GetNWBool("LeechUpgrade") and LeechMath == 1 then
			if (table.HasValue( NotAliveNPCS, b.Entity:GetClass() ) ) then return false end
			local effectdata = EffectData()
				effectdata:SetOrigin(b.HitPos)
				effectdata:SetNormal(b.HitNormal)
				effectdata:SetScale(20)
				util.Effect("tf2_leech_effect", effectdata)
			if b.Entity:IsNPC() then
				a:SetHealth(a:Health() + LeechMath2)
				if a:Health() > a:GetMaxHealth() then a:SetHealth(a:GetMaxHealth()) end
				local LeechEffect = ents.Create("info_particle_system")
				LeechEffect:SetPos(b.Entity:GetPos() + b.Entity:GetUp()*60)
				LeechEffect:SetKeyValue( "effect_name", "healthlost_red" )
				LeechEffect:SetKeyValue( "start_active", "1" )
				LeechEffect:Spawn()
				LeechEffect:Activate()
				timer.Simple(4, function()
				LeechEffect:Remove()
				end)
				local LeechEffect2 = ents.Create("info_particle_system")
				LeechEffect2:SetPos(a:GetPos() + a:GetUp()*60 - a:GetForward()*5)
				LeechEffect2:SetKeyValue( "effect_name", "healthgained_red" )
				LeechEffect2:SetKeyValue( "start_active", "1" )
				LeechEffect2:Spawn()
				LeechEffect2:Activate()
				timer.Simple(4, function()
				LeechEffect2:Remove()
				end)
			end
		end
	
		local vm = self.Owner:GetViewModel()
		local attach = vm:GetAttachment(vm:LookupAttachment("muzzle"))
		local worldattach = self:GetAttachment(self:LookupAttachment("muzzle"))
		
		ParticleEffectAttach( self.MuzzleEffect, PATTACH_POINT_FOLLOW, self.Owner:GetViewModel(), self.Owner:GetViewModel():LookupAttachment(self.MuzzleAttachment))
		
		if (SERVER) and self.Weapon.IsCModel == nil then
		local StartPosPart = ents.Create("info_particle_system")
		local EndPosPart = ents.Create("info_particle_system")
		EndPosPart:SetPos(b.HitPos)
		EndPosPart:Spawn()
		EndPosPart:Activate()
		EndPosPart:SetName( "tracer_entity" .. math.random(0, 9001) )
		if self.Weapon:GetNWBool("Critical") then
		StartPosPart:SetKeyValue( "effect_name", self.TracerCritEffect )
		else
		StartPosPart:SetKeyValue( "effect_name", self.TracerEffect )
		end
		StartPosPart:SetKeyValue( "cpoint1", EndPosPart:GetName() )
		StartPosPart:SetKeyValue( "start_active", "1" )
		StartPosPart:SetPos( attach.Pos )
		StartPosPart:SetParent(vm)
		StartPosPart:Spawn()
		StartPosPart:Activate()
		StartPosPart:Fire("kill","",0.3)
		EndPosPart:Fire("kill","",0.3)
		end
		
		if (SERVER) and self.Weapon.IsCModel then
		umsg.Start("DoBulletTracer")
		umsg.Entity(self)
		umsg.Vector(b.HitPos)
		if self.Weapon:GetNWBool("Critical") then
		umsg.String(self.TracerCritEffect)
		else
		umsg.String(self.TracerEffect)
		end
		umsg.End()
		umsg.Start("DoMuzzleEffect")
		umsg.Entity(self)
		umsg.String(self.MuzzleEffect)
		umsg.End()
		end
		return end

	self:FireBullets(bullet)					-- Fire the bullets
	self.Owner:SetAnimation(PLAYER_ATTACK1)       			-- 3rd Person Animation

end
