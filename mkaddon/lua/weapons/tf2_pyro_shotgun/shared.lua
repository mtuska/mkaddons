-- Read all weapon_ for comments not pist_ or sec_

local ShotgunPLimit = { }
ShotgunPLimit[ "buckshot" ] = 32

if (SERVER) then
	AddCSLuaFile("shared.lua")
	SWEP.HoldType		= "shotgun"
end

if (CLIENT) then
	SWEP.Category 			= "Team Fortress 2";
	SWEP.PrintName			= "Shotgun Pyro"	
	SWEP.Slot				= 1
	SWEP.SlotPos			= 1
	SWEP.IconLetter			= "S"
	SWEP.ViewModelFOV		= 55
	SWEP.DrawSecondaryAmmo = false
	killicon.AddFont("weapon_real_cs_ak47", "CSKillIcons", SWEP.IconLetter, Color( 0, 200, 0, 255 ) )
end

/*
MUZZLE FLASHES
"muzzle_pistol" - Scout/Engineer pistol muzzle flash
"muzzle_bignasty" - Force-a-Nature muzzle
"muzzle_minigun" - Sascha/Natascha muzzle flash
"muzzle_revolver" - Revolver muzzle
"muzzle_sniperrifle" - Sniper rifle muzzle
"muzzle_shotgun" - Shotgun muzzle flash

TRACER EFFECTS
"bullet_tracer01_blue" - Standard blue tracer
"bullet_tracer01_blue_crit" - Standard blue crit tracer
"bullet_tracer01_red" - Standard red tracer
"bullet_tracer01_red_crit" - Standard red crit tracer
"bullet_bignasty_tracer01_blue" - FaN blue tracer
"bullet_bignasty_tracer01_red" -  FaN red tracer
"bullet_bignasty_tracer01_blue_crit" -  FaN blue tracer
"bullet_bignasty_tracer01_red_crit" -  FaN red crit tracer
"bullet_shotgun_tracer01_blue" - Standard blue shotgun tracer
"bullet_shotgun_tracer01_red" - Standard red shotgun tracer
"bullet_shotgun_tracer01_blue_crit" - Standard blue crit shotgun tracer
"bullet_shotgun_tracer01_red_crit" - Standard red crit shotgun tracer
*/

SWEP.MuzzleEffect			= "muzzle_shotgun"
SWEP.TracerEffect = "bullet_shotgun_tracer01_red"
SWEP.TracerCritEffect = "bullet_shotgun_tracer01_red_crit"
SWEP.MuzzleAttachment		= "muzzle"

SWEP.Instructions			= ""
SWEP.Author   				= "buu342"
SWEP.Contact        		= ""

SWEP.Base 				= "weapon_tf_base"
SWEP.ViewModelFlip		= false

SWEP.Spawnable 			= true
SWEP.AdminSpawnable 	= true

SWEP.ViewModel			= "models/weapons/v_models/v_shotgun_pyro.mdl"
SWEP.WorldModel			= "models/weapons/w_models/w_shotgun.mdl"

SWEP.Primary.Sound			= Sound( "weapons/shotgun_shoot.wav" )
SWEP.Primary.CritSound			= Sound( "Weapon_Shotgun.SingleCrit" )
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= math.random(5,7)
SWEP.Primary.NumShots		= 10
SWEP.Primary.Cone			= 0.06
SWEP.Primary.ClipSize		= 6
SWEP.Primary.Delay			= 0.625
SWEP.Primary.DefaultClip	= 12
SWEP.Primary.Maxammo 		= 32
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"

SWEP.Secondary.ClipSize	= -1
SWEP.Secondary.DefaultClip 	= 0
SWEP.Secondary.Automatic 	= false
SWEP.Secondary.Ammo 		= "gravity"

SWEP.Penetration			= true
SWEP.PrimMaxPenetration 	= 12
SWEP.PrimMaxWoodPenetration = 14
SWEP.Ricochet			= false
SWEP.MaxRicochet			= 0

SWEP.IronSightsPos = Vector (0,0,0)
SWEP.IronSightsAng = Vector (0,0,0)


function SWEP:EjectBrass()
end

SWEP.data 				= {}
SWEP.mode 				= "auto"

SWEP.data.semi 			= {}

SWEP.data.auto 			= {}


/*---------------------------------------------------------
SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()
end
	
function SWEP:ShouldDropOnDie()
	return true
end

/*---------------------------------------------------------
PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()

	if self.Weapon:GetNWBool("InReload") then
	self.Weapon:SendWeaponAnim(ACT_RELOAD_FINISH)
	self.Reloadaftershoot = CurTime() + self.Owner:GetViewModel():SequenceDuration()
	self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
	self.Idle = CurTime() + self.Owner:GetViewModel():SequenceDuration()
	self.Weapon:SetNWBool("InReload",false)
	return false
	end

	if not self:CanPrimaryAttack() then return end
	-- If your gun have a problem or if you are under water, you'll not be able to fire
	
	if !self.Weapon:GetNWBool("InReload") then
	
	local CritMath = math.random(0,14)
	
	if CritMath == 5 or CritMath == 10 then
	self.Weapon:SetNWBool("Critical", true)
	end
	
	self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

	self.Reloadaftershoot = CurTime() + 0.1

	self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)

	local timescale = GetConVarNumber("host_timescale")
	if self.Weapon:GetNWBool("Critical") then
	self.Weapon:EmitSound(self.Primary.CritSound,100,timescale*100)
	else
	self.Weapon:EmitSound(self.Primary.Sound,100,timescale*100)
	end

	if self.Weapon:GetNWBool("Critical") then
	self:CSShootBullet(self.Primary.Damage*2.5, self.Primary.Recoil, self.Primary.NumShots, self.Primary.Cone)
	else
	self:CSShootBullet(self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self.Primary.Cone)
	end
	self.Owner:ViewPunch(Angle(-0.7,0,0))

	self:TakePrimaryAmmo(1)
	-- Take 1 ammo in you clip

	self.Idle = CurTime() + self.Owner:GetViewModel():SequenceDuration()
	
	self.Weapon:SetNWBool("Critical", false)

	if ((game.SinglePlayer() and SERVER) or CLIENT) then
		self.Weapon:SetNetworkedFloat("LastShootTime", CurTime())
	end
end
end

/*---------------------------------------------------------
Deploy
---------------------------------------------------------*/
function SWEP:Deploy()
	self:AmmoCheck()
	self.Weapon:SetNWBool("Critical",false)
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self.Idle = CurTime() + self.Owner:GetViewModel():SequenceDuration()
	self.Reloadaftershoot = CurTime() + self.Owner:GetViewModel():SequenceDuration()
	self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
	self.Weapon:SetNWBool("InReload",false)
	return true
end

function SWEP:Holster()
self.Weapon:SetNWBool("InReload",false)
return true
end

/*---------------------------------------------------------
Reload
---------------------------------------------------------*/
function SWEP:Reload()
if ( self.Reloadaftershoot > CurTime() ) then return end 

if self.Weapon:GetNWBool("InReload") then return end

	if ( self:Ammo1() <= 0 ) then return end

	if ( self.Weapon:Clip1() < self.Weapon.Primary.ClipSize ) then

	self.Weapon:SendWeaponAnim(ACT_RELOAD_START)
	self.Weapon:SetNWBool("InReload",true)
	self.Idle = nil
	self.AutoReload = CurTime() + self.Owner:GetViewModel():SequenceDuration()
	self.Reloadaftershoot = CurTime() + self.Owner:GetViewModel():SequenceDuration()
	self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())

end
end

function SWEP:AmmoCheck()
for k, v in pairs( ShotgunPLimit ) do
		if ( self.Owner:GetAmmoCount( k ) > v ) then
				self.Owner:RemoveAmmo ( self.Owner:GetAmmoCount( k ) - v ,k )
		end
	end
end

/*---------------------------------------------------------
Think
---------------------------------------------------------*/
function SWEP:Think()

if self.Weapon:GetNWBool("InReload") and self.AutoReload and CurTime()>=self.AutoReload then
if self.Weapon:Clip1() >= self.Weapon.Primary.ClipSize or self:Ammo1() <= 0 then
self.Weapon:SendWeaponAnim(ACT_RELOAD_FINISH)
self.Reloadaftershoot = CurTime() + self.Owner:GetViewModel():SequenceDuration()
self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
self.Idle = CurTime() + self.Owner:GetViewModel():SequenceDuration()
self.Weapon:SetNWBool("InReload",false)
else
self.Weapon:SendWeaponAnim(ACT_VM_RELOAD)
self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
self.Weapon:SetClip1(  self.Weapon:Clip1() + 1 )
self:EjectBrass()
self.Weapon:SetNextPrimaryFire(CurTime() + 0.45)
self.AutoReload = CurTime() + self.Owner:GetViewModel():SequenceDuration()
end
end
if self.Idle and CurTime()>=self.Idle then
self.Idle = nil
self.Weapon:SendWeaponAnim(ACT_VM_IDLE)
end
for k, v in pairs( ShotgunPLimit ) do
		if ( self.Owner:GetAmmoCount( k ) > v ) then
				self.Owner:RemoveAmmo ( self.Owner:GetAmmoCount( k ) - v ,k )
		end
	end
end

/*---------------------------------------------------------
CanPrimaryAttack
---------------------------------------------------------*/
function SWEP:CanPrimaryAttack()

	if ( self.Weapon:Clip1() <= 0 ) and self.Primary.ClipSize > -1 then
		local timescale = GetConVarNumber("host_timescale")
		self.Weapon:EmitSound("Weapon_Scatter_Gun.Empty",100,timescale*100)
		self.Weapon:SetNextPrimaryFire(CurTime() + 0.6)
		self:Reload()
		return false
	end
	return true
end

function SWEP:DrawHUD()
end