// RAWR!

function PROVIDER:GetData(ply, callback)
	local data = ply:MK_GameData_Get("pointshopData", {items = {}, points = 0})
	
	return callback(data.points or 0, data.items or {})
end

function PROVIDER:SetPoints( ply, set_points )
	self:GetData(ply, function(points, items)
		self:SetData(ply, set_points, items)
	end)
end

function PROVIDER:GivePoints( ply, add_points )
	self:GetData(ply, function(points, items)
		self:SetData(ply, points + add_points, items)
	end)
end

function PROVIDER:TakePoints( ply, points )
	self:GivePoints(ply, -points)
end

function PROVIDER:SaveItem( ply, item_id, data)
	self:GiveItem(ply, item_id, data)
end

function PROVIDER:GiveItem( ply, item_id, data)
	self:GetData(ply, function(points, items)
		local tmp = table.Copy(ply.PS_Items)
		tmp[item_id] = data
		self:SetData(ply, points, tmp)
	end)
end

function PROVIDER:TakeItem( ply, item_id )
	self:GetData(ply, function(points, items)
		local tmp = table.Copy(ply.PS_Items)
		tmp[item_id] = nil
		self:SetData(ply, points, tmp)
	end)
end

function PROVIDER:SetData(ply, points, items)
	ply:MK_GameData_Set("pointshopData", {items = items, points = points})
end