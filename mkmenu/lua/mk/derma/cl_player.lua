local PANEL = {}
	PANEL.selected = false
	
	MK.menus.players = {}
	MK.menus.selected = MK.menus.selected or {}
	
	function PANEL:Init()
		self:SetTall(36)
		self:DockPadding(2, 2, 2, 2)
		self:DockMargin(4, 4, 4, 0)
		
		self.avatar = self:Add("AvatarImage")
		self.avatar:Dock(LEFT)
		self.avatar:SetWide(32)
		
		self.icon = self:Add("DImage")
		self.icon:Dock(LEFT)
		self.icon:SetWide(16)
		self.icon:DockMargin(4, 8, 0, 8)
		self.icon:SetImage("icon16/user.png")

		self.name = self:Add("DLabel")
		self.name:SetFont("mk_TextFont")
		self.name:Dock(LEFT)
		self.name:DockMargin(3, 0, 0, 0)
		self.name:SetDark(true)
		self.name:SetText("Unknown Player")
		self.name:SizeToContents()
		
		self.rank = self:Add("DLabel")
		self.rank:SetFont("mk_TextFont")
		self.rank:Dock(LEFT)
		self.rank:DockMargin(3, 0, 0, 0)
		self.rank:SetDark(true)
		self.rank:SetText("(Unknown Rank)")
		self.rank:SizeToContents()
		
		self.checked = self:Add("DButton")
		self.checked:Dock(RIGHT)
		self.checked:SetText("")
		self.checked:SetWide(19)
		self.checked:DockMargin(6, 6, 8, 6)
		self.checked.DoClick = function(this)
			if (self.localSelection) then
				self.selected = !self.selected
				self:OnSelected(self.selected)
			else
				MK.menus.selected[self.player] = !MK.menus.selected[self.player]
				self:OnSelected(MK.menus.selected[self.player])
				
				if (!MK.menus.selected[self.player]) then
					MK.menus.selected[self.player] = nil
				end
			end
		end
		self.checked.Paint = function(this, w, h)
			surface.SetDrawColor(250, 250, 250, 150)
			surface.DrawRect(0, 0, w, h)
			
			surface.SetDrawColor(0, 0, 0, 120)
			surface.DrawOutlinedRect(0, 0, w, h)
			
			if (self:IsSelected()) then
				surface.SetDrawColor(50, 195, 50, 220 + math.sin(RealTime() * 1.8)*15)
				surface.DrawRect(2, 2, w - 4, h - 4)
			end
		end
	end
	
	function PANEL:OnSelected(bool)
		
	end
	
	function PANEL:IsSelected()
		if (self.localSelection) then
			return self.selected
		else
			return MK.menus.selected[self.player]
		end
	end
	
	function PANEL:Think()
		if (IsValid(self.player)) then
			local name = self.player:Name()
				if (self.deltaName and self.deltaName != name) then
					self.name:SetText(name)
					self.name:SizeToContents()
				end
			self.deltaName = name

			local rank = self.player:GetUserGroup()
				if (self.rank and self.deltaRank and self.deltaRank != rank) then
					local rankinfo = MK.ranks.Get(rank)
					self.rank:SetText("("..rankinfo.displayName..")")
					self.rank:SizeToContents()
					self.icon:SetImage("icon16/"..rankinfo.icon or "user"..".png")
				end
			self.deltaRank = rank
			
			if (self.rdm and CurTime() >= (self.recalc or 0)) then
				self.formula = math.floor(MK.rdm.Calc(self.player))
				self.stage = MK.rdm.Stage(self.formula)
				self.rdm:SetText(self.stage.title..": "..self.formula)
				self.rdm:SizeToContents()
				self.recalc = CurTime() + 0.5
			end
		elseif (self.playerSet) then
			self:Remove()
		end
	end

	function PANEL:OnCursorEntered()
		self.entered = true
	end
	
	function PANEL:OnCursorExited()
		self.entered = false
	end
	
	function PANEL:Paint(w, h)
		surface.SetDrawColor(0, 0, 0, 20)
		surface.DrawRect(0, 0, w, h)
		
		if (self.entered) then
			surface.SetDrawColor(255, 255, 255, 75)
			surface.DrawRect(0, 0, w, h)
		end
		
		surface.SetDrawColor(0, 0, 0, 60)
		surface.DrawOutlinedRect(0, 0, w, h)
		
		if (self.playerSet) then
			local teamColor = team.GetColor(self.player:Team())
				
			surface.SetDrawColor(teamColor)
			surface.DrawRect(w - 7, 1, 6, h - 2)
		end
	end
	
	function PANEL:SetPlayer(client, noRank, localSelection)
		self.player = client
		self.localSelection = localSelection or false
		self.playerSet = true
		self.avatar:SetPlayer(client)
		
		self.avatar.click = self.avatar:Add("DButton")
		self.avatar.click:SetText("")
		self.avatar.click:Dock(FILL)
		self.avatar.click.Paint = function() end
		self.avatar.click.DoClick = function(this)
			if (!IsValid(client)) then return end
			
			local menu = DermaMenu()
				local steamID = client:SteamID()
				local steamID64 = client:SteamID64() or "76561197960287930"
				local name = client:Name()

				menu:AddOption("Show Steam Profile", function()
					gui.OpenURL("http://steamcommunity.com/profiles/"..steamID64)
				end):SetImage("icon16/world_link.png")
				menu:AddSpacer()
				menu:AddOption("Copy Name", function()
					if (IsValid(client)) then
						SetClipboardText(client:Name())
					else
						SetClipboardText(name)
					end
				end):SetImage("icon16/tag_blue.png")
				menu:AddSpacer()
				menu:AddOption("Copy SteamID", function()
					SetClipboardText(steamID)
				end):SetImage("icon16/database.png")
				menu:AddOption("Copy CommunityID", function()
					SetClipboardText(steamID64)
				end):SetImage("icon16/database_lightning.png")
			menu:Open()
		end
		self.avatar.click:SetToolTip("Click to view this "..(client.SteamName and client:SteamName() or client:Name()).."'s Steam profile.")
		
		self.name:SetText(client:Name())
		self.name:SizeToContents()
		
		if (noRank) then
			self.rank:Remove()
		else
			local rank = self.player:GetUserGroup()
			local rankinfo = MK.ranks.Get(rank)
			self.rank:SetText("("..rankinfo.displayName..")")
			self.rank:SizeToContents()
			self.icon:SetImage("icon16/"..(rankinfo.icon or "user")..".png")
		end
	end
vgui.Register("mk_Player", PANEL, "DPanel")