// RAWR!

MK.net = MK.net or {}
MK.net.list = MK.net.list or {}
MK.net.recent = MK.net.recent or nil

MK.net.Incoming = MK.net.Incoming or net.Incoming

function net.Incoming( len, client )

	local i = net.ReadHeader()
	local strName = util.NetworkIDToString( i )
	
	if ( !strName ) then return end
	
	local func = net.Receivers[ strName:lower() ]
	if ( !func ) then return end

	--
	-- len includes the 16 bit int which told us the message name
	--
	len = len - 16
	
	func( len, client )
	
	//table.insert(MK.net.list, {name = strName:lower(), length = len})
	MK.net.list[strName:lower()] = MK.net.list[strName:lower()] or {}
	MK.net.list[strName:lower()].incount = (MK.net.list[strName:lower()].incount or 0) + 1
	MK.net.list[strName:lower()].outcount = (MK.net.list[strName:lower()].outcount or 0)
	MK.net.list[strName:lower()].inlength = (MK.net.list[strName:lower()].inlength or 0) + len
	MK.net.list[strName:lower()].outlength = (MK.net.list[strName:lower()].outlength or 0)
	MK.net.list[strName:lower()].time = CurTime()
end

MK.net.Start = MK.net.Start or net.Start

function net.Start(strName, unreliable)
	MK.net.recent = strName:lower()
	
	MK.net.Start(strName, unreliable)
end

MK.net.SendToServer = MK.net.SendToServer or net.SendToServer

function net.SendToServer()
	local strName = MK.net.recent
	
	MK.net.list[strName:lower()] = MK.net.list[strName:lower()] or {}
	MK.net.list[strName:lower()].incount = (MK.net.list[strName:lower()].incount or 0)
	MK.net.list[strName:lower()].outcount = (MK.net.list[strName:lower()].outcount or 0) + 1
	MK.net.list[strName:lower()].inlength = (MK.net.list[strName:lower()].inlength or 0)
	MK.net.list[strName:lower()].outlength = (MK.net.list[strName:lower()].outlength or 0) + net.BytesWritten()
	MK.net.list[strName:lower()].time = CurTime()
	
	MK.net.recent = nil
	
	MK.net.SendToServer()
end

local CATEGORY = {}
	CATEGORY.name = "Net Messages"
	CATEGORY.adminOnly = true

	function CATEGORY:Layout(panel)
		panel:Dock(FILL)
		
		panel.msgs = panel:Add("DListView")
		panel.msgs:Dock(FILL)
		panel.msgs:DockMargin(4, 4, 4, 0)
		panel.msgs:AddColumn("Msg")
		panel.msgs:AddColumn("In Count")
		panel.msgs:AddColumn("Out Count")
		panel.msgs:AddColumn("In")
		panel.msgs:AddColumn("Out")
		panel.msgs:AddColumn("Time")
		//panel.msgs:SetHeaderHeight(28)
		//panel.msgs:SetDataHeight(24)
		panel.msgs:SetHeight(panel:GetTall() - 2)
		
		local lines

		local function PopulateList()
			if (!panel||!panel.msgs||!IsValid(panel.msgs)) then return end
			panel.msgs:Clear()

			lines = {}
			panel.lines = {}
			
			for k,v in SortedPairsByMemberValue(MK.net.list, "inlength", true) do
				local line = panel.msgs:AddLine(k, v.incount or 0, v.outcount or 0, v.inlength or 0, v.outlength or 0, string.NiceTime(CurTime() - v.time))
				line.name = k
				
				table.insert(lines, line)
				
				panel.lines[k] = line:GetID()
			end
			
			local function UpdateList()
				if (!panel||!panel.msgs||!IsValid(panel.msgs)) then return end
				//panel.msgs:Clear()
				
				for _,line in pairs(lines) do
					if (!line||!IsValid(line)) then continue end
					line:SetColumnText(2, MK.net.list[line.name].incount or 0)
					line:SetColumnText(3, MK.net.list[line.name].outcount or 0)
					line:SetColumnText(4, MK.net.list[line.name].inlength or 0)
					line:SetColumnText(5, MK.net.list[line.name].outlength or 0)
					line:SetColumnText(6, string.NiceTime(CurTime() - MK.net.list[line.name].time))
				end
				for k,v in pairs(MK.net.list) do
					local found = false
					for _,line in pairs(lines) do
						if (line.name == k) then
							found = true
						end
					end
					if (!found) then
						local line = panel.msgs:AddLine(k, v.incount or 0, v.outcount or 0, v.inlength or 0, v.outlength or 0, string.NiceTime(CurTime() - v.time))
						line.name = k
						
						table.insert(lines, line)
						
						panel.lines[k] = line:GetID()
					end
				end
				timer.Simple(1, UpdateList)
			end
			UpdateList()
		end

		PopulateList()
	end	
MK.menus.list.netmsg = CATEGORY