// RAWR!

MK.menus = MK.menus or {}
MK.menus.list = MK.menus.list or {}
MK.menus.version = "0.1.0"

if (CLIENT) then
	MK.menus.colors = {
		red = Color(192, 57, 43),
		orange = Color(211, 84, 0),
		yellow = Color(241, 196, 15),
		green = Color(39, 174, 96),
		blue = Color(41, 128, 185),
		purple = Color(142, 68, 173),
		dark = Color(43, 45, 50),
		light =  Color(189, 195, 199)
	}
	
	MK_MAINCOLOR = CreateClientConVar("mk_color", "dark", true)
	
	cvars.AddChangeCallback("mk_color", function(conVar, previous, value)
		local value = MK.menus.colors[MK_MAINCOLOR:GetString()]

		if (value) then
			MK.menus.color = value
		end
	end)

	MK.menus.color = MK.menus.colors.dark

	local value = MK.menus.colors[MK_MAINCOLOR:GetString()]

	if (value) then
		MK.menus.color = value
	end
elseif (SERVER) then
	resource.AddFile("materials/moderator/leave.png")
	resource.AddFile("materials/moderator/menu.png")
end

function MK.menus.Initialize()
	MK.util.IncludeInternalDir("menus")
end