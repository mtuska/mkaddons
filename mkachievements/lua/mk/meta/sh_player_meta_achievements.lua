// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:MK_ProgressAchievement(id, count)
		local ACH = MK.achievements.Get(id)
		if (!ACH) then return end
		if (!ACH.Enabled) then return end
		if (self:MK_HasAchievement(id)) then return end
		if (ACH.RequiresPreviousUnlock) then
			if (!self:MK_HasAchievement(ACH.RequiresPreviousUnlock)) then return end
		end
		if (ACH.RequiresPreviousUnlocks) then
			for _,id in pairs(ACH.RequiresPreviousUnlocks) do
				if (!self:MK_HasAchievement(id)) then return end
			end
		end
		local achs = self:MK_Data_Get("achievements", {})
		achs[id] = (achs[id] or 0) + (count or 1)
		achs[ACH.Path] = nil
		self:MK_Data_Set("achievements", achs)
		hook.Run("MK_Achievements.OnProgress", self, id, count, achs[id], ACH.Count)
		if (achs[id] >= ACH.Count) then
			MK.achievements.Reward(id, self)
		end
	end

	function Player:MK_SetAchievement(id, count)
		local ACH = MK.achievements.Get(id)
		if (!ACH) then return end
		if (self:MK_HasAchievement(id)) then return end
		if (ACH.RequiresPreviousUnlock) then
			if (!self:MK_HasAchievement(ACH.RequiresPreviousUnlock)) then return end
		end
		local achs = self:MK_Data_Get("achievements", {})
		achs[id] = count
		achs[ACH.Path] = nil
		self:MK_Data_Set("achievements", achs)
		if (achs[id] >= ACH.Count) then
			MK.achievements.Reward(id, self)
		end
	end

	function Player:MK_ClearAchievements()
		self:MK_Data_Set("achievements", {})
	end
else
	function Player:MK_ProgressAchievement(id, count)

	end
	function Player:MK_SetAchievement(id, count)

	end
end

function Player:MK_GetAchievement(id)
	local achs = self:MK_Data_Get("achievements", {})
	return achs[id] or 0
end

function Player:MK_HasAchievement(id)
	local ACH = MK.achievements.Get(id)
	if (!ACH) then return false end
	local achs = self:MK_Data_Get("achievements", {})
	if (achs[id]&&achs[id] >= ACH.Count) then
		return true
	end
	return false
end
