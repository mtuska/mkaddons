// RAWR!

local function achievement(ply)
	net.Start("Achievement_Menu_Open")
	net.Send(ply)
end
local achievementsCommand = MK.commands.Add("player", "achievements", achievement, {"ach", "achmenu"})
achievementsCommand:DefaultRank("user")

local function resetachievements(ply, target)
	ply:MK_Data_Set("achievements", {})
end
local resetachievementsCommand = MK.commands.Add("player", "resetachievements", resetachievements)
resetachievementsCommand:DefaultRank("superadmin")
