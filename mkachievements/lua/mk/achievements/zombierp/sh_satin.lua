// RAWR!

ACH = ACH or {}
ACH.Name = "Satin"
ACH.Desc = "Become one with Satin."
ACH.Cat = "ZombieRP"
ACH.Icon = "materials/niandralades/ach_icons/ach_sdr.png"
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["DarkRPVarChanged"] = function(ply, var, oldValue, newValue)
    if (var == "level" && newValue == 66 && (ply:getDarkRPVar('prestige') or 0) == 6) then
        ply:MK_ProgressAchievement(achid, 1)
    end
end
