// RAWR!

ACH = ACH or {}
ACH.Name = "Rampant Rabbit"
ACH.Desc = "Jump, jump"
ACH.Cat = "General"
ACH.Icon = "materials/niandralades/ach_icons/ach_rr.png"
ACH.Count = 500

local achid = ACH.Id
ACH.Hooks["SetupMove"] = function(ply)
	if (ply:KeyDown(IN_JUMP) and not ply:KeyDownLast(IN_JUMP) and ply:Alive() and ply:OnGround()) then
		ply:MK_ProgressAchievement(achid, 1)
	end
end
