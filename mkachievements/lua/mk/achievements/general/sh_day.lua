// RAWR!

ACH = ACH or {}
ACH.Name = "Addicted"
ACH.Desc = "Play for an entire day!"
ACH.Cat = "General"
ACH.Icon = ""
ACH.Count = 86400

local achid = ACH.Id
ACH.Hooks["MK_Player.UpdatePlayTime"] = function(ply, seconds) // There is also "MK_Player_UpdatePlayTime_GM" for gamemode playtime
	ply:MK_SetAchievement(achid, seconds or 0)
end
