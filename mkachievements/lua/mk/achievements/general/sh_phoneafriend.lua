// RAWR!

ACH = ACH or {}
ACH.Name = "Phone-a-friend"
ACH.Desc = "Play with a friend!"
ACH.Cat = "General"
ACH.Icon = ""
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["MK_Player.ReceivedSteamFriends"] = function(ply)
	if (ply:MK_CountPlayingWithSteamFriends() > 0) then
		ply:MK_ProgressAchievement(achid, 1)
	end
end
