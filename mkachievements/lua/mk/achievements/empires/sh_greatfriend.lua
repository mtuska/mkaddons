// RAWR!

ACH = ACH or {}
ACH.Name = "A Great Friend"
ACH.Desc = "Begin a new alliance with a steam friend."
ACH.Cat = "Empires"
ACH.Icon = "materials/niandralades/ach_icons/ach_sdr.png"
ACH.Count = 1

local achid = ACH.Id
hook.Add("CreatedAlliance", "Em_AchievementCreatedAllianceFriends", function(ply, accepter)
	if (ply:MK_IsSteamFriend(accepter)) then
		//MKAchievements:AddCount(ply, "A Great Friend", 1)
		//MKAchievements:AddCount(accepter, "A Great Friend", 1)
		ply:MK_ProgressAchievement(achid, 1)
		accepter:MK_ProgressAchievement(achid, 1)
	end
end)