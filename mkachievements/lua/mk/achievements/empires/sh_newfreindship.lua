// RAWR!

ACH = ACH or {}
ACH.Name = "A New Friendship"
ACH.Desc = "Begin a new alliance."
ACH.Cat = "Empires"
ACH.Icon = "materials/niandralades/ach_icons/ach_rr.png"
ACH.Count = 1

local achid = ACH.Id
hook.Add("CreatedAlliance", "Em_AchievementCreatedAlliance", function(ply, accepter)
	//MKAchievements:AddCount(ply, "A New Friendship", 1)
	//MKAchievements:AddCount(accepter, "A New Friendship", 1)
	ply:MK_ProgressAchievement(achid, 1)
	accepter:MK_ProgressAchievement(achid, 1)
end)