// RAWR!

ACH = ACH or {}
ACH.Name = "Army of Tears"
ACH.Desc = "Kill off 100 of your own troops."
ACH.Cat = "Empires"
ACH.Icon = "materials/niandralades/ach_icons/ach_dbv.png"
ACH.Count = 100

local achid = ACH.Id
hook.Add("TroopDestroyed", "Em_AchievementTearsTroopDestroyed", function(victimOverlord, victim, attackerOverlord, attacker)
	if (attacker:IsWorld()&&victim:WaterLevel() >= 2) then
		victimOverlord:MK_ProgressAchievement(achid, 1)
	end
end)