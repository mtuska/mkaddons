// RAWR!

ACH = ACH or {}
ACH.Name = "Murderer"
ACH.Desc = "Kill 100 enemy troops."
ACH.Cat = "Empires"
ACH.Icon = "materials/niandralades/ach_icons/ach_facedown.png"
ACH.Count = 100

local achid = ACH.Id
hook.Add("TroopDestroyed", "Em_AchievementMurdererTroopDestroyed", function(victimOverlord, victim, attackerOverlord, attacker)
	if (victim:IsTroop() && attacker:IsTroop() && victim != attacker) then
		attackerOverlord:MK_ProgressAchievement(achid, 1)
	end
end)