// RAWR!

ACH = ACH or {}
ACH.Name = "Slammer was Slammed"
ACH.Desc = "Find your way into the slammer"
ACH.Cat = "DarkRP"
ACH.Icon = "materials/niandralades/ach_icons/ach_eotm.png"
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["playerArrested"] = function(arrestee, time, arrester)
    arrestee:MK_ProgressAchievement(achid, 1)
end
