// RAWR!

ACH = ACH or {}
ACH.Name = "Internship"
ACH.Desc = "Show someone a good trip"
ACH.Cat = "DarkRP"
ACH.Icon = "materials/niandralades/ach_icons/ach_firsthit.png"
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["onHitAccepted"] = function(hitman, target, customer)
    hitman:MK_ProgressAchievement(achid, 1)
end
