// RAWR!

ACH = ACH or {}
ACH.Name = "Travel Enthusiast"
ACH.Desc = "Invite others for a trip"
ACH.Cat = "DarkRP"
ACH.Icon = "materials/niandralades/ach_icons/ach_request.png"
ACH.Count = 10

local achid = ACH.Id
ACH.Hooks["onHitAccepted"] = function(hitman, target, customer)
    customer:MK_ProgressAchievement(achid, 1)
end
