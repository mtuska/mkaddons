// RAWR!

ACH = ACH or {}
ACH.Name = "Travel Agent"
ACH.Desc = "Send them all packing"
ACH.Cat = "DarkRP"
ACH.Icon = "materials/niandralades/ach_icons/ach_50hits.png"
ACH.Count = 50

local achid = ACH.Id
ACH.Hooks["onHitCompleted"] = function(hitman, target, customer)
    hitman:MK_ProgressAchievement(achid, 1)
end
