// RAWR!

ACH = ACH or {}
ACH.Name = "Snake Eyes"
ACH.Desc = "Win a lottery!"
ACH.Cat = "DarkRP"
ACH.Icon = "materials/niandralades/ach_icons/ach_snakeeyes.png"
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["lotteryEnded"] = function(p, winner, amount)
    winner:MK_ProgressAchievement(achid, 1)
end
