// RAWR!

ACH = ACH or {}
ACH.Name = "Warden's House"
ACH.Desc = "Be the warden"
ACH.Cat = "Jailbreak"
ACH.Icon = "materials/niandralades/ach_icons/ach_100rounds.png"
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["JB_SelectedWarden"] = function(ply)
	ply:MK_ProgressAchievement(achid, 1)
end
