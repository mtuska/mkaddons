// RAWR!

ACH = ACH or {}
ACH.Name = "Template" // Name to display, must be set or the achievement won't be added
ACH.Desc = "Templating." // Description to display
ACH.Cat = "General" // Category to display in
ACH.Icon = "" // Icon this achievement deserves
ACH.Count = 1 // Value the player needs to progress to
// In this case, how many times the player must say this phrase
ACH.Reward = function(ply)
	// Do whatever you like
	// This is run Server Side, remember that
end

local achid = ACH.Id // Achievement ID will be this file's name without sh_ sv_ or cl_(this file's id is "template")
// Use ACH Hooks, it'll only load hooks when needed
ACH.Hooks["PlayerSay"] = function(ply, text)
    if text == "#BlameMax" then // Example for a "secret phrase"
		ply:MK_ProgressAchievement(achid, 1)
		return ""
	end
end
