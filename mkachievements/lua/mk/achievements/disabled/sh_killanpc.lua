// RAWR!

ACH = ACH or {}
ACH.Name = "Kill a NPC"
ACH.Desc = "What do you think it means?"
ACH.Cat = "General"
ACH.Icon = ""
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["OnNPCKilled"] = function(victim, attacker, inflictor)
	if (IsValid(attacker) && attacker:IsPlayer()) then
		attacker:MK_ProgressAchievement(achid, 1)
	end
end
