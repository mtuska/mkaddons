// RAWR!

ACH = ACH or {}
ACH.Name = "Die"
ACH.Desc = "What do you think it means?"
ACH.Cat = "General"
ACH.Icon = ""
ACH.Count = 1

local achid = ACH.Id
ACH.Hooks["PlayerDeath"] = function(victim, inflictor, attacker)
	if (IsValid(victim) && victim:IsPlayer()) then
		victim:MK_ProgressAchievement(achid, 1)
	end
end
