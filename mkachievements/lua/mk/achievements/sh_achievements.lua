// RAWR!

// These are gamemode specific, they only enable these achievements on their appropriate gamemodes
MK.achievements.IncludeDir("empires", GAMEMODE_NAME == "mkempires")
MK.achievements.IncludeDir("jailbreak", GAMEMODE_NAME == "jailbreak")
MK.achievements.IncludeDir("zombierp", GAMEMODE_NAME == "zombierp")
MK.achievements.IncludeDir("darkrp", GAMEMODE_NAME == "darkrp" || GAMEMODE_NAME == "zombierp")
