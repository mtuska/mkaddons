// RAWR!

MK.achievements = MK.achievements or {}
MK.achievements.version = "1.0.1"
MK.achievements.list = MK.achievements.list or {}

if (SERVER) then
	util.AddNetworkString("Achievement_Menu_Open")
	util.AddNetworkString("Achievement_Menu_Completed")
	util.AddNetworkString("ACH_SendInitialTable")
	util.AddNetworkString("ACH_SendProgression")
	util.AddNetworkString("Achievements_NotifyAll")
end

local function precallback(fileName, filePath)
	ACH = {}
	ACH.Enabled = true
	ACH.Id = fileName
	ACH.Path = filePath
	ACH.Name = filePath
	ACH.Desc = ""
	ACH.Cat = "Unknown"
	ACH.Icon = "Unknown"
	ACH.Count = 1
	ACH.Reward = function(ply) end
	ACH.Hooks = {}
end

local function postcallback(fileName, filePath)
	if (ACH.Name != filePath) then
		MK.achievements.Add(ACH)
	end
	ACH = nil
end

function MK.achievements.GMInitialize()
	MK.achievements.IncludeDir("general")
	MK.util.IncludeInternalDir("achievements")
end

function MK.achievements.IncludeDir(dir, enableAchs)
	if (enableAchs == nil) then
		enableAchs = true
	end
	MK.util.IncludeInternalDir("achievements/"..dir, nil, precallback, function(fileName, filePath)
		ACH.Enabled = enableAchs
		postcallback(fileName, filePath)
	end)
end

function MK.achievements.Add(ACH)
	MK.achievements.list[ACH.Id] = ACH
	if (ACH.Enabled && SERVER) then
		for k,v in pairs(ACH.Hooks) do
			hook.Add(k, "ACH_"..ACH.Id.."."..k, function(...)
				return v(...)
			end)
		end
	end
end

function MK.achievements.Get(achid)
	return MK.achievements.list[achid] or nil
end

function MK.achievements.GetAll()
	return MK.achievements.list
end

if SERVER then
	function MK.achievements.Reward(achid, ply)
		local ACH = MK.achievements.Get(achid)
		if (!ACH) then return end
		if (!ACH.Enabled) then return end
		if (ACH.Reward) then ACH.Reward(ply) end
		// chatprint
		player.BroadcastChat(team.GetColor(ply:Team()), ply:Name(), Color(255,255,255), " has achieved ", Color(151, 198, 56), ACH.Name)
		net.Start("Achievement_Menu_Completed")
			net.WriteString(achid)
		net.Send(ply)
		ply:EmitSound("achievements/achievement_earned.mp3")
		local Offset = Vector(0, 0, 72)
		local Pos = ply:GetPos() + Offset
		//MK.sound.PlayURLAtLoc("http://gmod.mk-servers.com/music/achievement_earned.mp3", Pos)
		if (ply:Alive()) then
			local Effect = EffectData()
			Effect:SetOrigin(Pos)
			util.Effect("achievement_awarded", Effect, true, true)
		end
		hook.Run("MK_Achievements.OnReward", ply, achid)
	end
end

// TTT functions
function MK.achievements.GetAllDetectives(alive_only)
	local tbl = {}

	for k, v in pairs(player.GetAll()) do
		if v:GetRole() == ROLE_DETECTIVE then
			if not alive_only then
				table.insert(tbl,v)
			else
				if v:Alive() then
					table.insert(tbl,v)
				end
			end
		end
	end

	return tbl
end

function MK.achievements.GetAllInnoocents(alive_only)
	local tbl = {}

	for k, v in pairs(player.GetAll()) do
		if v:GetRole() == ROLE_INNOCENT then
			if not alive_only then
				table.insert(tbl,v)
			else
				if v:Alive() then
					table.insert(tbl,v)
				end
			end
		end
	end

	return tbl
end

function MK.achievements.GetAllTraitors(alive_only)
	local tbl = {}

	for k, v in pairs(player.GetAll()) do
		if v:GetRole() == ROLE_TRAITOR then
			if not alive_only then
				table.insert(tbl,v)
			else
				if v:Alive() then
					table.insert(tbl,v)
				end
			end
		end
	end

	return tbl
end

function MK.achievements.IsPlayingTTT()
	if TEAM_TERROR != nil then
		return true
	end

	return false
end
