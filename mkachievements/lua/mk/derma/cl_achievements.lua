// RAWR!

CreateClientConVar( "Achievements_DisplayCompletion_Notification", "1", true, false )
CreateClientConVar( "Achievements_DisplayOthersCompletion", "1", true, false )

local Achievements = Achievements or {}
Achievements.FKeyEnum = KEY_F9

//Menu
Achievements.FrameColour = Color(11,11,13, 255) -- What colour should the frame be?

//Icons
Achievements.IconUncompleted = Color(255, 255, 255, 255) -- What colour should the icon be when uncompleted?
Achievements.IconCompleted = Color(255, 255, 255, 255) -- What colour should the icon be when the user has completed the achievement?
Achievements.IconAlternative = "materials/niandralades/ach_icons/ach_missing.png"

//Achievements Disabled
Achievements.Disabled_Even = Color(20,20,20, 255) -- Decides what every EVEN numbered background panel for disabled achs should be.
Achievements.Disabled_Odd = Color(10,10,10, 255) -- Decides what every ODD numbered background panel for disabled achs should be.
Achievements.Disabled_Even_Prog = Color(10,10,10, 255) -- What ODD colour should that rectangle behind the progress counter be?
Achievements.Disabled_Odd_Prog = Color(10,10,10, 255) -- What EVEN colour should that rectangle behind the progress counter be?

//Achievements Locked
Achievements.Locked_Even = Color(44,44,70, 255) -- Decides what every EVEN numbered background panel for locked achs should be.
Achievements.Locked_Odd = Color(24,24,31, 255) -- Decides what every ODD numbered background panel for locked achs should be.
Achievements.Locked_Even_Prog = Color(60, 60, 93, 255) -- What ODD colour should that rectangle behind the progress counter be?
Achievements.Locked_Odd_Prog = Color(30, 30, 46, 255) -- What EVEN colour should that rectangle behind the progress counter be?

//Achievements Unlocked
Achievements.Unlocked_Even = Color(110, 38, 19, 255) -- Decides what every even button should be coloured.
Achievements.Unlocked_Odd = Color(90, 30, 24, 255) -- Decides what every odd button should be coloured.
Achievements.Unlocked_Even_Prog = Color(180,28,8,255) -- What ODD colour should that rectangle behind completed ach's?
Achievements.Unlocked_Odd_Prog = Color(150,20,14,255) -- What EVEN colour should that rectangle behind completed ach's?

//Achievements Category buttons
Achievements.ButtonsBackgroundPanel = Color(23,23,23) -- What colour should that rectangle behind the category buttons be?
Achievements.Buttons_Even = Color(55,55,72, 255) -- What colour should that rectangle behind the category buttons be?
Achievements.Buttons_Odd = Color(24,24,31, 255) -- What colour should that rectangle behind the category buttons be?
Achievements.Buttons_Completed = Color(55,55,72, 255) -- What colour should that rectangle behind the category buttons be?
Achievements.Buttons_Available = Color(24,24,31, 255) -- What colour should that rectangle behind the category buttons be?
Achievements.Buttons_All = Color(55,55,72, 255) -- What colour should that rectangle behind the category buttons be?

//Font Colours
Achievements.TextColour = Color(255, 255, 255, 255) -- What colour should all the fonts be?

//Misc
Achievements.HoverOverSound = "garrysmod/ui_return.wav" -- What sound should play when a user hovers over the buttons?

//Achievement Complete Notification
Achievements.CompletedFrameColour = Achievements.FrameColour -- What colour should the overall frame?
Achievements.IconBackgroundColour = Color(255, 255, 255, 255) -- What colour should the achievement icon be?
Achievements.SlideInTime = 1 -- How long should it take for the menu to slide in?
Achievements.SlideOutTime = 1 -- How long should it take for the menu to slide out?
Achievements.StayTime = 3 -- How long should the little panel stay on the user's screen for?

// END CONFIG
Achievements.CompletedClientside = Achievements.CompletedClientside or {}
Achievements.ProgressClientside = Achievements.ProgressClientside or {}

Achievements.open = false
hook.Add("Think", "ACH_OpenViaEnum", function()
	if (!Achievements.KeyPressed) then
		if (input.IsKeyDown(Achievements.FKeyEnum) and not Achievements.open) then
			Achievements:OpenOurMenu()
			Achievements.open = true
		elseif (input.IsKeyDown(Achievements.FKeyEnum) and Achievements.open) then
			Achievements:CloseMenu()
			Achievements.open = false
			gui.EnableScreenClicker(false)
		end
	end
	if (input.IsKeyDown(Achievements.FKeyEnum)) then
		Achievements.KeyPressed = true
	else
		Achievements.KeyPressed = false
	end
end)

function math.getnice(num)
	if tonumber(num) >= 1000 then
		num = math.Round(num/1000) .. "K"
	end

	return num
end

net.Receive("ACH_SendInitialTable", function()
	Achievements.CompletedClientside = net.ReadTable()
end)

net.Receive("ACH_SendProgression", function()
	local our_key = net.ReadString()
	local our_value = net.ReadString()
	Achievements.ProgressClientside[our_key] = our_value
end)

net.Receive("Achievement_Menu_Open", function(len)
	Achievements:OpenOurMenu()
end)

function Achievements:OpenOurMenu()
	if (Achievements.Menu and IsValid(Achievements.Menu)) then return end
	for id,count in pairs(LocalPlayer():MK_Data_Get("achievements", {})) do
		if (MK.achievements.Get(id) && count >= MK.achievements.Get(id).Count) then
			table.insert(Achievements.CompletedClientside, id)
		else
			Achievements.ProgressClientside[id] = count or 0
		end
	end

	gui.EnableScreenClicker(true)
	Achievements.SelectedButton = nil
	Achievements.DisplayTable = {}

	local frame = vgui.Create("DFrame")
	frame:SetSize(750, 550)
	frame:Center()
	frame:SetTitle("")
	frame:ShowCloseButton(false)
	frame.Paint = function()
		surface.SetDrawColor(Achievements.FrameColour)
		surface.DrawRect(0, 0, frame:GetWide(), frame:GetTall())
	end
	Achievements.Menu = frame

	local buttons_background_panel = vgui.Create("DPanel", frame)
	buttons_background_panel:SetPos(20,20)
	buttons_background_panel:SetSize(110,frame:GetTall()-40)
	buttons_background_panel.Paint = function()
		draw.RoundedBox(0,0,0,buttons_background_panel:GetWide(),buttons_background_panel:GetTall(),Achievements.ButtonsBackgroundPanel)
	end

	local backgroundpanel = vgui.Create("DScrollPanel", frame)
	backgroundpanel:SetSize(frame:GetWide()-40, frame:GetTall()-40)
	backgroundpanel:SetPos(150, 20)

	function Achievements:DisplayAchievements(category)

		local num = 0
		Achievements.DisplayTable = Achievements.DisplayTable or {}

		for k, v in pairs(MK.achievements.GetAll()) do

			if category then
				if not isnumber(category) then
					if v.Cat != category then
						continue
					end
				elseif (category == 1) then
					if not table.HasValue(Achievements.CompletedClientside, k) then
						continue
					end
				elseif (category == 2) then
					if not v.Enabled then
						continue
					end
				end
			end

			local current = tonumber(Achievements.ProgressClientside[k]) or 0
			local outof = v.Count

			local achievementpanel = vgui.Create("DPanel", backgroundpanel)
			achievementpanel:SetSize(580, 74)
			achievementpanel:SetPos(0, 0+74*num)
			if math.fmod(num, 2)  == 0 then
				achievementpanel.Paint = function()
					if table.HasValue(Achievements.CompletedClientside, k) then
						surface.SetDrawColor(Achievements.Unlocked_Even)
						surface.DrawRect(0, 0, 580, 148)
					elseif not v.Enabled then
						surface.SetDrawColor(Achievements.Disabled_Even)
						surface.DrawRect(0, 0, 580, 148)
					else
						surface.SetDrawColor(Achievements.Locked_Even)
						surface.DrawRect(0, 0, 580, 148)
					end
				end
			else
				achievementpanel.Paint = function()
					if table.HasValue(Achievements.CompletedClientside, k) then
						surface.SetDrawColor(Achievements.Unlocked_Odd)
						surface.DrawRect(0, 0, 580, 148)
					elseif not v.Enabled then
						surface.SetDrawColor(Achievements.Disabled_Odd)
						surface.DrawRect(0, 0, 580, 148)
					else
						surface.SetDrawColor(Achievements.Locked_Odd)
						surface.DrawRect(0, 0, 580, 148)
					end
				end
			end
			table.insert(Achievements.DisplayTable, achievementpanel)

			local achievementicon = vgui.Create("DImage", achievementpanel)
			achievementicon:SetPos(5,5)
			achievementicon:SetSize(64, 64)
			if (v.Icon != "") then
				achievementicon:SetImage(v.Icon)
			else
				achievementicon:SetImage(Achievements.IconAlternative)
			end
			if table.HasValue(Achievements.CompletedClientside, k) then
				achievementicon:SetImageColor(Achievements.IconCompleted)
			end

			local achievementitle = vgui.Create("DLabel", achievementpanel)
			achievementitle:SetText(v.Name)
			achievementitle:SetColor(Achievements.TextColour)
			achievementitle:SetFont("Roboto25")
			achievementitle:SetPos(74, 5)
			achievementitle:SizeToContents()

			local descriptionicon = vgui.Create("DImage", achievementpanel)
			descriptionicon:SetImage("materials/icon16/information.png")
			descriptionicon:SetSize(16, 16)
			descriptionicon:SetPos(74, 30)

			local achievementdesc = vgui.Create("DLabel", achievementpanel)
			achievementdesc:SetText(v.Desc)
			achievementdesc:SetColor(Achievements.TextColour)
			achievementdesc:SetPos(74+16+2, 30)
			achievementdesc:SizeToContents()

			local awardicon = vgui.Create("DImage", achievementpanel)
			awardicon:SetImage("materials/icon16/award_star_gold_1.png")
			awardicon:SetSize(16, 16)
			awardicon:SetPos(74, 30+20)

			local achievementaward = vgui.Create("DLabel", achievementpanel)
			if not table.HasValue(Achievements.CompletedClientside, k) then
				if (v.RewardName&&string.len(v.RewardName) != 0) then
					achievementaward:SetText(v.RewardName)
				else
					achievementaward:SetText("No reward!")
				end
			else
				achievementaward:SetText("Completed!")
			end
			achievementaward:SetColor(Achievements.TextColour)
			achievementaward:SetPos(74+16+2, 30+20)
			achievementaward:SizeToContents()

			local achievementprogress = vgui.Create("DButton", achievementpanel)
			achievementprogress:SetSize(150, achievementpanel:GetTall()-20)
			achievementprogress:SetPos(achievementpanel:GetWide()-10-150,10)
			achievementprogress:SetColor(Achievements.TextColour)
			if table.HasValue(Achievements.CompletedClientside, k) then
				achievementprogress:SetText(math.getnice(outof) .. "/" .. math.getnice(outof))
			elseif not v.Enabled then
				achievementprogress:SetText("Locked")
			else
				achievementprogress:SetText(math.getnice(current) .. "/" .. math.getnice(outof))
			end
			achievementprogress:SetFont("Bebas40")
			if math.fmod(num, 2)  == 0 then
				achievementprogress.Paint = function()
					if table.HasValue(Achievements.CompletedClientside, k) then
						draw.RoundedBox(0,0,0,achievementprogress:GetWide(),achievementprogress:GetTall(),Achievements.Unlocked_Even_Prog)
					elseif not v.Enabled then
						draw.RoundedBox(0,0,0,achievementprogress:GetWide(),achievementprogress:GetTall(),Achievements.Disabled_Even_Prog)
					else
						draw.RoundedBox(0,0,0,achievementprogress:GetWide(),achievementprogress:GetTall(),Achievements.Locked_Even_Prog)
					end
				end
			else
				achievementprogress.Paint = function()
					if table.HasValue(Achievements.CompletedClientside, k) then
						draw.RoundedBox(0,0,0,achievementprogress:GetWide(),achievementprogress:GetTall(),Achievements.Unlocked_Odd_Prog)
					elseif not v.Enabled then
						draw.RoundedBox(0,0,0,achievementprogress:GetWide(),achievementprogress:GetTall(),Achievements.Disabled_Odd_Prog)
					else
						draw.RoundedBox(0,0,0,achievementprogress:GetWide(),achievementprogress:GetTall(),Achievements.Locked_Odd_Prog)
					end
				end
			end
			num = num + 1
		end
	end

	local num_2 = 0
	local cats_made = {}
	for k, v in pairs(MK.achievements.GetAll()) do
		if not table.HasValue(cats_made, v.Cat) then
			local buttons_tbl = vgui.Create("DButton", buttons_background_panel)
			buttons_tbl:SetSize(buttons_background_panel:GetWide(), 30)
			buttons_tbl:SetPos(0,0+num_2*30)
			buttons_tbl:SetText(v.Cat)
			buttons_tbl:SetColor(Achievements.TextColour)
			buttons_tbl.DoClick = function()
				for x, y in pairs(Achievements.DisplayTable) do
					y:Remove()
				end
				Achievements:DisplayAchievements(v.Cat)
				Achievements.SelectedButton = v
			end
			if math.fmod(num_2, 2)  == 0 then
				buttons_tbl.Paint = function()
					if Achievements.SelectedButton and Achievements.SelectedButton == v then
						draw.RoundedBox(0,0,0,buttons_tbl:GetWide(),buttons_tbl:GetTall(),Achievements.Unlocked_Even)
					else
						draw.RoundedBox(0,0,0,buttons_tbl:GetWide(),buttons_tbl:GetTall(),Achievements.Buttons_Even)
					end
				end
			else
				buttons_tbl.Paint = function()
					if Achievements.SelectedButton and Achievements.SelectedButton == v then
						draw.RoundedBox(0,0,0,buttons_tbl:GetWide(),buttons_tbl:GetTall(),Achievements.Unlocked_Odd)
					else
						draw.RoundedBox(0,0,0,buttons_tbl:GetWide(),buttons_tbl:GetTall(),Achievements.Buttons_Odd)
					end
				end
			end

			table.insert(cats_made, v.Cat)
			num_2 = num_2 + 1
		end
	end

	local buttons_completed = vgui.Create("DButton", buttons_background_panel)
	buttons_completed:SetSize(buttons_background_panel:GetWide(),30)
	buttons_completed:SetPos(0,buttons_background_panel:GetTall()-90)
	buttons_completed:SetText("Completed")
	buttons_completed:SetColor(Achievements.TextColour)
	buttons_completed.Paint = function()
		if Achievements.SelectedButton and Achievements.SelectedButton == 1 then
			draw.RoundedBox(0,0,0,buttons_background_panel:GetWide(),30,Achievements.Unlocked_Even)
		else
			draw.RoundedBox(0,0,0,buttons_background_panel:GetWide(),30,Achievements.Buttons_Completed)
		end
	end
	buttons_completed.DoClick = function()
		for x, y in pairs(Achievements.DisplayTable) do
			y:Remove()
		end
		Achievements:DisplayAchievements(1)
		Achievements.SelectedButton = 1
	end

	local buttons_available = vgui.Create("DButton", buttons_background_panel)
	buttons_available:SetSize(buttons_background_panel:GetWide(),30)
	buttons_available:SetPos(0,buttons_background_panel:GetTall()-60)
	buttons_available:SetText("Available")
	buttons_available:SetColor(Achievements.TextColour)
	buttons_available.Paint = function()
		if Achievements.SelectedButton and Achievements.SelectedButton == 2 then
			draw.RoundedBox(0,0,0,buttons_background_panel:GetWide(),30,Achievements.Unlocked_Even)
		else
			draw.RoundedBox(0,0,0,buttons_background_panel:GetWide(),30,Achievements.Buttons_Available)
		end
	end
	buttons_available.DoClick = function()
		for x, y in pairs(Achievements.DisplayTable) do
			y:Remove()
		end
		Achievements:DisplayAchievements(2)
		Achievements.SelectedButton = 2
	end

	local buttons_all = vgui.Create("DButton", buttons_background_panel)
	buttons_all:SetSize(buttons_background_panel:GetWide(),30)
	buttons_all:SetPos(0,buttons_background_panel:GetTall()-30)
	buttons_all:SetText("All Achievements")
	buttons_all:SetColor(Achievements.TextColour)
	buttons_all.Paint = function()
		draw.RoundedBox(0,0,0,buttons_background_panel:GetWide(),30,Achievements.Buttons_All)
	end
	buttons_all.DoClick = function()
		Achievements:DisplayAchievements(false)
		Achievements.SelectedButton = nil
	end

	function Achievements:CloseMenu()
		if IsValid(frame) then
			frame:SetVisible(false)
			frame:Remove()
			Achievements.open = false
		end
	end


	local closebutton = vgui.Create("DImageButton", frame)
	closebutton:SetSize(16,16)
	closebutton:SetPos(frame:GetWide()-16-3, 3)
	closebutton:SetImage("materials/icon16/cancel.png")
	closebutton.DoClick = function()
		frame:Remove()
		gui.EnableScreenClicker(false)
	end

	local settings = vgui.Create("DImageButton", frame)
	settings:SetSize(16,16)
	settings:SetPos(frame:GetWide()-32-3, 3)
	settings:SetImage("materials/icon16/wrench.png")
	settings.DoClick = function()
		frame:Remove()
		Achievements:OpenSettingsMenu()
	end

	function Achievements:OpenSettingsMenu()
		local s_frame = vgui.Create("DFrame")
		s_frame:SetSize(400, 100)
		s_frame:Center()
		s_frame:SetTitle("Settings")
		s_frame.Paint = function()
			draw.RoundedBox(0,0,0,s_frame:GetWide(),s_frame:GetTall(),Achievements.FrameColour)
		end
		s_frame.OnClose = function()
			gui.EnableScreenClicker(false)
		end

		local back_button = vgui.Create("DButton", s_frame)
		back_button:SetSize(s_frame:GetWide()-20, 25)
		back_button:SetText("Back to achievements")
		back_button:SetPos(10,s_frame:GetTall()-35)
		back_button.DoClick = function()
			s_frame:Remove()
			Achievements:OpenOurMenu()
		end

		local ach_check_1 = vgui.Create("DCheckBox", s_frame)
		ach_check_1:SetPos(10, 25)
		ach_check_1:SetSize(16,16)
		ach_check_1:SetChecked(GetConVar("Achievements_DisplayCompletion_Notification"):GetBool())
		ach_check_1.OnChange = function()
			if ach_check_1:GetChecked() then
				RunConsoleCommand("Achievements_DisplayCompletion_Notification", "1")
			else
				RunConsoleCommand("Achievements_DisplayCompletion_Notification", "0")
			end
		end
		ach_check_1:SetVisible(true)

		local ach_desc_1 = vgui.Create("DLabel", s_frame)
		ach_desc_1:SetPos(10+26, 25)
		ach_desc_1:SetText("Display notification when completed achievements.")
		ach_desc_1:SizeToContents()

		local ach_check_2 = vgui.Create("DCheckBox", s_frame)
		ach_check_2:SetPos(10, 25+20)
		ach_check_2:SetSize(16,16)
		ach_check_2:SetChecked(GetConVar("Achievements_DisplayOthersCompletion"):GetBool())
		ach_check_2.OnChange = function()
			if ach_check_2:GetChecked() then
				RunConsoleCommand("Achievements_DisplayOthersCompletion", "1")
			else
				RunConsoleCommand("Achievements_DisplayOthersCompletion", "0")
			end
		end
		ach_check_2:SetVisible(true)

		local ach_desc_2 = vgui.Create("DLabel", s_frame)
		ach_desc_2:SetPos(10+26, 25+20)
		ach_desc_2:SetText("Display notification when OTHERS complete achievements.")
		ach_desc_2:SizeToContents()
	end

	function Achievements:CloseSettingsMenu()
		s_frame:Remove()
		gui.EnableScreenClicker(false)
	end

	Achievements:DisplayAchievements(2)
	Achievements.SelectedButton = 2
	//Achievements:DisplayAchievements(false)
	//Achievements.SelectedButton = nil
end

function Achievements.DisplayAchievement(achid)
	local ACH = MK.achievements.Get(achid)
	if (!ACH) then return end

	local frame = vgui.Create("DFrame")
	frame:SetTitle("")
	frame:ShowCloseButton(false)
	frame:SetSize(300, 104)
	frame.Paint = function()
		surface.SetDrawColor(Achievements.CompletedFrameColour)
		surface.DrawRect(0, 0, 300, 104)
	end
	Achievements.DisplayAchievementFrame = frame

	frame:SetPos(ScrW()-300, ScrH()+104)
	frame:MoveTo(ScrW()-300, ScrH()-104, Achievements.SlideInTime, 0, 5)

	Achievements.DisplayAchievementNextTime = CurTime() + Achievements.SlideInTime + Achievements.StayTime + Achievements.SlideOutTime

	timer.Simple(Achievements.StayTime, function()
		if (IsValid(frame)) then
			frame:MoveTo(ScrW()-300, ScrH()+104, Achievements.SlideOutTime, 0, 5)
		end
		if (IsValid(Achievements.DisplayAchievementFrame)) then
			Achievements.DisplayAchievementFrame:MoveTo(ScrW()-300, ScrH()+104, Achievements.SlideOutTime, 0, 5)
		end
	end)

	timer.Simple(Achievements.SlideInTime + Achievements.StayTime + Achievements.SlideOutTime, function()
		if (IsValid(frame)) then
			frame:Remove()
		end
		if (IsValid(Achievements.DisplayAchievementFrame)) then
			Achievements.DisplayAchievementFrame:Remove()
		end
		table.RemoveByValue(Achievements.DisplayAchievementQueue, achid)
		for k,v in pairs(Achievements.DisplayAchievementQueue) do
			Achievements.DisplayAchievement(v) // Try for the next one!
			break
		end
	end)

	local background = vgui.Create("DPanel", frame)
	background:SetSize(74, 74)
	background:SetPos(15, 15)
	background.Paint = function()
		surface.SetDrawColor(Achievements.IconBackgroundColour)
		surface.DrawRect(0, 0, 74, 74)
	end

	local background1 = vgui.Create("DPanel", background)
	background1:SetSize(64, 64)
	background1:SetPos(5, 5)
	background1.Paint = function()
		surface.SetDrawColor(Achievements.CompletedFrameColour)
		surface.DrawRect(0, 0, 64, 64)
	end

	local icon = vgui.Create("DImage", frame)
	icon:SetSize(64, 64)
	icon:SetPos(20, 20)
	if (ACH.Icon != "") then
		icon:SetImage(ACH.Icon)
	else
		icon:SetImage(Achievements.IconAlternative)
	end

	local text = vgui.Create("DLabel", frame)
	text:SetText("Achievement completed!")
	text:SizeToContents()
	text:SetPos(20+64+20, 20+10)

	local achievement = vgui.Create("DLabel", frame)
	achievement:SetText(ACH.Name)
	achievement:SetPos(20+64+20, 20+20+10)
	achievement:SetFont("Roboto25")
	achievement:SizeToContents()
end

net.Receive("Achievement_Menu_Completed", function()
	local achid = net.ReadString()

	if (!GetConVar("Achievements_DisplayCompletion_Notification"):GetBool()) then return end

	Achievements.DisplayAchievementQueue = Achievements.DisplayAchievementQueue or {}
	if (CurTime() < (Achievements.DisplayAchievementNextTime or 0)) then
		table.insert(Achievements.DisplayAchievementQueue, achid)
		return
	end

	Achievements.DisplayAchievement(achid)
end)
